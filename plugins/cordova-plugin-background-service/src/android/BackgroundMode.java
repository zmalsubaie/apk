package com.anrip.cordova;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.IBinder;

import org.json.JSONArray;
import org.json.JSONException;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;

import static android.content.Context.BIND_AUTO_CREATE;

public class BackgroundMode extends CordovaPlugin {
    // Plugin namespace
    private static final String JS_NAMESPACE = "window.BackgroundService";
    // Service that keeps the app awake
    private ForegroundService service;
    String params = null;
    // Used to (un)bind the service to with the activity
    private final ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ForegroundService.ForegroundBinder binder = (ForegroundService.ForegroundBinder) service;
            BackgroundMode.this.service = binder.getService();
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            fireEvent("failure", "service disconnected");
        }
    };

    /**
     * Executes the request.
     *
     * @param action   The action to execute.
     * @param args     The exec() arguments.
     * @param callback The callback context used when calling back into JavaScript.
     *
     * @return Returning false results in a "MethodNotFound" error.
     *
     * @throws JSONException
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callback) throws JSONException {
        Context context = this.cordova.getActivity().getApplicationContext();
        SharedPreferences settings = context.getSharedPreferences("LOGOUT_PARAMS", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("logoutParams",action);
        editor.commit();
        startService();
        callback.success();
        return true;
//        LOG.v("**************************************************",action);
//        if (action.equalsIgnoreCase("disable")) {
//            callback.success();
//            return true;
//        }
//        if (action.equalsIgnoreCase("enable")) {
//            callback.success();
//            return true;
//        }
//        return false;
    }

    /**
     * Called when the system is about to start resuming a previous activity.
     *
     * @param multitasking Flag indicating if multitasking is turned on for app.
     */
    @Override
    public void onPause(boolean multitasking) {
        super.onPause(multitasking);
        startService();
    }

    /**
     * Called when the activity will start interacting with the user.
     *
     * @param multitasking Flag indicating if multitasking is turned on for app.
     */
    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
        //stopService();
    }

    /**
     * Called when the activity will be destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        //stopService();
    }

    /**
     * Bind the activity to a background service and put them into foreground
     * state.
     */
    private void startService() {

        Activity context = cordova.getActivity();
        Intent intent = new Intent(context, ForegroundService.class);
        try {
            context.bindService(intent, connection, BIND_AUTO_CREATE);
            context.startService(intent);
            fireEvent("activate", "");
        } catch (Exception e) {
            fireEvent("failure", String.format("%s", e.getMessage()));
        }
    }

    /**
     * Bind the activity to a background service and put them into foreground state.
     */
    private void stopService() {
        Activity context = cordova.getActivity();
        Intent intent = new Intent(context, ForegroundService.class);

        context.unbindService(connection);
        context.stopService(intent);
        fireEvent("deactivate", "");
    }

    /**
     * Fire event with some parameters inside the web view.
     *
     * @param event The name of the event
     * @param message Optional message for the event
     */
    private void fireEvent(String event, String message) {

        final String script = String.format("%s.fire('%s','%s');", JS_NAMESPACE, event, message);

        cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:" + script);
            }
        });
    }

}
