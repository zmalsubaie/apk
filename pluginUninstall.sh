show_loader(){
   loader_chars="/-\|"
    while :; do
    for (( i=0; i<${#loader_chars}; i++ )); 
        do
            sleep 0.05
            echo -en "`tput bold` `tput setaf 5` ${loader_chars:$i:1}" "\r"
        done
    done
}
run_plugin_uninstall_procedures(){
     gulp clean-dir
     gulp cordova-copy
     cordova plugin rm $1
     gulp clean-platform-configs
     gulp copy-platform-configs
}
uninstall_success(){
  echo "`tput setaf 2` `tput bold` Uninstall Procedure Done !!! Please run the Prod/Dev Gulp to continue Development$ `tput sgr0`"
  exit 0
}
echo "`tput bold` Please Enter the Cordova Plugin Name/Git Link (Paste the Text After 'cordova plugin rm/remove' ----- ) `tput setaf 5`"
read pluginname
if [ ! -z "$pluginname" -a "$pluginname" != " " ]; then
    (
        run_plugin_uninstall_procedures $pluginname;
        uninstall_success
    ) & show_loader
 else
    echo "`tput setaf 1` Please Enter Plugin Name And Try Again !!!!! `tput sgr0`"
    exit  
fi
