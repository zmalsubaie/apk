var gulp = require('gulp');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var watch = require('gulp-watch');
var del = require('del');
var sourcemaps = require('gulp-sourcemaps');
var htmlmin = require('gulp-htmlmin');
var templateCache = require('gulp-angular-templatecache');
var uglify = require('gulp-uglify');
var pump = require('pump');
var minify = require('gulp-minify');
var gnirts = require('gulp-gnirts');
var jsonMinify = require('gulp-json-minify');
var js_obfuscator = require('gulp-js-obfuscator');
var javascriptObfuscator = require('gulp-javascript-obfuscator');
gulp.task('clean-dir', function () {
    return del.sync(['www/*','platforms/android/assets/www/*','platforms/ios/www/*'], {force: true});
});
gulp.task('clean-platform-configs', function () {
    return del.sync(['platform_config/ios/*','platform_config/android/*'], {force: true});
});
gulp.task('tempsDelete', function () {
    return del.sync([
        'src/app/app.source.js',
        'src/app/libs.source.js',
        'www/app/app.source.min.js',
        'www/app/libs.source.min.js',
        'platforms/ios/www/app/app.source.min.js',
        'platforms/ios/www/app/libs.source.min.js',
        'platforms/android/assets/www/app/app.source.min.js',
        'platforms/android/assets/www/app/libs.source.min.js'
    ], {force: true});
});
gulp.task('copy-prod-files', function() {
    gulp.src('src/index-prod.html')
    .pipe(concat('index.html'))
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('www/'));
    gulp.src('src/assets/css/*.css')
    .pipe(gulp.dest('www/assets/css'));
    gulp.src(['src/assets/img/**/*','src/assets/img/*.*'],{ base: 'src/assets/img' })
    .pipe(gulp.dest('www/assets/img'));
     gulp.src('src/app/utils/languageLocals/*.json')
    .pipe(jsonMinify())
    .pipe(gulp.dest('www/app/utils/languageLocals/'));
     gulp.src(['src/assets/lib/**/*.svg','src/assets/lib/**/*.ttf','src/assets/lib/**/*.eot','src/assets/lib/**/*.woff','src/assets/lib/**/*.woff2'],{ base: 'src/assets/lib' })
    .pipe(gulp.dest('www/assets/lib'));
     gulp.src('src/assets/lib/**/*.css', { base: 'src/assets/lib' })
    .pipe(gulp.dest('www/assets/lib'));
     gulp.src('src/assets/fonts/*', { base: 'src/assets/fonts' })
    .pipe(gulp.dest('www/assets/fonts'));
    gulp.src(['src/app/components/**/*.html','src/app/shared/directives/**/*.html','src/app/shared/layout/*.html'], { base: 'src/app' })
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('www/app'));
});
gulp.task('copy-dev-files', function() {
    gulp.src('src/index-dev.html')
    .pipe(concat('index.html'))
    .pipe(gulp.dest('www/'));
    gulp.src('src/app/**/*')
    .pipe(gulp.dest('www/app/'));
    gulp.src('src/assets/**/*')
    .pipe(gulp.dest('www/assets/'));
});
gulp.task('lib-js', function () {
    gulp.src([
        'src/assets/lib/bower_components/jquery/dist/jquery.min.js',
        'src/assets/lib/bower_components/jquery-ui/jquery-ui.min.js',
        'src/assets/lib/jquery.ui.touch-punch.js',
        'src/assets/lib/jquery.jrumble.1.3.min.js',
        'src/assets/lib/angular.min.js',
        'src/assets/lib/node_modules/angular-ui-router/release/angular-ui-router.min.js',
        'src/assets/lib/angular-animate.min.js',
        'src/assets/lib/node_modules/mobile-angular-ui/dist/js/mobile-angular-ui.min.js',
        'src/assets/lib/node_modules/mobile-angular-ui/dist/js/mobile-angular-ui.gestures.min.js',
        'src/assets/lib/node_modules/angular-touch/angular-touch.min.js',
        'src/assets/lib/bower_components/pubnub/web/pubnub.js',
        'src/assets/lib/bower_components/pubnub-angular/dist/pubnub-angular.js',
        'src/assets/lib/bower_components/moment/min/moment.min.js',
        'src/assets/lib/bower_components/angular-material/angular-material.min.js',
        'src/assets/lib/bower_components/angular-aria/angular-aria.min.js',
        'src/assets/lib/bower_components/angular-messages/angular-messages.min.js',
        'src/assets/lib/bower_components/angular-base64/angular-base64.min.js',
        'src/assets/lib/node_modules/chart.js/dist/Chart.min.js',
        'src/assets/lib/node_modules/angular-chart.js/dist/angular-chart.min.js',
        'src/assets/lib/node_modules/ng-material-datetimepicker/js/angular-material-datetimepicker.js',
        'src/assets/lib/bower_components/angular-translate/angular-translate.min.js',
        'src/assets/lib/bower_components/angular-translate-loader-partial/angular-translate-loader-partial.min.js',
        'src/assets/lib/angular-sanitize.min.js',
        'src/assets/lib/bower_components/fullcalendar/dist/fullcalendar.js',
        'src/assets/lib/bower_components/fullcalendar/dist/gcal.js',
        'src/assets/lib/ng-infinite-scroll.min.js',
        'src/assets/lib/bower_components/cryptojslib/components/core.js',
        'src/assets/lib/bower_components/cryptojslib/components/aes.js',
        'src/assets/lib/sortable.min.js',
        'src/assets/lib/angular-long-press.min.js',
        'src/assets/lib/jquery.tabSlideOut.v1.3.js',
        'src/assets/lib/node_modules/croppie/croppie.min.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('libs.source.js'))
    .pipe(gulp.dest('src/app'))
});
gulp.task('source-js', function () {
    gulp.src([
            'src/app/utils/constants.js',
            'src/app/utils/util.js',
            'src/app/utils/apiUrls.js',
            'src/app/utils/service.js',
            'src/app/app.module.js',
            'src/app/app.config.js',
            'src/app/shared/services/apiService.js',
            'src/app/shared/services/materialService.js',
            'src/app/shared/services/sharedDataService.js',
            'src/app/shared/services/pubnubService.js',
            'src/app/shared/services/pluginService.js',
            'src/app/shared/services/dbService.js',
            'src/app/components/login/login.config.js',
            'src/app/components/login/login.controller.js',
            'src/app/components/home/home.config.js',
            'src/app/components/home/home.controller.js',
            'src/app/components/dashboard/dashboard.config.js',
            'src/app/components/dashboard/dashboard.controller.js',
            'src/app/components/dashboard/dashboardList.controller.js',
            'src/app/components/map/map.config.js',
            'src/app/components/map/map.controller.js',
            'src/app/components/opportunity/opportunity.config.js',
            'src/app/components/opportunity/opportunity.list/opp.list.controller.js',
            'src/app/components/opportunity/opportunity.detail/opp.detail.controller.js',
            'src/app/components/opportunity/opportunity.detail/detail.components/Activity/opp.detail.activity.js',
            'src/app/components/opportunity/opportunity.detail/detail.components/Attachments/opp.details.attachments.js',
            'src/app/components/opportunity/opportunity.detail/detail.components/Details/opp.detail.details.js',
            'src/app/components/opportunity/opportunity.detail/detail.components/Map/opp.detail.map.js',
            'src/app/components/opportunity/opportunity.detail/detail.components/Notes/opp.detail.notes.js',
            'src/app/components/opportunity/opportunity.detail/detail.components/Products/opp.detail.products.js',
            'src/app/components/opportunity/opportunity.detail/detail.components/Quotes/opp.detail.quotes.js',
            'src/app/components/opportunity/opportunity.addition/opp.add.controller.js',
            'src/app/components/account/account.config.js',
            'src/app/components/account/account.controller.js',
            'src/app/components/quote/quote.config.js',
            'src/app/components/quote/quote.controller.js',
            'src/app/components/chat/chat.config.js',
            'src/app/components/chat/chat.controller.js',
            'src/app/components/calender/calender.config.js',
            'src/app/components/calender/calender.controller.js',
            'src/app/components/review/review.config.js',
            'src/app/components/review/review.controller.js',
            'src/app/components/settings/settings.config.js',
            'src/app/components/settings/settings.controller.js',
            'src/app/shared/directives/header/header.component.js',
            'src/app/shared/directives/loader/loader.component.js',
            'src/app/shared/directives/sideBar/sidebar.component.js',
            'src/app/shared/directives/chatSideBar/chatSidebar.component.js',
            'src/app/shared/directives/appScroll/appScroll.js',
            'src/app/shared/directives/scrollToTop/scrollToTop.js',
            'src/app/shared/directives/chatWindow/chatHandler.js',
            'src/app/shared/directives/popups/cityList/cityList.directive.js',
            'src/app/shared/directives/popups/accountList/accountlist.directive.js',
            'src/app/shared/directives/profilePicUploader/profilePhotoUploader.js',
            'src/app/components/inbox/inbox.config.js',
            'src/app/components/inbox/inbox.controller.js',
            'src/app/components/trackerview/trackerview.config.js',
            'src/app/components/trackerview/trackerview.controller.js',
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('app.source.js'))
    .pipe(gulp.dest('src/app'))
});
gulp.task('minify-source-js', function () {
    gulp.src('src/app/app.source.js')
    .pipe(sourcemaps.init())
    .pipe(concat('app.source.min.js'))
    .pipe(minify({
        ext:{
          min:'.js'
        },
        noSource: true,
        mangle:false
    }))
    .pipe(uglify({mangle:true,compress:true}))
    .pipe(gnirts())
    .pipe(gulp.dest('www/app'))
});
gulp.task('minify-lib-js', function () {
    gulp.src('src/app/libs.source.js')
    .pipe(sourcemaps.init())
    .pipe(concat('libs.source.min.js'))
    .pipe(minify({
        ext:{
          min:'.js'
        },
        noSource: true,
        mangle:false
    }))
    .pipe(uglify({mangle:true,compress:true}))
    .pipe(gnirts())
    .pipe(gulp.dest('www/app'))
});
gulp.task('obfuscator',function(){
   gulp.src(['www/app/libs.source.min.js','www/app/app.source.min.js'])
   //.pipe(js_obfuscator())
   .pipe(javascriptObfuscator({}))
   .pipe(concat('all.source.min.js')) 
   .pipe(gulp.dest('www/app'))
});
gulp.task('obfuscate-android-cordovaapi',function(){
    gulp.src(['platform_config/android/cordova-js-src/**/*.js'],{ base: 'platform_config/android/cordova-js-src'})
    .pipe(minify({
        ext:{
          min:'.js'
        },
        noSource: true,
        mangle:false
    }))
    .pipe(uglify({mangle:true,compress:true}))
    .pipe(gnirts())
    .pipe(javascriptObfuscator({}))
    .pipe(gulp.dest('platforms/android/assets/www/cordova-js-src'));
});
gulp.task('obfuscate-ios-cordovaapi',function(){
    gulp.src(['platform_config/ios/cordova-js-src/*.js'])
    .pipe(minify({
        ext:{
          min:'.js'
        },
        noSource: true,
        mangle:false
    }))
    .pipe(uglify({mangle:true,compress:true}))
    .pipe(gnirts())
    .pipe(javascriptObfuscator({}))
    .pipe(gulp.dest('platforms/ios/www/cordova-js-src'));
});
gulp.task('obfuscate-android-plugins',function(){
    gulp.src(['platform_config/android/plugins/**/*.js'],{ base: 'platform_config/android/plugins'})
    .pipe(minify({
        ext:{
          min:'.js'
        },
        noSource: true,
        mangle:false
    }))
    .pipe(uglify({mangle:true,compress:true}))
    .pipe(gnirts())
    .pipe(javascriptObfuscator({}))
    .pipe(gulp.dest('platforms/android/assets/www/plugins'));
});
gulp.task('obfuscate-android-cordovalib',function(){
    gulp.src(['platform_config/android/cordova_plugins.js','platform_config/android/cordova.js'])
    .pipe(minify({
        ext:{
          min:'.js'
        },
        noSource: true,
        mangle:false
    }))
    .pipe(uglify({mangle:true,compress:true}))
    .pipe(gnirts())
    .pipe(javascriptObfuscator({}))
    .pipe(gulp.dest('platforms/android/assets/www'));
});
gulp.task('obfuscate-ios-plugins',function(){
    gulp.src(['platform_config/ios/plugins/**/*.js'],{ base: 'platform_config/ios/plugins'})
    .pipe(minify({
        ext:{
          min:'.js'
        },
        noSource: true,
        mangle:false
    }))
    .pipe(uglify({mangle:true,compress:true}))
    .pipe(gnirts())
    .pipe(javascriptObfuscator({}))
    .pipe(gulp.dest('platforms/ios/www/plugins'));
});
gulp.task('obfuscate-ios-cordovalib',function(){
    gulp.src(['platform_config/ios/cordova_plugins.js','platform_config/ios/cordova.js'])
    .pipe(minify({
        ext:{
          min:'.js'
        },
        noSource: true,
        mangle:false
    }))
    .pipe(uglify({mangle:true,compress:true}))
    .pipe(gnirts())
    .pipe(javascriptObfuscator({}))
    .pipe(gulp.dest('platforms/ios/www'));
});
gulp.task('copy-android-cordovaapi',function(){
    gulp.src(['platform_config/android/cordova-js-src/**/*.js'],{ base: 'platform_config/android/cordova-js-src'})
    .pipe(gulp.dest('platforms/android/assets/www/cordova-js-src'));
});
gulp.task('copy-ios-cordovaapi',function(){
    gulp.src(['platform_config/ios/cordova-js-src/*.js'])
    .pipe(gulp.dest('platforms/ios/www/cordova-js-src'));
});
gulp.task('copy-android-plugins',function(){
    gulp.src(['platform_config/android/plugins/**/*.js'],{ base: 'platform_config/android/plugins'})
    .pipe(gulp.dest('platforms/android/assets/www/plugins'));
});
gulp.task('copy-android-cordovalib',function(){
    gulp.src(['platform_config/android/cordova_plugins.js','platform_config/android/cordova.js'])
    .pipe(gulp.dest('platforms/android/assets/www'));
});
gulp.task('copy-ios-plugins',function(){
    gulp.src(['platform_config/ios/plugins/**/*.js'],{ base: 'platform_config/ios/plugins'})
    .pipe(gulp.dest('platforms/ios/www/plugins'));
});
gulp.task('copy-ios-cordovalib',function(){
    gulp.src(['platform_config/ios/cordova_plugins.js','platform_config/ios/cordova.js'])
    .pipe(gulp.dest('platforms/ios/www'));
});
gulp.task('copy-platform-config-ios',function(){
    gulp.src(['platforms/ios/www/**'],{ base: 'platforms/ios/www'})
    .pipe(gulp.dest('platform_config/ios'));
});
gulp.task('copy-platform-config-android',function(){
    gulp.src(['platforms/android/assets/www/**'],{ base: 'platforms/android/assets/www'})
    .pipe(gulp.dest('platform_config/android'));
});
gulp.task('copy-output-to-platform-android',function(){
    gulp.src(['www/**'],{ base: 'www'})
    .pipe(gulp.dest('platforms/android/assets/www'));
});
gulp.task('copy-output-to-platform-ios',function(){
    gulp.src(['www/**'],{ base: 'www'})
    .pipe(gulp.dest('platforms/ios/www'));
});
gulp.task('cordova-obfuscator',['obfuscate-android-cordovaapi','obfuscate-ios-cordovaapi','obfuscate-android-plugins','obfuscate-android-cordovalib','obfuscate-ios-plugins','obfuscate-ios-cordovalib']);
gulp.task('cordova-copy',['copy-android-cordovaapi','copy-ios-cordovaapi','copy-android-plugins','copy-android-cordovalib','copy-ios-plugins','copy-ios-cordovalib']);
gulp.task('copy-platform-configs',['copy-platform-config-ios','copy-platform-config-android']);
gulp.task('copy-output-to-plaftorm',['copy-output-to-platform-android','copy-output-to-platform-ios']);
gulp.task('minify',['minify-lib-js','minify-source-js']);
gulp.task('build-dev',['clean-dir','copy-dev-files']);

gulp.task('build-prod',['clean-dir','copy-prod-files','lib-js','source-js']);
