package com.anrip.cordova;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;


import com.crm.stcmobile.OnLogoutCompletionListener;

import org.json.JSONObject;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static android.os.PowerManager.PARTIAL_WAKE_LOCK;

/**
 * Puts the service in a foreground state, where the system considers it to be
 * something the user is actively aware of and thus not a candidate for killing
 * when low on memory.
 */
public class ForegroundService extends Service implements OnLogoutCompletionListener {
    private final IBinder mBinder = new ForegroundBinder();
    private PowerManager.WakeLock wakeLock;
    String invalidateurl,token,user,dataparams;
    boolean isLoggingOut = false;
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        Runnable periodicTask = new Runnable() {
            public void run() {
                Boolean result = isAppActive("com.crm.stcmobile");
                if(result){
                    if(!isLoggingOut){
                        isLoggingOut = true;
                        invalidateThread.start();
                    }
                }
            }
        };
        executor.scheduleAtFixedRate(periodicTask, 0, 10, TimeUnit.SECONDS);
        keepAwake();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLogoutSuccess() {
        stopSelf();
    }

    @Override
    public void onLogoutFailure() {

    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class ForegroundBinder extends Binder {
        ForegroundService getService() {
            return ForegroundService.this;
        }
    }
    /**
     * Put the service in a foreground state to prevent app from being killed
     * by the OS.
     */
    private void keepAwake() {
        PowerManager powerMgr = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerMgr.newWakeLock(PARTIAL_WAKE_LOCK, "BackgroundMode");
        wakeLock.acquire();
    }
    /**
     * Stop background mode.
     */
    private void sleepWell() {
        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
    }
    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = params.keys();
        while(itr.hasNext()){
            String key= itr.next();
            Object value = params.get(key);
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return result.toString();
    }
    public boolean isAppActive(String PackageName){
        Boolean isAppForeground = false;
        Boolean shouldKillApp = false;
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> recentTasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
        List< ActivityManager.RunningTaskInfo > task = activityManager.getRunningTasks(1);
        ComponentName componentInfo = task.get(0).topActivity;
        if(componentInfo.getPackageName().equals(PackageName)) {
            isAppForeground = true;
        }
        else{
            isAppForeground = false;
        }
        if(!isAppForeground){
            for (int i = 0; i < recentTasks.size(); i++)
            {
                if(recentTasks.get(i).baseActivity.toShortString().contains(PackageName)){
                    shouldKillApp = false;
                }else{
                    shouldKillApp = true;
                }
            }
        }else{
            shouldKillApp = false;
        }
        return shouldKillApp;
    }
    Thread invalidateThread = new Thread(new Runnable() {
        public void run() {
            SharedPreferences shared = getSharedPreferences("LOGOUT_PARAMS", MODE_PRIVATE);
            String params = (shared.getString("logoutParams", "")).replaceAll("\\\\","").replace("\"", "");
            String [] items = params.split(",");
            invalidateurl = items[0];
            token = items[1];
            user = items[2];
            dataparams = items[3];
            try {
                URL url = new URL(invalidateurl);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestProperty ("Content-Type","application/json; charset=utf-8");
                con.setRequestProperty ("x-access-user",user);
                con.setRequestProperty ("x-access-token",token);
                con.setRequestMethod("POST");
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("Data", dataparams);
                con.setDoOutput(true);
                con.setDoInput(true);
                con.connect();
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(postDataParams.toString());
                wr.flush();
                wr.close();
                stopSelf();
                isLoggingOut = false;
            } catch (Throwable t) {}
        }
    });
}