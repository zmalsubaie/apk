package com.crm.stcmobile;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

public class LogOutOnKillAsyncTask extends AsyncTask<Void, Integer, String>{

    private OnLogoutCompletionListener listener;

    public LogOutOnKillAsyncTask(Context context) {
        if(context instanceof OnLogoutCompletionListener){
            listener = (OnLogoutCompletionListener)context;
        }
    }

    protected void onPreExecute (){
        super.onPreExecute();
    }

    protected String doInBackground(Void...arg0) {
        return "You are at PostExecute";

    }

    protected void onProgressUpdate(Integer...a){
        super.onProgressUpdate(a);
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(null != listener){
            listener.onLogoutSuccess();
        }

    }
}