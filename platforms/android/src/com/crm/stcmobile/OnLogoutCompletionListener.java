package com.crm.stcmobile;

/**
 * Created by ramchand on 06/07/17.
 */

public interface OnLogoutCompletionListener {

    void onLogoutSuccess();
    void onLogoutFailure();
}
