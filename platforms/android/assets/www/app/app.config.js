(function () {
    'use strict';
    var routerConfig = function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/login');
        $stateProvider.state('root', {
            abstract: true,
            views: {
                '': {
                    templateUrl: 'app/shared/layout/contentArea.template.html'
                }
            }
        });
    };
    var translateConfig = function ($translateProvider, $translatePartialLoaderProvider) {
        $translatePartialLoaderProvider.addPart('stcApp');
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: 'app/utils/languageLocals/locale-{lang}.json'
        });
        $translateProvider.preferredLanguage('en_US');
    };
    var materialConfig = function ($mdThemingProvider, $mdAriaProvider) {
        $mdAriaProvider.disableWarnings();
        $mdThemingProvider = Util.initializeAppTheme($mdThemingProvider);
    };
    var httpConfig = function ($httpProvider) {
        $httpProvider.defaults.timeout = 90000;
    };
    var appConfig = function ($compileProvider) {
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|ms-appx|ms-appx-web|x-wmapp0):|data:image\//);
    };
    var appEnvironmentSetup = function ($rootScope, $state, $injector, $translate, MaterialReusables, pluginService, dbService, sharedDataService, apiService, PubnubService) {
        if (!DEVICE_BUILD) {
            Util.pluginServiceScope = pluginService;
            dbService.init();
        }
        Util.saveAppStates($state);
        $translate(pluginService.translationsArray()).then(function (translations) {
            pluginService.setTranslations(translations);
        });
        document.addEventListener('deviceready', function (e) {
            if (DEVICE_BUILD) {
                Util.pluginServiceScope =  pluginService;
                dbService.init();
            }
            pluginService.listenToAllNotifications(MaterialReusables);
        }, false);
        document.addEventListener('backbutton', function (e) {
            if (MaterialReusables.isSideBarOpen()) {
                MaterialReusables.toggleSideBar();
            } else if (MaterialReusables.isChatBarOpen()) {
                MaterialReusables.toggleChatBar();
            } else if (Util.getMaterialCalenderInst() !== '') {
                Util.getMaterialCalenderInst()._dialog.cancel();
            } else {
                if($state.current.name === 'root.home'){
                    if(MaterialReusables.getDialogState()){
                        MaterialReusables.hideDialog();
                    }else{
                        var logOutdialogResult = function (confirmation) {
                            if (!sharedDataService.getNetworkState()) {
                                //$state.go('login');
                            } else {
                                if (confirmation) {
                                    MaterialReusables.showLogoutPopup('' + pluginService.getTranslations().loggingOut);
                                    apiService.triggerAppLogout(MaterialReusables,PubnubService,$state);
                                } else {
                                    MaterialReusables.hideDialog();
                                }
                            }
                        };
                        MaterialReusables.showConfirmDialog(event, '' + pluginService.getTranslations().logOut_Msg, logOutdialogResult);
                    }
                }else {
                    if ($state.current.name === 'login') {
                        navigator.app.exitApp();
                        resetPreferences();
                    } else {
                        console.log(MaterialReusables.getDialogState());
                        if (MaterialReusables.getDialogState()) {
                            MaterialReusables.hideDialog();
                            MaterialReusables.hideBottomSheetOptions();
                        } else {
                            Util.gotoPreviousView(Util.getCurrentScope(), $state, $rootScope);
                        }
                    }
                }
            }
        },false);
        $rootScope.sidenavClosed = function () {
            if ($state.current.name === 'root.map') {
                var map = plugin.google.maps.Map.getMap(document.getElementById("map_canvas"));
                map.setClickable(true);
            }
        };
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.stateBack = toState.previousState;
        });
        var resetPreferences = function () {
            sharedDataService.saveAssignedToList([]);
            sharedDataService.saveAssigneeToRowNum(0);
            sharedDataService.saveviewSelected(null);
            sharedDataService.saveIsManager(false);
            sharedDataService.saveSubordinateList([]);
        };
    };
    routerConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    translateConfig.$inject = ['$translateProvider', '$translatePartialLoaderProvider'];
    materialConfig.$inject = ['$mdThemingProvider', '$mdAriaProvider'];
    httpConfig.$inject = ['$httpProvider'];
    appConfig.$inject = ['$compileProvider'];
    appEnvironmentSetup.$inject = ['$rootScope', '$state', '$injector', '$translate', 'MaterialReusables', 'pluginService', 'dbService', 'sharedDataService', 'apiService', 'PubnubService'];
    stcApp.config(routerConfig).config(translateConfig).config(materialConfig).config(httpConfig).config(appConfig).run(appEnvironmentSetup);
})();

