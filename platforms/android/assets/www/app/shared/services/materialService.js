(function () {
  'use strict';
  var MaterialReusables = function ($mdToast, $mdDialog, $mdSidenav, $mdBottomSheet) {
    var dialogState = false;
    var MaterialReusables = {};
    MaterialReusables.setDialogState = function (state) {
      dialogState = state;
    };
    MaterialReusables.getDialogState = function () {
      return dialogState;
    };
    MaterialReusables.showToast = function (content, toastType) {
      $mdToast.hide($mdToast);
      return $mdToast.show(
        $mdToast.simple()
          .content(content)
          .position('bottom')
          .hideDelay(4000)
          .action('' + Util.pluginServiceScope.getTranslations().OK)
          .highlightAction(false)
          .theme(toastType + "-toast")
      )
    };
    MaterialReusables.showHomeEditToast = function (successCallBack) {
      var editToast = $mdToast.simple()
        .content(''+Util.pluginServiceScope.getTranslations().editHomeScreen) 
        .parent(document.querySelectorAll('#hometop'))
        .position('top left')
        .hideDelay(false)
        .action('' + Util.pluginServiceScope.getTranslations().Close)
        .highlightAction(false)
        .theme("edit-toast");
      return $mdToast.show(editToast).then(function (response) {
        if (response === 'ok') {
          $mdToast.hide($mdToast);
          successCallBack(true)
        }
      });
    };
    MaterialReusables.showChatMsgToast = function (sender, successCallBack) {
      $mdToast.hide($mdToast);
      var toast = $mdToast.simple()
        .textContent('' + Util.pluginServiceScope.getTranslations().newMessage + ' ' + sender)
        .position('top left')
        .action('VIEW')
        .hideDelay(4000)
        .highlightAction(false)
        .theme("success-toast");
      return $mdToast.show(toast).then(function (response) {
        if (response === 'ok') { successCallBack(true, sender) }
      });
    };
    MaterialReusables.toggleSideBar = function () {
      $mdSidenav('left').toggle();
    };
    MaterialReusables.toggleChatBar = function () {
      $mdSidenav('right').toggle();
    };
    MaterialReusables.isSideBarOpen = function () {
      return $mdSidenav('left').isOpen();
    };
    MaterialReusables.isChatBarOpen = function () {
      return $mdSidenav('right').isOpen();
    };
    MaterialReusables.showConfirmDialog = function (event, text, callback) {
      $mdDialog.cancel();
      var confirm = $mdDialog.confirm()
        .clickOutsideToClose(true)
        .title(text)
        .targetEvent(event)
        .ok('' + Util.pluginServiceScope.getTranslations().Yes)
        .cancel('' + Util.pluginServiceScope.getTranslations().No);
      MaterialReusables.setDialogState(true);
      $mdDialog.show(confirm).then(function () {
        MaterialReusables.setDialogState(false);
        $mdDialog.hide();
        callback(true);
      }, function () { callback(false); });
    };
    MaterialReusables.showAlert = function (text) {
      $mdDialog.cancel();
      alert = $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('' + Util.pluginServiceScope.getTranslations().err)
        .textContent(text)
        .ok('' + Util.pluginServiceScope.getTranslations().OK);
      MaterialReusables.setDialogState(true);
      $mdDialog
        .show(alert)
        .finally(function () {
          MaterialReusables.setDialogState(false);
          alert = null;
        });
    };
    MaterialReusables.showSuccessAlert = function (text, callback) {
      $mdDialog.cancel();
      alert = $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('')
        .textContent(text)
        .ok('' + Util.pluginServiceScope.getTranslations().OK);
      MaterialReusables.setDialogState(true);
      $mdDialog
        .show(alert)
        .finally(function () {
          alert = null;
          MaterialReusables.setDialogState(false);
          callback(true);
        });
    };
    MaterialReusables.showErrorAlert = function (text) {
      $mdDialog.cancel();
      alert = $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('')
        .textContent(text)
        .ok('' + Util.pluginServiceScope.getTranslations().OK);
      MaterialReusables.setDialogState(true);
      $mdDialog
        .show(alert)
        .finally(function () {
          MaterialReusables.setDialogState(false);
          alert = null;
        });
    };
    MaterialReusables.hideDialog = function () {
      MaterialReusables.setDialogState(false);
      $mdDialog.cancel();
      $mdDialog.hide();
    };
    MaterialReusables.showAlarmPopup = function (notification, successCallBack) {
      var confirm = $mdDialog.confirm()
        .clickOutsideToClose(false)
        .title(notification.title)
        .targetEvent(event)
        .htmlContent(notification.text)
        .ok('' + Util.pluginServiceScope.getTranslations().OK)
        .cancel('' + Util.pluginServiceScope.getTranslations().Cancel);
      MaterialReusables.setDialogState(true);
      $mdDialog.show(confirm).then(function () {
        MaterialReusables.setDialogState(false);
        MaterialReusables.hideDialog();
        successCallBack(true);
      }, function () { successCallBack(false); });
    };
    MaterialReusables.showAttachmentOptions = function (currentScope) {
      Util.setAttachmentScope(currentScope);
      var bottomSheetOptions = {
        templateUrl: 'app/shared/layout/attachmentOptions.template.html'
      };
      MaterialReusables.setDialogState(true);
      $mdBottomSheet.show(bottomSheetOptions);
    };
    MaterialReusables.hideBottomSheetOptions = function () {
      MaterialReusables.setDialogState(false);
      $mdBottomSheet.hide();
    };
    MaterialReusables.showInfoPopup = function (title, text, okBtnText, cancelBtnText, callback) {
      $mdDialog.cancel();
      var confirm = $mdDialog.confirm()
        .clickOutsideToClose(false)
        .title(title)
        .targetEvent(event)
        .htmlContent(text)
        .ok(okBtnText)
        .cancel(cancelBtnText);
      MaterialReusables.setDialogState(true);
      $mdDialog.show(confirm).then(function () {
        MaterialReusables.setDialogState(false);
        MaterialReusables.hideDialog();
        callback(true);
      }, function () { callback(false); });
    };
    MaterialReusables.showActivityListSheet = function (scope) {
      MaterialReusables.setDialogState(true);
      $mdBottomSheet.show({
        templateUrl: 'app/shared/layout/activityListBottomSheetTemplate.html',
        controller: angular.noop,
        controllerAs: 'ctrl',
        locals: { parent: scope },
        disableBackdrop: false,
        bindToController: true
      }).then(function (clickedItem) { });
    };
    MaterialReusables.showInMapChatWindow = function () {
      MaterialReusables.setDialogState(true);
      $mdBottomSheet.show({
        templateUrl: 'app/shared/layout/inMapChatBottomSheetTemplate.html',
        controller: trackerViewController,
        disableBackdrop: false,
        bindToController: true
      }).then(function (clickedItem) { });
    };
    MaterialReusables.showLogoutPopup = function (msg) {
      MaterialReusables.setDialogState(true);
      var templateDesign = '';
      if (msg === 'Logging out..Please wait') {
        templateDesign = '<md-dialog>' + '<span style="line-height: 6;font-size: 15px;padding-left: 37px;">' + msg + '</span>' + '</md-dialog>';
      } else {
        templateDesign = '<md-dialog>' + '<span style="font-size: 15px;padding: 5px;">' + msg + '</span>' + '</md-dialog>';
      }
      $mdDialog.show({
        targetEvent: '',
        clickOutsideToClose: false,
        template: templateDesign,
        controller: function () { }
      })
    };
    MaterialReusables.showOppSyncPopup = function (scope) {
      MaterialReusables.setDialogState(true);
      $mdDialog.show({
        templateUrl: 'app/shared/layout/oppStatusDisplayer.template.html',
        controller: angular.noop,
        controllerAs: 'ctrl',
        locals: { parent: scope },
        disableBackdrop: false,
        bindToController: true
      })
    };
    MaterialReusables.invokeCustomPopUp = function(controllerScope,type){
      MaterialReusables.setDialogState(true);
      var template = '';
      if(type === 'City'){
        template = '<city-list city-on-select="cityOnSelect(selectedCity)" city-on-close="cityOnClose()"></city-list>';
      }else if(type === 'Account'){
        template = '<account-list account-on-select="accountOnSelect(selectedAccount)" account-on-close="accountOnClose()"></account-list>';
      }
      $mdDialog.show({
          template: template,
          clickOutsideToClose: false,
          scope: controllerScope,
          preserveScope: true,
      });
    };
    MaterialReusables.showCustomPopup = function (controllerScope, designUrl) {
      MaterialReusables.setDialogState(true);
      $mdDialog.show({
        templateUrl: designUrl,
        clickOutsideToClose: true,
        scope: controllerScope,
        preserveScope: true,
        controller: ''
      })
    };
    MaterialReusables.showProfilePhotoOptions = function (scope) {
      var bottomSheetOptions = {
        template: '<profile-photo-uploader photo-on-select="selectedPhoto()"></profile-photo-uploader>',
        controller: '',
        clickOutsideToClose: true,
        scope:scope,
        preserveScope: true
      };
      MaterialReusables.setDialogState(true);
      $mdBottomSheet.show(bottomSheetOptions);
    };
    return MaterialReusables;
  };
  stcApp.service('MaterialReusables', MaterialReusables);
  MaterialReusables.$inject = ['$mdToast', '$mdDialog', '$mdSidenav', '$mdBottomSheet'];
})();