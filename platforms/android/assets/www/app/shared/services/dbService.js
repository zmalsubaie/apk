(function () {
  'use strict';
  var dbService = function ($q) {
    var dbService = {
      'TABLES': {
        'NOTIFICATION_TIME': {
          'modelName': 'stc.notificationTime',
          'columns': ['notificationTime INTEGER', 'updateRequired BOOLEAN']
        },
        'CREATE_OPP_DATA': {
          'modelName': 'stc.offlineOppCreate',
          'columns': ['oppID INTEGER', 'oppDetail VARCHAR', 'oppType VARCHAR', 'postStatus VARCHAR', 'recordStatus VARCHAR']
        },
        'SAVE_USER_CREDENTIALS': {
          'modelName': 'stc.userCredentials',
          'columns': ['username VARCHAR', 'password VARCHAR', 'userDetails VARCHAR']
        },
        'CITY_DATA': {
          'modelName': 'stc.cities',
          'columns': ['cities VARCHAR']
        },
        'ACTIVITY_STATUS_DATA': {
          'modelName': 'stc.activityStatus',
          'columns': ['activities VARCHAR']
        },
        'OPTTY_DATA': {
          'modelName': 'stc.opptys',
          'columns': ['optyID VARCHAR PRIMARY KEY', 'opportunities VARCHAR', 'payload VARCHAR', 'status VARCHAR', 'pushType VARCHAR', 'toBeUpdated VARCHAR', 'postStatus VARCHAR']
        },
        'ACCOUNT_DATA': {
          'modelName': 'stc.accounts',
          'columns': ['accounts VARCHAR', 'pushType VARCHAR']
        },
        'QUOTE_DATA': {
          'modelName': 'stc.quotes',
          'columns': ['quotes VARCHAR', 'pushType VARCHAR']
        },
        'PRODUCT_DATA': {
          'modelName': 'stc.products',
          'columns': ['productID VARCHAR', 'products VARCHAR']
        }
      }
    };
    var db;
    var createAllTables = function (tables) {
      var createTableSuccess = function () { };
      var createTableError = function () {
        console.log('Error in creating table!');
      };
      angular.forEach(tables, function (value, key) {
        dbService.createTable(value.modelName, value.columns, createTableSuccess, createTableError);
      });
    };
    dbService.init = function () {
      if(DEVICE_BUILD){
        db = window.sqlitePlugin.openDatabase({name: DATABASE_NAME, location: 'default'}); 
      } else {
        db = window.openDatabase(DATABASE_NAME, '1', 'STC', DB_SIZE);
      }
      createAllTables(dbService.TABLES);
    };
    dbService.createTable = function (modelName, columns, successCallBack, errorCallBack) {
      var table = dbService.getTableFromModel(modelName);
      var callbacks = dbService.setCallBacks(errorCallBack, successCallBack);
      var sql = 'CREATE TABLE IF NOT EXISTS ' + table + '(' + columns.join(',') + ');';
      db.transaction(dbService.executeQuery('CREATE', table, sql, callbacks));
    };
    dbService.insertIntoDBTable = function (tableName, parameters, errorCallBack, successCallBack) {
      var callbacks = dbService.setCallBacks(errorCallBack, successCallBack);
      var sql = "INSERT INTO " + tableName + (parameters.length > 0 ? " VALUES( '" + parameters.join("' , '") + "')" : "") + ";";
      db.transaction(dbService.executeQuery('INSERT', tableName, sql, callbacks));
    };
    dbService.getDetailsFromTable = function (tableName, parameters, errorCallBack, successCallBack) {
      var callbacks = dbService.setCallBacks(errorCallBack, successCallBack);
      var sql = 'SELECT * FROM ' + tableName + (parameters.length > 0 ? ' WHERE ' + parameters.join(' and ') : '') + ';';
      db.transaction(dbService.executeQuery('SELECT', tableName, sql, callbacks));
    };
    dbService.updateTable = function (tableName, valueParameters, checkParameters, errorCallBack, successCallBack) {
      var callbacks = dbService.setCallBacks(errorCallBack, successCallBack);
      //valueParameters = Util.convertObjectToSQL(valueParameters);
      //checkParameters = Util.convertObjectToSQL(checkParameters);
      var sql = 'UPDATE ' + tableName + ' SET ' + valueParameters.join(',') + (checkParameters.length > 0 ? ' WHERE ' + checkParameters.join(' and ') : '') + ';';
      db.transaction(dbService.executeQuery('UPDATE', tableName, sql, callbacks))
    };
    dbService.deleteFromTable = function (tableName, parameters, errorCallBack, successCallBack) {
      var callbacks = dbService.setCallBacks(errorCallBack, successCallBack);
      var sql = 'DELETE FROM ' + tableName + (parameters.length > 0 ? ' WHERE ' + parameters.join(' and ') : '') + ';';
      db.transaction(dbService.executeQuery('DELETE', tableName, sql, callbacks));
    };
    dbService.countFromTable = function (tableName, parameters, errorCallBack, successCallBack) {
      var callbacks = dbService.setCallBacks(errorCallBack, successCallBack);
      var sql = 'SELECT COUNT(*) AS RECORDS FROM ' + tableName + (parameters.length > 0 ? ' WHERE ' + parameters.join(' and ') : '') + ';';
      db.transaction(dbService.executeQuery('SELECT', tableName, sql, callbacks));
    };
    // dbService.deleteTable = function (tableName, parameters, errorCallBack, successCallBack) {
    //   var callbacks = dbService.setCallBacks(errorCallBack, successCallBack);
    //   var sql = 'DELETE TABLE ' + tableName + (parameters.length > 0 ? ' WHERE ' + parameters.join(' and ') : '') + ';';
    //   db.transaction(dbService.executeQuery('DELETE', tableName, sql, callbacks));
    // };
    dbService.deleteAllTable = function () {
      angular.forEach(dbService.TABLES, function (value, key) {
        var table = dbService.getTableFromModel(value.modelName);
        dbService.deleteFromTable(table, [], angular.noop, angular.noop);
      });
    };
    dbService.setCallBacks = function (errFunction, successFunction) {
      return { error: errFunction, success: successFunction };
    };
    dbService.executeQuery = function (operation, tableName, SQL, callbacks) {
      return function (tx) {
        tx.executeSql(SQL, [], dbService.dbSuccess(operation, tableName, callbacks), dbService.dbFail(operation, tableName, callbacks.error));
      };
    };
    dbService.dbSuccess = function (operation, tableName, callbacks) {
      var successCallBack = callbacks.success;
      var errorCallBack = callbacks.error;
      var response = '';
      return function (tx, SQLResultSet) {
        try {
          if (operation === 'SELECT') {
            var response = [];
            if (true) {
              for (var i = 0; i < SQLResultSet.rows.length; i++) {
                response[i] = SQLResultSet.rows.item(i);
              }
              successCallBack(response);
            }
          } else if (operation === 'UPDATE') {
            successCallBack(SQLResultSet.rowsAffected);
          } else {
            successCallBack();
          }
        } catch (err) {
          console.log('No parent CallBack specified : ', err.toString());
        }
      };
    };
    dbService.dbFail = function (operation, tableName, errorCallBack) {
      return function (sqlTransaction, sqlError) {
        try { errorCallBack(); }
        catch (err) { }
      };
    };
    dbService.getTableFromModel = function (model) {
      return Util.replaceAll(model, '.', '_');
    };
    dbService.getAllUpdatableRecords = function (successCallBack,errorCallBack) {
      var failedToGetRecords = function (error) { 
        errorCallBack(error);
      };
      var gotRecordsToPost = function (records) {
         successCallBack(records); 
      };
      dbService.getDetailsFromTable('stc_opptys', Util.convertObjectToSQL({ toBeUpdated: 'true' }), failedToGetRecords, gotRecordsToPost);
    };
    dbService.userDetailsHandler = function(userDetails){
        var gotUserDetails = function(userData){
          if(userData.length){
             if(userData[0].username !== userDetails.name && userData[0].password !== userDetails.password){
                localStorage.setItem('attemptCount', 0);
                localStorage.setItem('otpAttempts','0');
                dbService.deleteAllTable();
                createAllTables();
                dbService.insertIntoDBTable('stc_userCredentials',[userDetails.name,userDetails.password,JSON.stringify(userDetails.apiDetails)],angular.noop,angular.noop);
                dbService.injectNotificationTimeDetails();
             }
          }else{
            dbService.insertIntoDBTable('stc_userCredentials',[userDetails.name,userDetails.password,JSON.stringify(userDetails.apiDetails)],angular.noop,angular.noop);
          }
        };
        dbService.getDetailsFromTable('stc_userCredentials',[],angular.noop,gotUserDetails);
    };
    dbService.injectNotificationTimeDetails = function(){
       var gotNotificationTime = function(data){
          if(!data.length){
            dbService.insertIntoDBTable('stc_notificationTime',[10,true],angular.noop,angular.noop);
          }
       };
       dbService.getDetailsFromTable('stc_notificationTime',[],angular.noop,gotNotificationTime);
    };
    return dbService;
  };
  stcApp.service('dbService', dbService);
  dbService.$inject = ['$q'];
})();