(function () {
    'use strict';
    var sideBar = function ($state, $rootScope, $translate, MaterialReusables, sharedDataService, PubnubService, pluginService, apiService) {
        return {
            restrict: 'E',
            templateUrl: "app/shared/directives/sideBar/sidebar.template.html",
            transclude: true,
            compile: function compile(tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) {
                        scope.mainModuleTags = [];
                        scope.mainModuleTags = mainModuleProps_sidemenu;
                        scope.recordstoUpdate = false;
                    },
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        scope.$watch('sideBarFeatures', function (value) {
                            if (angular.isDefined($rootScope.sideBarFeatures)) {
                                scope.mainModuleTags = $rootScope.sideBarFeatures;
                            } else {
                                scope.mainModuleTags = mainModuleProps_sidemenu;
                            }
                        });
                        scope.openSidebarNav = function (pageName, pageToLoad, enableRouting) {
                            MaterialReusables.toggleSideBar();
                            if (enableRouting) { $state.go(pageToLoad); }
                            else { MaterialReusables.showToast(pageName + ' View is not available in Offline Mode', 'warning'); }
                        };
                        scope.showConfirm = function (ev) {
                            MaterialReusables.toggleSideBar();
                            var logOutdialogResult = function (confirmation) {
                                if (!sharedDataService.getNetworkState()) {
                                    $state.go('login');
                                } else {
                                    if (confirmation) {
                                        MaterialReusables.showLogoutPopup('' + pluginService.getTranslations().loggingOut);
                                        apiService.triggerAppLogout(MaterialReusables,PubnubService,$state);
                                    } else {
                                        MaterialReusables.hideDialog();
                                    }
                                }
                            };
                            if ($state.current.name === 'root.map') {
                                var map = plugin.google.maps.Map.getMap(document.getElementById("map_canvas"));
                                map.setClickable(false);
                            }
                            MaterialReusables.showConfirmDialog(event, '' + pluginService.getTranslations().logOut_Msg, logOutdialogResult);
                        };
                        scope.userStatus = 'Prayer';
                        $rootScope.$on('$stateChangeSuccess', function () {
                            scope.selectedMenu = $state.current.name;
                        });
                    }
                }
            }
        }
    };
    stcApp.directive('sideBar', sideBar);
    sideBar.$inject = ['$state', '$rootScope', '$translate', 'MaterialReusables', 'sharedDataService', 'PubnubService', 'pluginService', 'apiService'];
})();