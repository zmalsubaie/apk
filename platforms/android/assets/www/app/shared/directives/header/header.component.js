(function() {
  'use strict';
    var headerController = function($scope,$state,$stateParams,$translate,$rootScope,MaterialReusables,sharedDataService,dbService){
        $scope.headerComponent = {
            pageTitle:$state.current.pageTitle,
            backbutton:false,
            buttonProperty:[], 
            previousState:'',
            hideHeader:false,
            validateCurrentPage:false,
            enableSetLocation:false
        };
        var hideShowHeader = function(){
            if($state.current.name === 'root.home'){
                $scope.headerComponent.hideHeader = true;
            }else{
                $scope.headerComponent.hideHeader = false;  
            }
        };
        var setPageTitle = function(pageTitle,selectionId){
            if(typeof $stateParams.id!=='undefined'){
                $scope.headerComponent.pageTitle = pageTitle+''+selectionId;
            }else{
                $scope.headerComponent.pageTitle = pageTitle;
            }
        };
        var setButtonViews = function(currentState){
            if(typeof currentState.current.buttonProp!=='undefined'){
                $scope.headerComponent.buttonProperty = currentState.current.buttonProp;
            }else{
                $scope.headerComponent.buttonProperty = '';
            }
        };
        hideShowHeader();
        $scope.$on('$stateChangeSuccess', function() {
            Util.setCurrentScope($scope);
            $scope.headerComponent.backbutton =  $state.current.backbutton;
            $scope.headerComponent.previousState = $state.current.previousState;
            $scope.headerComponent.validateCurrentPage = $state.current.validatingPage;
            $scope.headerComponent.enableSetLocation = false;
            hideShowHeader();
            setPageTitle($state.current.pageTitle,$stateParams.id);
            setButtonViews($state);
        });
        $scope.triggerHeaderEvents = function(action){
            if(action === 'search'){ $rootScope.$broadcast('openSearch', []); }
            else if(action === 'add'){ $state.go('root.oppAddition',{fromView:$state.current.name}); }
            else if(action === 'refresh'){ $rootScope.$broadcast('refresh', []); }
            else if(action === 'save'){ $rootScope.$broadcast('save', []); }
        };
        var checkifMapExists = function(){
            if ($scope.headerComponent.buttonProperty.indexOf('map') > -1) {
                var index = $scope.headerComponent.buttonProperty.indexOf("map");
                $scope.headerComponent.buttonProperty.splice(index,1);
            }
        };
        $scope.$on('enableSetLocation', function (event, args) {
            checkifMapExists();
            $scope.headerComponent.buttonProperty.push('map');
            $scope.headerComponent.enableSetLocation = true;
        });
        $scope.$on('disableSetLocation', function (event, args) {
            checkifMapExists();
            $scope.headerComponent.enableSetLocation = false;
        });
        $scope.gotoPrevView = function(){
            Util.gotoPreviousView($scope,$state,$rootScope);
        };
        $scope.openSideNav = function(){
            MaterialReusables.toggleSideBar();
            var updateSideBarItems = function(updateOffline){
                angular.forEach(mainModuleProps_sidemenu,function(val,key){
                    if(updateOffline){
                        if(val.name === 'Offline' || val.name === 'Offline*'){
                          val.name = 'Offline*';
                        }
                    }else{
                        if(val.name === 'Offline' || val.name === 'Offline*'){
                          val.name = 'Offline';
                        }
                    }
                    if(!sharedDataService.getNetworkState()){
                        if(val.name === 'Dashboard' || val.name === 'Map' || val.name === 'Calendar'){
                            val.enable = false;
                        }
                    }else{
                        val.enable = true;
                    }
                });
            };
            var checkNewOppTable = function(){
                var dbOppCreateSuccess = function(oppCreateData){
                    if(oppCreateData.length){updateSideBarItems(true);}
                    else{updateSideBarItems(false);}
                };
                var dbOppCreateError = function(err){ updateSideBarItems(false); };
                dbService.getDetailsFromTable('stc_offlineOppCreate',[],dbOppCreateError, dbOppCreateSuccess);
            };
            var dbOppUpdateFetchSuccess = function(data){
                if(data.length){ updateSideBarItems(true); }
                else{ checkNewOppTable();}
            };
            var dbOppUpdateFetchError = function(){ checkNewOppTable(); };
            var updatableRecordsCount = Util.convertObjectToSQL({toBeUpdated:'true'});
            dbService.getDetailsFromTable('stc_opptys', updatableRecordsCount,dbOppUpdateFetchError, dbOppUpdateFetchSuccess);
            if($state.current.name === 'root.map'){
                var map = plugin.google.maps.Map.getMap(document.getElementById("map_canvas"));
                map.setClickable(false);
            }else if($state.current.name === 'root.trackerview'){
                var map = plugin.google.maps.Map.getMap(document.getElementById("trackerview_canvas"));
                map.setClickable(false);
            }
        };
        $scope.setSelectedLocation = function(){
        $rootScope.$broadcast('setSelectedLocation', []);
        };
        $scope.openChatBar = function(){
           MaterialReusables.toggleChatBar();
        };
        $scope.$on('enableUserSearch', function (event, args) {
            $scope.headerComponent.buttonProperty = [];
            $scope.headerComponent.buttonProperty.push('search');
        });
        $scope.$on('disableUserSearch', function (event, args) {
            $scope.headerComponent.buttonProperty = [];
            $scope.headerComponent.buttonProperty.push('');
        });
    };
    headerController.$inject = ['$scope','$state','$stateParams','$translate','$rootScope','MaterialReusables','sharedDataService','dbService'];
    var headerComponent = function(){
        return {
            restrict: 'E',
            replace: true,
            templateUrl: "app/shared/directives/header/header.template.html",
            controller: headerController
        }
    };
    stcApp.directive('headerComponent',headerComponent);
})();