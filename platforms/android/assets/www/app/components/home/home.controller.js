(function () {
  'use strict';
  var homeController = function ($rootScope, $scope, $state, $http, $filter, $translate, apiService, pluginService, MaterialReusables, PubnubService, sharedDataService, dbService) {
    $scope.homeModel = {
      searchBar: true,
      moduleTags: [],
      verNumber: '',
      isSyncingEvents: false,
      SyncLoaderText: '',
      isLongPressingIcon: false,
      sortableOptions: { 'disabled': true, 'ui-floating': true },
      updatePhotoSelect: false,
    };
    //var homeIconProps = mainModuleProps_dashboard;
    var homeIconProps = [];
    if (angular.isDefined(sharedDataService.getHomePreferenceArray())) {
      homeIconProps = sharedDataService.getHomePreferenceArray()
    } else {
      homeIconProps = mainModuleProps_dashboard;
    }
    //disable features which are not available in offline
    angular.forEach(homeIconProps, function (val, key) {
      if (!sharedDataService.getNetworkState()) {
        if (val.name === 'Dashboard' || val.name === 'Map' || val.name === 'Calendar') {
          val.enable = false;
        }
      } else {
        val.enable = true;
      }
    });
    $scope.homeModel.verNumber = pluginService.getAppVersion();
    $scope.chatControl = {};
    $scope.homeModel.moduleTags = homeIconProps;
    $scope.ToggleSearchBar = function () {
      $scope.homeModel.searchBar = !$scope.homeModel.searchBar;
    };
    //invoke toggle searchbar
    $scope.$on('searchClick', function (event, args) {
      $scope.ToggleSearchBar();
    });
    //disable/enable sidebar features based network
    $scope.openSideNav = function () {
      var updateSideBarItems = function (updateOffline) {
        angular.forEach(mainModuleProps_sidemenu, function (val, key) {
          if (updateOffline) {
            if (val.name === 'Offline' || val.name === 'Offline*') {
              val.name = 'Offline*';
            }
          } else {
            if (val.name === 'Offline' || val.name === 'Offline*') {
              val.name = 'Offline';
            }
          }
          if (!sharedDataService.getNetworkState()) {
            if (val.name === 'Dashboard' || val.name === 'Map' || val.name === 'Calendar') {
              val.enable = false;
            }
          } else {
            val.enable = true;
          }
        });
      };
      MaterialReusables.toggleSideBar();
      var checkNewOppTable = function () {
        var dbOppCreateSuccess = function (oppCreateData) {
          if (oppCreateData.length) { updateSideBarItems(true); }
          else { updateSideBarItems(false); }
        };
        var dbOppCreateError = function (err) { updateSideBarItems(false); };
        dbService.getDetailsFromTable('stc_offlineOppCreate', [], dbOppCreateError, dbOppCreateSuccess);
      };
      var dbOppUpdateFetchSuccess = function (data) {
        if (data.length) { updateSideBarItems(true); }
        else { checkNewOppTable(); }
      };
      var dbOppUpdateFetchError = function () { checkNewOppTable(); };
      var updatableRecordsCount = Util.convertObjectToSQL({ toBeUpdated: 'true' });
      dbService.getDetailsFromTable('stc_opptys', updatableRecordsCount, dbOppUpdateFetchError, dbOppUpdateFetchSuccess);
    };
    // return Arabic for given English word
    var getArabic = function (viewName) {
      var currentValue = '';
      switch (viewName) {
        case 'Dashboard': currentValue = "لوحة التحكم"; break;
        case 'Map': currentValue = "الخريطة"; break;
        case 'Calendar': currentValue = "التقويم"; break;
        default:////TODO
      }
      return currentValue;
    };
    //navigation to feature
    $scope.gotoView = function (viewName, selectedView, enableRouting) {
      if (!$scope.homeModel.isLongPressingIcon) {
        if (enableRouting) {
          $state.go(selectedView);
        } else {
          var currentView = '';
          if (sharedDataService.getLanguage() === 'ar_AR') {
            currentView = getArabic(viewName);
          } else {
            currentView = viewName;
          }
          MaterialReusables.showToast(currentView + ' ' + pluginService.getTranslations().noViewOff, 'warning');
        }
      }
    };
    //navigate to create opportunity
    $scope.addOpp = function () {
      $state.go('root.oppAddition', { fromView: 'root.home' });
    };
    //open chat window and refresh pubnub online users
    $scope.openChatSideNav = function () {
      if (!sharedDataService.getNetworkState()) {
        MaterialReusables.showToast('' + pluginService.getTranslations().noChatOff, 'warning');
      } else {
        PubnubService.updateAllOnlineUsers();
        MaterialReusables.toggleChatBar();
      }
    };
    //invoke chat initializations
    if (sharedDataService.getNetworkState()) {
      PubnubService.startChatProcedures();
    }
    //invoke activities api call and aprtner Id details API call while coming from login UI
    $scope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
      if (from.name === 'login') {
        if (sharedDataService.getIsManager()) {
          $rootScope.isManager = true;
        } else {
          $rootScope.isManager = false;
        }
        var updateNotificationTable = function () {
          if (device.platform === 'windows') {
            localStorage.removeItem('stc_notificationStatus');
            localStorage.setItem('stc_notificationStatus', 'false');
          } else {
            var sqlUpdateParameters = Util.convertObjectToSQL({ updateRequired: false });
            var sqlCheckParameters = Util.convertObjectToSQL({ rowid: 1 });
            dbService.updateTable('stc_notificationTime', sqlUpdateParameters, sqlCheckParameters, angular.noop, angular.noop);
          }
        };
        var fetchEvents = function () {
          var gotActivities = function (data) {
            updateNotificationTable();
            pluginService.clearAllNotifications();
            if (data.data.listOfAction !== null) {
              pluginService.createLocalNotification(data.data.listOfAction.action);
            }
          };
          var failedGettingActivities = function (error) {
          };
          var currentDate = new Date();
          var queryStartDate = currentDate.setDate(currentDate.getDate() - 1);
          var queryEndDate = currentDate.setDate(currentDate.getDate() + 12);
          var getActivityPayload = {
            'payload': {
              "searchSpec": "[STC Action Thin.Opportunity Primary Position Id]='" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND [STC Action Thin.Planned] > '" + $filter('date')(new Date(queryStartDate), 'MM/dd/yyyy HH:mm:ss') + "' AND [STC Action Thin.Planned] < '" + $filter('date')(new Date(queryEndDate), 'MM/dd/yyyy HH:mm:ss') + "'",
              "outputIntObjectName": "STC Action Thin"
            }
          };
          apiService.fetchDataFromApi('opp.activitiesList', getActivityPayload, gotActivities, failedGettingActivities);
        };
        var getAssignToList = function () {
          var assignedToPayLoad = {
            "stcDiv": "",
            "stcPartnerId": "",
            "rowId": sharedDataService.getUserDetails().PrimaryPositionId
          };
          var oppAssignedToListSuccess = function (data) {
            if (data.data.record.length !== 0) {
              if(data.data.record[0].stcPartnerId !== null){
                pluginService.watchUserPosition();
              }
              sharedDataService.saveAssignedToList(data);
             }else{
               var result = function (confirm) {
                 if (confirm) {
                  apiService.triggerAppLogout(MaterialReusables,PubnubService,$state);
                 }
               };
               MaterialReusables.showSuccessAlert('User Position Configurations Not Found!', result);
             } 
          };
          var oppAssignedToListError = function (data) {
            var result = function (confirm) {
              if (confirm) {
                apiService.triggerAppLogout(MaterialReusables,PubnubService,$state);
              }
            }
            MaterialReusables.showSuccessAlert('User Position Details Fetch Error Occured Due To ' + data.data.Message, result); 
          };
          apiService.fetchDataFromApi('opp.assignedToList', assignedToPayLoad, oppAssignedToListSuccess,oppAssignedToListError);
        };
        if (device.platform === 'windows') {
          if (localStorage.getItem('stc_notificationStatus') === 'true') {
            fetchEvents();
            getAssignToList();
          }
        } else {
          fetchEvents();
          getAssignToList();
        }
      }
    });
    var disableHomeScreenEdit = function () {
      $('.homecard-container').trigger('stopRumble');
      $scope.homeModel.sortableOptions.disabled = true;
      $scope.homeModel.isLongPressingIcon = false;
    };
    var enableHomeScreenEdit = function () {
      var callback = function (res) {
        if (res) {
          mainModuleProps_dashboard = $scope.homeModel.moduleTags;
          disableHomeScreenEdit();
        }
      };
      MaterialReusables.showHomeEditToast(callback);
      $scope.homeModel.sortableOptions.disabled = false;
      $('.homecard-container').trigger('startRumble');
    };
    $scope.handleHomeScreenEdit = function (event) {
      if (event === 'lngPress') {
        $scope.homeModel.isLongPressingIcon = true;
        $('.homemdcard').jrumble({ x: 0, y: 0, rotation: 5, speed: 30 });
      } else if (event === 'tchEnd') {
        if ($scope.homeModel.isLongPressingIcon && $scope.homeModel.sortableOptions.disabled) {
          enableHomeScreenEdit();
        }
      }
    };
    $scope.selectedPhoto = function(){
      MaterialReusables.showToast('' + pluginService.getTranslations().photoUpload, 'success');
      $scope.homeModel.updatePhotoSelect = !$scope.homeModel.updatePhotoSelect;
    };
    $scope.changePhoto = function () {
      $scope.homeModel.updatePhotoSelect = !$scope.homeModel.updatePhotoSelect;
      //MaterialReusables.showProfilePhotoOptions($scope);
    };
    $scope.updatePhoto = function () {
      MaterialReusables.showProfilePhotoOptions($scope);
    };
  };
  homeController.$inject = ['$rootScope', '$scope', '$state', '$http', '$filter', '$translate', 'apiService', 'pluginService', 'MaterialReusables', 'PubnubService', 'sharedDataService', 'dbService'];
  stcApp.controller('homeController', homeController);
})();