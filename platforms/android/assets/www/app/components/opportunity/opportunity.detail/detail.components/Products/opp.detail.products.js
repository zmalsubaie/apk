var detailProducts = function ($state, $translate, $rootScope, $timeout, sharedDataService, MaterialReusables, apiService, pluginService, dbService) {
    return {
        restrict: 'E',
        scope: {
            products: '=',
            updateoppFn: '&'
        },
        templateUrl: "app/components/opportunity/opportunity.detail/detail.components/Products/opp.detail.products.html",
        transclude: true,
        compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                    scope.productModel = {
                        rowId: '',
                        showAutoQuoteLoader: false,
                        typeSelected: '',
                        showProductList: false,
                        productListLoading: false,
                        typeProductList: [],
                        ratePlanList: [],
                        ratePlanLoading: false,
                        productView: 'list',
                        editProduct: [],
                        productsList: [],
                        productTypeList: [],
                        showProductAddLoader: false,
                        productAction: '',
                        salesStage: '',
                        name: '',
                        oppType: '',
                        autoQuoteMobileEnabled: false,
                        autoQuoteEnabled: false,
                        gsmTypeList: [],
                        gsmTypeSelected: '',
                        showGSMList: false,
                        gsmNumbersList: [],
                        accountSelected: '',
                        gsmListLoading: false,
                        gsmAssetId: '',
                        gsmIntegrationId: '',
                        gsmNumberSelected: '',
                        gsmServiceAccountId: '',
                        previousOpType: '',
                        previousAccount: '',
                        enableEdit: true,
                        opptyRevenueAmount: '',
                        preQuoteQuotedItems: 0,
                        postQuoteQuotedItems: 0,
                        productToEdit: ''
                    };
                    scope.productModel.enableEdit = scope.products.isEditable;
                    if (scope.products.allProducts !== null) {
                        scope.productModel.productsList = scope.products.allProducts.OpportunityProduct;
                    }
                    scope.productModel.previousOpType = scope.products.oppType;
                    scope.productModel.salesStage = scope.products.salesStage;
                    scope.productModel.rowId = scope.products.rowId;
                    scope.productModel.name = scope.products.name;
                    scope.productModel.productTypeList = productTypeList;
                    scope.productModel.oppType = scope.products.oppType;
                    scope.productModel.gsmTypeList = gsmTypeList;
                    scope.productModel.accountSelected = scope.products.accountId;
                    scope.productModel.previousAccount = scope.products.accountId;
                    if (scope.products.revenueAmount !== null && scope.products.revenueAmount !== '') {
                        scope.productModel.opptyRevenueAmount = scope.products.revenueAmount;
                    } else {
                        scope.productModel.opptyRevenueAmount = '0';
                    }
                    scope.showProductSlideIndicator = true;
                    var dbUpdateParams = { index: 0, param: '' };
                },
                post: function postLink(scope, iElement, iAttrs, controller) {
                    //products list view on load of products tab
                    scope.$on('Products', function (event, data) {
                        scope.productModel.productView = 'list';
                    });
                    //set the selected acount for fetching GSM Assets for mobile products
                    scope.$on('pushAcount', function (event, args) {
                        scope.productModel.accountSelected = args.accountObj;
                    });
                    //set oppty type selected for quote functionalities
                    scope.$on('pushOpType', function (event, args) {
                        scope.productModel.oppType = args.prodObj;
                    });
                    //product item swipe left
                    scope.onSwipeLeft = function (ev, context) {
                        if (scope.productModel.enableEdit) {
                            context.showProductSlideIndicator = true;
                            $('.pdtItem' + ev).removeClass('slideToRight');
                            $('.pdtItem' + ev).addClass('slideToLeft');
                        }
                    };
                    //product item swipe right
                    scope.onSwipeRight = function (ev, product, context) {
                        if (scope.productModel.enableEdit) {
                            if (product.quoteNumber2 === null) {
                                context.showProductSlideIndicator = false;
                                $('#productSlider-' + ev).css('height', $('#productBlock-' + ev).height());
                                $('.pdtItem' + ev).removeClass('slideToLeft');
                                $('.pdtItem' + ev).addClass('slideToRight');
                            }
                        }
                    };
                    //show/hide quote and quote mobile buttons in UI
                    var autoQuoteMobileEnable = function () {
                        scope.productModel.autoQuoteMobileEnabled = false;
                        scope.productModel.autoQuoteEnabled = false;
                        if (scope.productModel.oppType === 'Upgrade') {
                            angular.forEach(scope.productModel.productsList, function (value, key) {
                                if (value.sTCServiceType2 === 'Mobile' && value.quoteNumber2 === null) {
                                    scope.productModel.autoQuoteMobileEnabled = true;
                                }
                            });
                        } else {
                            scope.productModel.autoQuoteMobileEnabled = false;
                        }
                        angular.forEach(scope.productModel.productsList, function (value, key) {
                            if (value.quoteNumber2 === null && value.sTCServiceType2 !== 'Mobile' && scope.productModel.oppType === 'Upgrade') {
                                scope.productModel.autoQuoteEnabled = true;
                            }
                            if (value.quoteNumber2 === null && scope.productModel.oppType === 'New Sale') {
                                scope.productModel.autoQuoteEnabled = true;
                            }
                        });
                    };
                    autoQuoteMobileEnable();
                    //check non moibile products quoted or not
                    var nonMobileProductsQuoted = function () {
                        var newProducts = false;
                        angular.forEach(scope.productModel.productsList, function (value, key) {
                            if (scope.productModel.oppType === 'New Sale') {
                                if (value.quoteNumber2 === null) {
                                    newProducts = true;
                                }
                            } else {
                                if (value.sTCServiceType2 !== 'Mobile' && value.quoteNumber2 === null) {
                                    newProducts = true;
                                }
                            }
                        });
                        return newProducts;
                    };
                    //check moibile products quoted or not
                    var mobileProductsQuoted = function () {
                        var newProducts = false;
                        angular.forEach(scope.productModel.productsList, function (value, key) {
                            if (value.sTCServiceType2 === 'Mobile' && value.quoteNumber2 === null) {
                                newProducts = true;
                            }
                        });
                        return newProducts;
                    };
                    //report update api call
                    var reportActionCall = function (rowId, diffRevenueAmount, revenueAmount) {
                        var reportSuccess = function (data) {
                            scope.productModel.preQuoteQuotedItems = 0;
                            scope.productModel.postQuoteQuotedItems = 0;
                            scope.productModel.opptyRevenueAmount = revenueAmount;
                        };
                        var reportError = function (data) {
                            scope.productModel.preQuoteQuotedItems = 0;
                            scope.productModel.postQuoteQuotedItems = 0;
                            scope.productModel.opptyRevenueAmount = revenueAmount;
                        };
                        var reportData = '';
                        if (revenueAmount !== '') {
                            reportData = {
                                "opportunityId": rowId,
                                "action": 'UPD_REVENUE',
                                "totalRevenue": "" + diffRevenueAmount + ""
                            };
                        } else {
                            var noOfQuotesCreated = scope.productModel.postQuoteQuotedItems - scope.productModel.preQuoteQuotedItems;
                            reportData = {
                                "opportunityId": rowId,
                                "action": 'UPD_QUOTES',
                                "quotesCreated": '' + noOfQuotesCreated
                            };
                        }
                        var reportPayload = Util.reportActionPayload(reportData);
                        apiService.fetchDataFromApi('opp.reportaction', reportPayload, reportSuccess, reportError);
                    };
                    //automatic quote api call
                    scope.initiateAutoQuote = function (event) {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                        } else {
                            angular.forEach(scope.productModel.productsList, function (val, key) {
                                if (val.quoteNumber2 !== null) {
                                    scope.productModel.preQuoteQuotedItems++;
                                }
                            });
                            var result = function (confirm) {
                                if (confirm) {
                                    if (nonMobileProductsQuoted()) {
                                        var autoquotePayload = {
                                            'payload': {
                                                'processName': 'STC Auto Quote Inbound Call From LMS Mobile App',
                                                'rowId': scope.productModel.rowId
                                            }
                                        }
                                        var autoquoteSuccess = function (data) {
                                            if (data.data.errorMsg === 'Success') {
                                                scope.productModel.productsList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityProduct.OpportunityProduct;
                                                angular.forEach(scope.productModel.productsList, function (val, key) {
                                                    if (val.quoteNumber2 !== null) {
                                                        scope.productModel.postQuoteQuotedItems++;
                                                    }
                                                });
                                                $rootScope.$broadcast('pushQuote', { quoteObj: data.data.listOfStcOpportunity.opportunity[0].ListOfQuote2.Quote2 });
                                                autoQuoteMobileEnable();
                                                $timeout(function () {
                                                    MaterialReusables.showToast('' + pluginService.getTranslations().quoCreate, 'success');
                                                    scope.productModel.showAutoQuoteLoader = false;
                                                }, 100);
                                                reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId, '', '');
                                            } else {
                                                scope.productModel.showAutoQuoteLoader = false;
                                                Util.throwError(data.data.errorMsg, MaterialReusables);
                                            }
                                        };
                                        var autoquoteError = function (data) {
                                            scope.productModel.showAutoQuoteLoader = false;
                                            Util.throwError(data.data.Message, MaterialReusables);
                                        };
                                        scope.productModel.showAutoQuoteLoader = true;
                                        apiService.fetchDataFromApi('opp.autoquotewf', autoquotePayload, autoquoteSuccess, autoquoteError);
                                    } else {
                                        MaterialReusables.showToast('' + pluginService.getTranslations().alreQuoted, 'warning');
                                    }
                                }
                            }
                            MaterialReusables.showConfirmDialog(event, '' + pluginService.getTranslations().autoQuote, result);
                        }
                    };
                    //automatic quote mobile api call
                    scope.initiateAutoQuoteMobile = function (event) {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                        } else {
                            angular.forEach(scope.productModel.productsList, function (val, key) {
                                if (val.quoteNumber2 !== null) {
                                    scope.productModel.preQuoteQuotedItems++;
                                }
                            });
                            if (sharedDataService.getQuoteMobileAllowed()) {
                                var result = function (confirm) {
                                    if (confirm) {
                                        if (mobileProductsQuoted()) {
                                            var autoquoteMobileSuccess = function (data) {
                                                if (data.data.errorMsg === 'Success') {
                                                    scope.productModel.productsList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityProduct.OpportunityProduct;
                                                    angular.forEach(scope.productModel.productsList, function (val, key) {
                                                        if (val.quoteNumber2 !== null) {
                                                            scope.productModel.postQuoteQuotedItems++;
                                                        }
                                                    });
                                                    $rootScope.$broadcast('pushQuote', { quoteObj: data.data.listOfStcOpportunity.opportunity[0].ListOfQuote2.Quote2 });
                                                    autoQuoteMobileEnable();
                                                    $timeout(function () {
                                                        MaterialReusables.showToast('' + pluginService.getTranslations().quoMobCre, 'success');
                                                        scope.productModel.showAutoQuoteLoader = false;
                                                    }, 100);
                                                    reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId, '', '');
                                                } else {
                                                    scope.productModel.showAutoQuoteLoader = false;
                                                    Util.throwError(data.data.errorMsg, MaterialReusables);
                                                }
                                            };
                                            var autoquoteMobileError = function (data) {
                                                scope.productModel.showAutoQuoteLoader = false;
                                                Util.throwError(data.data.Message, MaterialReusables);
                                            };
                                            var autoquoteMobilePayLoad = {
                                                'payload': {
                                                    'processName': 'STC AutoQuoteMobile LMS Mobile App Inbound WF',
                                                    'rowId': scope.productModel.rowId
                                                }
                                            }
                                            scope.productModel.showAutoQuoteLoader = true;
                                            apiService.fetchDataFromApi('opp.autoquotewf', autoquoteMobilePayLoad, autoquoteMobileSuccess, autoquoteMobileError);
                                        } else {
                                            MaterialReusables.showToast('' + pluginService.getTranslations().alreQuoted, 'warning');
                                        }
                                    }
                                }
                                MaterialReusables.showConfirmDialog(event, '' + pluginService.getTranslations().quoteMobile, result);
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().noPrivilegeQuote, 'error');
                            }
                        }
                    };
                    //show products popup and api call
                    scope.showProductPopup = function () {
                        if (!sharedDataService.getNetworkState()) {
                            var oppDBFetchSuccess = function (data) {
                                var tempProductList = [];
                                angular.forEach(data, function (value, key) {
                                    var productItem = JSON.parse(value.products);
                                    if (productItem.stcSystemName === scope.productModel.typeSelected) {
                                        tempProductList.push(productItem);
                                    }
                                });
                                $timeout(function () {
                                    scope.productModel.typeProductList = tempProductList;
                                });
                            };
                            var oppDBFetchError = function () {
                                //scope.productModel.showProductList = false;
                                MaterialReusables.hideDialog();
                                MaterialReusables.showToast('' + pluginService.getTranslations().prdctsNotFound, 'error');
                            }
                            //scope.productModel.showProductList = true;
                            MaterialReusables.showCustomPopup(scope,'app/shared/layout/productsListPopupTemplate.html');
                            scope.productQuery = '';
                            scope.productModel.typeProductList = [];
                            dbService.getDetailsFromTable('stc_products', [], oppDBFetchError, oppDBFetchSuccess);
                        } else {
                            if (scope.productModel.typeSelected !== null && scope.productModel.typeSelected !== "") {
                                //scope.productModel.showProductList = true;
                                MaterialReusables.showCustomPopup(scope,'app/shared/layout/productsListPopupTemplate.html');
                                scope.productModel.ratePlanList = [];
                                if (scope.productModel.typeProductList.length === 0) {
                                    scope.productQuery = '';
                                    scope.productModel.productListLoading = true;
                                    scope.productModel.typeProductList = [];
                                    var searchProductPayLoad = {
                                        "searchSpec": "[Internal Product.STC Business Set Flag] = 'Y' AND [Internal Product.STC System Name]= '" + scope.productModel.typeSelected + "' AND [Internal Product.Service Type] = 'Root'",
                                        "queryByUserKey": "",
                                        "messageId": "",
                                        "busObjCacheSize": "",
                                        "outputIntObjectName": "STC LMS Mobile App Product Search",
                                        "primaryRowId": "",
                                        "siebelMessage": ""
                                    };
                                    var searchProductSuccess = function (data) {
                                        scope.productModel.productListLoading = false;
                                        if (data.data.ListOfStcLmsMobileAppProductSearch != null) {
                                            angular.forEach(data.data.ListOfStcLmsMobileAppProductSearch.InternalProduct, function (value, key) {
                                                scope.productModel.typeProductList.push(value);
                                            });
                                        } else {
                                            MaterialReusables.showToast('' + pluginService.getTranslations().noProds, 'error');
                                        }
                                    };
                                    var searchProductError = function (data) {
                                        scope.closeProductTab();
                                        scope.productModel.productListLoading = false;
                                        Util.throwError(data.data.Message, MaterialReusables);
                                    };
                                    apiService.fetchDataFromApi('opp.productWithRateplans', searchProductPayLoad, searchProductSuccess, searchProductError);
                                }
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().selProType, 'error');
                            }
                        }
                    };
                    //rate plan change set params
                    scope.oppRatePlanTypeChange = function (data) {
                        scope.productModel.editProduct.stcRatePlanAliasName = data.name;
                        scope.productModel.editProduct.ratePlanId = data.id;
                    };
                    //close products popup
                    scope.closeProductTab = function () {
                        //scope.productModel.showProductList = false;
                        MaterialReusables.hideDialog();
                    };
                    //fetch rate plans for the selected product
                    var ratePlanFetch = function (productSelected) {
                        if (sharedDataService.getNetworkState()) {
                            var rateplanSuccess = function (data) {
                                scope.productModel.ratePlanLoading = false;
                                if (angular.isDefined(data.data.ListOfStcLmsMobileAppProductSearch.InternalProduct[0].ratePlans)) {
                                    scope.productModel.ratePlanList = data.data.ListOfStcLmsMobileAppProductSearch.InternalProduct[0].ratePlans;
                                } else {
                                    MaterialReusables.showToast('' + pluginService.getTranslations().noRatePlan, 'warning');
                                }
                            };
                            var rateplanError = function (data) {
                                Util.throwError(data.data.Message, MaterialReusables);
                                scope.productModel.ratePlanLoading = false;
                            }
                            var rateplanPayLoad = {
                                "searchSpec": "[Internal Product.Id]= '" + productSelected.productId + "'",
                                "queryByUserKey": "",
                                "messageId": "",
                                "busObjCacheSize": "",
                                "outputIntObjectName": "STC LMS Mobile App Product Search",
                                "primaryRowId": "",
                                "siebelMessage": ""
                            };
                            scope.ratePlanIndex = '';
                            scope.productModel.ratePlanLoading = true;
                            apiService.fetchDataFromApi('opp.productWithRateplans', rateplanPayLoad, rateplanSuccess, rateplanError);
                        }
                    };
                    //set/reset values on product select
                    scope.onTypeProductClick = function (data) {
                        if (!sharedDataService.getNetworkState()) {
                            //scope.productModel.showProductList = false;
                            MaterialReusables.hideDialog();
                            scope.productModel.editProduct.productAliasName = data.aliasName;
                            scope.productModel.editProduct.productId = data.id;
                            scope.productSelected = data.name;
                            scope.productModel.ratePlanList = data.ratePlans;
                        } else {
                            //scope.productModel.showProductList = false;
                            MaterialReusables.hideDialog();
                            scope.productModel.editProduct.productAliasName = data.aliasName;
                            scope.productModel.editProduct.productId = data.id;
                            scope.productSelected = data.name;
                            if (angular.isDefined(data.ratePlans)) {
                                scope.productModel.ratePlanList = data.ratePlans;
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().noRatePlan, 'warning');
                            }
                        }
                    };
                    //qty field validation
                    scope.productQtyChange = function (newValue, index) {
                        scope.productModel.editProduct.productQuantity = newValue.replace(/[^0-9.-]/g, '').substring(0, 10);
                    };
                    //create product UI view,set params
                    scope.doAddProduct = function () {
                        if (scope.productModel.productsList.length === 0 && scope.productModel.oppType === 'Upgrade' && (scope.productModel.oppType !== scope.productModel.previousOpType)) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().opTypeSave, 'warning');
                        } else if (scope.productModel.oppType === 'Upgrade' && (scope.productModel.accountSelected !== scope.productModel.previousAccount)) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().accountTypSave, 'warning');
                        } else {
                            scope.productModel.ratePlanLoading = false;
                            scope.productModel.showProductAddLoader = false;
                            scope.productModel.typeProductList = [];
                            scope.productModel.editProduct = [];
                            scope.productModel.typeSelected = '';
                            scope.productModel.gsmTypeSelected = '';
                            scope.productModel.productView = 'create';
                            scope.productModel.productAction = 'create';
                        }
                    };
                    //set/reset values on product type select/change
                    scope.oppProductTypeChange = function (data) {
                        scope.productModel.editProduct.sTCServiceType2 = data.name;
                        scope.productModel.typeSelected = data.type;
                        scope.productModel.typeProductList = [];
                        scope.productModel.ratePlanList = [];
                        scope.productModel.gsmNumbersList = [];
                        scope.productModel.gsmTypeSelected = '';
                        scope.productModel.editProduct.productAliasName = '';
                        scope.productModel.editProduct.sTCRatePlanAliasName = '';
                        scope.productModel.editProduct.productQuantity = '';
                        scope.productModel.editProduct.productId = null;
                        scope.productModel.editProduct.ratePlanId = null;
                        scope.productModel.gsmAssetId = '';
                        scope.productModel.gsmIntegrationId = '';
                        scope.productModel.gsmNumberSelected = '';
                        scope.productModel.gsmServiceAccountId = '';
                    };
                    //create/update product - input field validations
                    var validateOppProductCreation = function () {
                        var validation = '';
                        if (angular.isUndefined(scope.productModel.editProduct.sTCServiceType2) || scope.productModel.editProduct.sTCServiceType2 === "") {
                            validation = '' + pluginService.getTranslations().enterProdType;
                        } else if ((scope.productModel.oppType === 'Upgrade' && scope.productModel.editProduct.sTCServiceType2 === 'Mobile') && (angular.isUndefined(scope.productModel.gsmTypeSelected) || scope.productModel.gsmTypeSelected === "")) {
                            validation = '' + pluginService.getTranslations().gsmConnType;
                        } else if ((scope.productModel.oppType === 'Upgrade' && scope.productModel.editProduct.sTCServiceType2 === 'Mobile') && (angular.isUndefined(scope.productModel.gsmNumberSelected) || scope.productModel.gsmNumberSelected === "")) {
                            validation = '' + pluginService.getTranslations().gsmNumber;
                        } else if (angular.isUndefined(scope.productModel.editProduct.productAliasName) || scope.productModel.editProduct.productAliasName === "") {
                            validation = '' + pluginService.getTranslations().enterProdName;
                        } else if ((scope.productModel.editProduct.sTCServiceType2 !== 'Data') && (angular.isUndefined(scope.productModel.editProduct.sTCRatePlanAliasName) || scope.productModel.editProduct.sTCRatePlanAliasName === "")) {
                            validation = '' + pluginService.getTranslations().enterProdRatePlan
                        } else if (angular.isUndefined(scope.productModel.editProduct.productQuantity) || scope.productModel.editProduct.productQuantity === "") {
                            validation = '' + pluginService.getTranslations().enterProdQuantity;
                        } else if ((scope.productModel.editProduct.productQuantity !== null) && (parseInt(scope.productModel.editProduct.productQuantity, 10) < 1)) {
                            validation = '' + pluginService.getTranslations().validQty;
                        }
                        return validation;
                    };
                    //report update api call
                    var reportActedCall = function (rowId) {
                        var reportData = {
                            "opportunityId": rowId,
                            "action": 'UPD_ACTED_LEADS',
                            "actedLeads": '1'
                        };
                        var reportPayload = Util.reportActionPayload(reportData);
                        apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
                    };
                    //product update success
                    var productUpdateSuccess = function (data) {
                        if (data.data.errorMessage === 'Success') {
                            scope.productModel.productsList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityProduct.OpportunityProduct;
                            scope.productModel.showProductAddLoader = false;
                            MaterialReusables.showToast('' + pluginService.getTranslations().productUpdated, 'success');
                            scope.productModel.productView = 'list';
                            autoQuoteMobileEnable();
                            if (scope.productModel.opptyRevenueAmount !== data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount) {
                                var amount = 0;
                                amount = parseInt(data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount, 10) - parseInt(scope.productModel.opptyRevenueAmount, 10);
                                reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId, amount, data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount);
                            }
                            reportActedCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                        } else {
                            scope.productModel.showProductAddLoader = false;
                            Util.throwError(data.data.errorMessage, MaterialReusables);
                        }
                        $('.defaultPditem').removeClass('slideToRight');
                        $('.defaultPditem').addClass('slideToLeft');
                    };
                    //product update error
                    var productUpdateError = function (data) {
                        scope.productModel.showProductAddLoader = false;
                        Util.throwError(data.data.Message, MaterialReusables);
                    };
                    //product create success
                    var productCreateSuccess = function (data) {
                        if (data.data.errorMessage === 'Success') {
                            scope.productModel.productsList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityProduct.OpportunityProduct;
                            scope.productModel.showProductAddLoader = false;
                            MaterialReusables.showToast('' + pluginService.getTranslations().productCreated, 'success');
                            scope.productModel.productView = 'list';
                            $rootScope.$broadcast('pushProduct', { prodObj: data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityProduct.OpportunityProduct });
                            autoQuoteMobileEnable();
                            if (scope.productModel.opptyRevenueAmount !== data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount) {
                                var amount = 0;
                                amount = parseInt(data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount, 10) - parseInt(scope.productModel.opptyRevenueAmount, 10);
                                reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId, amount, data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount);
                            }
                            reportActedCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                        } else {
                            scope.productModel.showProductAddLoader = false;
                            Util.throwError(data.data.errorMessage, MaterialReusables);
                        }
                        $('.defaultPditem').removeClass('slideToRight');
                        $('.defaultPditem').addClass('slideToLeft');
                    };//product create error
                    var productCreateError = function (data) {
                        scope.productModel.showProductAddLoader = false;
                        Util.throwError(data.data.Message, MaterialReusables);
                    };
                    //cancel product creation
                    scope.cancelProductEntry = function () {
                        $('.defaultPditem').removeClass('slideToRight');
                        $('.defaultPditem').addClass('slideToLeft');
                        scope.productModel.showProductAddLoader = false;
                        scope.productModel.editProduct = [];
                        scope.productModel.productView = 'list';
                    };
                    //update offline product create/update
                    var checkandUpdateOfflineProducts = function (prodObj, actionType) {
                        scope.cancelProductEntry();
                        var dbCheckParams = {};
                        var productDBPushStatus = function (result, params) {
                            if (actionType !== 'create') {
                                scope.productModel.productsList.splice(dbUpdateParams.index, 1);
                            }
                            if (actionType !== 'delete') {
                                var pushParams = params.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0];
                                pushParams.quoteNumber2 = null;
                                pushParams.sTCServiceType2 = pushParams.stcServiceType2;
                                pushParams.sTCRatePlanAliasName = pushParams.stcRatePlanAliasName;
                                pushParams.sTCQuoteComments = pushParams.stcQuoteComments;
                                scope.productModel.productsList.push(pushParams);
                            }
                        };
                        if (actionType !== 'create') {
                            dbCheckParams = { params: prodObj, lastParams: dbUpdateParams.param, type: 'Product', action: actionType, callback: productDBPushStatus };
                        } else {
                            dbCheckParams = { params: prodObj, lastParams: '', type: 'Product', action: actionType, callback: productDBPushStatus };
                        }
                        scope.updateoppFn(dbCheckParams);
                    };
                    //push products for offline
                    var pushProductToDB = function (payload) {
                        checkandUpdateOfflineProducts(payload, scope.productModel.productAction)
                    };
                    //invoke lead process API for posting to Siebel
                    var postProductOnline = function (payload) {
                        if (scope.productModel.productAction === 'edit') {
                            apiService.fetchDataFromApi('opp.leadProcess', payload, productUpdateSuccess, productUpdateError);
                        } else if (scope.productModel.productAction === 'create') {
                            apiService.fetchDataFromApi('opp.leadProcess', payload, productCreateSuccess, productCreateError);
                        } else if (scope.productModel.productAction === 'delete') {
                            apiService.fetchDataFromApi('opp.leadProcess', payload, scope.oppProductDeleteSuccess, scope.oppProductDeleteError);
                        }
                    };
                    //check for changes durign edit
                    var checkChanges = function (oldProduct, newProduct) {
                        var old = angular.copy(oldProduct);
                        var edited = angular.copy(newProduct);
                        delete old.operation;
                        delete old.$$hashKey;
                        var old = {
                            "productAliasName": old.productAliasName ? old.productAliasName : "",
                            "productId": old.productId ? old.productId : "",
                            "productQuantity": old.productQuantity ? old.productQuantity : "",
                            "sTCServiceType2": old.sTCServiceType2 ? old.sTCServiceType2 : "",
                            "sTCRatePlanAliasName": old.sTCRatePlanAliasName ? old.sTCRatePlanAliasName : "",
                            "sTCRatePlanId": old.sTCRatePlanId ? old.sTCRatePlanId : "",
                            "sTCQuoteComments": old.sTCQuoteComments ? old.sTCQuoteComments : "",
                            "id": old.id ? old.id : "",
                            "stcServiceAccountId": old.stcServiceAccountId ? old.stcServiceAccountId : "",
                            "stcGSMAssetId": old.stcGSMAssetId ? old.stcGSMAssetId : "",
                            "stcGSMIntegrationId": old.stcGSMIntegrationId ? old.stcGSMIntegrationId : "",
                            "stcGSMPhoneNumber": old.stcGSMPhoneNumber ? old.stcGSMPhoneNumber : ""
                        }
                        var objectsAreSame = true;
                        for (var propertyName in old) {
                            if (old[propertyName] !== edited[propertyName]) {
                                objectsAreSame = false;
                                break;
                            }
                        }
                        return objectsAreSame;
                    };
                    //create product
                    var addProduct = function () {
                        var changesExists;
                        if (scope.productModel.productAction == 'edit') {
                            var productPayLoad = scope.productModel.editProduct;
                            var editedData = {
                                "productAliasName": productPayLoad.productAliasName ? productPayLoad.productAliasName : "",
                                "productId": productPayLoad.productId ? productPayLoad.productId : "",
                                "productQuantity": productPayLoad.productQuantity ? productPayLoad.productQuantity : "",
                                "sTCServiceType2": productPayLoad.sTCServiceType2 ? productPayLoad.sTCServiceType2 : "",
                                "sTCRatePlanAliasName": productPayLoad.sTCRatePlanAliasName ? productPayLoad.sTCRatePlanAliasName : "",
                                "sTCRatePlanId": productPayLoad.ratePlanId ? productPayLoad.ratePlanId : "",
                                "sTCQuoteComments": productPayLoad.sTCQuoteComments ? productPayLoad.sTCQuoteComments : "",
                                "id": productPayLoad.id ? productPayLoad.id : "",
                                "stcServiceAccountId": scope.productModel.gsmServiceAccountId ? scope.productModel.gsmServiceAccountId : "",
                                "stcGSMAssetId": scope.productModel.gsmAssetId ? scope.productModel.gsmAssetId : "",
                                "stcGSMIntegrationId": scope.productModel.gsmIntegrationId ? scope.productModel.gsmIntegrationId : "",
                                "stcGSMPhoneNumber": scope.productModel.gsmNumberSelected ? scope.productModel.gsmNumberSelected : ""
                            }
                            changesExists = checkChanges(scope.productModel.productToEdit, editedData);
                        } else {
                            changesExists = false;
                        }
                        if (!changesExists) {
                            scope.productModel.showProductAddLoader = true;
                            var productPayLoad = scope.productModel.editProduct;
                            if (scope.productModel.productAction == 'edit') {
                                var headerData = {
                                    "operation": "skipnode",
                                    "rowId": scope.productModel.rowId
                                };
                                var productData = {
                                    "productAliasName": productPayLoad.productAliasName,
                                    "productId": productPayLoad.productId,
                                    "productQuantity": productPayLoad.productQuantity,
                                    "sTCServiceType2": productPayLoad.sTCServiceType2,
                                    "sTCRatePlanAliasName": productPayLoad.sTCRatePlanAliasName,
                                    "sTCRatePlanId": productPayLoad.ratePlanId,
                                    "sTCQuoteComments": productPayLoad.sTCQuoteComments,
                                    "id": productPayLoad.id,
                                    "stcServiceAccountId": scope.productModel.gsmServiceAccountId,
                                    "stcGSMAssetId": scope.productModel.gsmAssetId,
                                    "stcGSMIntegrationId": scope.productModel.gsmIntegrationId,
                                    "stcGSMPhoneNumber": scope.productModel.gsmNumberSelected,
                                    "operation": "update"
                                };
                                var productUpdatePayLoad = Util.leadProcessPayload(headerData, '', '', '', '', productData, '', '');
                                var productUpdateOfflinePayLoad = {
                                    "opptyRevenueAmount":scope.productModel.opptyRevenueAmount,
                                    "statusObject": "",
                                    "objectLevelTransactions": "",
                                    "setMinimalReturns": "",
                                    "messageId": "",
                                    "busObjCacheSize": "",
                                    "listOfStcOpportunity":
                                    {
                                        "opportunity":
                                        [{
                                            "id": scope.productModel.rowId ? scope.productModel.rowId : "",
                                            "listOfOpportunityProduct":
                                            {
                                                "opportunityProduct":
                                                [{
                                                    "stcServiceType2": productPayLoad.sTCServiceType2 ? productPayLoad.sTCServiceType2 : "",
                                                    "productQuantity": productPayLoad.productQuantity ? productPayLoad.productQuantity : "",
                                                    "opportunityName": scope.productModel.name ? scope.productModel.name : "",
                                                    "productId": productPayLoad.productId ? productPayLoad.productId : "",
                                                    "stcRatePlanAliasName": productPayLoad.sTCRatePlanAliasName ? productPayLoad.sTCRatePlanAliasName : "",
                                                    "opptyId2": scope.productModel.rowId ? scope.productModel.rowId : "",
                                                    "stcRatePlanId": productPayLoad.ratePlanId ? productPayLoad.ratePlanId : "",
                                                    "productAliasName": productPayLoad.productAliasName ? productPayLoad.productAliasName : "",
                                                    "id": productPayLoad.id ? productPayLoad.id : "",
                                                    "stcQuoteComments": productPayLoad.sTCQuoteComments ? productPayLoad.sTCQuoteComments : "",
                                                    "stcServiceAccountId": scope.productModel.gsmServiceAccountId ? scope.productModel.gsmServiceAccountId : "",
                                                    "stcGSMAssetId": scope.productModel.gsmAssetId ? scope.productModel.gsmAssetId : "",
                                                    "stcGSMIntegrationId": scope.productModel.gsmIntegrationId ? scope.productModel.gsmIntegrationId : "",
                                                    "stcGSMPhoneNumber": scope.productModel.gsmNumberSelected ? scope.productModel.gsmNumberSelected : ""
                                                }]
                                            }
                                        }]
                                    },
                                    "siebelMessage": ""
                                };
                                if (!sharedDataService.getNetworkState()) {
                                    pushProductToDB(productUpdateOfflinePayLoad);
                                } else {
                                    postProductOnline(productUpdatePayLoad);
                                }
                            } else if (scope.productModel.productAction == 'create') {
                                var headerData = {
                                    "operation": "skipnode",
                                    "rowId": scope.productModel.rowId
                                };
                                var productData = {
                                    "sTCCreatedBy": sharedDataService.getUserDetails().Id,
                                    "productAliasName": productPayLoad.productAliasName,
                                    "productId": productPayLoad.productId,
                                    "productQuantity": productPayLoad.productQuantity,
                                    "sTCServiceType2": productPayLoad.sTCServiceType2,
                                    "sTCRatePlanAliasName": productPayLoad.sTCRatePlanAliasName,
                                    "sTCRatePlanId": productPayLoad.ratePlanId,
                                    "sTCQuoteComments": productPayLoad.sTCQuoteComments,
                                    "id": "1234",
                                    "stcServiceAccountId": scope.productModel.gsmServiceAccountId,
                                    "stcGSMAssetId": scope.productModel.gsmAssetId,
                                    "stcGSMIntegrationId": scope.productModel.gsmIntegrationId,
                                    "stcGSMPhoneNumber": scope.productModel.gsmNumberSelected,
                                    "operation": "insert"
                                };
                                var productCreatePayLoad = Util.leadProcessPayload(headerData, '', '', '', '', productData, '', '');
                                var productCreateOfflinePayLoad = {
                                    "opptyRevenueAmount":scope.productModel.opptyRevenueAmount,
                                    "statusObject": "",
                                    "objectLevelTransactions": "",
                                    "setMinimalReturns": "",
                                    "messageId": "",
                                    "busObjCacheSize": "",
                                    "listOfStcOpportunity":
                                    {
                                        "opportunity":
                                        [{
                                            "id": scope.productModel.rowId ? scope.productModel.rowId : "",
                                            "listOfOpportunityProduct":
                                            {
                                                "opportunityProduct":
                                                [{
                                                    "stcServiceType2": productPayLoad.sTCServiceType2 ? productPayLoad.sTCServiceType2 : "",
                                                    "productQuantity": productPayLoad.productQuantity ? productPayLoad.productQuantity : "",
                                                    "opportunityName": scope.productModel.name ? scope.productModel.name : "",
                                                    "productId": productPayLoad.productId ? productPayLoad.productId : "",
                                                    "stcRatePlanAliasName": productPayLoad.sTCRatePlanAliasName ? productPayLoad.sTCRatePlanAliasName : "",
                                                    "opptyId2": scope.productModel.rowId ? scope.productModel.rowId : "",
                                                    "stcRatePlanId": productPayLoad.ratePlanId ? productPayLoad.ratePlanId : "",
                                                    "productAliasName": productPayLoad.productAliasName ? productPayLoad.productAliasName : "",
                                                    "id": "12345",//while create product it can't be null
                                                    "stcQuoteComments": productPayLoad.sTCQuoteComments ? productPayLoad.sTCQuoteComments : "",
                                                    "stcCreatedBy": sharedDataService.getUserDetails().Id ? sharedDataService.getUserDetails().Id : "",
                                                    "stcServiceAccountId": scope.productModel.gsmServiceAccountId ? scope.productModel.gsmServiceAccountId : "",
                                                    "stcGSMAssetId": scope.productModel.gsmAssetId ? scope.productModel.gsmAssetId : "",
                                                    "stcGSMIntegrationId": scope.productModel.gsmIntegrationId ? scope.productModel.gsmIntegrationId : "",
                                                    "stcGSMPhoneNumber": scope.productModel.gsmNumberSelected ? scope.productModel.gsmNumberSelected : ""
                                                }]
                                            }
                                        }]
                                    },
                                    "siebelMessage": ""
                                };
                                if (!sharedDataService.getNetworkState()) {
                                    pushProductToDB(productCreateOfflinePayLoad);
                                } else {
                                    postProductOnline(productCreatePayLoad);
                                }
                            }
                        } else {
                            MaterialReusables.showSuccessAlert('Nothing To Update', angular.noop);
                        }
                    };
                    //validate and continue create product
                    scope.saveProduct = function () {
                        var validationResult = validateOppProductCreation();
                        if (validationResult === '') {
                            addProduct();
                        } else {
                            MaterialReusables.showToast('' + validationResult, 'error');
                        }
                    };
                    //check item exists in array already
                    function indexOf(array, item) {
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].aliasName === item.aliasName) return i;
                        }
                        return -1;
                    }
                    //edit product item
                    scope.gotoEditProduct = function (currentindex, data) {
                        if (angular.isDefined(data.quoteNumber2) && data.quoteNumber2 !== null) {
                            $('.defaultPditem').removeClass('slideToRight');
                            $('.defaultPditem').addClass('slideToLeft');
                            MaterialReusables.showToast('' + pluginService.getTranslations().quoNoEdit, 'warning');
                        } else {
                            scope.productModel.typeProductList = [];
                            scope.productModel.ratePlanList = [];
                            scope.productEdited = angular.copy(data);
                            scope.productModel.productToEdit = data;
                            dbUpdateParams = { index: currentindex, param: data };
                            scope.productModel.editProduct = scope.productEdited;
                            if (scope.productEdited.sTCRatePlanId !== null) {
                                scope.productModel.ratePlanList.push({ 'aliasName': scope.productEdited.sTCRatePlanAliasName, 'id': scope.productEdited.sTCRatePlanId });
                                scope.productModel.editProduct.ratePlanId = scope.productEdited.sTCRatePlanId;
                                if (data.productId !== null) {
                                    ratePlanFetch(data);
                                }
                            } else {
                                scope.productModel.ratePlanList = [];
                            }
                            scope.productModel.productView = 'create';
                            scope.productModel.productAction = 'edit';
                            scope.productModel.gsmTypeSelected = scope.productEdited.stcPrePostCategory;
                            scope.productModel.gsmAssetId = scope.productEdited.stcGSMAssetId;
                            scope.productModel.gsmIntegrationId = scope.productEdited.stcGSMIntegrationId;
                            scope.productModel.gsmNumberSelected = scope.productEdited.stcGSMPhoneNumber;
                            scope.productModel.gsmServiceAccountId = scope.productEdited.stcServiceAccountId;
                            if (scope.productEdited.sTCServiceType2 === 'Mobile') {
                                scope.productModel.typeSelected = 'CRMG';
                            } else if (scope.productEdited.sTCServiceType2 === 'Data') {
                                scope.productModel.typeSelected = 'CRMD';
                            } else if (scope.productEdited.sTCServiceType2 === 'Landline') {
                                scope.productModel.typeSelected = 'ICMS';
                            } else if (scope.productEdited.sTCServiceType2 === 'Other') {
                                scope.productModel.typeSelected = 'Other';
                            }
                        }
                    };
                    //delete product item
                    scope.doDeleteProduct = function (context, ev, productId, product) {
                        if (angular.isDefined(product.quoteNumber2) && product.quoteNumber2 !== null) {
                            $('.defaultPditem').removeClass('slideToRight');
                            $('.defaultPditem').addClass('slideToLeft');
                            MaterialReusables.showToast('' + pluginService.getTranslations().quoNoDelete, 'warning');
                        } else {
                            scope.oppProductDeleteSuccess = function (data) {
                                context.showProductDeleteLoader = false;
                                if (data.data.errorMessage === 'Success') {
                                    if (data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityProduct !== null) {
                                        scope.productModel.productsList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityProduct.OpportunityProduct;
                                    } else {
                                        scope.productModel.productsList = [];
                                    }
                                    $rootScope.$broadcast('pushProduct', { prodObj: scope.productModel.productsList });
                                    MaterialReusables.showToast('' + pluginService.getTranslations().pro + ' ' + productId + ' ' + pluginService.getTranslations().deleted, 'success');
                                    autoQuoteMobileEnable();
                                    if (scope.productModel.opptyRevenueAmount !== data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount) {
                                        var amount = 0;
                                        amount = parseInt(data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount, 10) - parseInt(scope.productModel.opptyRevenueAmount, 10);
                                        reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId, amount, data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount);
                                    }
                                    reportActedCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                                } else {
                                    Util.throwError(data.data.errorMessage, MaterialReusables);
                                }
                            };
                            scope.oppProductDeleteError = function (data) {
                                context.showProductDeleteLoader = false;
                                Util.throwError(data.data.Message, MaterialReusables);
                            };
                            var result = function (confirm) {
                                if (confirm) {
                                    $('.defaultPditem').removeClass('slideToRight');
                                    $('.defaultPditem').addClass('slideToLeft');
                                    scope.productModel.productAction = 'delete';
                                    dbUpdateParams = { index: ev, param: product };
                                    context.showProductDeleteLoader = true;
                                    var headerData = {
                                        "operation": "skipnode",
                                        "rowId": scope.productModel.rowId
                                    };
                                    var productData = {
                                        "id": productId,
                                        "operation": "delete"
                                    };
                                    var productDeletePayLoad = Util.leadProcessPayload(headerData, '', '', '', '', productData, '', '');
                                    // var productDeleteOfflinePayLoad = {
                                    //     'rowId': productId
                                    // };
                                    if (!sharedDataService.getNetworkState()) {
                                        product.opptyRevenueAmount = scope.productModel.opptyRevenueAmount;
                                        pushProductToDB(product);
                                    } else {
                                        postProductOnline(productDeletePayLoad);
                                    }
                                } else {
                                    $('.defaultPditem').removeClass('slideToRight');
                                    $('.defaultPditem').addClass('slideToLeft');
                                }
                            };
                            MaterialReusables.showConfirmDialog(ev, '' + pluginService.getTranslations().productDelete_confirm, result);
                        }
                    };
                    //GSM conection type change set-reset values
                    scope.oppConnectionTypeChange = function (type) {
                        scope.productModel.gsmTypeSelected = type.name;
                        scope.productModel.ratePlanList = [];
                        scope.productModel.gsmNumbersList = [];
                        scope.productModel.editProduct.productAliasName = '';
                        scope.productModel.editProduct.sTCRatePlanAliasName = '';
                        scope.productModel.editProduct.productQuantity = '';
                        scope.productModel.editProduct.productId = null;
                        scope.productModel.editProduct.ratePlanId = null;
                        scope.productModel.gsmAssetId = '';
                        scope.productModel.gsmIntegrationId = '';
                        scope.productModel.gsmNumberSelected = '';
                        scope.productModel.gsmServiceAccountId = '';
                    };
                    //close GSM assets popup
                    scope.closeGSMTab = function () {
                        //scope.productModel.showGSMList = false;
                        MaterialReusables.hideDialog();
                    };
                    //show GSM assets popup
                    scope.showGSMPopup = function () {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                        } else {
                            if (scope.productModel.typeSelected !== null && scope.productModel.typeSelected !== "") {
                                if (scope.productModel.accountSelected !== null && scope.productModel.accountSelected !== "") {
                                    if (scope.productModel.gsmTypeSelected !== null && scope.productModel.gsmTypeSelected !== '') {
                                        //scope.productModel.showGSMList = true;
                                        MaterialReusables.showCustomPopup(scope,'app/shared/layout/gsmProductsListPopupTemplate.html');
                                        scope.productModel.gsmListLoading = true;
                                        scope.productModel.gsmNumbersList = [];
                                        var searchGSMPayLoad = {
                                            "sortSpec": "",
                                            "searchSpec": "[Asset Mgmt - Asset (Order Mgmt).Service Account Id]='" + scope.productModel.accountSelected + "' AND [Asset Mgmt - Asset (Order Mgmt).Status]='Active' AND [Asset Mgmt - Asset (Order Mgmt).Serial Number] IS NOT NULL AND [Asset Mgmt - Asset (Order Mgmt).Parent Asset Id] IS NULL",
                                            "startRowNum": "0",
                                            "pageSize": "100",
                                            "newQuery": "true",
                                            "viewMode": "",
                                            "busObjCacheSize": "",
                                            "outputIntObjectName": "STC Asset Mgmt - Asset IO"
                                        };
                                        var searchGSMSuccess = function (data) {
                                            scope.productModel.gsmListLoading = false;
                                            if (data.data.listOfStcAssetMgmt !== null) {
                                                angular.forEach(data.data.listOfStcAssetMgmt.assetMgmt, function (value, key) {
                                                    if ((value.sTCProductCategory === scope.productModel.gsmTypeSelected) || (value.sTCProductCategory === null)) {
                                                        scope.productModel.gsmNumbersList.push(value);
                                                    }
                                                });
                                                if (scope.productModel.gsmNumbersList.length === 0) {
                                                    scope.closeGSMTab();
                                                    MaterialReusables.showToast('' + pluginService.getTranslations().noGSM + ' GSM ' + scope.productModel.gsmTypeSelected + ' ' + pluginService.getTranslations().proFou, 'error');
                                                }
                                            } else {
                                                scope.closeGSMTab();
                                                MaterialReusables.showToast('' + pluginService.getTranslations().noGSM + ' GSM ' + scope.productModel.gsmTypeSelected + ' ' + pluginService.getTranslations().proFou, 'error');
                                            }
                                        };
                                        var searchGSMError = function (data) {
                                            scope.closeGSMTab();
                                            scope.productModel.gsmListLoading = false;
                                            Util.throwError(data.data.Message, MaterialReusables);
                                        };
                                        apiService.fetchDataFromApi('opp.gsmproducts', searchGSMPayLoad, searchGSMSuccess, searchGSMError);
                                    } else {
                                        MaterialReusables.showToast('' + pluginService.getTranslations().connType, 'error');
                                    }
                                } else {
                                    MaterialReusables.showToast('' + pluginService.getTranslations().noAccount, 'error');
                                }
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().pdctType, 'error');
                            }
                        }
                    };
                    //select GSM asset
                    scope.onTypeGSMClick = function (data) {
                        //scope.productModel.showGSMList = false;
                        MaterialReusables.hideDialog();
                        scope.productModel.editProduct.productAliasName = data.sTCProductDisplayName;
                        scope.productModel.editProduct.productId = data.productId;
                        scope.productModel.gsmAssetId = data.assetId;
                        scope.productModel.gsmIntegrationId = data.integrationId;
                        scope.productModel.gsmNumberSelected = data.serialNumber;
                        scope.productModel.gsmServiceAccountId = data.serviceAccountId;
                        scope.productSelected = data.productName;
                        //rate plan API call up on product selection
                        if (scope.productSelected !== null) {
                            if (!sharedDataService.getNetworkState()) {
                                MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                            } else {
                                ratePlanFetch(data);
                            }
                        } else {
                            MaterialReusables.showToast('' + pluginService.getTranslations().ratePlanLoad, 'error');
                            scope.productModel.ratePlanLoading = false;
                        }
                    };
                    //comments field additional validation
                    scope.commentsValidate = function (newValue) {
                        scope.productModel.editProduct.sTCQuoteComments = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                }
            }
        }
    }
};
opportunityModule.directive('detailProducts', detailProducts);
detailProducts.$inject = ['$state', '$translate', '$rootScope', '$timeout', 'sharedDataService', 'MaterialReusables', 'apiService', 'pluginService', 'dbService'];