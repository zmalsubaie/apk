var detailQuotes = function($state,$translate,sharedDataService,pluginService){
    return {
        restrict: 'E',
        scope: {
            quotes : '=',
            updateoppFn: '&'
        },
        templateUrl: "app/components/opportunity/opportunity.detail/detail.components/Quotes/opp.detail.quotes.html",
        transclude : true,
        compile: function compile(tElement, tAttrs, transclude) {
          return {
            pre: function preLink(scope, iElement, iAttrs, controller) {
                scope.quoteModel = {
                    quotesList:[],
                    details:'',
                    enableEdit:true
                };
                scope.quoteModel.enableEdit = scope.quotes.isEditable;
                if(scope.quotes.allQuotes!==null){
                    scope.quoteModel.quotesList = scope.quotes.allQuotes.Quote2;
                } 
                scope.quoteModel.details = scope.quotes.details;
            },
            post: function postLink(scope, iElement, iAttrs, controller) {
                //set initial quotes items on load
                scope.$on('pushQuote', function (event, args) {
                    scope.quoteModel.quotesList = args.quoteObj;
                });
                //invoke phone dialer upon on tap in mobile number
                scope.openDialer = function(contactNo){
                       if(contactNo || contactNo!==''){
                            var openedDialer = function(success){};
                            var failedtoDial = function(error){};
                            pluginService.dialNumber(contactNo,openedDialer,failedtoDial);
                       }
                };
            }
          }
        }
    }
};
opportunityModule.directive('detailQuotes',detailQuotes);
detailQuotes.$inject = ['$state','$translate','sharedDataService','pluginService'];