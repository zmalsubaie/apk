var detailNotes = function ($state, $translate, $filter, $timeout, sharedDataService, MaterialReusables, apiService, pluginService) {
    return {
        restrict: 'E',
        scope: {
            notes: '=',
            updateoppFn: '&'
        },
        templateUrl: "app/components/opportunity/opportunity.detail/detail.components/Notes/opp.detail.notes.html",
        transclude: true,
        compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                    scope.notesModel = {
                        rowId: '',
                        notesList: [],
                        noteTypeList: [],
                        noteAddRequestParams: [],
                        noteView: 'list',
                        noteAction: '',
                        showNoteAddLoader: false,
                        salesStage: '',
                        enableEdit: true,
                        showNoteDeleteLoader: false,
                        noteDeleteRequestId: '',
                        noteDeleteRequestIndex: '',
                        noteToEdit: ''
                    };
                    scope.notesModel.enableEdit = scope.notes.isEditable;
                    if (scope.notes.allNotes !== null) {
                        scope.notesModel.notesList = scope.notes.allNotes.opportunityNote;
                    }
                    scope.notesModel.noteTypeList = noteTypeList;
                    scope.notesModel.salesStage = scope.notes.salesStage;
                    scope.notesModel.rowId = scope.notes.rowId;
                    var dbUpdateParams = { index: 0, param: '' };
                },
                post: function postLink(scope, iElement, iAttrs, controller) {
                    if (scope.notesModel.notesList != null) {
                        scope.notesModel.notesList = convertDates(scope.notesModel.notesList);
                    }
                    //load notes tile view initially
                    scope.$on('Notes', function (event, data) {
                        scope.notesModel.noteView = 'list';
                        scope.notesModel.noteAddRequestParams = [];
                        scope.notesModel.noteAction = '';
                    });
                    //create note
                    scope.doCreateNote = function () {
                        scope.notesModel.noteAddRequestParams = [];
                        scope.notesModel.noteAddRequestParams.sTCCreatedByLogin = sharedDataService.getUserDetails().loginName;
                        scope.notesModel.noteAddRequestParams.createdDate = moment().format("DD/MM/YYYY HH:mm:ss");
                        scope.notesModel.noteView = 'create';
                        scope.notesModel.noteAction = 'create';
                    };
                    //report update api call
                    var reportActionCall = function (rowId) {
                        var reportData = {
                            "opportunityId": rowId,
                            "action": 'UPD_ACTED_LEADS',
                            "actedLeads": '1'
                        };
                        var reportPayload = Util.reportActionPayload(reportData);
                        apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
                    };
                    //note create API call - success
                    var oppNoteAddSuccess = function (data) {
                        if (data.data.errorMessage === 'Success') {
                            scope.notesModel.notesList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityNote.opportunityNote;
                            MaterialReusables.showToast('' + pluginService.getTranslations().addedNote, 'success');
                            scope.notesModel.noteView = 'list';
                            scope.notesModel.showNoteAddLoader = false;
                            reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                        } else {
                            scope.notesModel.showNoteAddLoader = false;
                            MaterialReusables.showToast('' + pluginService.getTranslations().notCreaFail, 'error');
                        }
                    };
                    //note update API call - success
                    var oppNoteUpdateSuccess = function (data) {
                        if (data.data.errorMessage === 'Success') {
                            scope.notesModel.notesList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityNote.opportunityNote;
                            MaterialReusables.showToast('' + pluginService.getTranslations().noteUpdate, 'success');
                            scope.notesModel.noteView = 'list';
                            scope.notesModel.showNoteAddLoader = false;
                            reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                        } else {
                            scope.notesModel.showNoteAddLoader = false;
                            MaterialReusables.showToast('' + pluginService.getTranslations().notUpFail, 'error');
                        }
                    };
                    //note create/update error
                    var oppNoteAddError = function (data) {
                        scope.notesModel.showNoteAddLoader = false;
                        Util.throwError(data.data.Message, MaterialReusables);
                    }
                    //validate input field values
                    var validateOppNoteCreation = function () {
                        var validation = '';
                        if (angular.isUndefined(scope.notesModel.noteAddRequestParams.noteType)) {
                            validation = '' + pluginService.getTranslations().enterNoteType;
                        }
                        return validation;
                    };
                    //update notes in offline
                    var checkandUpdateOfflineNotes = function (noteObj, actionType) {
                        scope.cancelNoteEntry();
                        var dbCheckParams = {};
                        var noteDBPushStatus = function (result, params) {
                            if (actionType !== 'create') {
                                scope.notesModel.notesList.splice(dbUpdateParams.index, 1);
                            }
                            if (actionType !== 'delete') {
                                params.sTCCreatedByLogin = sharedDataService.getUserDetails().loginName;
                                params.createdDate = new Date();
                                scope.notesModel.notesList.push(convertDates([params])[0]);
                            }
                        };
                        if (actionType !== 'create') {
                            dbCheckParams = { params: noteObj, lastParams: dbUpdateParams.param, type: 'Note', action: actionType, callback: noteDBPushStatus };
                        } else {
                            dbCheckParams = { params: noteObj, lastParams: '', type: 'Note', action: actionType, callback: noteDBPushStatus };
                        }
                        scope.updateoppFn(dbCheckParams);
                    };
                    //push data to local DB
                    var pushNoteToDB = function (payload) {
                        checkandUpdateOfflineNotes(payload, scope.notesModel.noteAction)
                    };
                    //note delete API call - success
                    var oppNoteDeleteSuccess = function (data) {
                        scope.notesModel.showNoteDeleteLoader = false;
                        scope.notesModel.notesList[scope.notesModel.noteDeleteRequestIndex].showNoteDeleteLoader = false;
                        if (data.data.errorMessage === 'Success') {
                            if (data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityNote !== null) {
                                scope.notesModel.notesList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityNote.opportunityNote;
                            } else {
                                scope.notesModel.notesList = [];
                            }
                            MaterialReusables.showToast('' + pluginService.getTranslations().note + ' ' + scope.notesModel.noteDeleteRequestId + ' ' + pluginService.getTranslations().deleted, 'success');
                            reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                        } else {
                            MaterialReusables.showToast('' + pluginService.getTranslations().notDelFail, 'error');
                        }
                    };
                    //note delete API call - error
                    var oppNoteDeleteError = function (data) {
                        scope.notesModel.showNoteDeleteLoader = false;
                        scope.notesModel.notesList[scope.notesModel.noteDeleteRequestIndex].showNoteDeleteLoader = false;
                        Util.throwError(data.data.Message, MaterialReusables);
                    };
                    //note delete api call
                    scope.doDeleteNote = function (context, ev, note) {
                        var result = function (confirm) {
                            if (confirm) {
                                dbUpdateParams = { index: ev, param: note };
                                scope.notesModel.noteAction = 'delete';
                                scope.notesModel.notesList[ev].showNoteDeleteLoader = true;
                                scope.notesModel.noteDeleteRequestIndex = ev;
                                var headerData = {
                                    "operation": "skipnode",
                                    "rowId": scope.notesModel.rowId
                                };
                                var noteData = {
                                    "id": note.id,
                                    "operation": "delete"
                                };
                                var noteDeletePayLoad = Util.leadProcessPayload(headerData, '', '', noteData, '', '', '', '');
                                scope.notesModel.noteDeleteRequestId = note.id;
                                if (!sharedDataService.getNetworkState()) {
                                    pushNoteToDB(note);
                                } else {
                                    postNoteOnline(noteDeletePayLoad);
                                }
                            }
                        };
                        MaterialReusables.showConfirmDialog(ev, '' + pluginService.getTranslations().deleteNoteConfirm, result);
                    };
                    //note - create/update/delete API calls to siebel
                    var postNoteOnline = function (payload) {
                        if (scope.notesModel.noteAction === 'create') {
                            apiService.fetchDataFromApi('opp.leadProcess', payload, oppNoteAddSuccess, oppNoteAddError);
                        } else if (scope.notesModel.noteAction === 'edit') {
                            apiService.fetchDataFromApi('opp.leadProcess', payload, oppNoteUpdateSuccess, oppNoteAddError);
                        } else if (scope.notesModel.noteAction === 'delete') {
                            apiService.fetchDataFromApi('opp.leadProcess', payload, oppNoteDeleteSuccess, oppNoteDeleteError);
                        }
                    };
                    //check for changes durign edit
                    var checkChanges = function (old, edited) {
                        old.operation = '';
                        edited.operation = '';
                        old.$$hashKey = '';
                        edited.$$hashKey = '';
                        var objectsAreSame = true;
                        for (var propertyName in old) {
                            if (old[propertyName] !== edited[propertyName]) {
                                objectsAreSame = false;
                                break;
                            }
                        }
                        return objectsAreSame;
                    };
                    //save note
                    scope.saveNote = function () {
                        var changesExists;
                        if (scope.notesModel.noteAction == 'edit') {
                            changesExists = checkChanges(scope.notesModel.noteToEdit, scope.notesModel.noteAddRequestParams);
                        } else {
                            changesExists = false;
                        }
                        if (!changesExists) {
                            var validationResult = validateOppNoteCreation();
                            if (validationResult === '') {
                                scope.notesModel.showNoteAddLoader = true;
                                var payLoad = scope.notesModel.noteAddRequestParams;
                                if (scope.notesModel.noteAction == 'create') {
                                    var headerData = {
                                        "operation": "skipnode",
                                        "rowId": scope.notesModel.rowId
                                    };
                                    var noteData = {
                                        "note": payLoad.note,
                                        "noteType": payLoad.noteType,
                                        "id": "1234",
                                        "sTCCreatedBy": sharedDataService.getUserDetails().Id,
                                        "operation": "insert"
                                    };
                                    var noteAddPayLoad = Util.leadProcessPayload(headerData, '', '', noteData, '', '', '', '');
                                    var noteAddOfflinePayLoad = {
                                        'opportunityId': scope.notesModel.rowId ? scope.notesModel.rowId : '',
                                        'noteType': payLoad.noteType ? payLoad.noteType : '',
                                        'note': payLoad.note ? payLoad.note : '',
                                        'sTCCreatedBy': sharedDataService.getUserDetails().Id ? sharedDataService.getUserDetails().Id : ''
                                    }
                                    if (!sharedDataService.getNetworkState()) {
                                        pushNoteToDB(noteAddOfflinePayLoad);
                                    } else {
                                        postNoteOnline(noteAddPayLoad);
                                    }
                                } else if (scope.notesModel.noteAction == 'edit') {
                                    var headerData = {
                                        "operation": "skipnode",
                                        "rowId": scope.notesModel.rowId
                                    };
                                    var noteData = {
                                        "note": payLoad.note,
                                        "noteType": payLoad.noteType,
                                        "id": payLoad.id,
                                        "operation": "update"
                                    };
                                    var noteUpdatePayLoad = Util.leadProcessPayload(headerData, '', '', noteData, '', '', '', '');
                                    var noteUpdateOfflinePayLoad = {
                                        'id': payLoad.id ? payLoad.id : '',
                                        'noteType': payLoad.noteType ? payLoad.noteType : '',
                                        'note': payLoad.note ? payLoad.note : ''
                                    };
                                    if (!sharedDataService.getNetworkState()) {
                                        pushNoteToDB(noteUpdateOfflinePayLoad);
                                    } else {
                                        postNoteOnline(noteUpdatePayLoad);
                                    }
                                }
                            } else {
                                MaterialReusables.showToast('' + validationResult, 'error');
                            }
                        } else {
                            MaterialReusables.showSuccessAlert('Nothing To Update', angular.noop);
                        }
                    };
                    //cancel note entry
                    scope.cancelNoteEntry = function () {
                        scope.notesModel.showNoteAddLoader = false;
                        scope.notesModel.noteView = 'list';
                    };
                    //edit note item
                    scope.doEditNote = function (currentindex, noteData) {
                        var noteEditData = angular.copy(noteData);
                        dbUpdateParams = { index: currentindex, param: noteEditData };
                        scope.notesModel.noteAction = 'edit';
                        scope.notesModel.noteToEdit = noteData;
                        scope.noteEdited = angular.copy(noteData);
                        scope.notesModel.noteAddRequestParams = scope.noteEdited;
                        scope.notesModel.noteView = 'create';
                    };
                    //date conversion
                    function convertDates(notesList) {
                        var notes = [];
                        angular.forEach(notesList, function (value, key) {
                            if (value.createdDate !== null) {
                                value.createdDate = moment(value.createdDate).format("DD/MM/YYYY HH:mm:ss");
                            }
                            notes.push(value);
                        });
                        return notes;
                    };
                    //additional validation
                    scope.noteValidate = function (newValue) {
                        scope.notesModel.noteAddRequestParams.note = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                }
            }
        }
    }
};
opportunityModule.directive('detailNotes', detailNotes);
detailNotes.$inject = ['$state', '$translate', '$filter', '$timeout', 'sharedDataService', 'MaterialReusables', 'apiService', 'pluginService'];