var opportunityListController = function ($rootScope, $scope, $timeout, $state, apiService, pluginService, sharedDataService, MaterialReusables, dbService) {
  $scope.oppListModel = {
    isLoading: true,
    subCategoryList: [],
    selectedCategory: 'LEAD',
    selectedIndex: '',
    search: false,
    opportunitiesLead: [],
    opportunitiesCustVisit: [],
    opportunitiesCustContact: [],
    opportunitiesProposal: [],
    opportunitiesWin: [],
    opportunitiesLost: [],
    searchText: '',
    startRow: '0',
    isRefresh: false,
    startRowFirstCall: '0',
    isRefreshTop: false,
    myView: true,
    startRowNum: '1'
  };
  //oppty stsus list
  $scope.oppListModel.subCategoryList = [
    { index: 0, 'name': 'Lead', 'isSelected': true, 'notifierCount': '0' },
    { index: 1, 'name': 'Cust:Contact', 'isSelected': false, 'notifierCount': '0' },
    { index: 2, 'name': 'Cust:Visits', 'isSelected': false, 'notifierCount': '0' },
    { index: 3, 'name': 'Proposal', 'isSelected': false, 'notifierCount': '0' },
    { index: 4, 'name': 'Win', 'isSelected': false, 'notifierCount': '0' },
    { index: 5, 'name': 'Lost', 'isSelected': false, 'notifierCount': '0' }
  ];
  $scope.currentTab = null;
  //invoke API calls on navigation between status tabs
  $scope.onOppListTabChanges = function (selectedIndex) {
    $scope.currentTab = selectedIndex;
    $scope.oppListModel.searchText = '';
    $scope.oppListModel.selectedCategory = selectedIndex.name;
    angular.forEach($scope.oppListModel.subCategoryList, function (key, val) {
      if (selectedIndex.index === key.index) { key.isSelected = true; }
      else { key.isSelected = false; }
    });
    var fetchOpportunitiesFromAPI = function () {
      if ($scope.oppListModel.myView) {
        //if (!$scope.oppListModel.isRefreshTop) {
        //my view API call
        $scope.oppListModel.startRow = 0;
        $scope.endOfPage = false;
        $scope.action = 'tabSwitch';
        myViewListFetch(selectedIndex.name);
        //}
      } else {
        //if (!$scope.oppListModel.isRefreshTop) {
        //team view API call
        $scope.oppListModel.startRowNum = 1;
        $scope.endOfPage = false;
        $scope.action = 'tabSwitch';
        teamViewListFetch(selectedIndex.name);
        // }
      }
    };
    var fetchOpportunitiesFromDB = function () {
      var oppDBFetchSuccess = function (data) {
        var tempOppList = { Lead: [], custContact: [], custVisit: [], proposal: [], win: [], lost: [] };
        if (data.length) {
          angular.forEach(data, function (value, key) {
            var oppObj = JSON.parse(value.opportunities);
            oppObj.colorCode = getColorCode(oppObj.sTCBulkLeadCreatedDate);
            oppObj.serviceName = getServiceTypes(oppObj.ListOfOpportunityProduct);
            if (value.status === 'Lead') {
              tempOppList.Lead.push(oppObj);
            } else if (value.status == 'Cust:Contact') {
              tempOppList.custContact.push(oppObj);
            } else if (value.status == 'Cust:Visits') {
              tempOppList.custVisit.push(oppObj);
            } else if (value.status == 'Proposal') {
              tempOppList.proposal.push(oppObj);
            } else if (value.status == 'Win') {
              tempOppList.win.push(oppObj);
            } else if (value.status == 'Lost') {
              tempOppList.lost.push(oppObj);
            }
          });
        }
        $timeout(function () {
          $scope.oppListModel.opportunitiesLead = tempOppList.Lead;
          $scope.oppListModel.opportunitiesCustContact = tempOppList.custContact;
          $scope.oppListModel.opportunitiesCustVisit = tempOppList.custVisit
          $scope.oppListModel.opportunitiesProposal = tempOppList.proposal;
          $scope.oppListModel.opportunitiesWin = tempOppList.win;
          $scope.oppListModel.opportunitiesLost = tempOppList.lost;
        });
        $scope.oppListModel.isRefreshTop = false;
        $scope.oppListModel.isLoading = false;
      };
      $scope.oppListModel.opportunitiesLead = [];
      $scope.oppListModel.opportunitiesCustVisit = [];
      $scope.oppListModel.opportunitiesCustContact = [];
      $scope.oppListModel.opportunitiesProposal = [];
      $scope.oppListModel.opportunitiesWin = [];
      $scope.oppListModel.opportunitiesLost = [];
      var listSearchParameters = Util.convertObjectToSQL({ status: selectedIndex.name, toBeUpdated: 'false' });
      $scope.oppListModel.isRefreshTop = true;
      dbService.getDetailsFromTable('stc_opptys', listSearchParameters, angular.noop, oppDBFetchSuccess);
    };
    if (!$scope.oppListModel.isRefreshTop) {
      if (!sharedDataService.getNetworkState()) {
        fetchOpportunitiesFromDB();
      } else {
        fetchOpportunitiesFromAPI();
      }
    }
    $scope.oppListModel.search = false;
    $scope.oppListModel.searchText = '';
  };
  //right swipe navigation
  $scope.loadNextCategory = function () {
    if ($scope.oppListModel.selectedIndex < 6 && !$scope.oppListModel.isRefreshTop) {
      $scope.oppListModel.selectedIndex = $scope.oppListModel.selectedIndex + 1;
    }
  };
  //left swipe navigation
  $scope.loadPrevCategory = function () {
    if ($scope.oppListModel.selectedIndex > 0 && !$scope.oppListModel.isRefreshTop) {
      $scope.oppListModel.selectedIndex = $scope.oppListModel.selectedIndex - 1;
    }
  };
  //insert oppty details to local DB while navigating to details page
  var insertOpportunityDatatoDB = function (selectedId, detailToPush) {
    var defaultPayloadToPush = {
      Activity: { create: [], edit: [], delete: [] },
      Notes: { create: [], edit: [], delete: [] },
      Products: { create: [], edit: [], delete: [] },
      Details: { update: [] }
    };
    var oppDBGetError = function (data) {
      dbService.insertIntoDBTable('stc_opptys', [detailToPush.rowId, JSON.stringify(detailToPush), JSON.stringify(defaultPayloadToPush), $scope.currentTab.name, 'online', 'false',''], angular.noop, angular.noop);
      $state.go('root.oppDetail', { id: selectedId, oppDetails: detailToPush, fromView: $state.current.name, enableEdit: true });
    };
    var oppDBGetSuccess = function (data) {
      if (!data.length) {
        dbService.insertIntoDBTable('stc_opptys', [detailToPush.rowId, JSON.stringify(detailToPush), JSON.stringify(defaultPayloadToPush), $scope.currentTab.name, 'online', 'false',''], angular.noop, angular.noop);
        $state.go('root.oppDetail', { id: selectedId, oppDetails: detailToPush, fromView: $state.current.name, enableEdit: true });
      } else {
        if (data[0].toBeUpdated === 'true') {
          var revertCallback = function (result) {
            if (result) {
              var deleteSuccess = function () {
                dbService.insertIntoDBTable('stc_opptys', [detailToPush.rowId, JSON.stringify(detailToPush), JSON.stringify(defaultPayloadToPush), $scope.currentTab.name, 'online', 'false',''], angular.noop, angular.noop);
                $state.go('root.oppDetail', { id: selectedId, oppDetails: detailToPush, fromView: $state.current.name, enableEdit: true });
              };
              var sqlParameters = Util.convertObjectToSQL({ optyID: detailToPush.rowId });
              dbService.deleteFromTable('stc_opptys', sqlParameters, angular.noop, deleteSuccess);
            }
          };
          MaterialReusables.showConfirmDialog('', 'Selected Opportunity is Already Updated in Offline.Would you like To Revert All Changes And Continue ?', revertCallback);
        } else {
          var oppDeleteSuccess = function () {
            dbService.insertIntoDBTable('stc_opptys', [detailToPush.rowId, JSON.stringify(detailToPush), JSON.stringify(defaultPayloadToPush), $scope.currentTab.name, 'online', 'false',''], angular.noop, angular.noop);
            $state.go('root.oppDetail', { id: selectedId, oppDetails: detailToPush, fromView: $state.current.name, enableEdit: true });
          };
          var sqlDeleteParameters = Util.convertObjectToSQL({ optyID: detailToPush.rowId });
          dbService.deleteFromTable('stc_opptys', sqlDeleteParameters, angular.noop, oppDeleteSuccess);
        }
      }
    };
    var searchDBById = Util.convertObjectToSQL({ optyID: detailToPush.rowId });
    dbService.getDetailsFromTable('stc_opptys', searchDBById, oppDBGetError, oppDBGetSuccess);
  };
  //save currently selected list state
  var saveListState = function (tab) {
    switch (tab) {
      case 'Lead': sharedDataService.saveOpptyList($scope.oppListModel.opportunitiesLead);
        break;
      case 'Cust:Contact': sharedDataService.saveOpptyList($scope.oppListModel.opportunitiesCustContact);
        break;
      case 'Cust:Visits': sharedDataService.saveOpptyList($scope.oppListModel.opportunitiesCustVisit);
        break;
      case 'Proposal': sharedDataService.saveOpptyList($scope.oppListModel.opportunitiesProposal);
        break;
      case 'Win': sharedDataService.saveOpptyList($scope.oppListModel.opportunitiesWin);
        break;
      case 'Lost': sharedDataService.saveOpptyList($scope.oppListModel.opportunitiesLost);
        break;
      default:
      ////TODO
    }
    if ($scope.oppListModel.searchText !== null) {
      sharedDataService.saveOpptyFilterText($scope.oppListModel.searchText);
    } else {
      sharedDataService.saveOpptyFilterText(null);
    }
  };
  //API call to fetch selected oppty details and navigation to oppty details
  $scope.gotoOpportunityDetails = function (selectedId, detailObj) {
    var oppListOpptySuccess = function (data) {
      $scope.oppListModel.isRefreshTop = false;
      if (angular.isDefined(data.data.listOfStcOpportunity.opportunity[0])) {
        sharedDataService.setSelectedTab($scope.currentTab);
        //saveListState($scope.currentTab.name);
        insertOpportunityDatatoDB(selectedId, data.data.listOfStcOpportunity.opportunity[0]);
      } else {
        MaterialReusables.showToast('' + pluginService.getTranslations().oppDetNoFound, 'error');
      }
    };
    var oppListOpptyError = function (data) {
      $scope.oppListModel.isRefreshTop = false;
      Util.throwError(data.data.Message, MaterialReusables);
    };
    var oppListOpptyPayload = Util.oppPayload("1", "[Opportunity.Id] = '" + detailObj.rowId + "' ", "", '0');
    $scope.oppListModel.isRefreshTop = true;
    if (!sharedDataService.getNetworkState()) {
      $state.go('root.oppDetail', { id: selectedId, oppDetails: detailObj, fromView: $state.current.name, enableEdit: true });
    } else {
      apiService.fetchDataFromApi('opp.list', oppListOpptyPayload, oppListOpptySuccess, oppListOpptyError);
    }
  };
  //search filter input validation
  $scope.searchFilter = function (obj) {
    if (typeof obj !== 'undefined') {
      $scope.oppListModel.searchText = $scope.oppListModel.searchText.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
      var re = new RegExp($scope.oppListModel.searchText, 'i');
      return !$scope.oppListModel.searchText || re.test(obj.name) || re.test(obj.sTCLeadNumber);
    }
  };
  //oppty search API call
  $scope.searchOppty = function (text) {
    if (sharedDataService.getNetworkState()) {
      if (text !== '') {
        if ($scope.oppListModel.myView) {
          var opptySuccess = function (data) {
            $scope.oppListModel.isRefreshTop = false;
            if (angular.isDefined(data.data.listOfStcOpportunity.opportunity)) {
              angular.forEach(data.data.listOfStcOpportunity.opportunity, function (value, key) {
                var opptyData = {};
                value.serviceName = getServiceTypes(value.ListOfOpportunityProduct);
                value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
                opptyData = { 'rowId': value.rowId, 'sTCLeadNumber': value.sTCLeadNumber, 'name': value.name, 'sTCOpportunityType': value.sTCOpportunityType, 'salesStage': value.salesStage, 'sTCSubStatusSME': value.sTCSubStatusSME, 'sTCBulkLeadContactNumber': value.sTCBulkLeadContactNumber, 'sTCBulkLeadContactFirstName': value.sTCBulkLeadContactFirstName, 'serviceName': value.serviceName, 'sTCLeadContactFirstName': value.sTCLeadContactFirstName, 'PrimaryRevenueAmount': value.PrimaryRevenueAmount, 'sTCOpportunityPriority': value.sTCOpportunityPriority, 'sTCOptyCity': value.sTCOptyCity, 'sTCBulkLeadCreatedDate': value.sTCBulkLeadCreatedDate, 'sTCContactNumber2': value.sTCContactNumber2, 'sTCContactNumber3': value.sTCContactNumber3, 'sTCFeedback': value.sTCFeedback, 'sTCFeedback2': value.sTCFeedback2, 'sTCFeedback3': value.sTCFeedback3, 'colorCode': value.colorCode };
                if (tab == 'Lead') {
                  if (indexOf($scope.oppListModel.opportunitiesLead, opptyData) == -1) {
                    $scope.oppListModel.opportunitiesLead.push(opptyData);
                  }
                } else if (tab == 'Cust:Contact') {
                  if (indexOf($scope.oppListModel.opportunitiesCustContact, opptyData) == -1) {
                    $scope.oppListModel.opportunitiesCustContact.push(opptyData);
                  }
                } else if (tab == 'Cust:Visits') {
                  if (indexOf($scope.oppListModel.opportunitiesCustVisit, opptyData) == -1) {
                    $scope.oppListModel.opportunitiesCustVisit.push(opptyData);
                  }
                } else if (tab == 'Proposal') {
                  if (indexOf($scope.oppListModel.opportunitiesProposal, opptyData) == -1) {
                    $scope.oppListModel.opportunitiesProposal.push(opptyData);
                  }
                } else if (tab == 'Win') {
                  if (indexOf($scope.oppListModel.opportunitiesWin, opptyData) == -1) {
                    $scope.oppListModel.opportunitiesWin.push(opptyData);
                  }
                } else if (tab == 'Lost') {
                  if (indexOf($scope.oppListModel.opportunitiesLost, opptyData) == -1) {
                    $scope.oppListModel.opportunitiesLost.push(opptyData);
                  }
                }
              });
            } else {
              MaterialReusables.showToast('' + pluginService.getTranslations().noOpp, 'warning');
            }
          };
          var opptyError = function (data) {
            $scope.oppListModel.isRefreshTop = false;
            MaterialReusables.showToast('' + pluginService.getTranslations().noOpp, 'error');
          };
          $scope.oppListModel.isRefreshTop = true;
          var tab = $scope.oppListModel.selectedCategory;
          if (tab == 'Lead') {
            var opptyPayload = Util.oppThinIoPayload("20", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*' ) AND [Opportunity.STC Sub Status SME] = 'Lead')", "", '0');
          } else if (tab == 'Cust:Contact') {
            var opptyPayload = Util.oppThinIoPayload("20", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*' ) AND [Opportunity.Sales Stage] LIKE '*Contact customer*')", "", '0');
          } else if (tab == 'Cust:Visits') {
            var opptyPayload = Util.oppThinIoPayload("20", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*' ) AND [Opportunity.Sales Stage] LIKE '*Customer Visit*')", "", '0');
          } else if (tab == 'Proposal') {
            var opptyPayload = Util.oppThinIoPayload("20", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*') AND [Opportunity.Sales Stage] LIKE '*Proposal*')", "", '0');
          } else if (tab == 'Win') {
            var opptyPayload = Util.oppThinIoPayload("20", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*' ) AND [Opportunity.Sales Stage] LIKE '*Win*')", "", '0');
          } else if (tab == 'Lost') {
            var opptyPayload = Util.oppThinIoPayload("20", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*' ) AND [Opportunity.Sales Stage] LIKE '*Lost*')", "", '0');
          }
          apiService.fetchDataFromApi('opp.opportunitylistmyview', opptyPayload, opptySuccess, opptyError);
        } else {
          var opptySearchMgrSuccess = function (data) {
            $scope.oppListModel.isRefreshTop = false;
            if (angular.isDefined(data.data.opportunityList)) {
              angular.forEach(data.data.opportunityList, function (value, key) {
                value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
                if (tab == 'Lead') {
                  if (indexOf($scope.oppListModel.opportunitiesLead, value) == -1) {
                    $scope.oppListModel.opportunitiesLead.push(value);
                  }
                } else if (tab == 'Cust:Contact') {
                  if (indexOf($scope.oppListModel.opportunitiesCustContact, value) == -1) {
                    $scope.oppListModel.opportunitiesCustContact.push(value);
                  }
                } else if (tab == 'Cust:Visits') {
                  if (indexOf($scope.oppListModel.opportunitiesCustVisit, value) == -1) {
                    $scope.oppListModel.opportunitiesCustVisit.push(value);
                  }
                } else if (tab == 'Proposal') {
                  if (indexOf($scope.oppListModel.opportunitiesProposal, value) == -1) {
                    $scope.oppListModel.opportunitiesProposal.push(value);
                  }
                } else if (tab == 'Win') {
                  if (indexOf($scope.oppListModel.opportunitiesWin, value) == -1) {
                    $scope.oppListModel.opportunitiesWin.push(value);
                  }
                } else if (tab == 'Lost') {
                  if (indexOf($scope.oppListModel.opportunitiesLost, value) == -1) {
                    $scope.oppListModel.opportunitiesLost.push(value);
                  }
                }
              });
            }
          };
          var opptySearchMgrError = function (data) {
            $scope.oppListModel.isRefreshTop = false;
            MaterialReusables.showToast('' + pluginService.getTranslations().noOpp, 'error');
          };
          var tab = $scope.oppListModel.selectedCategory;
          var salesStageSelected = '';
          if (tab == 'Lead') {
            salesStageSelected = 'Lead ~ فرصه';
          } else if (tab == 'Cust:Contact') {
            salesStageSelected = 'Contact customer ~ الاتصال بالعميل';
          } else if (tab == 'Cust:Visits') {
            salesStageSelected = 'Customer Visit ~ زياره العميل';
          } else if (tab == 'Proposal') {
            salesStageSelected = 'Proposal ~ اقتراح';
          } else if (tab == 'Win') {
            salesStageSelected = 'Win ~ الربح';
          } else if (tab == 'Lost') {
            salesStageSelected = 'Lost ~ فقدان';
          }
          var opptySearchMgrPayload = {
            "opportunityName": "%" + text + "%",
            "endindex": "51",
            "startindex": "1",
            "opportunityNumber": "%" + text + "%",
            "login": sharedDataService.getUserDetails().loginName,
            "opptyStatus": salesStageSelected
          };
          $scope.oppListModel.isRefreshTop = true;
          apiService.fetchDataFromApi('opp.odsoppquerysearchformanager', opptySearchMgrPayload, opptySearchMgrSuccess, opptySearchMgrError);
        }
      }
    }
  }
  //check item exists in array or not
  function indexOf(array, item) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].rowId === item.rowId) return i;
    }
    return -1;
  }
  //open search input
  $scope.$on('openSearch', function (event, args) {
    $scope.oppListModel.search = !$scope.oppListModel.search;
    if (!$scope.oppListModel.search) {
      $scope.oppListModel.searchText = '';
    }
  });
  //refresh API call
  $scope.$on('refresh', function (event, args) {
    if (!sharedDataService.getNetworkState()) {
      MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
    } else {
      $scope.action = 'refresh';
      if ($scope.oppListModel.myView) {
        if (!$scope.oppListModel.isRefreshTop) {
          $scope.oppListModel.startRow = 0;
          $scope.endOfPage = false;
          myViewListFetch($scope.oppListModel.selectedCategory);
        }
        myViewCount();//oppty count data fetch for normal user
      } else {
        if (!$scope.oppListModel.isRefreshTop) {
          $scope.oppListModel.startRowNum = 1;
          $scope.endOfPage = false;
          teamViewListFetch($scope.oppListModel.selectedCategory);
        }
        teamViewCount();//oppty count data fetch for manager
      }
    }

  });
  //Oppty API call - success
  var oppListSuccess = function (opp) {
    $scope.oppListModel.isRefresh = false;
    $scope.oppListModel.isRefreshTop = false;
    if (opp.data.listOfStcOpportunity.opportunity !== null) {
      if ($scope.action !== 'loadmore') {
        $scope.oppListModel.opportunitiesLead = [];
        $scope.oppListModel.opportunitiesCustVisit = [];
        $scope.oppListModel.opportunitiesCustContact = [];
        $scope.oppListModel.opportunitiesProposal = [];
        $scope.oppListModel.opportunitiesWin = [];
        $scope.oppListModel.opportunitiesLost = [];
      };
      $scope.listStatus = true;
      $scope.oppListModel.startRow = parseInt($scope.oppListModel.startRow, 10) + 50;
      angular.forEach(opp.data.listOfStcOpportunity.opportunity, function (value, key) {
        if ((value.salesStage !== null) && (angular.isDefined(value.salesStage))) {
          var statusValue = value.salesStage;
          if (statusValue.includes('Lead') && !statusValue.includes('Lead Enrichment')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            value.serviceName = getServiceTypes(value.ListOfOpportunityProduct);
            $scope.oppListModel.opportunitiesLead.push(value);

          } else if (statusValue.includes('Customer Visit')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            value.serviceName = getServiceTypes(value.ListOfOpportunityProduct);
            $scope.oppListModel.opportunitiesCustVisit.push(value);

          } else if (statusValue.includes('Contact customer')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            value.serviceName = getServiceTypes(value.ListOfOpportunityProduct);
            $scope.oppListModel.opportunitiesCustContact.push(value);

          } else if (statusValue.includes('Proposal')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            value.serviceName = getServiceTypes(value.ListOfOpportunityProduct);
            $scope.oppListModel.opportunitiesProposal.push(value);

          } else if (statusValue.includes('Win')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            value.serviceName = getServiceTypes(value.ListOfOpportunityProduct);
            $scope.oppListModel.opportunitiesWin.push(value);

          } else if (statusValue.includes('Lost')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            value.serviceName = getServiceTypes(value.ListOfOpportunityProduct);
            $scope.oppListModel.opportunitiesLost.push(value);
          } else { }
        } else {
          return;
          ////TODO
        }
      });
      if ($scope.action === 'refresh') {
        MaterialReusables.showToast('' + pluginService.getTranslations().firstTen, 'success');
      };
      if (opp.data.listOfStcOpportunity.lastPage === 'true') {
        MaterialReusables.showToast('' + pluginService.getTranslations().endofList, 'warning');
        $scope.endOfPage = true;
      }
    }
    $scope.oppListModel.isLoading = false;
  };
  //Oppty API call - error
  var oppListError = function (opp) {
    $scope.listStatus = true;
    $scope.oppListModel.isLoading = false;
    $scope.oppListModel.isRefresh = false;
    $scope.oppListModel.isRefreshTop = false;
    Util.throwError(opp.data.Message, MaterialReusables);
  };
  //open dialer to make phone clal with selected number
  $scope.openDialer = function (contactNo, $event) {
    $event.stopPropagation();
    if (contactNo || contactNo !== '') {
      var openedDialer = function (success) { };
      var failedtoDial = function (error) { };
      pluginService.dialNumber(contactNo, openedDialer, failedtoDial);
    }
  };
  //get service types of prodcuts
  var getServiceTypes = function (productsList) {
    var types = '';
    if (productsList !== null) {
      if (productsList.OpportunityProduct !== null && angular.isDefined(productsList.OpportunityProduct)) {
        angular.forEach(productsList.OpportunityProduct, function (value, key) {
          if (value.sTCServiceType2 !== null) {
            if (!types.includes(value.sTCServiceType2)) {
              types = types + value.sTCServiceType2 + ",";
            }
          }
        });
      }
    }
    if (types !== '') {
      types = types.replace(/,\s*$/, "");//remove last comma
    }
    return types;
  };
  //colorcode selection
  var getColorCode = function (leadCreatedDate) {
    var colorCode;
    var leadDate = new Date(leadCreatedDate);
    var sysDate = new Date();
    var dateDiff = parseInt((sysDate - leadDate) / (24 * 3600 * 1000), 10);
    if (dateDiff < 4) {
      colorCode = 'green';
    } else if (dateDiff > 3 && dateDiff < 10) {
      colorCode = 'blue';
    } else if (dateDiff > 9 && dateDiff < 16) {
      colorCode = 'yellow';
    } else if (dateDiff > 15 && dateDiff < 21) {
      colorCode = 'orange';
    } else {
      colorCode = 'red';
    }
    return colorCode;
  };
  $scope.oppListModel.isLoading = true;
  $scope.listStatus = true;
  $scope.endOfPage = false;
  //loading more opptys by scrolling over list: top to bottom
  $scope.loadMoreOpportunities = function () {
    if (sharedDataService.getNetworkState()) {
      $scope.action = 'loadmore';
      if ($scope.oppListModel.myView) {
        if (!$scope.oppListModel.isRefreshTop && !$scope.endOfPage) {
          myViewListFetch($scope.oppListModel.selectedCategory);
        };
      } else {
        if (!$scope.oppListModel.isRefreshTop && !$scope.endOfPage) {
          teamViewListFetch($scope.oppListModel.selectedCategory);
        };
      }
    }
  };
  //reset counts
  var resetCounts = function() {
    $scope.oppListModel.subCategoryList[0].notifierCount = 0;
    $scope.oppListModel.subCategoryList[1].notifierCount = 0;
    $scope.oppListModel.subCategoryList[2].notifierCount = 0;
    $scope.oppListModel.subCategoryList[3].notifierCount = 0;
    $scope.oppListModel.subCategoryList[4].notifierCount = 0;
  };
  //user view oppty count call
  function myViewCount() {
    var oppListCountPayload = {
      "login": sharedDataService.getUserDetails().loginName
    };
    var oppListCountSuccess = function (countData) {
      if (countData.data !== null) {
        if (angular.isDefined(countData.data.odsOppCountofStatusOutput)) {
          angular.forEach(countData.data.odsOppCountofStatusOutput, function (value, key) {
            if (value.opportunityStatus.includes('Lead') && !value.opportunityStatus.includes('Lead Enrichment')) {
              if (value.opportunitySubStatus === 'Lead') {
                if (value.countOpportunityId !== null) {
                  $scope.oppListModel.subCategoryList[0].notifierCount = value.countOpportunityId;
                } else {
                  $scope.oppListModel.subCategoryList[0].notifierCount = 0;
                }
              }
            } else if (value.opportunityStatus.includes('Contact customer')) {
              if (value.opportunitySubStatus === 'Call back') {
                if (value.countOpportunityId !== null) {
                  $scope.oppListModel.subCategoryList[1].notifierCount = value.countOpportunityId;
                } else {
                  $scope.oppListModel.subCategoryList[1].notifierCount = 0;
                }
              }
            } else if (value.opportunityStatus.includes('Customer Visit')) {
              if (value.opportunitySubStatus === 'Set Appt') {
                if (value.countOpportunityId !== null) {
                  $scope.oppListModel.subCategoryList[2].notifierCount = value.countOpportunityId;
                } else {
                  $scope.oppListModel.subCategoryList[2].notifierCount = 0;
                }
              }
            } else if (value.opportunityStatus.includes('Proposal')) {
              if (value.opportunitySubStatus === 'Contract Signed') {
                if (value.countOpportunityId !== null) {
                  $scope.oppListModel.subCategoryList[3].notifierCount = value.countOpportunityId;
                } else {
                  $scope.oppListModel.subCategoryList[3].notifierCount = 0;
                }
              }
            } else if (value.opportunityStatus.includes('Win')) {
              if (value.opportunitySubStatus === 'Order Placed') {
                if (value.countOpportunityId !== null) {
                  $scope.oppListModel.subCategoryList[4].notifierCount = value.countOpportunityId;
                } else {
                  $scope.oppListModel.subCategoryList[4].notifierCount = 0;
                }
              }
            }
          });
        } else {
          MaterialReusables.showToast('' + pluginService.getTranslations().noOppCou, 'warning');
          resetCounts();
        }
      } else {
        MaterialReusables.showToast('' + pluginService.getTranslations().noOppCou, 'warning');
        resetCounts();
      }
    };
    var oppListCountError = function (data) {
      resetCounts();
    };
    resetCounts();
    if (sharedDataService.getNetworkState()) {
      apiService.fetchDataFromApi('opp.odsoppcountofstatus', oppListCountPayload, oppListCountSuccess, oppListCountError);
    }
  };
  //manager team view oppty count call - will be invoked based on drop dwon selection as my team view and ismnanager is true
  function teamViewCount() {
    var oppListMgrCountPayload = {
      "loginName": sharedDataService.getUserDetails().loginName
    };
    var oppListMgrCountSuccess = function (data) {
      if (data.data.records !== null) {
        angular.forEach(data.data.records, function (value, key) {
          if (value.opportunityStatus.includes('Lead') && !value.opportunityStatus.includes('Lead Enrichment')) {
            if (value.opportunitySubStatus === 'Lead') {
              if (value.count !== null) {
                $scope.oppListModel.subCategoryList[0].notifierCount = value.count;
              } else {
                $scope.oppListModel.subCategoryList[0].notifierCount = 0;
              }
            }
          } else if (value.opportunityStatus.includes('Contact customer')) {
            if (value.opportunitySubStatus === 'Call back') {
              if (value.count !== null) {
                $scope.oppListModel.subCategoryList[1].notifierCount = value.count;
              } else {
                $scope.oppListModel.subCategoryList[1].notifierCount = 0;
              }
            }
          } else if (value.opportunityStatus.includes('Customer Visit')) {
            if (value.opportunitySubStatus === 'Set Appt') {
              if (value.count !== null) {
                $scope.oppListModel.subCategoryList[2].notifierCount = value.count;
              } else {
                $scope.oppListModel.subCategoryList[2].notifierCount = 0;
              }
            }
          } else if (value.opportunityStatus.includes('Proposal')) {
            if (value.opportunitySubStatus === 'Contract Signed') {
              if (value.count !== null) {
                $scope.oppListModel.subCategoryList[3].notifierCount = value.count;
              } else {
                $scope.oppListModel.subCategoryList[3].notifierCount = 0;
              }
            }
          } else if (value.opportunityStatus.includes('Win')) {
            if (value.opportunitySubStatus === 'Order Placed') {
              if (value.count !== null) {
                $scope.oppListModel.subCategoryList[4].notifierCount = value.count;
              } else {
                $scope.oppListModel.subCategoryList[4].notifierCount = 0;
              }
            }
          }
        });
      } else {
        MaterialReusables.showToast('' + pluginService.getTranslations().noOppCou, 'warning');
        $scope.oppListModel.subCategoryList[0].notifierCount = 0;
        $scope.oppListModel.subCategoryList[1].notifierCount = 0;
        $scope.oppListModel.subCategoryList[2].notifierCount = 0;
        $scope.oppListModel.subCategoryList[3].notifierCount = 0;
        $scope.oppListModel.subCategoryList[4].notifierCount = 0;
        //$scope.oppListModel.subCategoryList[5].notifierCount = 0;
      }
    };
    var oppListMgrCountError = function (data) {
      $scope.oppListModel.subCategoryList[0].notifierCount = 0;
      $scope.oppListModel.subCategoryList[1].notifierCount = 0;
      $scope.oppListModel.subCategoryList[2].notifierCount = 0;
      $scope.oppListModel.subCategoryList[3].notifierCount = 0;
      $scope.oppListModel.subCategoryList[4].notifierCount = 0;
      //MaterialReusables.showToast('' + pluginService.getTranslations().couFetchFailed, 'error');
    };
    $scope.oppListModel.subCategoryList[0].notifierCount = 0;
    $scope.oppListModel.subCategoryList[1].notifierCount = 0;
    $scope.oppListModel.subCategoryList[2].notifierCount = 0;
    $scope.oppListModel.subCategoryList[3].notifierCount = 0;
    $scope.oppListModel.subCategoryList[4].notifierCount = 0;
    if (sharedDataService.getNetworkState()) {
      apiService.fetchDataFromApi('opp.odscountbysubstatus', oppListMgrCountPayload, oppListMgrCountSuccess, oppListMgrCountError);
    }
  };
  function myViewListFetch(tab) {
    $scope.oppListModel.isRefreshTop = true;
    switch (tab) {
      case 'Lead':
        var oppListPayload = Util.oppThinIoPayload("50", "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "') AND ([Opportunity.STC Sub Status SME] = 'Lead'))", "Created (DESCENDING)", $scope.oppListModel.startRow);
        apiService.fetchDataFromApi('opp.opportunitylistmyview', oppListPayload, oppListSuccess, oppListError);
        break;
      case 'Cust:Visits':
        var oppListPayload = Util.oppThinIoPayload("50", "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "') AND ([Opportunity.Sales Stage] LIKE '*Customer Visit*'))", "Created (DESCENDING)", $scope.oppListModel.startRow);
        apiService.fetchDataFromApi('opp.opportunitylistmyview', oppListPayload, oppListSuccess, oppListError);
        break;
      case 'Cust:Contact':
        var oppListPayload = Util.oppThinIoPayload("50", "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "') AND ([Opportunity.Sales Stage] LIKE '*Contact customer*'))", "Created (DESCENDING)", $scope.oppListModel.startRow);
        apiService.fetchDataFromApi('opp.opportunitylistmyview', oppListPayload, oppListSuccess, oppListError);
        break;
      case 'Proposal':
        var oppListPayload = Util.oppThinIoPayload("50", "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "') AND ([Opportunity.Sales Stage] LIKE '*Proposal*'))", "Created (DESCENDING)", $scope.oppListModel.startRow);
        apiService.fetchDataFromApi('opp.opportunitylistmyview', oppListPayload, oppListSuccess, oppListError);
        break;
      case 'Win':
        var oppListPayload = Util.oppThinIoPayload("50", "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "') AND ([Opportunity.Sales Stage] LIKE '*Win*'))", "Created (DESCENDING)", $scope.oppListModel.startRow);
        apiService.fetchDataFromApi('opp.opportunitylistmyview', oppListPayload, oppListSuccess, oppListError);
        break;
      case 'Lost':
        var oppListPayload = Util.oppThinIoPayload("50", "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "') AND ([Opportunity.Sales Stage] LIKE '*Lost*'))", "Created (DESCENDING)", $scope.oppListModel.startRow);
        apiService.fetchDataFromApi('opp.opportunitylistmyview', oppListPayload, oppListSuccess, oppListError);
        break;
      default:
        ////TODO      
    };
  };
  //Mgr Oppty API call - Error
  var oppListMgrError = function (opp) {
    $scope.oppListModel.isRefresh = false;
    $scope.oppListModel.isRefreshTop = false;
    $scope.oppListModel.isLoading = false;
    Util.throwError(opp.data.Message, MaterialReusables);
  };
  //Mgr Oppty API call - Success
  var oppListMgrSuccess = function (opp) {
    $scope.oppListModel.isRefreshTop = false;
    if (opp.data !== null) {
      $scope.oppListModel.startRowNum = parseInt($scope.oppListModel.startRowNum, 10) + 50;
      if ($scope.action !== 'loadmore') {
        $scope.oppListModel.opportunitiesLead = [];
        $scope.oppListModel.opportunitiesCustVisit = [];
        $scope.oppListModel.opportunitiesCustContact = [];
        $scope.oppListModel.opportunitiesProposal = [];
        $scope.oppListModel.opportunitiesWin = [];
        $scope.oppListModel.opportunitiesLost = [];
      }
      angular.forEach(opp.data.opportunityList, function (value, key) {
        if ((value.salesStage !== null) && (angular.isDefined(value.salesStage))) {
          var statusValue = value.salesStage;
          if (statusValue.includes('Lead') && !statusValue.includes('Lead Enrichment')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            $scope.oppListModel.opportunitiesLead.push(value);

          } else if (statusValue.includes('Customer Visit')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            $scope.oppListModel.opportunitiesCustVisit.push(value);

          } else if (statusValue.includes('Contact customer')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            $scope.oppListModel.opportunitiesCustContact.push(value);

          } else if (statusValue.includes('Proposal')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            $scope.oppListModel.opportunitiesProposal.push(value);

          } else if (statusValue.includes('Win')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            $scope.oppListModel.opportunitiesWin.push(value);

          } else if (statusValue.includes('Lost')) {
            value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
            $scope.oppListModel.opportunitiesLost.push(value);

          } else { }
        } else {
          return;
          ////TODO
        }
      });
      if ($scope.oppListModel.myView && $scope.action === 'refresh') {
        MaterialReusables.showToast('' + pluginService.getTranslations().firstTen, 'success');
      } else if (!$scope.oppListModel.myView && $scope.action === 'refresh') {
        MaterialReusables.showToast('' + pluginService.getTranslations().fiftyRecords, 'success');
      }
      if (opp.data.opportunityList.length < 50) {
        if ($scope.action !== 'refresh') {
          MaterialReusables.showToast('' + pluginService.getTranslations().endofList, 'warning');
        }
        $scope.endOfPage = true;
      }
    } else {
      MaterialReusables.showToast('' + pluginService.getTranslations().noOpp, 'warning');
      $scope.endOfPage = true;
    }
    $scope.oppListModel.isLoading = false;
  };
  //Mgr view API call
  function teamViewListFetch(status) {
    $scope.oppListModel.isRefreshTop = true;
    var salesStageSelected = '';
    if (status == 'Lead') {
      salesStageSelected = 'Lead ~ فرصه';
    } else if (status == 'Cust:Contact') {
      salesStageSelected = 'Contact customer ~ الاتصال بالعميل';
    } else if (status == 'Cust:Visits') {
      salesStageSelected = 'Customer Visit ~ زياره العميل';
    } else if (status == 'Proposal') {
      salesStageSelected = 'Proposal ~ اقتراح';
    } else if (status == 'Win') {
      salesStageSelected = 'Win ~ الربح';
    } else if (status == 'Lost') {
      salesStageSelected = 'Lost ~ فقدان';
    }
    var oppListMgrPayload = Util.oppMgrPayload(sharedDataService.getUserDetails().loginName, salesStageSelected, $scope.oppListModel.startRowNum, parseInt($scope.oppListModel.startRowNum, 10) + 50);
    if (sharedDataService.getViewSelected() === 'user' || sharedDataService.getViewSelected() == null) {
      apiService.fetchDataFromApi('opp.oppListNormalUser', oppListMgrPayload, oppListMgrSuccess, oppListMgrError);
    } else if (sharedDataService.getViewSelected() === 'team' && sharedDataService.getIsManager()) {
      apiService.fetchDataFromApi('opp.opplistformanager', oppListMgrPayload, oppListMgrSuccess, oppListMgrError);
    }
  };
  //load previous view oppty data from shared data
  var loadPreviousOpptyList = function (tab) {
    switch (tab) {
      case 'Lead': $scope.oppListModel.opportunitiesLead = sharedDataService.getOpptyList();
        break;
      case 'Cust:Contact': $scope.oppListModel.opportunitiesCustContact = sharedDataService.getOpptyList();
        break;
      case 'Cust:Visits': $scope.oppListModel.opportunitiesCustVisit = sharedDataService.getOpptyList();
        break;
      case 'Proposal': $scope.oppListModel.opportunitiesProposal = sharedDataService.getOpptyList();
        break;
      case 'Win': $scope.oppListModel.opportunitiesWin = sharedDataService.getOpptyList();
        break;
      case 'Lost': $scope.oppListModel.opportunitiesLost = sharedDataService.getOpptyList();
        break;
      default:
      ////TODO
    }
    sharedDataService.saveOpptyList([]);
    if (sharedDataService.getOpptyFilterText() !== null) {
      $scope.oppListModel.searchText = sharedDataService.getOpptyFilterText();
      $scope.oppListModel.search = !$scope.oppListModel.search;
      sharedDataService.saveOpptyFilterText(null);
    }
  };
  //manager view - user view switching
  $scope.switchViews = function (isUserView) {
    $scope.oppListModel.opportunitiesLead = [];
    $scope.oppListModel.opportunitiesCustVisit = [];
    $scope.oppListModel.opportunitiesCustContact = [];
    $scope.oppListModel.opportunitiesProposal = [];
    $scope.oppListModel.opportunitiesWin = [];
    $scope.oppListModel.opportunitiesLost = [];
    if (isUserView) {
      sharedDataService.saveviewSelected('user');
      $scope.oppListModel.myView = true;
      if ($scope.currentTab !== null) {
        $scope.onOppListTabChanges($scope.currentTab);//call user view with current tab status
      } else {
        if (angular.isDefined(sharedDataService.getSelectedTab()) && sharedDataService.getSelectedTab() !== null) {
          // if (angular.isDefined(sharedDataService.getOpptyList()) && sharedDataService.getOpptyList().length !== 0) {
          //   loadPreviousOpptyList(sharedDataService.getSelectedTab().name);
          // } else {
          //   $scope.onOppListTabChanges(sharedDataService.getSelectedTab());//call user view with previous selected status
          // }
          $scope.onOppListTabChanges(sharedDataService.getSelectedTab());//call user view with previous selected status
          $scope.oppListModel.selectedIndex = sharedDataService.getSelectedTab().index;
          sharedDataService.setSelectedTab(null);
        } else {
          $scope.onOppListTabChanges($scope.oppListModel.subCategoryList[0]);//call user view with lead status
        }
      }
      myViewCount();
      if (sharedDataService.getIsManager()) {
        MaterialReusables.showToast('' + pluginService.getTranslations().myviewloading, 'success');
      }
    } else {
      if (!sharedDataService.getNetworkState()) {
        MaterialReusables.showToast('' + pluginService.getTranslations().featureNotAvblInOffline, 'warning');
      } else {
        sharedDataService.saveviewSelected('team');
        $scope.oppListModel.myView = false;
        if ($scope.currentTab !== null) {
          $scope.onOppListTabChanges($scope.currentTab);//call manager view with current tab status
        } else {
          if (angular.isDefined(sharedDataService.getSelectedTab()) && sharedDataService.getSelectedTab() !== null) {
            $scope.onOppListTabChanges(sharedDataService.getSelectedTab());//call user view with previous selected status
            $scope.oppListModel.selectedIndex = sharedDataService.getSelectedTab().index;
            sharedDataService.setSelectedTab(null);
          } else {
            $scope.onOppListTabChanges($scope.oppListModel.subCategoryList[0]);//call manager view with lead status
          }
        }
        teamViewCount();
        MaterialReusables.showToast('' + pluginService.getTranslations().tvLoad, 'success');
      }
    }
  };
  //view on load of oppty ctlr from home & back from details
  if (angular.isDefined(sharedDataService.getViewSelected())) {
    if (sharedDataService.getViewSelected() === 'user' || sharedDataService.getViewSelected() == null) {
      $scope.switchViews(true);//user
      $scope.oppListModel.myView = true;
    } else if (sharedDataService.getViewSelected() === 'team' && sharedDataService.getIsManager()) {
      $scope.switchViews(false);//manager
      $scope.oppListModel.myView = false;
    }
  } else {
    $scope.oppListModel.myView = true;
    $scope.switchViews(true);//user default loading user view
  }
  //hide and show view toggle button
  if (sharedDataService.getIsManager()) {
    $scope.switchViewEnabled = true;
  } else {
    $scope.switchViewEnabled = false;
  }
};
opportunityModule.controller('opportunityListController', opportunityListController);
opportunityListController.$inject = ['$rootScope', '$scope', '$timeout', '$state', 'apiService', 'pluginService', 'sharedDataService', 'MaterialReusables', 'dbService'];