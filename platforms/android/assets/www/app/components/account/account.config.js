accountModule.config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('root.account', {
        url : '/account',
        templateUrl: 'app/components/account/account.list.template.html',
        controller: 'accountController',
        pageTitle:'Accounts',
        enableSideMenu:true,
        buttonProp:['search'],
        previousState:'root.home'
    })
}
]);