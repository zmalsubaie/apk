calenderModule.config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('root.calender', {
        url : '/calender',
        templateUrl: 'app/components/calender/calender.template.html',
        controller: 'calenderController',
        pageTitle:'Calendar',
        enableSideMenu:true,
        buttonProp:[],
        previousState:'root.home'
    })
   }
]);