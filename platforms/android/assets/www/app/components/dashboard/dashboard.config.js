dashboardModule.config(['$stateProvider',function ($stateProvider) { 
    $stateProvider.state('root.dashboard',{
            url: '/dashboard',
            templateUrl: 'app/components/dashboard/dashboard.template.html',
            controller: 'dashboardController',
            pageTitle:'Dashboard',
             buttonProp:[''],
            previousState:'root.home'
        })
        .state('root.dashboardlist',{
            url: '/dashboardlist',
            templateUrl: 'app/components/dashboard/dashboardUsersList.template.html',
            controller: 'dashboardListController',
            pageTitle:'Opportunities',
            backbutton:true,
            buttonProp:['search'],
            previousState:'root.dashboard',
            params:{mainTab:'',secTab:'',subTabCount:'',amount:'',text:'',chartSelected:'',loggedInUser:'',topThreeEndIndex:'',mrcCountIndex:''}
        })
             .state('root.ddetails',{
            url: '/dashboarddetails',
            templateUrl: 'app/components/dashboard/dashboardChartDetails.template.html',
            controller: 'dashboardController',
            pageTitle:'Dashboard-Details',
            backbutton:true,
            buttonProp:[''],
            previousState:'root.dashboardlist'
        })
    }
]);