inboxModule.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('root.inbox', {
        url: '/inbox',
        templateUrl: 'app/components/inbox/inbox.template.html',
        controller: 'inboxController',
        pageTitle: 'Inbox',
        backbutton: false,
        previousState: 'root.home',
        buttonProp: ['refresh']
    })
}
]);