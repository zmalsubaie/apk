var dashboardController = function ($scope, $rootScope, $http, $timeout, apiService, sharedDataService, $state, MaterialReusables, $translate, pluginService) {
    $scope.dashboardModel = {
        isLoading: true,
        subCategoryList: [],
        selectedCategory: 'My View',
        selectedCategorySec: 'Lead',
        selectedIndex: 0,
        data: [],
        options: {},
        topFiveDataBarAmount: [],
        topFiveDataBarText: [],
        topThreeDataBarAmount: [],
        topThreeDataBarText: [],
        topThreeDataBarColour: [],
        oppoSubStatusAmount: [],
        oppoSubStatusText: [],
        oppoSubStatusColour: [],
        highCountText: [],
        highCountAmount: [],
        highCountColour: [],
        subStatusLoading: false,
        highCountLoading: false,
        topThreeLoading: false,
        topFiveLoading: false,
        noDataFoundHighCount: false,
        noDataFoundTopThree: false,
        noDataFoundTopFive: false,
        noDataFoundSubStatus: false,
        oppoSubStatusTextFiltered: [],
        mrcBarAmount: [],
        mrcBarText: [],
        mrcLoading: false,
        noDataFoundMrc: false,
        subCategoryListTop: [],
        namesArray: [],
        selectedCategorySecAllView: true,
        noDataTabChange: false,
        noDataHighTabChange: false,
        subordinateListLoading: false,
        subordinateDataList: [],
        subordinateList: [],
        userDataLoading: false,
        userDataShow: false,
        manager: false,
        tabWiseCount: true,
        topTDataTextUnclipped: [],
        topFDataTextUnclipped: [],
        search: false,
        searchText: '',
        enableSearch: false,
        outerTabsChnaged: false,
        topThreeProData: [],
        topThreeRateData: [],
        mrcBarEndIndex: [],
        mainSelectedIndex: 0,
        isNetworkAvailable: false
    };
    var offlineDashboard = {
        offlineCountBySubStatus: [],
        offlineRevenue: [],
        offlineHighCount: [],
        offlineTopThree: [],
        offlineTopFive: [],
        offlineUserList: []
    };
    //common count fetch payload for API call
    var odsCommonPayLoad = {
        "loginName": sharedDataService.getUserDetails().loginName
    };
    var selectedTab = 'Lead';
    var selectedMainTab = 'My View';
    var subTabCount = 0;
    var textSelected = '';
    var amountSelected = '';
    var chartSelected = '';
    var textToDisplay = '';
    var clickEnable = true;
    var endIndexCount = 0;
    var topThreeEndIndex = 0;
    var mrcCountIndex = 0;
    var firstTabSelected = '';
    var secondTabSelected = '';
    var myViewSelec = false;
    var teamViewSelec = false;
    var dummyArray = [];
    firstTabSelected = sharedDataService.getSelectedFirstTab();
    secondTabSelected = sharedDataService.getSelectedSecondTab();
    if (typeof firstTabSelected !== 'undefined' && typeof secondTabSelected !== 'undefined') {
        selectedTab = secondTabSelected;
        selectedMainTab = firstTabSelected;
        if (typeof firstTabSelected !== 'undefined') {
            if (selectedMainTab == 'My View') {
                $scope.dashboardModel.mainSelectedIndex = 0;
            } else if (selectedMainTab == 'Team View') {
                $scope.dashboardModel.mainSelectedIndex = 1;
            } else if (selectedMainTab == 'User') {
                $scope.dashboardModel.mainSelectedIndex = 2;
            }
        }
        $timeout(function () {
            selectedTab = secondTabSelected;
            if (typeof secondTabSelected !== 'undefined') {
                if (selectedTab == 'Lead') {
                    $scope.dashboardModel.selectedIndex = 0;
                } else if (selectedTab == 'Cust:Contact') {
                    $scope.dashboardModel.selectedIndex = 1;
                } else if (selectedTab == 'Cust:Visits') {
                    $scope.dashboardModel.selectedIndex = 2;
                } else if (selectedTab == 'Proposal') {
                    $scope.dashboardModel.selectedIndex = 3;
                } else if (selectedTab == 'Win') {
                    $scope.dashboardModel.selectedIndex = 4;
                } else if (selectedTab == 'Lost') {
                    $scope.dashboardModel.selectedIndex = 5;
                }
            }
        }, 6000);
    } else {
        $scope.dashboardModel.mainSelectedIndex = 0;
        $scope.dashboardModel.selectedIndex = 0;
        selectedTab = 'Lead';
        selectedMainTab = 'My View';
    };
    if (sharedDataService.getIsManager()) {
        $scope.teamView = true;
        $scope.dashboardModel.subCategoryListTop = [
            {
                index: 0,
                //'name': pluginService.getTranslations().myViewList,
                'name': 'My View',
                'isSelected': true
            },
            {
                index: 1,
                //'name': pluginService.getTranslations().teamViewList,
                'name': 'Team View',
                'isSelected': false
            },
            {
                index: 2,
                'name': 'User',
                'isSelected': false
            }
        ];
    } else {
        $scope.teamView = false;
        $scope.dashboardModel.subCategoryListTop = [
            {
                index: 0,
                'name': 'My View',
                'isSelected': true
            }
        ];
    }
    $scope.options = {
        scaleShowVerticalLines: false,
        scales: {
            xAxes: [{
                ticks: {
                    fontSize: 5
                }
            }],
            yAxes: [{
                ticks: {
                    fontSize: 5,
                    min: 0
                }
            }]
        }
    };
    $scope.optionsDoughnut = {
        legend: {
            display: true,
            position: 'left',
            labels: {
                fontSize: 10,
                boxWidth: 8
            }
        }
    };
    //substring text based on length and append with ..
    var clipData = function (data) {
        var cData = '';
        if (typeof (data) != 'undefined') {
            if (data.length > 5) {
                cData = data.substring(0, 10) + '..';
            } else {
                cData = data;
            }
            return cData;
        }
    };
    //get Arabic for the English words
    var arabicValues = function (value) {
        var arabic = '';
        switch (value) {
            case 'Lead':
                arabic = "فرصه";
                break;
            case 'Lost':
                arabic = "فقدان";
                break;
            case 'Call back':
                arabic = "رد الاتصال";
                break;
            case 'No Answer 1':
                arabic = "لا يرد 1";
                break;
            case 'No Answer 2':
                arabic = "لا يرد 2";
                break;
            case 'Set Appt':
                arabic = "تحديد موعد";
                break;
            case 'Confirmed Appt':
                arabic = "الموعد المؤكد";
                break;
            case 'Better offers from competitor':
                arabic = "عرض أفضل من المنافس";
                break;
            case 'Bill experience issue':
                arabic = "مشاكل في الفوترة";
                break;
            case 'Customer experience issue':
                arabic = "خدمة عملاء غير جيدة";
                break;
            case 'He does not know the company':
                arabic = "لا يعمل في الشركة";
                break;
            case 'He has product with competitor':
                arabic = "لديه الخدمة من المنافس";
                break;
            case 'Has Same Product from Consumer':
                arabic = "لديه خدمة مماثلة من STCالأفراد";
                break;
            case 'Line disconnected':
                arabic = "الرقم مفصول";
                break;
            case 'Low Coverage/Tech specs':
                arabic = "تغطية/مواصفات تقنية غير مناسبة";
                break;
            case 'No Answer After Max attempt':
                arabic = "لايرد بعد الحد الأقصى";
                break;
            case 'No Need for service offered':
                arabic = "لا يرغب بالخدمة";
                break;
            case 'other':
                arabic = "غير ذلك";
                break;
            case 'Product offer is too expensive':
                arabic = "كلفة الخدمة عالية";
                break;
            case 'Wrong No':
                arabic = "كل الأرقام غير صحيحة";
                break;
            case 'Completed':
                arabic = "مكتمل";
                break;
            case 'Order Placed':
                arabic = "تم وضع الامر";
                break;
            case 'Discount approval':
                arabic = "موافقة على التخفيض";
                break;
            case 'Sent Proposal':
                arabic = "تم إرسال اقتراح";
                break;
            case 'Sent Quotation':
                arabic = "تم إرسال عرض سعر";
                break;
            case 'Waiting Feedback':
                arabic = "بانتظار الإفادة";
                break;
            case 'Contract Signed':
                arabic = "تم توقيع العقد";
                break;
        }
        if (arabic === '') {
            arabic = value;
        }
        return arabic;
    };
    //get English for the Arabic words
    var englishValues = function (value) {
        var english = '';
        switch (value) {
            case 'فرصه':
                english = "Lead";
                break;
            case 'فقدان':
                english = "Lost";
                break;
            case 'رد الاتصال':
                english = "Call back";
                break;
            case "لا يرد 1":
                english = 'No Answer 1';
                break;
            case "لا يرد 2":
                english = 'No Answer 2';
                break;
            case 'تحديد موعد':
                english = "Set Appt";
                break;
            case "الموعد المؤكد":
                english = "Confirmed Appt";
                break;
            case 'عرض أفضل من المنافس':
                english = "Better offers from competitor";
                break;
            case 'مشاكل في الفوترة':
                english = "Bill experience issue";
                break;
            case 'خدمة عملاء غير جيدة':
                english = "Customer experience issue";
                break;

            case "لا يعمل في الشركة":
                english = 'He does not know the company';
                break;
            case 'لديه الخدمة من المنافس':
                english = " He has product with competitor";
                break;
            case 'لديه خدمة مماثلة من STCالأفراد':
                english = "Has Same Product from Consumer";
                break;
            case 'الرقم مفصول':
                english = "Line disconnected";
                break;
            case 'تغطية/مواصفات تقنية غير مناسبة':
                english = "Low Coverage/Tech specs";
                break;
            case "لايرد بعد الحد الأقصى":
                english = 'No Answer After Max attempt';
                break;
            case "لا يرغب بالخدمة":
                english = 'No Need for service offered';
                break;
            case 'غير ذلك':
                english = "other";
                break;
            case 'كلفة الخدمة عالية':
                english = "Product offer is too expensive";
                break;
            case 'كل الأرقام غير صحيحة':
                english = "Wrong No";
                break;
            case 'مكتمل':
                english = "Completed";
                break;
            case 'تم وضع الامر':
                english = "Order Placed";
                break;
            case 'موافقة على التخفيض':
                english = " Discount approval";
                break;
            case 'تم إرسال اقتراح':
                english = "Sent Proposal";
                break;
            case 'تم إرسال عرض سعر':
                english = "Sent Quotation";
                break;
            case 'بانتظار الإفادة':
                english = "Waiting Feedback";
                break;
            case 'تم توقيع العقد':
                english = "Contract Signed";
                break;
        }
        if (english === '') {
            english = value;
        }
        return english;
    };
    //split and select word on ~ based on app language selected
    function fetchSide(word) {
        var value = '';
        if (word.includes('~')) {
            if (sharedDataService.getLanguage() === 'en_US') {
                value = word.split('~')[0];
            } else {
                value = word.split('~')[1];
            }
        } else {
            value = word;
        }
        return value;
    }
    //tab list
    $scope.dashboardModel.subCategoryList = [
        {
            index: 0,
            'name': 'Lead',
            'isSelected': true,
            'notifierCount': '0'
        },
        {
            index: 1,
            'name': 'Cust:Contact',
            'isSelected': false,
            'notifierCount': '0'
        },
        {
            index: 2,
            'name': 'Cust:Visits',
            'isSelected': false,
            'notifierCount': '0'
        },
        {
            index: 3,
            'name': 'Proposal',
            'isSelected': false,
            'notifierCount': '0'
        },
        {
            index: 4,
            'name': 'Win',
            'isSelected': false,
            'notifierCount': '0'
        },
        {
            index: 5,
            'name': 'Lost',
            'isSelected': false,
            'notifierCount': '0'
        }
    ];
    //user view opportunity count success
    var oppListCountSuccess = function (data) {
        if (data.data == null) {
            $scope.dashboardModel.tabWiseCount = false;
        } else {
            $scope.dashboardModel.tabWiseCount = true;
            $scope.oppByStatus = data.data.odsOppCountofStatusOutput;
            angular.forEach($scope.oppByStatus, function (value, key) {
                if (value.opportunityStatus.includes('Customer Visit')) {
                    if (value.opportunitySubStatus === 'Set Appt') {
                        $scope.dashboardModel.subCategoryList[2].notifierCount = value.countOpportunityId;
                    }
                } else if (value.opportunityStatus.includes('Contact customer')) {
                    if (value.opportunitySubStatus === 'Call back') {
                        $scope.dashboardModel.subCategoryList[1].notifierCount = value.countOpportunityId;
                    }
                } else if (value.opportunityStatus.includes('Proposal')) {
                    if (value.opportunitySubStatus === 'Contract Signed') {
                        $scope.dashboardModel.subCategoryList[3].notifierCount = value.countOpportunityId;
                    }
                } else if (value.opportunityStatus.includes('Win')) {
                    if (value.opportunitySubStatus === 'Order Placed') {
                        $scope.dashboardModel.subCategoryList[4].notifierCount = value.countOpportunityId;
                    }
                } else if (value.opportunityStatus.includes('Lead') && !value.opportunityStatus.includes('Lead Enrichment')) {
                    if (value.opportunitySubStatus === 'Lead') {
                        $scope.dashboardModel.subCategoryList[0].notifierCount = value.countOpportunityId;
                    }
                } else {
                    ////TODO
                }
            });
        }
        $scope.getSubCategoryList();
    };
    //user opportunity count failure
    var oppListCountFailure = function (data) {
        Util.throwError(data.data.Message, MaterialReusables);
        $scope.dashboardModel.subStatusLoading = false;
    }
    //fetch user based opportunity counts API call
    var getUserBasedCount = function () {
        var oppListCountPayload = {
            "login": sharedDataService.getUserDetails().loginName
        }
        apiService.fetchDataFromApi('opp.odsoppcountofstatus', oppListCountPayload, oppListCountSuccess, oppListCountFailure);
    };
    //check MRC data exists or not
    var isMrcCountNull = function (data) {
        if (data !== null && typeof data !== 'undefined') {
            var results = [];
            if (data.length === 1) {
                if (data[0] === '0') {
                    results = [];
                } else {
                    results = ['0'];
                }
            } else {
                for (var i = 0; i < data.length - 1; i++) {
                    if (data[i + 1] !== data[i]) {
                        results.push(data[i]);
                    }
                }
            }
            if (results.length === 0) {
                $scope.dashboardModel.noDataFoundMrc = true;
            } else {
                $scope.dashboardModel.noDataFoundMrc = false;
            }
        } else {
            $scope.dashboardModel.noDataFoundMrc = true;
        }
    };
    //manager view opportunity count success
    var countByStatusSuccess = function (data) {
        if (data.data == null) {
            $scope.dashboardModel.tabWiseCount = false;
        } else {
            $scope.dashboardModel.tabWiseCount = true;
            angular.forEach(data.data.records, function (value, key) {
                if (value.opportunityStatus.includes('Customer Visit')) {
                    $scope.dashboardModel.subCategoryList[2].notifierCount = value.count;
                } else if (value.opportunityStatus.includes('Contact customer')) {
                    $scope.dashboardModel.subCategoryList[1].notifierCount = value.count;
                } else if (value.opportunityStatus.includes('Proposal')) {
                    $scope.dashboardModel.subCategoryList[3].notifierCount = value.count;
                } else if (value.opportunityStatus.includes('Win')) {
                    $scope.dashboardModel.subCategoryList[4].notifierCount = value.count;
                } else if (value.opportunityStatus.includes('Lead') && !(value.opportunityStatus.includes('Lead Enrichment'))) {
                    $scope.dashboardModel.subCategoryList[0].notifierCount = value.count;
                } else {
                    ////TODO
                }
            });
            $scope.getSubCategoryList();
        }
    };
    $scope.getSubCategoryList = function () {
        $scope.dashboardModel.selectedIndex = 0;
        if ($scope.dashboardModel.subCategoryList[0].notifierCount === '0') {
            $scope.dashboardModel.noDataTabChange = true;
        } else {
            $scope.dashboardModel.noDataTabChange = false;
        }
    };
    //manager view opportunity count failure
    var countByStatusError = function (data) {
        Util.throwError(data.data.Message, MaterialReusables);
    };
    //fetch manager based opportunity counts API call
    var countByStatus = function () {
        apiService.fetchDataFromApi('opp.odscountbystatus', odsCommonPayLoad, countByStatusSuccess, countByStatusError);
    };
    //count By Sub Status success- Normal User
    var countBySubStatusSuccess = function (data) {
        $scope.dashboardModel.subStatusLoading = false;
        $scope.leadCountSubStatus = [];
        $scope.leadenrichmentCountSubStatus = [];
        $scope.contactcustomerCountSubStatus = [];
        $scope.customerVisitCountSubStatus = [];
        $scope.proposalCountSubStatus = [];
        $scope.winCountSubStatus = [];
        $scope.lostCountSubStatus = [];
        $scope.leadDataSubStatus = [];
        $scope.customerVisitDataSubStatus = [];
        $scope.leadenrichmentDataSubStatus = [];
        $scope.contactcustomerDataSubStatus = [];
        $scope.proposalDataSubStatus = [];
        $scope.winDataSubStatus = [];
        $scope.lostDataSubStatus = [];
        if (data.data == null) {
            $scope.dashboardModel.noDataFoundSubStatus = true;
        } else {
            $scope.dashboardModel.noDataFoundSubStatus = false;
            offlineDashboard.offlineCountBySubStatus = data.data.records;
            angular.forEach(data.data.records, function (value, key) {
                if (value.opportunityStatus.includes('Lead') && !value.opportunityStatus.includes('Lead Enrichment')) {
                    $scope.leadCountSubStatus.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.leadDataSubStatus.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Customer Visit')) {
                    $scope.customerVisitCountSubStatus.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.customerVisitDataSubStatus.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Contact customer')) {
                    $scope.contactcustomerCountSubStatus.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.contactcustomerDataSubStatus.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Proposal')) {
                    $scope.proposalCountSubStatus.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.proposalDataSubStatus.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Win')) {
                    $scope.winCountSubStatus.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.winDataSubStatus.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Lost')) {
                    $scope.lostCountSubStatus.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.lostDataSubStatus.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Lead Enrichment')) {
                    $scope.leadenrichmentCountSubStatus.push(value.count);
                    $scope.leadenrichmentDataSubStatus.push(value.opportunitySubStatus);
                } else {
                    ////TODO
                }
            });
        }
        $scope.dashboardModel.oppoSubStatusAmount = $scope.leadCountSubStatus;
        $scope.dashboardModel.oppoSubStatusText = $scope.leadDataSubStatus;
        $scope.dashboardModel.oppoSubStatusColour = ['#34c0c2', '#f85d5d', '#db095b', '#a948b6', '#FFFF00', '#32CD32', '#EEE8AA'];
        angular.forEach($scope.dashboardModel.oppoSubStatusText, function (value, key) {
            if (value != "") {
                $scope.dashboardModel.oppoSubStatusTextFiltered.push(value);
            }
        });
    };
    //count By Sub Status failure - Normal User
    var countBySubStatusFailure = function (data) {
        Util.throwError(data.data.Message, MaterialReusables);
        $scope.dashboardModel.subStatusLoading = false;
    };
    //count By Sub Status API call - Normal User
    var countBySubStatus = function () {
        $scope.dashboardModel.subStatusLoading = true;
        if ($scope.dashboardModel.subCategoryListTop[0].isSelected) {
            apiService.fetchDataFromApi('opp.subStatusUser', odsCommonPayLoad, countBySubStatusSuccess, countBySubStatusFailure);
        } else if (sharedDataService.getIsManager() && $scope.dashboardModel.subCategoryListTop[1].isSelected) {
            apiService.fetchDataFromApi('opp.odscountbysubstatus', odsCommonPayLoad, countBySubStatusSuccess, countBySubStatusFailure);
        } else {
            ////TODO
        }
    };
    //High Count Per Status - success
    var highCountSuccess = function (data) {
        $scope.leadHighCount = [];
        $scope.contactcustomerHighCount = [];
        $scope.customerVisitHighCount = [];
        $scope.proposalHighCount = [];
        $scope.winHighCount = [];
        $scope.lostHighCount = [];
        $scope.leadHighCountData = [];
        $scope.customerVisitHighCountData = [];
        $scope.contactcustomerHighCountData = [];
        $scope.proposalHighCountData = [];
        $scope.winHighCountData = [];
        $scope.lostHighCountData = [];
        $scope.dashboardModel.highCountLoading = false;
        if (data.data == null) {
            $scope.dashboardModel.noDataFoundHighCount = true;
        } else {
            $scope.dashboardModel.noDataFoundHighCount = false;
            $scope.highCountData = data.data.records;
            offlineDashboard.offlineHighCount = data.data.records;
            angular.forEach(data.data.records, function (value, key) {
                if (value.opportunityStatus.includes('Lead') && !value.opportunityStatus.includes('Lead Enrichment')) {
                    $scope.leadHighCount.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.leadHighCountData.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Customer Visit')) {
                    $scope.customerVisitHighCount.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.customerVisitHighCountData.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Contact customer')) {
                    $scope.contactcustomerHighCount.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.contactcustomerHighCountData.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Proposal')) {
                    $scope.proposalHighCount.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.proposalHighCountData.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Win')) {
                    $scope.winHighCount.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.winHighCountData.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Lost')) {
                    $scope.lostHighCount.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.lostHighCountData.push(value.opportunitySubStatus);
                } else {
                    ////TODO
                }
            });
            $scope.dashboardModel.highCountAmount = $scope.leadHighCount;
            $scope.dashboardModel.highCountText = fetchSide($scope.leadHighCountData);
        }
    };
    //High Count Per Status - failure
    var highCountFailure = function (data) {
        Util.throwError(data.data.Message, MaterialReusables);
        $scope.dashboardModel.highCountLoading = false;
    };
    //High Count Per Status - API call
    var getHighCount = function () {
        $scope.dashboardModel.highCountLoading = true;
        if ($scope.dashboardModel.subCategoryListTop[0].isSelected) {
            apiService.fetchDataFromApi('opp.highCountUser', odsCommonPayLoad, highCountSuccess, highCountFailure);
        } else if (sharedDataService.getIsManager() && $scope.dashboardModel.subCategoryListTop[1].isSelected) {
            apiService.fetchDataFromApi('opp.odsHighCount', odsCommonPayLoad, highCountSuccess, highCountFailure);
        } else {
            ////TODO
        }
    };
    //set data to bar charts
    var commonBarData = function (data) {
        var sumRevAmtZero = true;
        angular.forEach(data, function (value, key) {
            if ((typeof value.sumRevAmt !== 'undefined') && (value.sumRevAmt !== '0')) {
                $scope.dashboardModel.topFiveDataBarAmount.push(value.sumRevAmt);
                $scope.dashboardModel.topFDataTextUnclipped.push(value.opportunityName);
                $scope.dashboardModel.topFiveDataBarText.push(value.opportunityName);
            }
            if (value.sumRevAmt !== '0') {
                sumRevAmtZero = false;
            }
        });
        if (sumRevAmtZero) {
            $scope.dashboardModel.noDataFoundTopFive = true;
        }
    };
    //MRC Per Status - success
    var mrcSuccess = function (data) {
        $scope.dashboardModel.mrcLoading = false;
        $scope.leadCountMrc = [];
        $scope.leadenrichmentCountMrc = [];
        $scope.contactcustomerCountMrc = [];
        $scope.customerVisitCountMrc = [];
        $scope.proposalCountMrc = [];
        $scope.winCountMrc = [];
        $scope.lostCountMrc = [];
        $scope.leadDataMrc = [];
        $scope.customerVisitDataMrc = [];
        $scope.leadenrichmentDataMrc = [];
        $scope.contactcustomerDataMrc = [];
        $scope.proposalDataMrc = [];
        $scope.winDataMrc = [];
        $scope.lostDataMrc = [];
        $scope.leadCountMrcEndIndex = [];
        $scope.customerVisitCountMrcEndIndex = [];
        $scope.contactcustomerCountMrcEndIndex = [];
        $scope.proposalCountMrcEndIndex = [];
        $scope.winCountMrcEndIndex = [];
        $scope.lostCountMrcEndIndex = [];
        if (data.data == null) {
            $scope.dashboardModel.noDataFoundMrc = true;
        } else {
            $scope.dashboardModel.noDataFoundMrc = false;
            offlineDashboard.offlineRevenue = data.data.records;
            angular.forEach(data.data.records, function (value, key) {
                if (value.opportunityStatus.includes('Lead') && !value.opportunityStatus.includes('Lead Enrichment')) {
                    $scope.leadCountMrc.push(value.totalMRC);
                    $scope.leadCountMrcEndIndex.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.leadDataMrc.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Customer Visit')) {
                    $scope.customerVisitCountMrc.push(value.totalMRC);
                    $scope.customerVisitCountMrcEndIndex.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.customerVisitDataMrc.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Contact customer')) {
                    $scope.contactcustomerCountMrc.push(value.totalMRC);
                    $scope.contactcustomerCountMrcEndIndex.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.contactcustomerDataMrc.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Proposal')) {
                    $scope.proposalCountMrc.push(value.totalMRC);
                    $scope.proposalCountMrcEndIndex.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.proposalDataMrc.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Win')) {
                    $scope.winCountMrc.push(value.totalMRC);
                    $scope.winCountMrcEndIndex.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.winDataMrc.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Lost')) {
                    $scope.lostCountMrc.push(value.totalMRC);
                    $scope.lostCountMrcEndIndex.push(value.count);
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        value.opportunitySubStatus = arabicValues(value.opportunitySubStatus);
                    }
                    $scope.lostDataMrc.push(value.opportunitySubStatus);
                } else if (value.opportunityStatus.includes('Lead Enrichment')) {
                    $scope.leadenrichmentCountMrc.push(value.totalMRC);
                    $scope.leadenrichmentDataMrc.push(value.opportunitySubStatus);
                } else {
                    ////TODO
                }
            });
        }
        $scope.dashboardModel.mrcBarEndIndex = $scope.leadCountMrcEndIndex;
        $scope.dashboardModel.mrcBarAmount = $scope.leadCountMrc;
        $scope.dashboardModel.mrcBarText = $scope.leadDataMrc;
        isMrcCountNull($scope.leadCountMrc);
        $scope.dashboardModel.oppoSubStatusColour = ['#34c0c2', '#f85d5d', '#db095b', '#a948b6', '#FFFF00', '#32CD32', '#EEE8AA'];
        angular.forEach($scope.dashboardModel.oppoSubStatusText, function (value, key) {
            if (value !== "") {
                $scope.dashboardModel.oppoSubStatusTextFiltered.push(value);
            }
        });
    };
    //MRC Per Status - failure
    var mrcFailure = function (data) {
        Util.throwError(data.data.Message, MaterialReusables);
        $scope.dashboardModel.mrcLoading = false;
    }
    //MRC Per Status - API call
    var getMrc = function () {
        $scope.dashboardModel.mrcLoading = true;
        if ($scope.dashboardModel.subCategoryListTop[0].isSelected) {
            apiService.fetchDataFromApi('opp.mrcUser', odsCommonPayLoad, mrcSuccess, mrcFailure);
        } else if (sharedDataService.getIsManager() && $scope.dashboardModel.subCategoryListTop[1].isSelected) {
            apiService.fetchDataFromApi('opp.odsMrc', odsCommonPayLoad, mrcSuccess, mrcFailure);
        } else {
            ////TODO
        }
    };
    //Top 3 Products - process data to display
    var topThreeProductDataDisplay = function (count, data, productname, rate) {
        try {
            var number = data.length;
            if (number === 0) {
                $scope.dashboardModel.noDataFoundTopThree = true;
            } else {
                $scope.dashboardModel.noDataFoundTopThree = false;
            }
            $scope.topThreeDataFinal = data;
            $scope.topThreeCountFinal = count;
            if (number === 1) {
                $scope.dataTopThree = [];
                $scope.dashboardModel.topThreeDataBarAmount = [$scope.topThreeCountFinal[number - 1]];
                $scope.dashboardModel.topThreeDataBarText = [clipData($scope.topThreeDataFinal[number - 1])];
                $scope.dashboardModel.topTDataTextUnclipped = [$scope.topThreeDataFinal[number - 1]];
                $scope.dashboardModel.topThreeProData = [productname[number - 1]];
                $scope.dashboardModel.topThreeRateData = [rate[number - 1]];
            } else if (number === 2) {
                $scope.dataTopThree = [];
                $scope.dashboardModel.topThreeDataBarAmount = [$scope.topThreeCountFinal[number - 1], $scope.topThreeCountFinal[number - 2]];
                $scope.dashboardModel.topThreeDataBarText = [clipData($scope.topThreeDataFinal[number - 1]), clipData($scope.topThreeDataFinal[number - 2])];
                $scope.dashboardModel.topTDataTextUnclipped = [$scope.topThreeDataFinal[number - 1], $scope.topThreeDataFinal[number - 2]];
                $scope.dashboardModel.topThreeProData = [productname[number - 1], productname[number - 2]];
                $scope.dashboardModel.topThreeRateData = [rate[number - 1], rate[number - 2]];
            } else {
                $scope.dataTopThree = [];
                $scope.dashboardModel.topThreeDataBarAmount = [$scope.topThreeCountFinal[number - 1], $scope.topThreeCountFinal[number - 2], $scope.topThreeCountFinal[number - 3]];
                $scope.dashboardModel.topThreeDataBarText = [clipData($scope.topThreeDataFinal[number - 1]), clipData($scope.topThreeDataFinal[number - 2]), clipData($scope.topThreeDataFinal[number - 3])];
                $scope.dashboardModel.topTDataTextUnclipped = [$scope.topThreeDataFinal[number - 1], $scope.topThreeDataFinal[number - 2], $scope.topThreeDataFinal[number - 3]];
                $scope.dashboardModel.topThreeProData = [productname[number - 1], productname[number - 2], productname[number - 3]];
                $scope.dashboardModel.topThreeRateData = [rate[number - 1], rate[number - 2], rate[number - 3]];
            }
        } catch (e) {
            ////TODO
        }
    };
    //Top 3 Products - success
    var topThreeSuccess = function (data) {
        $scope.dashboardModel.topThreeLoading = false;
        $scope.leadCountTThree = [];
        $scope.leadenrichmentCountTThree = [];
        $scope.contactcustomerCountTThree = [];
        $scope.customerVisitCountTThree = [];
        $scope.proposalCountTThree = [];
        $scope.winCountTThree = [];
        $scope.lostCountTThree = [];
        $scope.leadDataTThree = [];
        $scope.customerVisitDataTThree = [];
        $scope.leadenrichmentDataTThree = [];
        $scope.contactcustomerDataTThree = [];
        $scope.proposalDataTThree = [];
        $scope.winDataTThree = [];
        $scope.lostDataTThree = [];
        $scope.leadDataProTThree = [];
        $scope.leadDataRateTThree = [];
        $scope.customerVisitDataProTThree = [];
        $scope.customerVisitDataRateTThree = [];
        $scope.contactcustomerDataProTThree = [];
        $scope.contactcustomerDataRateTThree = [];
        $scope.proposalDataProTThree = [];
        $scope.proposalDataRateTThree = [];
        $scope.winDataProTThree = [];
        $scope.winDataRateTThree = [];
        $scope.lostDataProTThree = [];
        $scope.lostDataRateTThree = [];
        if (data.data == null) {
            $scope.dashboardModel.noDataFoundTopThree = true;
        } else {
            $scope.dashboardModel.noDataFoundTopThree = false;
            $scope.topThreeData = data.data.records;
            offlineDashboard.offlineTopThree = data.data.records;
            $scope.topThreeData.sort(function (a, b) {
                return a.count - b.count
            })
            angular.forEach($scope.topThreeData, function (value, key) {
                if (value.opportunityStatus.includes('Lead') && !value.opportunityStatus.includes('Lead Enrichment')) {
                    $scope.leadCountTThree.push(value.count);
                    $scope.leadDataTThree.push(value.productName + '' + value.ratePlan);
                    $scope.leadDataProTThree.push(value.productName);
                    $scope.leadDataRateTThree.push(value.ratePlan);
                } else if (value.opportunityStatus.includes('Customer Visit')) {
                    $scope.customerVisitCountTThree.push(value.count);
                    $scope.customerVisitDataTThree.push(value.productName + '' + value.ratePlan);
                    $scope.customerVisitDataProTThree.push(value.productName);
                    $scope.customerVisitDataRateTThree.push(value.ratePlan);
                } else if (value.opportunityStatus.includes('Contact customer')) {
                    $scope.contactcustomerCountTThree.push(value.count);
                    $scope.contactcustomerDataTThree.push(value.productName + '' + value.ratePlan);
                    $scope.contactcustomerDataProTThree.push(value.productName);
                    $scope.contactcustomerDataRateTThree.push(value.ratePlan);
                } else if (value.opportunityStatus.includes('Proposal')) {
                    $scope.proposalCountTThree.push(value.count);
                    $scope.proposalDataTThree.push(value.productName + '' + value.ratePlan);
                    $scope.proposalDataProTThree.push(value.productName);
                    $scope.proposalDataRateTThree.push(value.ratePlan);
                } else if (value.opportunityStatus.includes('Win')) {
                    $scope.winCountTThree.push(value.count);
                    $scope.winDataTThree.push(value.productName + '' + value.ratePlan);
                    $scope.winDataProTThree.push(value.productName);
                    $scope.winDataRateTThree.push(value.ratePlan);
                } else if (value.opportunityStatus.includes('Lost')) {
                    $scope.lostCountTThree.push(value.count);
                    $scope.lostDataTThree.push(value.productName);
                    $scope.lostDataProTThree.push(value.productName);
                    $scope.lostDataRateTThree.push(value.ratePlan);
                } else if (value.opportunityStatus.includes('Lead Enrichment')) {
                    $scope.leadenrichmentCountTThree.push(value.count);
                    $scope.leadenrichmentDataTThree.push(value.productName + '' + value.ratePlan);
                } else {
                    ////TODO
                }
            });
            topThreeProductDataDisplay($scope.leadCountTThree, $scope.leadDataTThree, $scope.leadDataProTThree, $scope.leadDataRateTThree);
            $scope.firstValue = 0;
        }
    };
    //Top 3 Products - failure
    var topThreeFailure = function (data) {
        Util.throwError(data.data.Message, MaterialReusables);
        $scope.dashboardModel.topThreeLoading = false;
    }
    //Top 3 Products - API call
    var getTopThree = function () {
        $scope.dashboardModel.topThreeLoading = true;
        if (!sharedDataService.getIsManager() && $scope.dashboardModel.subCategoryListTop[0].isSelected) {
            apiService.fetchDataFromApi('opp.topThreeUser', odsCommonPayLoad, topThreeSuccess, topThreeFailure);
        } else if (sharedDataService.getIsManager() && $scope.dashboardModel.subCategoryListTop[1].isSelected) {
            apiService.fetchDataFromApi('opp.odsTopThree', odsCommonPayLoad, topThreeSuccess, topThreeFailure);
        } else if (sharedDataService.getIsManager() && $scope.dashboardModel.subCategoryListTop[0].isSelected) {
            apiService.fetchDataFromApi('opp.topThreeUser', odsCommonPayLoad, topThreeSuccess, topThreeFailure);
        } else {
            ////TODO
        }
    };
    //Top 5 Opportunities - Success
    var topFiveSuccess = function (data) {
        $scope.dashboardModel.topFiveLoading = false;
        $scope.dashboardModel.topFiveDataBarAmount = [];
        $scope.dashboardModel.topFiveDataBarText = [];
        if (data.data == null) {
            $scope.dashboardModel.noDataFoundTopFive = true;
        } else {
            $scope.dashboardModel.noDataFoundTopFive = false;
            offlineDashboard.offlineTopFive = data.data.records;
            $scope.topFiveData = data.data.records;
            commonBarData($scope.topFiveData);
        }
    };
    //Top 5 Opportunities - failure
    var topFiveFailure = function (data) {
        Util.throwError(data.data.Message, MaterialReusables);
        $scope.dashboardModel.topFiveLoading = false;
    };
    //Top 5 Opportunities - API call
    var getTopFive = function (status) {
        var topFivePayload = {
            "loginName": sharedDataService.getUserDetails().loginName,
            "opportunityStatus": status
        };
        $scope.dashboardModel.topFiveLoading = true;
        if ($scope.dashboardModel.subCategoryListTop[0].isSelected) {
            apiService.fetchDataFromApi('opp.topFiveUser', topFivePayload, topFiveSuccess, topFiveFailure);
        } else if (sharedDataService.getIsManager() && $scope.dashboardModel.subCategoryListTop[1].isSelected) {
            apiService.fetchDataFromApi('opp.odsTopFive', topFivePayload, topFiveSuccess, topFiveFailure);
        } else {
            ////TODO
        }
    };
    //Top 5 API call invoke on tab change
    $scope.dashBoardTabChangesSecList = function (selectedIndex) {
        $scope.dashboardModel.selectedCategorySec = selectedIndex.name;
        selectedTab = selectedIndex.name;
        //top5 products API call based on status
        if (sharedDataService.getNetworkState()) {
            $scope.dashboardModel.isNetworkAvailable = true;
            var currentStatus = '';
            if (selectedIndex.name === 'Lead') {
                currentStatus = 'Lead ~ فرصه';
            } else if (selectedIndex.name === 'Cust:Contact') {
                currentStatus = 'Contact customer ~ الاتصال بالعميل';
            } else if (selectedIndex.name === 'Cust:Visits') {
                currentStatus = 'Customer Visit ~ زياره العميل';
            } else if (selectedIndex.name === 'Lost') {
                currentStatus = 'Lost ~ فقدان';
            } else if (selectedIndex.name === 'Proposal') {
                currentStatus = 'Proposal ~ اقتراح';
            } else if (selectedIndex.name === 'Win') {
                currentStatus = 'Win ~ الربح';
            } else {
                ////TODO
            }
            getTopFive(currentStatus);
        } else {
            $scope.dashboardModel.isNetworkAvailable = false;
        }
        angular.forEach($scope.dashboardModel.subCategoryList, function (key, val) {
            if (selectedIndex.index === key.index) {
                key.isSelected = true;
            } else {
                key.isSelected = false;
            }
        });
        if (selectedIndex.notifierCount === "0" && selectedIndex.name !== "Lost") {
            $scope.dashboardModel.noDataTabChange = true;
        } else {
            $scope.dashboardModel.noDataTabChange = false;
        }
        if ($scope.dashboardModel.selectedCategorySec === "Lead") {
            $scope.dashboardModel.highCountAmount = [];
            $scope.dashboardModel.highCountText = [];
            $scope.dashboardModel.topFiveDataBarAmount = [];
            $scope.dashboardModel.topFiveDataBarText = [];
            $scope.dashboardModel.oppoSubStatusAmount = $scope.leadCountSubStatus;
            $scope.dashboardModel.oppoSubStatusText = $scope.leadDataSubStatus;
            $scope.dashboardModel.highCountAmount = $scope.leadHighCount;
            $scope.dashboardModel.highCountText = $scope.leadHighCountData;
            $scope.dashboardModel.mrcBarAmount = $scope.leadCountMrc;
            $scope.dashboardModel.mrcBarText = $scope.leadDataMrc;
            $scope.dashboardModel.mrcBarEndIndex = $scope.leadCountMrcEndIndex
            if (typeof ($scope.dashboardModel.oppoSubStatusAmount) !== 'undefined') {
                if ($scope.dashboardModel.oppoSubStatusAmount.length === 0 && $scope.dashboardModel.oppoSubStatusText.length === 0) {
                    $scope.dashboardModel.noDataFoundSubStatus = true;
                } else {
                    $scope.dashboardModel.noDataFoundSubStatus = false;
                }
            }
            if (typeof ($scope.dashboardModel.highCountAmount) !== 'undefined') {
                if ($scope.dashboardModel.highCountAmount.length === 0 && $scope.dashboardModel.highCountText.length === 0) {
                    $scope.dashboardModel.noDataFoundHighCount = true;
                } else {
                    $scope.dashboardModel.noDataFoundHighCount = false;
                }
            }
            if (typeof ($scope.dashboardModel.mrcBarAmount) !== 'undefined') {
                if ($scope.dashboardModel.mrcBarAmount.length === 0 && $scope.dashboardModel.mrcBarText.length === 0) {
                    $scope.dashboardModel.noDataFoundMrc = true;
                } else {
                    $scope.dashboardModel.noDataFoundMrc = false;
                }
            }
            isMrcCountNull($scope.dashboardModel.mrcBarAmount);
            topThreeProductDataDisplay($scope.leadCountTThree, $scope.leadDataTThree, $scope.leadDataProTThree, $scope.leadDataRateTThree);
        } else if ($scope.dashboardModel.selectedCategorySec === "Win") {
            $scope.dashboardModel.highCountAmount = [];
            $scope.dashboardModel.highCountText = [];
            $scope.dashboardModel.topFiveDataBarAmount = [];
            $scope.dashboardModel.topFiveDataBarText = [];
            $scope.dashboardModel.oppoSubStatusAmount = $scope.winCountSubStatus;
            $scope.dashboardModel.oppoSubStatusText = $scope.winDataSubStatus;
            $scope.dashboardModel.highCountAmount = $scope.winHighCount;
            $scope.dashboardModel.highCountText = $scope.winHighCountData;
            $scope.dashboardModel.mrcBarAmount = $scope.winCountMrc;
            $scope.dashboardModel.mrcBarText = $scope.winDataMrc;
            $scope.dashboardModel.mrcBarEndIndex = $scope.winCountMrcEndIndex;
            if ($scope.dashboardModel.oppoSubStatusAmount.length === 0 && $scope.dashboardModel.oppoSubStatusText.length === 0) {
                $scope.dashboardModel.noDataFoundSubStatus = true;
            } else {
                $scope.dashboardModel.noDataFoundSubStatus = false;
            }
            if (typeof $scope.dashboardModel.highCountAmount !== 'undefined') {
                if ($scope.dashboardModel.highCountAmount.length === 0 && $scope.dashboardModel.highCountText.length === 0) {
                    $scope.dashboardModel.noDataFoundHighCount = true;
                } else {
                    $scope.dashboardModel.noDataFoundHighCount = false;
                }
            }
            isMrcCountNull($scope.dashboardModel.mrcBarAmount);
            topThreeProductDataDisplay($scope.winCountTThree, $scope.winDataTThree, $scope.winDataProTThree, $scope.winDataRateTThree);
            if ($scope.dashboardModel.mrcBarAmount.length === 0 && $scope.dashboardModel.mrcBarText.length === 0) {
                $scope.dashboardModel.noDataFoundMrc = true;
            } else {
                $scope.dashboardModel.noDataFoundMrc = false;
            }
        } else if ($scope.dashboardModel.selectedCategorySec === "Cust:Visits") {
            $scope.dashboardModel.highCountAmount = [];
            $scope.dashboardModel.highCountText = [];
            $scope.dashboardModel.topFiveDataBarAmount = [];
            $scope.dashboardModel.topFiveDataBarText = [];
            $scope.dashboardModel.oppoSubStatusAmount = $scope.customerVisitCountSubStatus;
            $scope.dashboardModel.oppoSubStatusText = $scope.customerVisitDataSubStatus;
            $scope.dashboardModel.highCountAmount = $scope.customerVisitHighCount;
            $scope.dashboardModel.highCountText = $scope.customerVisitHighCountData;
            $scope.dashboardModel.mrcBarAmount = $scope.customerVisitCountMrc;
            $scope.dashboardModel.mrcBarText = $scope.customerVisitDataMrc;
            $scope.dashboardModel.mrcBarEndIndex = $scope.customerVisitCountMrcEndIndex;
            if ($scope.dashboardModel.oppoSubStatusAmount.length === 0 && $scope.dashboardModel.oppoSubStatusText.length === 0) {
                $scope.dashboardModel.noDataFoundSubStatus = true;
            } else {
                $scope.dashboardModel.noDataFoundSubStatus = false;
            }
            if (typeof $scope.dashboardModel.highCountAmount !== 'undefined') {
                if ($scope.dashboardModel.highCountAmount.length === 0 && $scope.dashboardModel.highCountText.length === 0) {
                    $scope.dashboardModel.noDataFoundHighCount = true;
                } else {
                    $scope.dashboardModel.noDataFoundHighCount = false;
                }
            }
            if ($scope.dashboardModel.mrcBarAmount.length === 0 && $scope.dashboardModel.mrcBarText.length === 0) {
                $scope.dashboardModel.noDataFoundMrc = true;
            } else {
                $scope.dashboardModel.noDataFoundMrc = false;
            }
            if ($scope.dashboardModel.mrcBarAmount.length === 0 && $scope.dashboardModel.mrcBarText.length === 0) {
                $scope.dashboardModel.noDataFoundMrc = true;
            } else {
                $scope.dashboardModel.noDataFoundMrc = false;
            }
            isMrcCountNull($scope.dashboardModel.mrcBarAmount);
            topThreeProductDataDisplay($scope.customerVisitCountTThree, $scope.customerVisitDataTThree, $scope.customerVisitDataProTThree, $scope.customerVisitDataRateTThree);
        } else if ($scope.dashboardModel.selectedCategorySec === "Cust:Contact") {
            $scope.dashboardModel.highCountAmount = [];
            $scope.dashboardModel.highCountText = [];
            $scope.dashboardModel.topFiveDataBarAmount = [];
            $scope.dashboardModel.topFiveDataBarText = [];
            $scope.dashboardModel.oppoSubStatusAmount = $scope.contactcustomerCountSubStatus;
            $scope.dashboardModel.oppoSubStatusText = $scope.contactcustomerDataSubStatus;
            $scope.dashboardModel.highCountAmount = $scope.contactcustomerHighCount;
            $scope.dashboardModel.highCountText = $scope.contactcustomerHighCountData;
            $scope.dashboardModel.mrcBarAmount = $scope.contactcustomerCountMrc;
            $scope.dashboardModel.mrcBarText = $scope.contactcustomerDataMrc;
            $scope.dashboardModel.mrcBarEndIndex = $scope.contactcustomerCountMrcEndIndex;
            if ($scope.dashboardModel.oppoSubStatusAmount.length === 0 && $scope.dashboardModel.oppoSubStatusText.length === 0) {
                $scope.dashboardModel.noDataFoundSubStatus = true;
            } else {
                $scope.dashboardModel.noDataFoundSubStatus = false;
            }
            if (typeof $scope.dashboardModel.highCountAmount !== 'undefined') {
                if ($scope.dashboardModel.highCountAmount.length === 0 && $scope.dashboardModel.highCountText.length === 0) {
                    $scope.dashboardModel.noDataFoundHighCount = true;
                } else {
                    $scope.dashboardModel.noDataFoundHighCount = false;
                }
            }
            if ($scope.dashboardModel.mrcBarAmount.length === 0 && $scope.dashboardModel.mrcBarText.length === 0) {
                $scope.dashboardModel.noDataFoundMrc = true;
            } else {
                $scope.dashboardModel.noDataFoundMrc = false;
            }
            if ($scope.dashboardModel.mrcBarAmount.length === 0 && $scope.dashboardModel.mrcBarText.length === 0) {
                $scope.dashboardModel.noDataFoundMrc = true;
            } else {
                $scope.dashboardModel.noDataFoundMrc = false;
            }
            isMrcCountNull($scope.dashboardModel.mrcBarAmount);
            topThreeProductDataDisplay($scope.contactcustomerCountTThree, $scope.contactcustomerDataTThree, $scope.contactcustomerDataProTThree, $scope.contactcustomerDataRateTThree);
        } else if ($scope.dashboardModel.selectedCategorySec === "Proposal") {
            $scope.dashboardModel.highCountAmount = [];
            $scope.dashboardModel.highCountText = [];
            $scope.dashboardModel.topFiveDataBarAmount = [];
            $scope.dashboardModel.topFiveDataBarText = [];
            $scope.dashboardModel.oppoSubStatusAmount = $scope.proposalCountSubStatus;
            $scope.dashboardModel.oppoSubStatusText = $scope.proposalDataSubStatus;
            $scope.dashboardModel.highCountAmount = $scope.proposalHighCount;
            $scope.dashboardModel.highCountText = $scope.proposalHighCountData;
            $scope.dashboardModel.mrcBarAmount = $scope.proposalCountMrc;
            $scope.dashboardModel.mrcBarText = $scope.proposalDataMrc;
            $scope.dashboardModel.mrcBarEndIndex = $scope.proposalCountMrcEndIndex;
            if ($scope.dashboardModel.oppoSubStatusAmount.length === 0 && $scope.dashboardModel.oppoSubStatusText.length === 0) {
                $scope.dashboardModel.noDataFoundSubStatus = true;
            } else {
                $scope.dashboardModel.noDataFoundSubStatus = false;
            }
            if (typeof $scope.dashboardModel.highCountAmount !== 'undefined') {
                if ($scope.dashboardModel.highCountAmount.length === 0 && $scope.dashboardModel.highCountText.length === 0) {
                    $scope.dashboardModel.noDataFoundHighCount = true;
                } else {
                    $scope.dashboardModel.noDataFoundHighCount = false;
                }
            }
            if ($scope.dashboardModel.mrcBarAmount.length === 0 && $scope.dashboardModel.mrcBarText.length === 0) {
                $scope.dashboardModel.noDataFoundMrc = true;
            } else {
                $scope.dashboardModel.noDataFoundMrc = false;
            }
            isMrcCountNull($scope.dashboardModel.mrcBarAmount);
            topThreeProductDataDisplay($scope.proposalCountTThree, $scope.proposalDataTThree, $scope.proposalDataProTThree, $scope.proposalDataRateTThree);
        } else if ($scope.dashboardModel.selectedCategorySec === "Lost") {
            $scope.dashboardModel.highCountAmount = [];
            $scope.dashboardModel.highCountText = [];
            $scope.dashboardModel.topFiveDataBarAmount = [];
            $scope.dashboardModel.topFiveDataBarText = [];
            $scope.dashboardModel.oppoSubStatusAmount = $scope.lostCountSubStatus;
            $scope.dashboardModel.oppoSubStatusText = $scope.lostDataSubStatus;
            $scope.dashboardModel.highCountAmount = $scope.lostHighCount;
            $scope.dashboardModel.highCountText = $scope.lostHighCountData;
            $scope.dashboardModel.mrcBarAmount = $scope.lostCountMrc;
            $scope.dashboardModel.mrcBarText = $scope.lostDataMrc;
            $scope.dashboardModel.mrcBarEndIndex = $scope.lostCountMrcEndIndex;
            if ($scope.dashboardModel.oppoSubStatusAmount.length === 0 && $scope.dashboardModel.oppoSubStatusText.length === 0) {
                $scope.dashboardModel.noDataFoundSubStatus = true;
            } else {
                $scope.dashboardModel.noDataFoundSubStatus = false;
            }
            if (typeof $scope.dashboardModel.highCountAmount !== 'undefined') {
                if ($scope.dashboardModel.highCountAmount.length === 0 && $scope.dashboardModel.highCountText.length === 0) {
                    $scope.dashboardModel.noDataFoundHighCount = true;
                } else {
                    $scope.dashboardModel.noDataFoundHighCount = false;
                }
            }
            if ($scope.dashboardModel.mrcBarAmount.length === 0 && $scope.dashboardModel.mrcBarText.length === 0) {
                $scope.dashboardModel.noDataFoundMrc = true;
            } else {
                $scope.dashboardModel.noDataFoundMrc = false;
            }
            isMrcCountNull($scope.dashboardModel.mrcBarAmount);
            topThreeProductDataDisplay($scope.lostCountTThree, $scope.lostDataTThree, $scope.lostDataProTThree, $scope.lostDataRateTThree);
        }
    };
    //fetch DB 5 APIs on load
    var fiveApiCalls = function () {
        $timeout(function () {
            countBySubStatus();
        }, 1000);
        $timeout(function () {
            getMrc();
        }, 2000);
        $timeout(function () {
            getHighCount();
        }, 3000);
        $timeout(function () {
            getTopThree();
        }, 4000);
        $timeout(function () {
            getTopFive('Lead ~ فرصه');
        }, 5000);
    };
    //set no data display messages based on network conenction
    if (!sharedDataService.getNetworkState()) {
        $scope.dashboardModel.isNetworkAvailable = false;
        $scope.dashboardModel.noDataFoundSubStatus = true;
        $scope.dashboardModel.noDataFoundMrc = true;
        $scope.dashboardModel.noDataFoundHighCount = true;
        $scope.dashboardModel.noDataFoundTopThree = true;
        $scope.dashboardModel.noDataFoundTopFive = true;
    } else {
        $scope.dashboardModel.isNetworkAvailable = true;
    };
    //close user based count view
    $scope.closeUserCounts = function (data) {
        data.userDataShow = false;
    };
    //fetch user image
    var fetchUSerProfileImage = function (userData) {
        var downloadSuccess = function (data) {
            userData.fetchingImage = false;
            if (data.data.photo !== null) {
                userData.userImage = 'data:image/jpeg;base64,' + data.data.photo;
            } else {
                userData.userImage = null;
            }
        };
        var downloadError = function (data) {
            userData.fetchingImage = false;
            userData.userImage = null;
        };
        var imagePayload = {
            "userId": userData.subordinateLogin
        };
        userData.fetchingImage = true;
        apiService.fetchDataFromApi('user.image', imagePayload, downloadSuccess, downloadError);
    };
    //user based opportunity count API call of subordinates
    $scope.fetchUserBasedCounts = function (userData) {
        userData.leadCount = 0;
        userData.custVisitCount = 0;
        userData.contactcustCount = 0;
        userData.proposalCount = 0;
        userData.winCount = 0;
        userData.lostCount = 0;
        var userStatusCountPayLoad = {
            "login": userData.subordinateLogin
        };
        var userStatusCountSuccess = function (data) {
            if (data.data !== null) {
                if (data.data.odsOppCountofStatusOutput !== null) {
                    offlineDashboard.offlineUserList = data.data.odsOppCountofStatusOutput;
                    angular.forEach(data.data.odsOppCountofStatusOutput, function (value, key) {
                        if ((value.opportunityStatus.includes('Lead')) && (!value.opportunityStatus.includes('Lead Enrichment'))) {
                            userData.leadCount = value.countOpportunityId;
                        } else if (value.opportunityStatus.includes('Customer Visit')) {
                            userData.custVisitCount = value.countOpportunityId;
                        } else if (value.opportunityStatus.includes('Contact customer')) {
                            userData.contactcustCount = value.countOpportunityId;
                        } else if (value.opportunityStatus.includes('Proposal')) {
                            userData.proposalCount = value.countOpportunityId;
                        } else if (value.opportunityStatus.includes('Win')) {
                            userData.winCount = value.countOpportunityId;
                        } else if (value.opportunityStatus.includes('Lost')) {
                            userData.lostCount = value.countOpportunityId;
                        } else {
                            ////TODO
                        }
                    });
                    userData.userDataShow = true;
                }
            } else {
                userData.leadCount = 0;
                userData.custVisitCount = 0;
                userData.contactcustCount = 0;
                userData.proposalCount = 0;
                userData.winCount = 0;
                userData.lostCount = 0;
                //MaterialReusables.showToast('' + pluginService.getTranslations().userCountNull, 'warning');
            }
            userData.userDataShow = true;
            userData.userDataLoading = false;
        };
        var userStatusCountError = function (data) {
            Util.throwError(data.data.Message, MaterialReusables);
            userData.userDataLoading = false;
        };
        userData.userDataLoading = true;
        apiService.fetchDataFromApi('opp.odsoppcountofstatus', userStatusCountPayLoad, userStatusCountSuccess, userStatusCountError);
        fetchUSerProfileImage(userData);
        //apiService.fetchDataFromApi('opp.odscountbystatus', userStatusCountPayLoad, userStatusCountSuccess, userStatusCountError);
    };
    //user name search in suboridnates list tab
    $scope.$on('openSearch', function (event, args) {
        $scope.dashboardModel.search = !$scope.dashboardModel.search;
        if (!$scope.dashboardModel.search) {
            $scope.dashboardModel.searchText = '';
        }
    });
    //user name filter upon typing in suboridnates list tab
    $scope.searchFilter = function (obj) {
        if (typeof obj !== 'undefined') {
            var re = new RegExp($scope.dashboardModel.searchText, 'i');
            return !$scope.dashboardModel.searchText || re.test(obj.subordinateLogin);
        }
    };
    //right swipe between tabs
    $scope.loadNextCategory = function () {
        if ($scope.dashboardModel.selectedIndex < 6 && !$scope.dashboardModel.topFiveLoading) {
            $scope.dashboardModel.selectedIndex = $scope.dashboardModel.selectedIndex + 1;
        }
    };
    //left swipe between tabs
    $scope.loadPrevCategory = function () {
        if ($scope.dashboardModel.selectedIndex > 0 && !$scope.dashboardModel.topFiveLoading) {
            $scope.dashboardModel.selectedIndex = $scope.dashboardModel.selectedIndex - 1;
        }
    };
    $scope.onClick = function (points, evt) {
        ///TODO
    };
    $scope.chart;
    //graph click popup with details
    $scope.chartClickOne = function (event, amount, text) {
        var pointsClicked = $scope.chart.getBarsAtEvent(event);
        if (!pointsClicked || pointsClicked.length == 0)
            return;
        if (pointsClicked[0].label)
            $scope.label = 'You clicked on ' + pointsClicked[0].label;
    };
    //graph click popup with details and view more drill down option
    var onEventPopup = function (feedback) {
        if (feedback) {
            sharedDataService.setLastDashBoardListPayload('');
            sharedDataService.setSelectedFirstTab($scope.dashboardModel.selectedCategory);
            sharedDataService.setSelectedSecondTab($scope.dashboardModel.selectedCategorySec);
            var passingText = englishValues(textSelected);
            $state.go('root.dashboardlist', {
                mainTab: selectedMainTab,
                secTab: selectedTab,
                subTabCount: endIndexCount,
                text: passingText,
                amount: amountSelected,
                chartSelected: chartSelected,
                loggedInUser: '',
                topThreeEndIndex: topThreeEndIndex,
                mrcCountIndex: mrcCountIndex
            });
        } else {

        }
    };
    //CLICK EVENT TO DETAILS START - Bar graph
    $scope.onClickBar = function (points, evt) {
        var barClicked = points[0]._index;
        if (selectedTab === 'Lead') {
            subTabCount = $scope.dashboardModel.subCategoryList[0].notifierCount;
        } else if (selectedTab === 'Cust:Contact') {
            subTabCount = $scope.dashboardModel.subCategoryList[1].notifierCount;
        } else if (selectedTab === 'Cust:Visits') {
            subTabCount = $scope.dashboardModel.subCategoryList[2].notifierCount;
        } else if (selectedTabe === 'Proposal') {
            subTabCount = $scope.dashboardModel.subCategoryList[3].notifierCount;
        } else if (selectedTab === 'Win') {
            subTabCount = $scope.dashboardModel.subCategoryList[4].notifierCount;
        } else if (selectedTab === 'Lost') {
            subTabCount = $scope.dashboardModel.subCategoryList[5].notifierCount;
        }
    };
    //set selected chart
    $scope.onClickCharts = function (text) {
        chartSelected = text;
    };
    $scope.onClickDouSStaus = function (points, evt) {
        if (clickEnable == true) {
            var barClicked = points[0]._index;
            if (selectedTab === 'Lead') {
                subTabCount = $scope.dashboardModel.subCategoryList[0].notifierCount;
            } else if (selectedTab === 'Cust:Contact') {
                subTabCount = $scope.dashboardModel.subCategoryList[1].notifierCount;
            } else if (selectedTab === 'Cust:Visits') {
                subTabCount = $scope.dashboardModel.subCategoryList[2].notifierCount;
            } else if (selectedTab === 'Proposal') {
                subTabCount = $scope.dashboardModel.subCategoryList[3].notifierCount;
            } else if (selectedTab === 'Win') {
                subTabCount = $scope.dashboardModel.subCategoryList[4].notifierCount;
            } else if (selectedTab === 'Lost') {
                subTabCount = $scope.dashboardModel.subCategoryList[5].notifierCount;
            }
            topThreeEndIndex = 0;
            mrcCountIndex = 0;
            if (chartSelected === 'chOne') {
                endIndexCount = 0;
                textSelected = $scope.dashboardModel.oppoSubStatusText[barClicked];
                amountSelected = $scope.dashboardModel.oppoSubStatusAmount[barClicked];
                angular.forEach($scope.dashboardModel.oppoSubStatusAmount, function (value, key) {
                    endIndexCount = endIndexCount + parseInt(value, 10);
                });
                textToDisplay = '<span>' + pluginService.getTranslations().subStatus + ' :<span> </span>' + textSelected + '</span><br><span> ' + pluginService.getTranslations().opportys + ':<span> </span>' + amountSelected + '</span><span></span>'
            } else if (chartSelected === 'chTwo') {
                textSelected = $scope.dashboardModel.mrcBarText[barClicked];
                amountSelected = $scope.dashboardModel.mrcBarAmount[barClicked];
                angular.forEach($scope.dashboardModel.mrcBarEndIndex, function (value, key) {
                    mrcCountIndex = mrcCountIndex + parseInt(value, 10);
                });
                textToDisplay = '<span>' + pluginService.getTranslations().subStatus + ' :<span> </span>' + textSelected + '</span><br><span> ' + pluginService.getTranslations().amount + ': <span> </span>' + amountSelected + '</span><span></span>'
            } else if (chartSelected === 'chFive') {
                textSelected = $scope.dashboardModel.topFiveDataBarText[barClicked];
                amountSelected = $scope.dashboardModel.topFiveDataBarAmount[barClicked];
                textToDisplay = '<span>' + pluginService.getTranslations().opptyName + ' :<span> </span>' + textSelected + '</span><br><span> ' + pluginService.getTranslations().amount + ':<span> </span>' + amountSelected + '</span><span></span>'
            } else if (chartSelected === 'chThree') {
                endIndexCount = 0;
                textSelected = $scope.dashboardModel.highCountText[barClicked];
                amountSelected = $scope.dashboardModel.highCountAmount[barClicked];
                angular.forEach($scope.dashboardModel.highCountAmount, function (value, key) {
                    endIndexCount = endIndexCount + parseInt(value, 10);
                });
                textToDisplay = '<span>' + pluginService.getTranslations().subStatus + ' :<span> </span>' + textSelected + '</span><br><span> ' + pluginService.getTranslations().opportys + ':<span> </span>' + amountSelected + '</span><span></span>'
            } else if (chartSelected === 'chFour') {
                textSelected = $scope.dashboardModel.topThreeProData[barClicked];
                amountSelected = $scope.dashboardModel.topThreeRateData[barClicked]; //rate is passed here as amount
                var productAmount = $scope.dashboardModel.topThreeDataBarAmount[barClicked];
                topThreeEndIndex = productAmount;
                textToDisplay = '<span>' + pluginService.getTranslations().prdctName + ' :<span> </span>' + textSelected + '</span><br><span> ' + pluginService.getTranslations().ratePlan + ':<span> </span>' + amountSelected + '</span><br><span> ' + pluginService.getTranslations().amount + ':' + productAmount + '</span><span></span>'
            }

            MaterialReusables.showInfoPopup('' + pluginService.getTranslations().chartdetails, textToDisplay, '' + pluginService.getTranslations().vm, '' + pluginService.getTranslations().Close, onEventPopup);
        } else {
            ////TODO
        }
    };
    //set tab wise counts to zero
    var removeCountFromTabs = function () {
        if ($scope.dashboardModel.subCategoryList.length !== 0) {
            $scope.oppByStatus = [];
            $scope.dashboardModel.subCategoryList[0].notifierCount = 0;
            $scope.dashboardModel.subCategoryList[1].notifierCount = 0;
            $scope.dashboardModel.subCategoryList[2].notifierCount = 0;
            $scope.dashboardModel.subCategoryList[3].notifierCount = 0;
            $scope.dashboardModel.subCategoryList[4].notifierCount = 0;
            $scope.dashboardModel.subCategoryList[5].notifierCount = 0;
        }
    };
    //clear DB UI all data
    var eraseAllData = function () {
        $scope.dashboardModel.oppoSubStatusAmount = [];
        $scope.dashboardModel.oppoSubStatusText = [];
        $scope.dashboardModel.mrcBarAmount = [];
        $scope.dashboardModel.mrcBarText = [];
        $scope.dashboardModel.mrcBarEndIndex = [];
        $scope.dashboardModel.topThreeDataBarAmount = [];
        $scope.dashboardModel.topFiveDataBarAmount = [];
        $scope.dashboardModel.highCountText = [];
        $scope.dashboardModel.highCountAmount = [];
        $scope.dashboardModel.topThreeDataBarText = [];
        $scope.dashboardModel.topFiveDataBarText = [];
    };
    //load DB UI based on previous selection for user tab if no selection
    $scope.dashBoardTabChanges = function (selectedIndex) {
        if (!sharedDataService.getNetworkState()) {
            $scope.dashboardModel.isNetworkAvailable = false;
        } else {
            $scope.dashboardModel.isNetworkAvailable = true;
            $scope.dashboardModel.isLoading = true;
            $scope.dashboardModel.selectedCategory = selectedIndex.name;
            selectedTab = 'Lead';
            selectedMainTab = selectedIndex.name;
            if (selectedIndex.name == "User") {
                $timeout(function () {
                    $scope.dashboardModel.isLoading = false;
                }, 2000);
                $scope.dashboardModel.subordinateList = [];
                if (angular.isDefined(sharedDataService.getSubordinateList())) {
                    angular.forEach(sharedDataService.getSubordinateList(), function (value, key) {
                        value.userDataShow = false;
                        $scope.dashboardModel.subordinateList.push(value);
                    });
                };
            };
            angular.forEach($scope.dashboardModel.subCategoryListTop, function (key, val) {
                if (selectedIndex.index === key.index) {
                    key.isSelected = true;
                } else {
                    key.isSelected = false;
                }
                if (selectedIndex.name == "User") {
                    $scope.dashboardModel.enableSearch = true;
                    $rootScope.$broadcast('enableUserSearch', []);
                } else {
                    $rootScope.$broadcast('disableUserSearch', []);
                    $scope.dashboardModel.enableSearch = false;
                }
            });
            if (selectedIndex.index == 0) {
                removeCountFromTabs();
                eraseAllData();
                $scope.secondLineDisplay = true;
                getUserBasedCount();
                fiveApiCalls();
                $timeout(function () {
                    $scope.dashboardModel.isLoading = false;
                }, 7000);
            }
            if (selectedIndex.index == 1) {
                if ($scope.teamView) {
                    $scope.secondLineDisplay = true;
                } else {
                    $scope.secondLineDisplay = false;
                }
                removeCountFromTabs();
                eraseAllData();
                countByStatus();
                fiveApiCalls();
                $timeout(function () {
                    $scope.dashboardModel.isLoading = false;
                }, 7000);
            }
        }
    };
    //navigation to opportunity list from view more option in pop ups
    $scope.goToUsersListPage = function (loginName, substatusNoOfUser, countOfUserSub) {
        sharedDataService.setLastDashBoardListPayload('');
        sharedDataService.setSelectedFirstTab($scope.dashboardModel.selectedCategory);
        sharedDataService.setSelectedSecondTab($scope.dashboardModel.selectedCategorySec);
        var substatusOfUser = '';
        var mainTabUser = 'User';
        if (substatusNoOfUser == 'subOne') {
            substatusOfUser = "Lead";
        } else if (substatusNoOfUser == 'subTwo') {
            substatusOfUser = "Cust:Contact";
        } else if (substatusNoOfUser == 'subThree') {
            substatusOfUser = "Cust:Visits";
        } else if (substatusNoOfUser == 'subFour') {
            substatusOfUser = "Proposal";
        } else if (substatusNoOfUser == 'subFive') {
            substatusOfUser = "Win";
        } else if (substatusNoOfUser == 'subSix') {
            substatusOfUser = "Lost";
        }
        $state.go('root.dashboardlist', {
            mainTab: mainTabUser,
            secTab: substatusOfUser,
            subTabCount: countOfUserSub,
            text: '',
            amount: '',
            chartSelected: '',
            loggedInUser: loginName,
            topThreeEndIndex: '',
            mrcCountIndex: ''

        });
    };
    //set selected tab on navigation from DB UI
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if ((fromState.name == 'root.dashboardlist' && toState.name == 'root.dashboard') || (fromState.name == 'root.dashboard' && toState.name == 'root.dashboardlist')) {
            ////TODO
        } else {
            sharedDataService.setSelectedFirstTab('');
            sharedDataService.setSelectedSecondTab('');
        }
    });
    $timeout(function () {
        $scope.dashboardModel.isLoading = false;
    }, 7000);
};
dashboardModule.controller('dashboardController', dashboardController);
dashboardController.$inject = ['$scope', '$rootScope', '$http', '$timeout', 'apiService', 'sharedDataService', '$state', 'MaterialReusables', '$translate', 'pluginService'];