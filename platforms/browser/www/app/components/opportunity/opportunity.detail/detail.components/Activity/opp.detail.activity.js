var detailActivity = function ($state, $translate, $timeout, $filter, sharedDataService, apiService, MaterialReusables, pluginService) {
    return {
        restrict: 'E',
        scope: {
            activity: '=',
            updateoppFn: '&'
        },
        templateUrl: "app/components/opportunity/opportunity.detail/detail.components/Activity/opp.detail.activity.html",
        transclude: true,
        compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                    scope.activityModel = {
                        rowId: '',
                        activityView: 'list',
                        activityRequestParams: [],
                        activityAction: '',
                        showActivityAddLoader: false,
                        activitiesList: [],
                        activityStatusList: [],
                        activityPriorityList: [],
                        activityTypeList: [],
                        salesStage: '',
                        alarmChecked: true,
                        doneChecked: true,
                        enableEdit: true,
                        activityToEdit: ''
                    };
                    scope.activityModel.enableEdit = scope.activity.isEditable;
                    scope.activityModel.activityTypeList = activityTypeList;
                    scope.activityModel.activityPriorityList = activityPriorityList;
                    scope.activityModel.rowId = scope.activity.rowId;
                    scope.activityModel.salesStage = scope.activity.salesStage;
                    scope.activityModel.activityStatusList = sharedDataService.getActivityList();
                    if (scope.activity.allActivities !== null) {
                        scope.activityModel.activitiesList = scope.activity.allActivities.action;
                    }
                    scope.showSlideIndicator = true;
                    var dbActivityUpdateParams = { index: 0, param: '' };
                },
                post: function postLink(scope, iElement, iAttrs, controller) {
                    //load activity list view on page load
                    scope.$on('Activity', function (event, data) {
                        scope.activityModel.activityView = 'list';
                        scope.activityModel.activityRequestParams = [];
                        scope.activityModel.activityAction = '';
                    });
                    //apply color and convert dates in available activity items on page load
                    if (scope.activityModel.activitiesList != null) {
                        scope.activityModel.activitiesList = applyColor(scope.activityModel.activitiesList);
                        scope.activityModel.activitiesList = convertDates(scope.activityModel.activitiesList);
                    }
                    //push activity items for local notification
                    var pushEvents = function () {
                        var eventToPush = [{
                            'planned': scope.activityModel.activityRequestParams.planned,
                            'plannedCompletion': scope.activityModel.activityRequestParams.plannedCompletion,
                            'description2': scope.activityModel.activityRequestParams.description2,
                            'sTCEBUOpptyActivityType': scope.activityModel.activityRequestParams.sTCEBUOpptyActivityType,
                            'alarm': scope.activityModel.activityRequestParams.alarm,
                            'done': scope.activityModel.activityRequestParams.done
                        }];
                        pluginService.createLocalNotification(eventToPush);
                    };
                    //check and push to local DB for offline
                    var checkandUpdateOfflineActivites = function (obj, activityType) {
                        scope.activityModel.activityView = 'list';
                        scope.activityModel.showActivityAddLoader = false;
                        var dbCheckParams = {};
                        var activityDBPushStatus = function (result, params) {
                            if (activityType === 'create') {
                                pushEvents();
                            }
                            if (activityType !== 'create') {
                                scope.activityModel.activitiesList.splice(dbActivityUpdateParams.index, 1);
                            }
                            if (activityType !== 'delete') {
                                params.description2 = params.description;
                                params.sTCEBUOpptyActivityType = params.stcEBUOpptyActivityType;
                                scope.activityModel.activitiesList.push(convertDates([params])[0]);
                                scope.activityModel.activitiesList = applyColor(scope.activityModel.activitiesList);
                            }
                        };
                        if (activityType !== 'create') {
                            dbCheckParams = { params: obj, lastParams: dbActivityUpdateParams.param, type: 'Activity', action: activityType, callback: activityDBPushStatus };
                        } else {
                            dbCheckParams = { params: obj, lastParams: '', type: 'Activity', action: activityType, callback: activityDBPushStatus };
                        }
                        scope.updateoppFn(dbCheckParams);
                    };
                    //check and proceed for offline push
                    var pushActivityToDB = function (payload) {
                        checkandUpdateOfflineActivites(payload, scope.activityModel.activityAction);
                    };
                    //activity lead process calls
                    var postActivityOnline = function (payload) {
                        if (scope.activityModel.activityAction == 'create') {
                            apiService.fetchDataFromApi('opp.leadProcess', payload, oppAddActivitySuccess, oppAddActivityError);
                        } else if (scope.activityModel.activityAction == 'edit') {
                            apiService.fetchDataFromApi('opp.leadProcess', payload, oppUpdateActivitySuccess, oppAddActivityError);
                        } else if (scope.activityModel.activityAction === 'delete') {
                            apiService.fetchDataFromApi('opp.leadProcess', payload, scope.oppActivityDeleteSuccess, scope.oppActivityDeleteError);
                        }
                    };
                    //check for changes durign edit
                    var checkChanges = function (oldActivity, newActivity) {
                        var old = angular.copy(oldActivity);
                        var edited = angular.copy(newActivity);
                        var old = {
                            "activityId": old.activityId ? old.activityId : "",
                            "alarm": old.alarm ? old.alarm : "",
                            "description2": old.description2 ? old.description2 : "",
                            "done": old.done ? old.done : "N",
                            "due": old.due ? old.due : "",
                            "planned": old.planned ? old.planned : "",
                            "plannedCompletion": old.plannedCompletion ? old.plannedCompletion : "",
                            "priority": old.priority ? old.priority : "",
                            "sTCEBUOpptyActivityType": old.sTCEBUOpptyActivityType ? old.sTCEBUOpptyActivityType : "",
                            "status": old.status ? old.status : "",
                            "id": old.id ? old.id : ""
                        }
                        var objectsAreSame = true;
                        for (var propertyName in old) {
                            if (old[propertyName] !== edited[propertyName]) {
                                objectsAreSame = false;
                                break;
                            }
                        }
                        return objectsAreSame;
                    };
                    //create activity
                    var addActivity = function () {
                        var activityPayLoad = scope.activityModel.activityRequestParams;
                        var activityCheckBoxes = { 'done': '', 'alarm': '' };
                        if (activityPayLoad.done === true) {
                            activityCheckBoxes.done = 'Y';
                        } else {
                            activityCheckBoxes.done = 'N';
                        }
                        if (activityPayLoad.alarm === true) {
                            activityCheckBoxes.alarm = 'Y';
                        } else {
                            activityCheckBoxes.alarm = 'N';
                        }
                        var changesExists;
                        if (scope.activityModel.activityAction == 'edit') {
                            var editedData = {
                                "activityId": activityPayLoad.activityId ? activityPayLoad.activityId : "",
                                "alarm": activityCheckBoxes.alarm ? activityCheckBoxes.alarm : "",
                                "description2": activityPayLoad.description2 ? activityPayLoad.description2 : "",
                                "done": activityCheckBoxes.done ? activityCheckBoxes.done : "N",
                                "due": activityPayLoad.due ? moment(activityPayLoad.due).format("DD/MM/YYYY HH:mm:ss") : "",
                                "planned": activityPayLoad.planned ? moment(activityPayLoad.planned).format("DD/MM/YYYY HH:mm:ss") : "",
                                "plannedCompletion": activityPayLoad.plannedCompletion ? moment(activityPayLoad.plannedCompletion).format("DD/MM/YYYY HH:mm:ss") : "",
                                "priority": activityPayLoad.priority ? activityPayLoad.priority : "",
                                "sTCEBUOpptyActivityType": activityPayLoad.sTCEBUOpptyActivityType ? activityPayLoad.sTCEBUOpptyActivityType : "",
                                "status": activityPayLoad.statusvalue ? activityPayLoad.statusvalue : activityPayLoad.status,
                                "id": activityPayLoad.activityId ? activityPayLoad.activityId : ""
                            }
                            changesExists = checkChanges(scope.activityModel.activityToEdit, editedData);
                        } else {
                            changesExists = false;
                        }
                        if (!changesExists) {
                            scope.activityModel.showActivityAddLoader = true;
                            if (scope.activityModel.activityAction == 'create') {
                                var headerData = {
                                    "operation": "skipnode",
                                    "rowId": scope.activityModel.rowId
                                };
                                var actionData = {
                                    "alarm": activityCheckBoxes.alarm,
                                    "description2": activityPayLoad.description2,
                                    "doneFlag": activityCheckBoxes.done,
                                    "due": $filter('date')(activityPayLoad.due, 'MM/dd/yyyy HH:mm:ss'),
                                    "planned": $filter('date')(activityPayLoad.planned, 'MM/dd/yyyy HH:mm:ss'),
                                    "plannedCompletion": $filter('date')(activityPayLoad.plannedCompletion, 'MM/dd/yyyy HH:mm:ss'),
                                    "priority": activityPayLoad.priority,
                                    "sTCEBUOpptyActivityType": activityPayLoad.sTCEBUOpptyActivityType,
                                    "status": activityPayLoad.statusvalue,
                                    "id": "1234",
                                    "sTCCreatedBy": sharedDataService.getUserDetails().Id,
                                    "operation": "insert"
                                };
                                var oppActivityAddPayload = Util.leadProcessPayload(headerData, '', '', '', '', '', '', actionData);
                                var oppActivityAddOfflinePayload = {
                                    "opportunityId": scope.activityModel.rowId ? scope.activityModel.rowId : '',
                                    "stcEBUOpptyActivityType": activityPayLoad.sTCEBUOpptyActivityType ? activityPayLoad.sTCEBUOpptyActivityType : activityPayLoad.sTCEBUOpptyActivityType,
                                    "description": activityPayLoad.description2 ? activityPayLoad.description2 : "",
                                    "priority": activityPayLoad.priority ? activityPayLoad.priority : activityPayLoad.priority,
                                    "status": activityPayLoad.statusvalue ? activityPayLoad.statusvalue : activityPayLoad.status,
                                    "planned": $filter('date')(activityPayLoad.planned, 'MM/dd/yyyy HH:mm:ss') ? $filter('date')(activityPayLoad.planned, 'MM/dd/yyyy HH:mm:ss') : "",
                                    "plannedCompletion": $filter('date')(activityPayLoad.plannedCompletion, 'MM/dd/yyyy HH:mm:ss') ? $filter('date')(activityPayLoad.plannedCompletion, 'MM/dd/yyyy HH:mm:ss') : "",
                                    "due": $filter('date')(activityPayLoad.due, 'MM/dd/yyyy HH:mm:ss') ? $filter('date')(activityPayLoad.due, 'MM/dd/yyyy HH:mm:ss') : "",
                                    "doneFlag": activityCheckBoxes.done ? activityCheckBoxes.done : "N",
                                    "alarm": activityCheckBoxes.alarm ? activityCheckBoxes.alarm : "N",
                                    "stcCreatedBy": sharedDataService.getUserDetails().Id ? sharedDataService.getUserDetails().Id : "",
                                    "primaryOwnedBy": sharedDataService.getUserDetails().loginName ? sharedDataService.getUserDetails().loginName : ""
                                };
                                if (!sharedDataService.getNetworkState()) {
                                    pushActivityToDB(oppActivityAddOfflinePayload);
                                } else {
                                    postActivityOnline(oppActivityAddPayload);
                                }
                            } else if (scope.activityModel.activityAction == 'edit') {
                                var headerData = {
                                    "operation": "skipnode",
                                    "rowId": scope.activityModel.rowId
                                };
                                var actionData = {
                                    "activityId": activityPayLoad.activityId,
                                    "alarm": activityCheckBoxes.alarm,
                                    "description2": activityPayLoad.description2,
                                    "doneFlag": activityCheckBoxes.done,
                                    "due": $filter('date')(activityPayLoad.due, 'MM/dd/yyyy HH:mm:ss'),
                                    "planned": $filter('date')(activityPayLoad.planned, 'MM/dd/yyyy HH:mm:ss'),
                                    "plannedCompletion": $filter('date')(activityPayLoad.plannedCompletion, 'MM/dd/yyyy HH:mm:ss'),
                                    "priority": activityPayLoad.priority,
                                    "sTCEBUOpptyActivityType": activityPayLoad.sTCEBUOpptyActivityType,
                                    "status": activityPayLoad.statusvalue ? activityPayLoad.statusvalue : activityPayLoad.status,
                                    "id": activityPayLoad.activityId,
                                    //"sTCCreatedBy": sharedDataService.getUserDetails().Id,
                                    "operation": "update"
                                };
                                var oppActivityUpdatePayload = Util.leadProcessPayload(headerData, '', '', '', '', '', '', actionData);
                                var oppActivityUpdateOfflinePayload = {
                                    "activityId": activityPayLoad.activityId ? activityPayLoad.activityId : '',
                                    "opportunityId": scope.activityModel.rowId ? scope.activityModel.rowId : '',
                                    "stcEBUOpptyActivityType": activityPayLoad.sTCEBUOpptyActivityType ? activityPayLoad.sTCEBUOpptyActivityType : activityPayLoad.sTCEBUOpptyActivityType,
                                    "description": activityPayLoad.description2 ? activityPayLoad.description2 : "",
                                    "priority": activityPayLoad.priority ? activityPayLoad.priority : activityPayLoad.priority,
                                    "status": activityPayLoad.statusvalue ? activityPayLoad.statusvalue : activityPayLoad.status,
                                    "planned": $filter('date')(activityPayLoad.planned, 'MM/dd/yyyy HH:mm:ss') ? $filter('date')(activityPayLoad.planned, 'MM/dd/yyyy HH:mm:ss') : "",
                                    "plannedCompletion": $filter('date')(activityPayLoad.plannedCompletion, 'MM/dd/yyyy HH:mm:ss') ? $filter('date')(activityPayLoad.plannedCompletion, 'MM/dd/yyyy HH:mm:ss') : "",
                                    "due": $filter('date')(activityPayLoad.due, 'MM/dd/yyyy HH:mm:ss') ? $filter('date')(activityPayLoad.due, 'MM/dd/yyyy HH:mm:ss') : "",
                                    "doneFlag": activityCheckBoxes.done ? activityCheckBoxes.done : "N",
                                    "alarm": activityCheckBoxes.alarm ? activityCheckBoxes.alarm : "N"
                                };
                                if (!sharedDataService.getNetworkState()) {
                                    pushActivityToDB(oppActivityUpdateOfflinePayload);
                                } else {
                                    postActivityOnline(oppActivityUpdatePayload);
                                }
                            }
                        } else {
                            MaterialReusables.showSuccessAlert('Nothing To Update', angular.noop);
                        }
                    };
                    //swipe left on activity items
                    scope.onSwipeLeftActivities = function (ev, context) {
                        if (scope.activityModel.enableEdit) {
                            context.showSlideIndicator = true;
                            $('.pdtItemActivity' + ev).removeClass('slideToRight');
                            $('.pdtItemActivity' + ev).addClass('slideToLeft');
                        }
                    };
                    //swipe right on activity items
                    scope.onSwipeRightActivities = function (ev, context) {
                        if (scope.activityModel.enableEdit) {
                            context.showSlideIndicator = false;
                            $('#activitySlider-' + ev).css('height', $('#activityBlock-' + ev).height());
                            $('.pdtItemActivity' + ev).removeClass('slideToLeft');
                            $('.pdtItemActivity' + ev).addClass('slideToRight');
                        }
                    };
                    //set params for create activity
                    scope.doAddActivity = function () {
                        scope.activityModel.activityRequestParams = [];
                        scope.activityModel.activityStatusList = sharedDataService.getActivityList();
                        scope.activityModel.activityView = 'create';
                        scope.activityModel.activityAction = 'create';
                        var currentDate = new Date(moment().format("MM/DD/YYYY HH:mm:ss"));
                        scope.activityModel.activityRequestParams.planned = currentDate;
                        scope.activityModel.activityRequestParams.plannedCompletion = currentDate;
                    };
                    //activity field validations
                    var validateOppActivityCreation = function () {
                        var validation = '';
                        if (angular.isUndefined(scope.activityModel.activityRequestParams.sTCEBUOpptyActivityType)) {
                            validation = '' + pluginService.getTranslations().enterActivityType;
                        } else if (angular.isUndefined(scope.activityModel.activityRequestParams.priority)) {
                            validation = '' + pluginService.getTranslations().enterPriority;
                        } else if (angular.isUndefined(scope.activityModel.activityRequestParams.status)) {
                            validation = '' + pluginService.getTranslations().enterStatus;
                        }
                        return validation;
                    };
                    //check, validate and proceed to create activity
                    scope.saveActivity = function (context) {
                        var validationResult = validateOppActivityCreation();
                        if (validationResult === '') {

                            addActivity();
                        } else {
                            MaterialReusables.showToast('' + validationResult, 'error');
                        }
                    };
                    //compare start and end date,fire error if end date < start date
                    scope.compareDates = function (date, datetoCompareWith, prevEvent) {
                        var dateCompareResult = false;
                        if (datetoCompareWith || datetoCompareWith !== '' || typeof datetoCompareWith != 'undefined') {
                            dateCompareResult = Util.compareDate(moment(date), moment(datetoCompareWith));
                            if (!dateCompareResult) {
                                scope.activityModel.activityRequestParams.plannedCompletion = '';
                                MaterialReusables.showToast('' + pluginService.getTranslations().aDateGrtrThan + ' ' + prevEvent + ' ' + pluginService.getTranslations().Date, 'warning');
                            }
                        } else {
                            scope.activityModel.activityRequestParams.plannedCompletion = '';
                            MaterialReusables.showToast('' + pluginService.getTranslations().pleaseEnter + ' ' + prevEvent + ' ' + pluginService.getTranslations().Date, 'warning');
                        }
                    };
                    //report update api call
                    var reportActionCall = function (rowId) {
                        var reportData = {
                            "opportunityId": rowId,
                            "action": 'UPD_ACTED_LEADS',
                            "actedLeads": '1'
                        };
                        var reportPayload = Util.reportActionPayload(reportData);
                        apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
                    };
                    //create activity - success
                    var oppAddActivitySuccess = function (data) {
                        if (data.data.errorMessage === 'Success') {
                            pushEvents();
                            scope.activityModel.activitiesList = applyColor(data.data.listOfStcOpportunity.opportunity[0].listOfAction.action);
                            scope.activityModel.activitiesList = convertDates(scope.activityModel.activitiesList);
                            MaterialReusables.showToast('' + pluginService.getTranslations().activityAdded, 'success');
                            scope.activityModel.activityView = 'list';
                            scope.activityModel.showActivityAddLoader = false;
                            reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                        } else {
                            scope.activityModel.showActivityAddLoader = false;
                            Util.throwError(data.data.errorMessage, MaterialReusables);
                        }
                        $('.defaultPditem').removeClass('slideToRight');
                        $('.defaultPditem').addClass('slideToLeft');
                    };
                    //update activity -success
                    var oppUpdateActivitySuccess = function (data) {
                        if (data.data.errorMessage === 'Success') {
                            scope.activityModel.activitiesList = applyColor(data.data.listOfStcOpportunity.opportunity[0].listOfAction.action);
                            scope.activityModel.activitiesList = convertDates(scope.activityModel.activitiesList);
                            MaterialReusables.showToast('' + pluginService.getTranslations().activityUpdated, 'success');
                            scope.activityModel.activityView = 'list';
                            scope.activityModel.showActivityAddLoader = false;
                            reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                        } else {
                            scope.activityModel.showActivityAddLoader = false;
                            Util.throwError(data.data.errorMessage, MaterialReusables);
                        }
                        $('.defaultPditem').removeClass('slideToRight');
                        $('.defaultPditem').addClass('slideToLeft');
                    };
                    //create activity - error
                    var oppAddActivityError = function (data) {
                        scope.activityModel.showActivityAddLoader = false;
                        Util.throwError(data.data.Message, MaterialReusables);
                    };
                    //cancel create activtiy operation
                    scope.cancelActivityEntry = function () {
                        $('.defaultPditem').removeClass('slideToRight');
                        $('.defaultPditem').addClass('slideToLeft');
                        scope.activityModel.showActivityAddLoader = false;
                        scope.activityModel.activityView = 'list';
                    };
                    //edit activity
                    scope.gotoEditActivity = function (currentIndex, activity) {
                        dbActivityUpdateParams = { index: currentIndex, param: activity };
                        scope.activityEdited = angular.copy(activity);
                        scope.activityModel.activityToEdit = activity;
                        if (scope.activityEdited.planned !== null) {
                            var dateArrayPlanned = scope.activityEdited.planned.split('/');
                            scope.activityEdited.planned = new Date(dateArrayPlanned[1] + '/' + dateArrayPlanned[0] + '/' + dateArrayPlanned[2]);
                        }
                        if (scope.activityEdited.plannedCompletion !== null) {
                            var dateArrayPlannedCompletion = scope.activityEdited.plannedCompletion.split('/');
                            scope.activityEdited.plannedCompletion = new Date(dateArrayPlannedCompletion[1] + '/' + dateArrayPlannedCompletion[0] + '/' + dateArrayPlannedCompletion[2]);
                        }
                        if (scope.activityEdited.due !== null) {
                            var dateArrayDue = scope.activityEdited.due.split('/');
                            scope.activityEdited.due = new Date(dateArrayDue[1] + '/' + dateArrayDue[0] + '/' + dateArrayDue[2]);
                        }
                        scope.activityModel.activityRequestParams = scope.activityEdited;
                        scope.activityModel.activityAction = 'edit';
                        scope.activityModel.alarmChecked = null;
                        scope.activityModel.doneChecked = null;
                        scope.activityModel.showActivityAddLoader = false;
                        if (scope.activityModel.activityRequestParams.alarm == 'N' || scope.activityModel.activityRequestParams.alarm == null || scope.activityModel.activityRequestParams.alarm == false) {
                            scope.activityModel.activityRequestParams.alarm = false;
                        } else {
                            scope.activityModel.activityRequestParams.alarm = true;
                        }
                        if (scope.activityModel.activityRequestParams.done == 'N' || scope.activityModel.activityRequestParams.done == null || scope.activityModel.activityRequestParams.done == false) {
                            scope.activityModel.activityRequestParams.done = false;
                        } else {
                            scope.activityModel.activityRequestParams.done = true;
                        }
                        scope.activityModel.activityStatusList = sharedDataService.getActivityList();
                        scope.activityModel.activityView = 'create';
                    };
                    //select or change activity status
                    scope.activityStatusChange = function (activity) {
                        scope.activityModel.activityRequestParams.statusvalue = activity.name;
                        scope.activityModel.activityRequestParams.status = activity.value;
                    };
                    //delete activity
                    scope.doDeleteActivity = function (context, ev, activity) {
                        scope.oppActivityDeleteSuccess = function (data) {
                            context.showActivityDeleteLoader = false;
                            if (data.data.errorMessage == 'Success') {
                                if (data.data.listOfStcOpportunity.opportunity[0].listOfAction !== null) {
                                    scope.activityModel.activitiesList = data.data.listOfStcOpportunity.opportunity[0].listOfAction.action;
                                } else {
                                    scope.activityModel.activitiesList = [];
                                }
                                scope.activityModel.activityView = 'list';
                                MaterialReusables.showToast('' + pluginService.getTranslations().actiPlug + ' ' + activity.activityId + ' ' + pluginService.getTranslations().deleted, 'success');
                                reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                            } else {
                                Util.throwError(data.data.errorMessage, MaterialReusables);
                            }
                        };
                        scope.oppActivityDeleteError = function (data) {
                            context.showActivityDeleteLoader = false;
                            Util.throwError(data.data.Message, MaterialReusables);
                        };
                        var result = function (confirm) {
                            if (confirm) {
                                scope.activityModel.activityAction = 'delete';
                                dbActivityUpdateParams = { index: ev, param: activity };
                                $('.defaultPditem').removeClass('slideToRight');
                                $('.defaultPditem').addClass('slideToLeft');
                                context.showActivityDeleteLoader = true;
                                // var activityDeletePayLoad = {
                                //     'activityId': activity.activityId
                                // };
                                var headerData = {
                                    "operation": "skipnode",
                                    "rowId": scope.activityModel.rowId
                                };
                                var actionData = {
                                    "id": activity.activityId,
                                    "operation": "delete"
                                };
                                var activityDeletePayLoad = Util.leadProcessPayload(headerData, '', '', '', '', '', '', actionData);
                                if (!sharedDataService.getNetworkState()) {
                                    pushActivityToDB(activity);
                                } else {
                                    postActivityOnline(activityDeletePayLoad);
                                }
                            } else {
                                $('.defaultPditem').removeClass('slideToRight');
                                $('.defaultPditem').addClass('slideToLeft');
                            }
                        };
                        MaterialReusables.showConfirmDialog(ev, '' + pluginService.getTranslations().activityDelete_confirm, result);
                    };
                    //date conversion format changing
                    function convertDates(activityList) {
                        var activities = [];
                        angular.forEach(activityList, function (value, key) {
                            if (value.planned !== null && value.planned !== '') {
                                value.planned = moment(value.planned).format("DD/MM/YYYY HH:mm:ss");
                            }
                            if (value.plannedCompletion !== null && value.plannedCompletion !== '') {
                                value.plannedCompletion = moment(value.plannedCompletion).format("DD/MM/YYYY HH:mm:ss");
                            }
                            if (value.due !== null && value.due !== '') {
                                value.due = moment(value.due).format("DD/MM/YYYY HH:mm:ss");
                            }
                            activities.push(value);
                        });
                        return activities;
                    };
                    //apply color to activity items based on date start end end times
                    function applyColor(activityList) {
                        var activities = [];
                        var eventStatus = '';
                        angular.forEach(activityList, function (value, key) {
                            if (moment() > moment(value.planned) && moment() < moment(value.plannedCompletion)) {
                                eventStatus = 'active';
                            } else if (moment() > moment(value.plannedCompletion)) {
                                eventStatus = 'past';
                            } else if (moment() < moment(value.planned)) {
                                eventStatus = 'future';
                            }
                            value.color = eventStatus;
                            activities.push(value);
                        });
                        return activities;
                    };
                    //comments field - additional validation
                    scope.descValidate = function (newValue) {
                        scope.activityModel.activityRequestParams.description2 = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                }
            }
        }
    }
};
opportunityModule.directive('detailActivity', detailActivity);
detailActivity.$inject = ['$state', '$translate', '$timeout', '$filter', 'sharedDataService', 'apiService', 'MaterialReusables', 'pluginService'];