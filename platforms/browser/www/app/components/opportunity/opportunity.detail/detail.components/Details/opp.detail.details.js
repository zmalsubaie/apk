
var detailDetails = function ($state, $translate, $rootScope, $http, $timeout, $filter, sharedDataService, MaterialReusables, apiService, pluginService, dbService) {
    return {
        restrict: 'E',
        scope: {
            details: '=',
            mapcallFn: '&',
            updateoppFn: '&'
        },
        templateUrl: "app/components/opportunity/opportunity.detail/detail.components/Details/opp.detail.details.html",
        transclude: true,
        compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                    scope.detailModel = {
                        tempDetailStorage: [],
                        details: '',
                        oppTypeList: [],
                        showDetailsAddLoader: false,
                        showAssignedToList: false,
                        accountNames: [],
                        loadAccounts: false,
                        enableAccountSearch: false,
                        contactName: '',
                        contactNameList: [],
                        isFetchingContacts: false,
                        ocrLoading: false,
                        isFetchingAssignedTo: false,
                        assignedTo: [],
                        showCity: false,
                        loadMore: false,
                        startRow: 0,
                        cityListLoading: false,
                        activityStatusList: [],
                        statusList: [],
                        subStatusList: [],
                        account: [],
                        preferredTimeList: [],
                        noProductsAdded: true,
                        assignStartRow: 0,
                        assignQuery: '',
                        loadingMoreAssigns: false,
                        lastAssigneeReached: false,
                        isAssigneeEditable: true,
                        activeLoginName: '',
                        tabChanges: false,
                        showAccountList: false,
                        loadingAccounts: false,
                        accountQuery: '',
                        accountStartRow: 0,
                        lastAccountReached: true,
                        enableEdit: true,
                        isOffline: false
                    };
                    scope.detailModel.enableEdit = scope.details.isEditable;
                    scope.detailModel.tempDetailStorage = angular.copy(scope.details.allDetails);
                    scope.detailModel.details = scope.details.allDetails;
                    scope.detailModel.oppTypeList = oppTypeList;
                    scope.detailModel.preferredTimeList = preferredTimeList;
                    scope.detailModel.isOffline = !sharedDataService.getNetworkState();
                    var currentStatus = '';
                    var currentSubStatus = '';
                    scope.currentStatusIndex = '0';
                    scope.currentSubStatusIndex = '0';
                    scope.currentOppTypeIndex = '0';
                },
                post: function postLink(scope, iElement, iAttrs, controller) {
                    //check for products added in products tab
                    scope.$on('pushProduct', function (event, args) {
                        scope.products = args.prodObj;
                        if (scope.products !== null) {
                            if (scope.products.length === 0) {
                                scope.detailModel.noProductsAdded = true;
                            } else {
                                scope.detailModel.noProductsAdded = false;
                            }
                        } else {
                            scope.detailModel.noProductsAdded = true;
                        }
                    });
                    //push quotes in quotes tab
                    scope.$on('pushQuote', function (event, args) {
                        assigneeEditable(args.quoteObj);
                    });
                    //check and validate for 5 characters - enabling accoutn search
                    scope.$watch('detailModel.details.account', function (newVal, oldVal) {
                        try {
                            if (scope.detailModel.details.account.length >= 5) {
                                scope.detailModel.enableAccountSearch = true;
                            } else {
                                scope.detailModel.enableAccountSearch = false;
                            }
                        } catch (err) {
                        }
                    }, true);
                    //check for item exists in array and return postion if exists
                    var getArrayIndex = function (array, value) {
                        var position = 0;
                        if ((value !== null) && (angular.isDefined(value))) {
                            for (var i = 0; i < array.length; i++) {
                                if (value.includes(array[i].name)) {
                                    position = i;
                                }
                            }
                        }
                        return position;
                    };
                    scope.loadMap = function () { scope.mapcallFn(); };
                    //upload business card scanned upon mobile number reading processd succesfully
                    var uploadBusinessCard = function (imgData) {
                        var bcardName = 'Business_Card_' + Math.round(new Date().getTime() / 1000);
                        var businessCardUploadPayload = {
                            'payload': {
                                "busObjCacheSize": "",
                                "messageId": "", "objectLevelTransactions": "",
                                "setMinimalReturns": "",
                                "statusObject": "",
                                "listOfStcOpportunityAttachment": {
                                    "Opportunity": [{
                                        "id": scope.detailModel.details.rowId,
                                        "name": scope.detailModel.details.name,
                                        "listOfOpportunityAttachment": {
                                            "OpportunityAttachment": [{
                                                "comment": "",
                                                "opportunitySecureFlag": "",
                                                "opptyId2": scope.detailModel.details.rowId,
                                                "opptyFileExt": "jpeg",
                                                "opptyFileName": bcardName,
                                                "opptyName": scope.detailModel.details.name,
                                                "opptyFileBuffer": imgData.replace(/\r?\n|\r/g, ""),
                                                "stcCreatedBy": sharedDataService.getUserDetails().Id ? sharedDataService.getUserDetails().Id : "",
                                                "id": scope.detailModel.details.rowId
                                            }]
                                        }
                                    }]
                                }
                            }
                        };
                        var businessCardUploadSuccess = function (data) {
                            if (data.data.errorMessage === 'Success') {
                                $rootScope.$broadcast('pushAttachment', { attachmentList: data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityAttachment.OpportunityAttachment });
                                MaterialReusables.showToast('' + pluginService.getTranslations().businessCardUploaded, 'success');
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().bCardUp, 'error');
                            }
                        };
                        var businessCardUploadFailed = function () {
                            MaterialReusables.showToast('' + pluginService.getTranslations().bCardUp, 'error');
                        };
                        apiService.fetchDataFromApi('opp.upsertAttachment', businessCardUploadPayload, businessCardUploadSuccess, businessCardUploadFailed);
                    };
                    //opportunity update validations
                    var validateOppUpdate = function () {
                        var validation = '';
                        if (scope.detailModel.oppTypeList.sTCOpportunityType === 'New Sale') {
                            var newSaleValidators = [
                                { 'field': scope.detailModel.details.name, 'message': pluginService.getTranslations().enterOppName }
                            ];
                            validation = Util.validateElements(angular, newSaleValidators);
                        } else if (scope.detailModel.oppTypeList.sTCOpportunityType === 'Upgrade') {
                            var upgradeValidators = [];
                            if (!sharedDataService.getNetworkState()) {
                                upgradeValidators = [
                                    { 'field': scope.detailModel.details.sTCOptyCity, 'message': pluginService.getTranslations().enterCity },
                                    { 'field': scope.detailModel.details.accountNumber, 'message': pluginService.getTranslations().enterAccName },
                                    { 'field': scope.detailModel.details.name, 'message': pluginService.getTranslations().enterOppName }
                                ];
                            } else {
                                upgradeValidators = [
                                    { 'field': scope.detailModel.details.sTCOptyCity, 'message': pluginService.getTranslations().enterCity },
                                    { 'field': scope.detailModel.details.keyContactId, 'message': pluginService.getTranslations().enterContact },
                                    { 'field': scope.detailModel.details.account, 'message': pluginService.getTranslations().enterAccName },
                                    { 'field': scope.detailModel.details.name, 'message': pluginService.getTranslations().enterOppName }
                                ];
                            }
                            validation = Util.validateElements(angular, upgradeValidators);
                        }
                        return validation;
                    };
                    //invoke phone dialer on tap of in mobile number
                    scope.openDialer = function () {
                        var telNo = ''
                        if (scope.detailModel.details.sTCFeedback === 'Authorized person' || scope.detailModel.details.sTCFeedback === null && scope.detailModel.details.sTCFeedback2 !== 'Authorized person' && scope.detailModel.details.sTCFeedback3 !== 'Authorized person') {
                            telNo = scope.detailModel.details.sTCBulkLeadContactNumber;
                        } else if (scope.detailModel.details.sTCFeedback2 === 'Authorized person') {
                            telNo = scope.detailModel.details.sTCContactNumber2;
                        } else if (scope.detailModel.details.sTCFeedback3 === 'Authorized person') {
                            telNo = scope.detailModel.details.sTCContactNumber3;
                        }
                        if (telNo !== '' || telNo) {
                            var openedDialer = function (success) { };
                            var failedtoDial = function (error) { };
                            pluginService.dialNumber(telNo, openedDialer, failedtoDial);
                        }
                    };
                    //report update api call
                    var reportActionCall = function (rowId, status) {
                        var reportData = '';
                        if (status.includes('Lost')) {
                            reportData = {
                                "opportunityId": rowId,
                                "action": 'UPD_CANCEL_LEADS',
                                "cancelledLeads": '1'
                            };
                        } else {
                            reportData = {
                                "opportunityId": rowId,
                                "action": 'UPD_ACTED_LEADS',
                                "actedLeads": '1'
                            };
                        }
                        var reportPayload = Util.reportActionPayload(reportData);
                        apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
                    };
                    //check for chnages to make update api call
                    var checkChangesExists = function (data) {
                        var oldData = angular.copy(scope.detailModel.tempDetailStorage);
                        var oldArray = {
                            "accountId": oldData.accountId ? oldData.accountId : "",
                            "description": oldData.description ? oldData.description : "",
                            "rowId": oldData.rowId ? oldData.rowId : "",
                            "name": oldData.name ? oldData.name : "",
                            "salesStage": oldData.salesStage ? oldData.salesStage : "",
                            "sTCOptyCity": oldData.sTCOptyCity ? oldData.sTCOptyCity : "",
                            "sTCOpportunityType": oldData.sTCOpportunityType ? oldData.sTCOpportunityType : "",
                            "sTCBulkLeadContactFirstName": oldData.sTCBulkLeadContactFirstName ? oldData.sTCBulkLeadContactFirstName : "",
                            "sTCSubStatusSME": oldData.sTCSubStatusSME ? oldData.sTCSubStatusSME : "",
                            "sTCLeadNumber": oldData.sTCLeadNumber ? oldData.sTCLeadNumber : "",
                            "sTCCommercialRegn": oldData.sTCCommercialRegn ? oldData.sTCCommercialRegn : "",
                            "sTCLeadCampaignName": oldData.sTCLeadCampaignName ? oldData.sTCLeadCampaignName : "",
                            "sTCBestTimeToCall": oldData.sTCBestTimeToCall ? oldData.sTCBestTimeToCall : "",
                            "sTCBulkLeadContactNumber": oldData.sTCBulkLeadContactNumber ? oldData.sTCBulkLeadContactNumber : "",
                            "sTCFeedback": oldData.sTCFeedback ? oldData.sTCFeedback : "",
                            "sTCContactNumber2": oldData.sTCContactNumber2 ? oldData.sTCContactNumber2 : "",
                            "sTCFeedback2": oldData.sTCFeedback2 ? oldData.sTCFeedback2 : "",
                            "sTCContactNumber3": oldData.sTCContactNumber3 ? oldData.sTCContactNumber3 : "",
                            "sTCFeedback3": oldData.sTCFeedback3 ? oldData.sTCFeedback3 : "",
                            "assignee": oldData.listOfopportunityPosition.opportunityPosition[0].salesRep ? oldData.listOfopportunityPosition.opportunityPosition[0].salesRep : "",
                            "contactId": oldData.keyContactId ? oldData.keyContactId : ""
                        }
                        var newData = angular.copy(data.payload.listOfStcOpportunity.opportunity[0]);
                        if (newData.listOfopportunityPosition === "") {
                            newData.assignee = oldData.listOfopportunityPosition.opportunityPosition[0].salesRep;
                        } else {
                            newData.assignee = newData.listOfopportunityPosition.opportunityPosition[0].salesRep;
                        }
                        if (newData.listOfopportunityContact === "") {
                            newData.contactId = oldData.keyContactId;
                        } else {
                            newData.contactId = newData.listOfopportunityContact.opportunityContact[0].employeeNumber;
                        }
                        if (newData.salesStage === "") {
                            newData.salesStage = oldData.salesStage;
                        }
                        if (newData.sTCSubStatusSME === "") {
                            newData.sTCSubStatusSME = oldData.sTCSubStatusSME;
                        }
                        var newArray = {
                            "accountId": newData.accountId,
                            "description": newData.description,
                            "rowId": newData.rowId,
                            "name": newData.name,
                            "salesStage": newData.salesStage,
                            "sTCOptyCity": newData.sTCOptyCity,
                            "sTCOpportunityType": newData.sTCOpportunityType,
                            "sTCBulkLeadContactFirstName": newData.sTCBulkLeadContactFirstName,
                            "sTCSubStatusSME": newData.sTCSubStatusSME,
                            "sTCLeadNumber": newData.sTCLeadNumber,
                            "sTCCommercialRegn": newData.sTCCommercialRegn,
                            "sTCLeadCampaignName": newData.sTCLeadCampaignName,
                            "sTCBestTimeToCall": newData.sTCBestTimeToCall,
                            "sTCBulkLeadContactNumber": newData.sTCBulkLeadContactNumber,
                            "sTCFeedback": newData.sTCFeedback,
                            "sTCContactNumber2": newData.sTCContactNumber2,
                            "sTCFeedback2": newData.sTCFeedback2,
                            "sTCContactNumber3": newData.sTCContactNumber3,
                            "sTCFeedback3": newData.sTCFeedback3,
                            "assignee": newData.assignee,
                            "contactId": newData.contactId
                        }
                        var objectsAreSame = true;
                        for (var propertyName in oldArray) {
                            if (oldArray[propertyName] !== newArray[propertyName]) {
                                objectsAreSame = false;
                                break;
                            }
                        }
                        return objectsAreSame;
                    }
                    //update oppty API call
                    scope.updateOppData = function (ev, oppData, oppDetailData) {
                        var validationResult = validateOppUpdate();
                        if (validationResult === '') {
                            var result = function (confirm) {
                                if (confirm) {
                                    //oppty update - success
                                    var oppUpdateSuccess = function (data) {
                                        if (data.data.errorMessage === 'Success') {
                                            scope.detailModel.showDetailsAddLoader = false;
                                            MaterialReusables.showToast('' + pluginService.getTranslations().oppoUpdated, 'success');
                                            $state.go('root.oppList');
                                            reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId, data.data.listOfStcOpportunity.opportunity[0].salesStage);
                                        } else if (data.data.errorMessage !== 'Success') {
                                            scope.detailModel.showDetailsAddLoader = false;
                                            Util.throwError(data.data.errorMessage, MaterialReusables);
                                        } else {
                                            scope.detailModel.showDetailsAddLoader = false;
                                            MaterialReusables.showToast('' + pluginService.getTranslations().oppUpFail, 'error');
                                        }
                                    };
                                    //oppty update - error
                                    var oppUpdateError = function (data) {
                                        scope.detailModel.showDetailsAddLoader = false;
                                        Util.throwError(data.data.Message, MaterialReusables);
                                    };
                                    //get arabic name of status selected
                                    if (oppDetailData.statusList.salesStage === 'Lead') {
                                        scope.statusSelected = 'Lead ~ فرصه';
                                    } else if (oppDetailData.statusList.salesStage === 'Contact customer') {
                                        scope.statusSelected = 'Contact customer ~ الاتصال بالعميل';
                                    } else if (oppDetailData.statusList.salesStage === 'Customer Visit') {
                                        scope.statusSelected = 'Customer Visit ~ زياره العميل';
                                    } else if (oppDetailData.statusList.salesStage === 'Lead Enrichment') {
                                        scope.statusSelected = 'Lead Enrichment ~ معلومات اضافية عن الفرصة';
                                    } else if (oppDetailData.statusList.salesStage === 'Lost') {
                                        if (PROD_BUILD) {
                                            scope.statusSelected = 'Lost ~ فقدان';
                                        } else {
                                            scope.statusSelected = 'Lost~ فقدان';
                                        }
                                    } else if (oppDetailData.statusList.salesStage === 'Proposal') {
                                        scope.statusSelected = 'Proposal ~ اقتراح';
                                    } else if (oppDetailData.statusList.salesStage === 'Win') {
                                        scope.statusSelected = 'Win ~ الربح';
                                    }
                                    //pass null if status and sub status not changed
                                    if (scope.detailModel.details.salesStage === scope.statusSelected) {
                                        scope.statusSelected = null;
                                    }
                                    //pass null if status and sub status not changed
                                    if (scope.detailModel.details.sTCSubStatusSME === oppDetailData.subStatusList.sTCSubStatusSME) {
                                        scope.subStatusSelected = null;
                                    } else {
                                        scope.subStatusSelected = oppDetailData.subStatusList.sTCSubStatusSME;
                                    }
                                    //map updated contact id if new contact selected
                                    if (angular.isDefined(scope.detailModel.details.contactId)) {
                                        if (scope.detailModel.details.keyContactId !== scope.detailModel.details.contactId) {
                                            scope.updatedContactId = scope.detailModel.details.contactId;
                                        } else {
                                            scope.updatedContactId = scope.detailModel.details.keyContactId;
                                        }
                                    } else {
                                        scope.updatedContactId = scope.detailModel.details.keyContactId;
                                    }
                                    //mark auth person if contact num newly added
                                    if (oppData.sTCBulkLeadContactNumber === null || oppData.sTCBulkLeadContactNumber === '') {
                                        oppData.sTCFeedback = '';
                                    } else {
                                        if (oppData.sTCFeedback2 !== 'Authorized person' && oppData.sTCFeedback3 !== 'Authorized person') {
                                            oppData.sTCFeedback = 'Authorized person';
                                        }
                                    }
                                    //set feed back to null if user deleted mapped contact and updating
                                    if (oppData.sTCContactNumber2 === null || oppData.sTCContactNumber2 === '') {
                                        oppData.sTCFeedback2 = '';
                                    }
                                    //set feed back to null if user deleted mapped contact and updating
                                    if (oppData.sTCContactNumber3 === null || oppData.sTCContactNumber3 === '') {
                                        oppData.sTCFeedback3 = '';
                                    }
                                    //push update data for offline
                                    var pushDetailstoDB = function (payload) {
                                        var detailDBPushStatus = function (result, params) {
                                            $state.go($state.current.previousState);
                                            scope.detailModel.showDetailsAddLoader = false;
                                        };
                                        if (scope.detailModel.tempDetailStorage.accountNumber !== scope.detailModel.details.accountNumber) {
                                            payload.payload.listOfstcOpportunity.opportunity[0].accountNo = scope.detailModel.details.accountNumber;
                                        } else {
                                            payload.payload.listOfstcOpportunity.opportunity[0].accountNo = '';
                                        }
                                        scope.updateoppFn({ params: payload, lastParams: '', type: 'Detail', action: 'update', callback: detailDBPushStatus });
                                    };
                                    //update oppty api call
                                    var postDetailsOnline = function (payload) {
                                        apiService.fetchDataFromApi('opp.leadProcess', payload, oppUpdateSuccess, oppUpdateError);
                                    };
                                    //lead process payload generation
                                    var headerData = {
                                        "accountId": oppData.accountId,
                                        "description": oppData.description,
                                        "rowId": oppData.rowId,
                                        "name": oppData.name,
                                        "salesStage": scope.statusSelected,
                                        "sTCOptyCity": scope.sTCOptyCityName,
                                        "sTCOpportunityType": oppDetailData.oppTypeList.sTCOpportunityType,
                                        "sTCBulkLeadContactFirstName": oppData.sTCBulkLeadContactFirstName,
                                        "sTCSubStatusSME": scope.subStatusSelected,
                                        "sTCLeadNumber": oppData.sTCLeadNumber,
                                        "sTCCommercialRegn": oppData.sTCCommercialRegn,
                                        "sTCLeadCampaignName": oppData.sTCLeadCampaignName,
                                        "sTCBestTimeToCall": oppDetailData.preferredTimeList.sTCBestTimeToCall,
                                        "sTCBulkLeadContactNumber": oppData.sTCBulkLeadContactNumber,
                                        "sTCFeedback": oppData.sTCFeedback,
                                        "sTCContactNumber2": oppData.sTCContactNumber2,
                                        "sTCFeedback2": oppData.sTCFeedback2,
                                        "sTCContactNumber3": oppData.sTCContactNumber3,
                                        "sTCFeedback3": oppData.sTCFeedback3,
                                        "operation": "updatesync"
                                    };
                                    var assigneeData = {
                                        "isPrimaryMVG": "Y",
                                        "position": scope.updatedAssignedToPosition,
                                        "operation": "insertsync"
                                    };
                                    var contactData = {
                                        "personUId": scope.detailModel.details.contactId,
                                        "employeeNumber": scope.detailModel.details.contactId,
                                        "firstName": scope.contactFirstName,
                                        "lastName": scope.contactLaststName,
                                        "isPrimaryMVG": "N",
                                        "operation": "insertsync"
                                    };
                                    //assignedTofield mapping update if assigned to is changed
                                    angular.forEach(scope.detailModel.details.listOfopportunityPosition.opportunityPosition, function (value, key) {
                                        if (value.isPrimaryMVG === 'Y' && value.salesRep !== null) {
                                            if (value.salesRep === oppDetailData.activeLoginName) {
                                                assigneeData = '';
                                            }
                                        }
                                    });
                                    //contact update to opportunity api call if contact changed
                                    if (angular.isDefined(scope.detailModel.details.contactId)) {
                                        if (scope.detailModel.tempDetailStorage.keyContactId === scope.detailModel.details.contactId) {
                                            contactData = '';
                                        }
                                    } else {
                                        contactData = '';
                                    }
                                    var oppUpdatePayLoad = Util.leadProcessPayload(headerData, assigneeData, contactData, '', '', '', '', '');
                                    //offline oppty update payload
                                    var oppUpdateOfflinePayLoad = {
                                        'payload': {
                                            "statusObject": "",
                                            "objectLevelTransactions": "",
                                            "setMinimalReturns": "",
                                            "listOfstcOpportunity": {
                                                "opportunity": [{
                                                    "accountId": oppData.accountId ? oppData.accountId : "",
                                                    "contactId": scope.updatedContactId ? scope.updatedContactId : "",
                                                    "description": oppData.description ? oppData.description : "",
                                                    "id": oppData.rowId ? oppData.rowId : "",
                                                    "name": oppData.name ? oppData.name : "",
                                                    "salesStage": scope.statusSelected ? scope.statusSelected : "",
                                                    "stcOptyCity": scope.sTCOptyCityName ? scope.sTCOptyCityName : oppData.sTCOptyCity,
                                                    "stcOpportunityType": oppDetailData.oppTypeList.sTCOpportunityType ? oppDetailData.oppTypeList.sTCOpportunityType : "",
                                                    "sTCBulkLeadContactFirstName": oppData.sTCBulkLeadContactFirstName ? oppData.sTCBulkLeadContactFirstName : "",
                                                    "stcSubStatusSME": scope.subStatusSelected ? scope.subStatusSelected : "",
                                                    "stcLeadNumber": oppData.sTCLeadNumber ? oppData.sTCLeadNumber : "",
                                                    "stcCommercialRegn": oppData.sTCCommercialRegn ? oppData.sTCCommercialRegn : "",
                                                    "stcLeadCampaignName": oppData.sTCLeadCampaignName ? oppData.sTCLeadCampaignName : "",
                                                    "stcBestTimeToCall": oppDetailData.preferredTimeList.sTCBestTimeToCall ? oppDetailData.preferredTimeList.sTCBestTimeToCall : "",
                                                    "stcBulkLeadContactNumber": oppData.sTCBulkLeadContactNumber ? oppData.sTCBulkLeadContactNumber : "",
                                                    "stcFeedback": oppData.sTCFeedback ? oppData.sTCFeedback : "",
                                                    "stcContactNumber2": oppData.sTCContactNumber2 ? oppData.sTCContactNumber2 : "",
                                                    "stcFeedback2": oppData.sTCFeedback2 ? oppData.sTCFeedback2 : "",
                                                    "stcContactNumber3": oppData.sTCContactNumber3 ? oppData.sTCContactNumber3 : "",
                                                    "stcFeedback3": oppData.sTCFeedback3 ? oppData.sTCFeedback3 : ""
                                                }]
                                            },
                                            "messageId": "",
                                            "busObjCacheSize": ""
                                        }
                                    };
                                    //check and proceed based on network state
                                    if (!sharedDataService.getNetworkState()) {
                                        scope.detailModel.showDetailsAddLoader = true;
                                        pushDetailstoDB(oppUpdateOfflinePayLoad);
                                    } else {
                                        var changesExists = checkChangesExists(oppUpdatePayLoad);
                                        if (!changesExists) {
                                            scope.detailModel.showDetailsAddLoader = true;
                                            postDetailsOnline(oppUpdatePayLoad);
                                        } else { MaterialReusables.showSuccessAlert('Nothing To Update', angular.noop); }
                                    }
                                }
                            };
                            MaterialReusables.showConfirmDialog(event, '' + pluginService.getTranslations().oppoUpdateConfirm, result);
                        } else { MaterialReusables.showToast('' + validationResult, 'error'); }
                    };
                    //set values on assginee change
                    scope.updateAssignedPosition = function (data) {
                        //scope.detailModel.showAssignedToList = false;
                        MaterialReusables.hideDialog();
                        if (data.name !== null) {
                            scope.detailModel.activeLoginName = data.activeLoginName;
                            scope.updatedAssignedToPosition = data.name;
                        } else {
                            scope.updatedAssignedToPosition = null;
                        }
                    };
                    //fetch and load accounts in accounts popup
                    // scope.fetchAccountPart = function (searchText, startRow, type) {
                    //     scope.detailModel.loadingAccounts = true;
                    //     var oppAccountSuccess = function (data) {
                    //         if (data.data !== null) {
                    //             if (data.data.account !== null && data.data.account[0].name) {
                    //                 if (data.data.account.length < 10) {
                    //                     scope.detailModel.lastAccountReached = true;
                    //                 } else {
                    //                     scope.detailModel.lastAccountReached = false;
                    //                 }
                    //                 scope.detailModel.accountStartRow = parseInt(scope.detailModel.accountStartRow, 10) + 10;
                    //                 scope.detailModel.loadingAccounts = false;
                    //                 angular.forEach(data.data.account, function (value, key) {
                    //                     scope.detailModel.accountNames.push({ 'id': value.rowId, 'name': value.name, 'contacts': value.contacts });
                    //                 });
                    //             } else {
                    //                 if (type === 'loadMore') {
                    //                     MaterialReusables.showToast('' + pluginService.getTranslations().noAssigneesForSearch, 'warning');
                    //                 } else {
                    //                     MaterialReusables.showToast('' + pluginService.getTranslations().noaccount, 'error');
                    //                 }
                    //                 scope.detailModel.loadingAccounts = false;
                    //                 scope.detailModel.lastAccountReached = true;
                    //                 scope.detailModel.details.account = '';
                    //             }
                    //         } else {
                    //             if (type === 'loadMore') {
                    //                 MaterialReusables.showToast('' + pluginService.getTranslations().noAssigneesForSearch, 'warning');
                    //             } else {
                    //                 MaterialReusables.showToast('' + pluginService.getTranslations().noaccount, 'error');
                    //             }
                    //             scope.detailModel.loadingAccounts = false;
                    //             scope.detailModel.lastAccountReached = true;
                    //             scope.detailModel.details.account = '';
                    //         }
                    //     };
                    //     var oppAccountError = function (data) {
                    //         scope.detailModel.loadingAccounts = false;
                    //         scope.detailModel.lastAccountReached = true;
                    //         Util.throwError(data.data.Message, MaterialReusables);
                    //     };
                    //     //check text Arabic/English
                    //     var arabic = /[\u0600-\u06FF]/;
                    //     if (arabic.test(searchText)) {
                    //         scope.searchAccount = "*" + searchText;
                    //     } else {
                    //         scope.searchAccount = searchText + "*";
                    //     }
                    //     var newQuery = '';
                    //     if (startRow === 0) {
                    //         newQuery = true;
                    //     } else {
                    //         newQuery = false;
                    //     }
                    //     var oppAccountPayload = {
                    //         "sortSpec": "",
                    //         "searchSpec": "(([Account.STC Managed]='No' AND [Account.Account Type Code]='Customer') AND ([Account.Name] LIKE '" + scope.searchAccount + "' OR [Account.STC Commercial Registration Number] LIKE  '" + scope.searchAccount + "' OR [Account.CSN] LIKE  '" + scope.searchAccount + "' ))",
                    //         "startRowNum": startRow,
                    //         "pageSize": 10,
                    //         "newQuery": newQuery,
                    //         "viewMode": "All",
                    //         "busObjCacheSize": "10",
                    //         "outputIntObjectName": "STC Account Contact"
                    //     };
                    //     apiService.fetchDataFromApi('opp.account', oppAccountPayload, oppAccountSuccess, oppAccountError);
                    // };
                    //check and proceed to accounts api call
                    // scope.getAccountNames = function (searchText) {
                    //     if ((searchText.length >= 5) && angular.isDefined(searchText)) {
                    //         scope.detailModel.accountQuery = searchText;
                    //         scope.detailModel.loadingAccounts = true;
                    //         scope.detailModel.lastAccountReached = true;
                    //         scope.detailModel.accountStartRow = 0;
                    //         scope.detailModel.accountNames = [];
                    //         scope.fetchAccountPart(searchText, scope.detailModel.accountStartRow, 'firstCall');
                    //     } else {
                    //         MaterialReusables.showToast('' + pluginService.getTranslations().min5charctrs, 'error');
                    //     }
                    // };
                    //on contact select/change
                    scope.oppContactSelect = function (contact) {
                        scope.detailModel.details.contactId = contact.contactId;
                        scope.detailModel.details.keyContactId = contact.contactId;
                        scope.contactFirstName = contact.firstName;
                        scope.contactLaststName = contact.lastName;
                    };
                    //set opty type on select and change and broadcast it to all details tab
                    scope.oppTypeChange = function (type) {
                        $rootScope.$broadcast('pushOpType', { prodObj: type });
                    };
                    //check item exists in array or not
                    var itemExists = function (array, item) {
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].contactName === item) return i;
                        }
                        return -1;
                    }
                    //fetch contacts from selected account, if not fetch from API
                    scope.accountOnSelect = function (selectedAccount) {
                        MaterialReusables.hideDialog();
                        scope.detailModel.contactNameList = [];
                        scope.detailModel.details.contactId = '';
                        scope.detailModel.details.keyContactId = '';
                        scope.detailModel.details.account = selectedAccount.name;
                        scope.detailModel.details.accountId = selectedAccount.id;
                        $rootScope.$broadcast('pushAcount', { accountObj: scope.detailModel.details.accountId });
                        if (angular.isDefined(selectedAccount.contacts)) {
                            if (selectedAccount.contacts.length > 0) {
                                angular.forEach(selectedAccount.contacts, function (value, key) {
                                    if (itemExists(scope.detailModel.contactNameList, value.FirstName + ' ' + value.lastName) == -1) {
                                        scope.detailModel.contactNameList.push({ "contactName": value.FirstName + ' ' + value.lastName, "contactId": value.contactId, "firstName": value.FirstName, "lastName": value.lastName });
                                    }
                                });
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().nooAccCFou, 'error');
                            }
                        } else {
                            MaterialReusables.showToast('' + pluginService.getTranslations().nooAccCFou, 'error');
                        }
                    };
                    //discard pending changes in update tab
                    scope.discardOppChanges = function () {
                        scope.pendingChanges();
                    };
                    //invoke camera taken photo for OCR processing
                    scope.getPhoto = function () {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().featureNotAvblInOffline, 'warning');
                        } else {
                            var camerFailed = function (msg) { };
                            var cameraOpened = function (imageURI) {
                                var ocrSuccess = function (ocrData) {
                                    extractedNumbers = [];
                                    var contactNumber = get_numbers(ocrData.data.ParsedResults[0].ParsedText);
                                    try {
                                        if (contactNumber.length !== 0) {
                                            for (var i = 0; i < contactNumber.length; i++) {
                                                extractedNumbers.push(contactNumber[i].trim());
                                            }
                                            validateContact(extractedNumbers);
                                            if (scope.cardProcessed) {
                                                uploadBusinessCard(imageURI);
                                            }
                                        } else {
                                            scope.detailModel.details.sTCBulkLeadContactNumber = '';
                                            scope.detailModel.ocrLoading = false;
                                            MaterialReusables.showAlert('' + pluginService.getTranslations().ocrRecog);
                                        }
                                    } catch (error) {
                                        scope.detailModel.details.sTCBulkLeadContactNumber = '';
                                        scope.detailModel.ocrLoading = false;
                                        MaterialReusables.showAlert('' + pluginService.getTranslations().ocrRecog);
                                    }
                                };
                                var ocrFailed = function (msg) { };
                                scope.detailModel.ocrLoading = true;
                                pluginService.getOCRDetails($http, imageURI, ocrSuccess, ocrFailed);
                            };
                            pluginService.openCamera(cameraOpened, camerFailed, true, 'default');
                        }
                    };
                    //set status on select and change
                    scope.oppStatusChange = function (status) {
                        statusSwitch(status);
                        scope.currentSubStatusIndex = '0';
                    };
                    //set sub status on select and change
                    scope.oppSubStatusChange = function (subStatus) {
                    };
                    //check for valid digits
                    function get_numbers(input) {
                        return input.match(/([0-9+-. ]{8,16})\d/g);
                    };
                    //fecth assignee section by section by API call
                    scope.fetchAssignByPart = function (query, assignStartRow) {
                        scope.detailModel.loadingMoreAssigns = true;
                        var positionAssignedPayload = {
                            'payload': {
                                "busObjCacheSize": "3",
                                "newQuery": "true",
                                "outputIntObjectName": "STC Position - LMS Mobile App",
                                "pageSize": "100",
                                "searchSpec": query,
                                "sortSpec": "Active Login Name",
                                "startRowNum": "" + assignStartRow,
                                "viewMode": ""
                            }
                        };
                        var assignedToSuccess = function (data) {
                            if (data.data.listOfStcPositionLmsMobileApp.lastpage === 'false') {
                                scope.detailModel.lastAssigneeReached = false;
                            } else {
                                scope.detailModel.lastAssigneeReached = true;
                            }
                            scope.detailModel.loadingMoreAssigns = false;
                            scope.detailModel.assignStartRow = parseInt(scope.detailModel.assignStartRow, 10) + 100;
                            if (angular.isDefined(data.data.listOfStcPositionLmsMobileApp.position)) {
                                for (var i = 0; i < data.data.listOfStcPositionLmsMobileApp.position.length; i++) {
                                    scope.detailModel.assignedTo.push({ "activeLoginName": data.data.listOfStcPositionLmsMobileApp.position[i].activeLoginName, "name": data.data.listOfStcPositionLmsMobileApp.position[i].name });
                                }
                            } else {
                                scope.detailModel.loadingMoreAssigns = false;
                                scope.detailModel.lastAssigneeReached = true;
                                MaterialReusables.showToast('' + pluginService.getTranslations().noAssigneesFound, 'error');
                            }
                        };
                        var assignedToError = function (data) {
                            scope.detailModel.loadingMoreAssigns = false;
                            Util.throwError(data.data.Message, MaterialReusables);
                        };
                        apiService.fetchDataFromApi('opp.posQueryAssignedTo', positionAssignedPayload, assignedToSuccess, assignedToError);
                    };
                    //process data and prepare payload for assignee fecth
                    scope.fetchAssignedList = function (searchText) {
                        var oppAssignedToListSuccess = function (data) {
                            if (data.data.record.length !== 0) {
                                sharedDataService.saveAssignedToList(data);
                                if (data.data.record[0].stcPartnerId === null) {
                                    if (angular.isUndefined(searchText) || searchText === "") {
                                        scope.detailModel.assignQuery = "[Position.STC Div] = 'STC EBU SME'";
                                    } else {
                                        scope.detailModel.assignQuery = "  (([Position.STC Div] = 'STC EBU SME') AND (([Position.Name] LIKE '*" + searchText + "*') OR ([Position.Active Login Name] LIKE '*" + searchText + "*')))";
                                    }
                                } else if (Util.checkifPresent(data.data.record[0].name, 'Account') && data.data.record[0].stcPartnerId !== null) {
                                    if (angular.isUndefined(searchText) || searchText === "") {
                                        scope.detailModel.assignQuery = " (([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] = '" + data.data.record[0].stcPartnerId + "') OR ([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] IS NULL) OR ([Position.Name] LIKE '*Account*' AND [Position.STC Partner Id] IS NOT NULL))";
                                    } else {
                                        scope.detailModel.assignQuery = "((([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] = '" + data.data.record[0].stcPartnerId + "') OR ([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] IS NULL) OR ([Position.Name] LIKE '*Account*' AND [Position.STC Partner Id] IS NOT NULL)) AND ([Position.Active Login Name] LIKE '*" + searchText + "*') OR ([Position.Name] LIKE '*" + searchText + "*'))";
                                    }
                                } else {
                                    if (angular.isUndefined(searchText) || searchText === "") {
                                        scope.detailModel.assignQuery = "(([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] = '" + data.data.record[0].stcPartnerId + "') OR ([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] IS NULL))";
                                    } else {
                                        scope.detailModel.assignQuery = "((([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] = '" + data.data.record[0].stcPartnerId + "') OR ([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] IS NULL)) AND ([Position.Active Login Name] LIKE '*" + searchText + "*') OR ([Position.Name] LIKE '*" + searchText + "*'))";
                                    }
                                }
                                scope.fetchAssignByPart(scope.detailModel.assignQuery, scope.detailModel.assignStartRow);
                            }
                        };
                        var oppAssignedToListError = function (data) {
                            scope.detailModel.lastAssigneeReached = true;
                            scope.detailModel.loadingMoreAssigns = false;
                            Util.throwError(data.data.Message, MaterialReusables);
                        };
                        var assignedToPayLoad = {
                            "stcDiv": "",
                            "stcPartnerId": "",
                            "rowId": sharedDataService.getUserDetails().PrimaryPositionId
                        };
                        if (angular.isDefined(sharedDataService.getAssignedList()) && sharedDataService.getAssignedList().length !== 0) {
                            oppAssignedToListSuccess(sharedDataService.getAssignedList());
                        } else {
                            apiService.fetchDataFromApi('opp.assignedToList', assignedToPayLoad, oppAssignedToListSuccess, oppAssignedToListError);
                        }
                    };
                    //show assginee search and select popup
                    scope.assignedToList = function () {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().featureNotAvblInOffline, 'warning');
                        } else {
                            scope.detailModel.assignedTo = [];
                            scope.assignToQuery = '';
                            scope.detailModel.loadingMoreAssigns = false;
                            //scope.detailModel.showAssignedToList = true;
                            MaterialReusables.showCustomPopup(scope, 'app/shared/layout/assigneePopupTemplate.html');
                            scope.detailModel.lastAssigneeReached = true;
                        }
                    };
                    //invoke assignee popup
                    scope.assigneeSearch = function (searchText) {
                        scope.detailModel.assignedTo = [];
                        scope.detailModel.assignStartRow = 0;
                        scope.detailModel.loadingMoreAssigns = true;
                        scope.detailModel.lastAssigneeReached = true;
                        scope.fetchAssignedList(searchText);
                    };
                    //validate extracted mobile number and set to contact number filed
                    function validateContact(extractedNumbers) {
                        for (var i = 0; i < extractedNumbers.length; i++) {
                            if (extractedNumbers[i].length > 8) {
                                scope.detailModel.ocrLoading = false;
                                scope.cardProcessed = true;
                                if (scope.detailModel.details.sTCFeedback2 === 'Authorized person') {
                                    scope.detailModel.details.sTCContactNumber2 = extractedNumbers[i];
                                } else if (scope.detailModel.details.sTCFeedback3 === 'Authorized person') {
                                    scope.detailModel.details.sTCContactNumber3 = extractedNumbers[i];
                                } else {
                                    scope.detailModel.details.sTCBulkLeadContactNumber = extractedNumbers[i];
                                    scope.detailModel.details.sTCFeedback = 'Authorized person';
                                }
                                break;
                            } else {
                                scope.detailModel.details.sTCBulkLeadContactNumber = '';
                                scope.cardProcessed = false;
                            }
                        }
                    };
                    //invoke city popup
                    scope.cityOnSelect = function (city) {
                        MaterialReusables.hideDialog();
                        scope.detailModel.details.sTCOptyCity = city.value;
                        scope.sTCOptyCityName = city.name;
                    };
                    //close city popup
                    scope.cityOnClose = function () {
                        MaterialReusables.hideDialog();
                        scope.detailModel.loadMore = false;
                    };
                    //close assignee popup
                    scope.closeAssigneeListTab = function () {
                        MaterialReusables.hideDialog();
                    };
                    //invoke city search city
                    scope.searchCity = function () {
                        MaterialReusables.invokeCustomPopUp(scope, 'City');
                    };
                    //status change LOV -select and update
                    function statusSwitch(currentStatus) {
                        switch (currentStatus) {
                            case 'Lead':
                                scope.detailModel.subStatusList = leadSubStatusList;
                                scope.currentSubStatusIndex = '0';
                                break;
                            case 'Customer Visit':
                                scope.detailModel.subStatusList = custVisitSubStatusList;
                                scope.currentSubStatusIndex = getArrayIndex(custVisitSubStatusList, currentSubStatus);
                                break;
                            case 'Contact customer':
                                scope.detailModel.subStatusList = custContactSubStatusList;
                                scope.currentSubStatusIndex = getArrayIndex(custContactSubStatusList, currentSubStatus);
                                break;
                            case 'Proposal':
                                scope.detailModel.subStatusList = proposalSubStatusList;
                                scope.currentSubStatusIndex = getArrayIndex(proposalSubStatusList, currentSubStatus);
                                break;
                            case 'Win':
                                scope.detailModel.subStatusList = winSubStatusList;
                                scope.currentSubStatusIndex = getArrayIndex(winSubStatusList, currentSubStatus);
                                break;
                            case 'Lead Enrichment':
                                scope.detailModel.subStatusList = leadEnrichmentSubStatusList;
                                scope.currentSubStatusIndex = getArrayIndex(leadEnrichmentSubStatusList, currentSubStatus);
                                break;
                            case 'Lost':
                                scope.detailModel.subStatusList = lostSubStatusList;
                                scope.currentSubStatusIndex = getArrayIndex(lostSubStatusList, currentSubStatus);
                                break;
                            default:
                                scope.detailModel.subStatusList = leadSubStatusList;
                                scope.currentStatusIndex = '0';
                                scope.currentSubStatusIndex = '0';
                        }
                    }
                    //contact number - set feedback param
                    scope.oppNumberChange = function (newValue, index) {
                        if (scope.detailModel.details.sTCFeedback === 'Authorized person') {
                            scope.detailModel.details.sTCBulkLeadContactNumber = newValue.replace(/[^0-9-. ]/g, '').substring(0, 18);
                        } else if (scope.detailModel.details.sTCFeedback2 === 'Authorized person') {
                            scope.detailModel.details.sTCContactNumber2 = newValue.replace(/[^0-9-. ]/g, '').substring(0, 18);
                        } else if (scope.detailModel.details.sTCFeedback3 === 'Authorized person') {
                            scope.detailModel.details.sTCContactNumber3 = newValue.replace(/[^0-9-. ]/g, '').substring(0, 18);
                        } else {
                            scope.detailModel.details.sTCBulkLeadContactNumber = newValue.replace(/[^0-9-. ]/g, '').substring(0, 18);
                        }
                    };
                    //close accounts popup
                    scope.accountOnClose = function () {
                        MaterialReusables.hideDialog();
                    };
                    //invoke accounts popup
                    scope.accountList = function () {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                        } else {
                            scope.accountText = '';
                            scope.detailModel.accountNames = [];
                            scope.detailModel.loadingAccounts = false;
                            scope.detailModel.lastAccountReached = true;
                            //scope.detailModel.showAccountList = true;
                            MaterialReusables.invokeCustomPopUp(scope, 'Account');
                            //MaterialReusables.showCustomPopup(scope,'app/shared/layout/accountListUpdatePopupTemplate.html');
                        }
                    };
                    //check for pending pages before navigation
                    scope.pendingChanges = function () {
                        var changes = false;
                        if (scope.detailModel.tempDetailStorage.sTCOpportunityType !== scope.detailModel.oppTypeList.sTCOpportunityType) {
                            changes = true;
                        } else if (scope.detailModel.tempDetailStorage.accountId !== scope.detailModel.details.accountId) {
                            changes = true;
                        }
                        if (changes) {
                            var result = function (confirm) {
                                if (confirm) {
                                    $state.go($state.current.previousState);
                                }
                            };
                            MaterialReusables.showConfirmDialog(event, '' + pluginService.getTranslations().unsavedChangesConfirm, result);
                        } else {
                            $state.go($state.current.previousState);
                        }
                    };
                    //fetch contacts based on selected account number
                    var contactNamesFetch = function (accountId) {
                        var oppContactPayload = {
                            "accountId": accountId
                        };
                        var oppContactSuccess = function (data) {
                            if (data.data !== null) {
                                if (data.data.record !== null) {
                                    angular.forEach(data.data.record, function (value, key) {
                                        if (itemExists(scope.detailModel.contactNameList, value.FirstName + ' ' + value.lastName) == -1) {
                                            scope.detailModel.contactNameList.push({ "contactName": value.FirstName + ' ' + value.lastName, "contactId": value.contactId, "firstName": value.FirstName, "lastName": value.lastName });
                                        }
                                    });
                                } else {
                                    MaterialReusables.showToast('' + pluginService.getTranslations().nooAccCFou, 'warning');
                                }
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().nooAccCFou, 'warning');
                            }
                            scope.detailModel.isFetchingContacts = false;
                        };
                        var oppContactError = function (data) {
                            scope.detailModel.isFetchingContacts = false;
                            Util.throwError(data.data.Message, MaterialReusables);
                        };
                        scope.detailModel.isFetchingContacts = true;
                        apiService.fetchDataFromApi('opp.contactname', oppContactPayload, oppContactSuccess, oppContactError);
                    };
                    //push for pendingChanges() call
                    scope.$on('validate', function (event, args) {
                        scope.pendingChanges();
                    });
                    //set params on tab load
                    scope.$on('Details', function (event, data) {
                        MaterialReusables.hideDialog();
                    });
                    //name field - additional validation
                    scope.nameValidate = function (newValue) {
                        scope.detailModel.details.name = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                    //customer contact name field - additional validation
                    scope.custnameValidate = function (newValue) {
                        scope.detailModel.details.sTCBulkLeadContactFirstName = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                    //accout number field - additional validation
                    scope.accountNumberValidate = function (newValue) {
                        scope.detailModel.details.accountNumber = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                    //comments field - additional validation
                    scope.commentsValidate = function (newValue) {
                        scope.detailModel.details.description = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                    //account search input field - additional validation
                    scope.accountSearchValidate = function (newValue) {
                        scope.accountText = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                    //assginee search input field - additional validation
                    scope.assignedToValidate = function (newValue) {
                        scope.assignToQuery = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                    //initilize controller
                    var initializeDetailsController = function () {
                        //check and set value based on available products mapped in oppty
                        if (scope.details.allDetails.ListOfOpportunityProduct !== null) {
                            if (scope.details.allDetails.ListOfOpportunityProduct.OpportunityProduct.length === 0) {
                                scope.detailModel.noProductsAdded = true;
                            } else {
                                scope.detailModel.noProductsAdded = false;
                            }
                        } else {
                            scope.detailModel.noProductsAdded = true;
                        }
                        //check and set value for assignee is editable or not based on quotes created or not
                        var assigneeEditable = function (quotesList) {
                            if (quotesList !== null) {
                                if (quotesList.length > 0) {
                                    scope.detailModel.isAssigneeEditable = false;
                                }
                            }
                        };
                        //invoke assigneeEditable() if quotes are available
                        if (scope.details.allDetails.ListOfQuote2 !== null) {
                            assigneeEditable(scope.details.allDetails.ListOfQuote2.Quote2);
                        }
                        //invoke acitivity status vlaues if available in shared service
                        if (sharedDataService.getActivityList.length !== 0) {
                            scope.detailModel.activityStatusList = sharedDataService.getActivityList;
                        }
                        //set params according to phone number field
                        if (scope.detailModel.details.sTCLeadContactFirstName !== null || scope.detailModel.details.sTCLeadContactLastName !== null) {
                            scope.detailModel.details.sTCLeadContactFirstName = scope.detailModel.details.sTCLeadContactFirstName + ' ' + scope.detailModel.details.sTCLeadContactLastName;
                            scope.detailModel.contactNameList.contactName = scope.detailModel.details.sTCLeadContactFirstName;
                        }
                        //set params according to phone number field
                        if (scope.detailModel.details.sTCBulkLeadContactNumber !== null || scope.detailModel.details.sTCContactNumber2 !== null || scope.detailModel.details.sTCContactNumber3 !== null) {
                            if (scope.detailModel.details.sTCFeedback !== 'Authorized person' && scope.detailModel.details.sTCFeedback2 !== 'Authorized person' && scope.detailModel.details.sTCFeedback3 !== 'Authorized person') {
                                scope.detailModel.details.sTCBulkLeadContactNumber = null;
                                scope.detailModel.details.sTCFeedback = null;
                            }
                        }
                        //set LOV intdex = preferred time to call LOV
                        if (scope.detailModel.details.sTCBestTimeToCall === null) {
                            scope.currentPreferredTimeIndex = null;
                        } else {
                            scope.currentPreferredTimeIndex = getArrayIndex(scope.detailModel.preferredTimeList, scope.detailModel.details.sTCBestTimeToCall);
                        }
                        //set LOV intdex = oppty types LOV
                        if (scope.detailModel.details.sTCOpportunityType === 'New Sale') {
                            scope.currentOppTypeIndex = '0';
                        } else {
                            scope.currentOppTypeIndex = '1';
                        }
                        //set assignee value on page load
                        if (scope.detailModel.details.listOfopportunityPosition != null) {
                            if (scope.detailModel.details.listOfopportunityPosition.opportunityPosition.length !== 0) {
                                angular.forEach(scope.detailModel.details.listOfopportunityPosition.opportunityPosition, function (value, key) {
                                    if (value.isPrimaryMVG === 'Y' && value.salesRep !== null) {
                                        scope.detailModel.activeLoginName = value.salesRep;
                                    }
                                });
                            };
                        }
                        //check and select status from status list based on string match 
                        if (typeof scope.detailModel.details.salesStage !== 'undefined') {
                            currentStatus = scope.detailModel.details.salesStage;
                            currentSubStatus = scope.detailModel.details.sTCSubStatusSME;
                            if (currentStatus.includes('Lead')) {
                                currentStatus = 'Lead';
                                scope.detailModel.statusList = leadStatusToList;
                            } else if (currentStatus.includes('Customer Visit')) {
                                currentStatus = 'Customer Visit';
                                scope.detailModel.statusList = customerVisitToList;
                            } else if (currentStatus.includes('Contact customer')) {
                                currentStatus = 'Contact customer';
                                scope.detailModel.statusList = customerContactToList;
                            } else if (currentStatus.includes('Proposal')) {
                                currentStatus = 'Proposal';
                                scope.detailModel.statusList = proposalToList;
                            } else if (currentStatus.includes('Win')) {
                                currentStatus = 'Win';
                                scope.detailModel.statusList = winToList;
                            } else if (currentStatus.includes('Lost')) {
                                currentStatus = 'Lost';
                                scope.detailModel.statusList = lostToList;
                            }
                            statusSwitch(currentStatus);
                        };
                        //check and proceed for contacts fetch API call
                        if (scope.details.allDetails.accountId !== null) {
                            contactNamesFetch(scope.details.allDetails.accountId);
                        }
                        //get arabic city name for the selected city in arabic mode
                        if (scope.detailModel.details.sTCOptyCity !== null) {
                            if (sharedDataService.getLanguage() === 'ar_AR') {
                                var getArabicCityNamePayLoad = {
                                    "busObjCacheSize": "",
                                    "newQuery": true,
                                    "outputIntObjectName": "CRMDesktopListOfValuesIO",
                                    "pageSize": "10",
                                    "searchSpec": "[List Of Values.Type]='ACCOUNT_CITY' AND [List Of Values.Language]='ARA' AND [List Of Values.Active]='Y' AND [List Of Values.Name] LIKE '*" + scope.detailModel.details.sTCOptyCity + "*'",
                                    "sortSpec": "",
                                    "startRowNum": 0,
                                    "viewMode": ""
                                };
                                var arabicCityNameError = function (data) { };
                                var arabicCityNameSuccess = function (data) {
                                    if (data.data.listOfCRMDesktopListOfValuesIO !== null) {
                                        angular.forEach(data.data.listOfCRMDesktopListOfValuesIO.listOfValues, function (val, key) {
                                            if (val.name === scope.detailModel.details.sTCOptyCity) {
                                                scope.sTCOptyCityName = val.name;
                                                scope.detailModel.details.sTCOptyCity = val.value;
                                            }
                                        });
                                    }
                                };
                                apiService.fetchDataFromApi('opp.searchcity', getArabicCityNamePayLoad, arabicCityNameSuccess, arabicCityNameError);
                            } else {
                                scope.sTCOptyCityName = scope.detailModel.details.sTCOptyCity;
                            }
                        }
                        //fetch activity from shared service, if not API call fro status values
                        var fetchActivityStatusOnline = function () {
                            if (angular.isUndefined(sharedDataService.getActivityList())) {
                                var langtype = '';
                                if (sharedDataService.getLanguage() === 'en_US') {
                                    langtype = 'ENU';
                                } else {
                                    langtype = 'ARA';
                                }
                                var activityStatusPayLoad = {
                                    "busObjCacheSize": "",
                                    "newQuery": "",
                                    "outputIntObjectName": "CRMDesktopListOfValuesIO",
                                    "pageSize": "100",
                                    "searchSpec": "[List Of Values.Type]='EVENT_STATUS' AND [List Of Values.Language]='" + langtype + "' AND [List Of Values.Active]='Y' AND [List Of Values.Name] NOT LIKE 'Signed*'",
                                    "sortSpec": "",
                                    "startRowNum": "0",
                                    "viewMode": ""
                                }
                                apiService.fetchDataFromApi('opp.searchcity', activityStatusPayLoad, activityStatusSuccess, activityStatusError);
                            }
                        };
                        var activityStatusSuccess = function (data) {
                            if (data.data.listOfCRMDesktopListOfValuesIO != null) {
                                angular.forEach(data.data.listOfCRMDesktopListOfValuesIO.listOfValues, function (val, key) {
                                    scope.detailModel.activityStatusList.push({ "index": key, "name": val.name, "value": val.value });
                                });
                            }
                            sharedDataService.saveActivityStatusList(scope.detailModel.activityStatusList);
                        };
                        var activityStatusError = function (data) {
                            Util.throwError(data.data.Message, MaterialReusables);
                        };
                        var gotActivityStatus = function (activityStatus) {
                            if (activityStatus.length) {
                                tempActivityList = [];
                                angular.forEach(activityStatus, function (val, key) {
                                    tempActivityList.push(JSON.parse(val.activities));
                                });
                                sharedDataService.saveActivityStatusList(tempActivityList);
                            } else {
                                fetchActivityStatusOnline();
                            }
                        };
                        var failedToGetStatus = function () {
                            fetchActivityStatusOnline();
                        };
                        dbService.getDetailsFromTable('stc_activityStatus', [], failedToGetStatus, gotActivityStatus);
                    };
                    initializeDetailsController();
                }
            }
        }
    }
};
opportunityModule.directive('detailDetails', detailDetails);
detailDetails.$inject = ['$state', '$translate', '$rootScope', '$http', '$timeout', '$filter', 'sharedDataService', 'MaterialReusables', 'apiService', 'pluginService', 'dbService'];