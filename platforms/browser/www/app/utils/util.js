var Util = {
    scope: '',
    attachmentScope: '',
    pluginServiceScope: '',
    materialCalenderInst: '',
    appStates: '',
    initializeAppTheme: function (themeProvider) {
        var appPalette = themeProvider.extendPalette('deep-purple', { '500': '7b1fa2' });
        themeProvider.definePalette('customPalette', appPalette);
        themeProvider.theme('default').primaryPalette('customPalette').dark();
        themeProvider.theme("success-toast");
        themeProvider.theme("error-toast");
        themeProvider.theme("warning-toast");
        return themeProvider;
    },
    getChatCredentials: function () {
        var chatCredentials = {
            chatChannel: 'STC-CoreChannel',
            pubKey: 'pub-c-fcf7a1ba-a285-40d4-9323-0951d564f908',
            subKey: 'sub-c-af3e89e0-2436-11e7-a5a9-0619f8945a4f',
            cipherKey: 'stcwinnerchathandler@123'
        };
        return chatCredentials;
    },
    isChannelSubscribed: function (incomingMsg, channel) {
        var channelUsers = Util.getSubcribersFromChannel(incomingMsg);
        var subscribedChannel = '';
        var check1 = channel.channels.indexOf('' + channelUsers[0] + '-' + channelUsers[1] + '-channel');
        var check2 = channel.channels.indexOf('' + channelUsers[1] + '-' + channelUsers[0] + '-channel');
        if (check1 > -1) {
            subscribedChannel = '' + channelUsers[0] + '-' + channelUsers[1] + '-channel';
        }
        if (check2 > -1) {
            subscribedChannel = '' + channelUsers[1] + '-' + channelUsers[0] + '-channel';
        }
        return subscribedChannel;
    },
    getSubcribersFromChannel: function (channelName) {
        var subscribers = [];
        var channelStrng = channelName.substring(0, channelName.indexOf('-channel'));
        subscribers.push(channelStrng.substr(0, channelStrng.indexOf("-")));
        subscribers.push(channelStrng.substr(channelStrng.indexOf("-") + 1));
        return subscribers;
    },
    oppPayload: function (pageSize, searchSpec, sortSpec, startRow) {
        var query = '';
        if (startRow === '0' || startRow === 0) {
            query = true;
        } else {
            query = false;
        }
        var oppListPayload = {
            'payload': {
                'busObjCacheSize': '',
                'newQuery': query,
                'outputIntObjectName': 'STC Opportunity',
                'pageSize': pageSize,
                'searchSpec': searchSpec,
                'sortSpec': sortSpec,
                'startRowNum': startRow,
                'viewMode': '',
                'returnAttachment': 'false'
            }
        };
        return oppListPayload;
    },
    oppThinIoPayload: function (pageSize, searchSpec, sortSpec, startRow) {
        var query = '';
        if (startRow === '0' || startRow === 0) {
            query = true;
        } else {
            query = false;
        }
        var oppListThinPayload = {
            'payload': {
                'busObjCacheSize': '',
                'newQuery': query,
                'outputIntObjectName': 'STC Opportunity LMS Mobile App',
                'pageSize': pageSize,
                'searchSpec': searchSpec,
                'sortSpec': sortSpec,
                'startRowNum': startRow,
                'viewMode': '',
                'returnAttachment': 'false'
            }
        };
        return oppListThinPayload;
    },
    oppMgrPayload: function (username, salesStage, startRow, endRow) {
        var oppListMgrPayload = {
            'payload': {
                "login": username,
                "opptyStatus": salesStage,
                "startIndex": startRow,
                "endIndex": endRow
            }
        };
        return oppListMgrPayload;
    },
    validateElements: function (angular, tobeValidated) {
        var result = '';
        angular.forEach(tobeValidated, function (val, key) {
            if (angular.isUndefined(val.field) || val.field === null || val.field === '') {
                result = val.message;
            }
        });
        return result;
    },
    setCurrentScope: function (currentScope) {
        scope = currentScope;
    },
    getCurrentScope: function () {
        return scope;
    },
    gotoPreviousView: function (scope, state, rootScope) {
        if (!scope.headerComponent.validateCurrentPage) {
            var prevState = scope.headerComponent.previousState;
            state.go(prevState);
        } else { rootScope.$broadcast('validate', []); }
    },
    //api error handling
    fetchError: function (errorMessage) {
        if (angular.isDefined(errorMessage)) {
            if (errorMessage !== null && errorMessage.includes('\n')) {
                errorMessage = errorMessage.replace(/\n/g, '');
            }
        }
        return errorMessage;
    },
    //split siebel error geenral parts
    splitSiebelPart: function(data) {
        var exactError = null;
        data = data.split('returned the following error:')[1];
        data = data.split('\"(SBL')[0];
        exactError = data.split('\"')[1];
        return exactError;
    },
    //showError
    showError: function(error, MaterialReusables){
        var fetchedError = '';
        if (error.length < 150) {
            MaterialReusables.showToast(error, 'error');
            fetchedError = error;
        } else {
            if(error.includes('returned the following error:')){
                error = Util.splitSiebelPart(error);
                if(error !== null){
                    MaterialReusables.showToast(error, 'error');
                    fetchedError = error;
                } else {
                    error = error.substring(0, 147) + "...";
                    MaterialReusables.showToast(error, 'error');
                    fetchedError = error;
                }
            } else {
                error = error.substring(0, 147) + "...";
                MaterialReusables.showToast(error, 'error');
                fetchedError = error;
            }
        }
        return fetchedError;
    },
    //throw API response error
    throwError: function (errorString, MaterialReusables) {
        var fetchedError = '';
        if (errorString !== null) {
            if ((errorString === 'Token Verification Failed') || (errorString === 'Auth code validate failed. Either it could have expired or is an invalid one')) {
                if (!MaterialReusables.getDialogState() && appStates.current.name !== 'login') {
                    MaterialReusables.showLogoutPopup('' + Util.pluginServiceScope.getTranslations().sessionExpired);
                }
                setTimeout(function () {
                    appStates.go('login');
                    MaterialReusables.hideDialog();
                }, 2000);
            } else {
                if (errorString.length < 150) {
                    MaterialReusables.showToast(errorString, 'error');
                    fetchedError = errorString;
                } else {
                    var error = Util.fetchError(errorString);
                    if (error !== null) {
                        fetchedError = Util.showError(error, MaterialReusables);
                    } else {
                        MaterialReusables.showToast('' + Util.pluginServiceScope.getTranslations().servFail, 'error');
                        fetchedError = Util.pluginServiceScope.getTranslations().servFail;
                    }
                }
            }
        } else {
            MaterialReusables.showToast('' + Util.pluginServiceScope.getTranslations().internErr, 'error');
            fetchedError = Util.pluginServiceScope.getTranslations().internErr;
        }
        return fetchedError;
    },
    //throw Quote API response error
    throwQuoteResponse: function (errorString, MaterialReusables) {
        var quoteCreated = false;
        if (errorString !== null) {
            if (Util.checkifPresent(errorString, 'Success')) {
                quoteCreated = true;
            } else {
                var error = Util.fetchError(errorString);
                if (error !== null) {
                    if (error.length < 150) {
                        MaterialReusables.showToast(error, 'error');
                    } else {
                        error = error.substring(0, 147) + "...";
                        MaterialReusables.showToast(error, 'error');
                    }
                } else {
                    MaterialReusables.showToast('' + pluginServiceScope.getTranslations().servFail, 'error');
                }
            }
        } else {
            MaterialReusables.showToast('' + pluginServiceScope.getTranslations().internErr, 'error');
        }
        return quoteCreated;
    },
    setAttachmentScope: function (scope) {
        attachmentScope = scope;
    },
    //fetch base64 data from selected file
    getBase64fromInputFile: function (fileInfo, fileName) {
        var reader = new FileReader();
        reader.readAsDataURL(fileInfo);
        reader.onload = function () {
            attachmentScope.getDeviceAttachmentData(reader.result, fileName, '');
        };
        reader.onerror = function () {
            attachmentScope.getDeviceAttachmentData('', '', '');
        };
    },
    //fetch attachment and check attachment size
    getBottomSheetFileInfo: function (target, current) {
        attachmentScope.closeAttachmentOptions();
        if (target === 'storage') {
            attachmentScope.attachments.showuploadAttachments = true;
            attachmentScope.attachments.showAttachmentLoader = true;
            // var fileSize = (current.files[0].size / 1048576).toFixed(3);
            // if (fileSize < 10.1) {
                Util.getBase64fromInputFile(current.files[0], current.files[0].name);
            // } else {
            //     attachmentScope.getDeviceAttachmentData('', '', 'Please Select a File Less Than 10 MB');
            // }
        } else {
            attachmentScope.openAttachmentGallery();
        }
    },
    //check given word exists in string
    checkifPresent: function (stringtoCheck, string) {
        var result = false;
        if (stringtoCheck.indexOf(string) >= 0) {
            result = true;
        } else {
            result = false;
        }
        return result;
    },
    replaceAll: function (source, find, replace) {
        return source.split(find).join(replace);
    },
    //convert obj to sql
    convertObjectToSQL: function (params) {
        var particularColumn;
        var sqlParameterObject = [];
        for (var key in params) {
            if (params.hasOwnProperty(key)) {
                particularColumn = key + "='" + params[key] + "'";
                sqlParameterObject.push(particularColumn);
            }
        }
        return sqlParameterObject;
    },
    compareDate: function (dateToCheck, dateToCompareWith) {
        var dateCheck = false;
        if (dateToCheck > dateToCompareWith) {
            dateCheck = true;
        }
        return dateCheck;
    },
    containsObject: function (obj, list) {
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i] === obj) {
                return true;
            }
        }
        return false;
    },
    diff_minutes: function (dt2, dt1) {
        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.abs(Math.round(diff));
    },
    getMaterialCalenderInst: function () {
        return this.materialCalenderInst;
    },
    getFormattedDateTimeMonth:function(input){
      if(input.toString().length<2){
        input = '0'+input;
      }
      return input;
    },
    setMaterialCalenderInst: function (instance) {
        this.materialCalenderInst = instance;
    },
    getDateOrdinals: function (date) {
        var ordinal = ''
        if (date > 3 && date < 21)
            ordinal = 'th';
        switch (date % 10) {
            case 1: ordinal = 'st';
                break;
            case 2: ordinal = 'nd';
                break;
            case 3: ordinal = 'rd';
                break;
            default: ordinal = 'th';
        }
        return ordinal;
    },
    saveAppStates: function (state) {
        appStates = state;
    },
    payloadFormatter: function (payload, encKey) {
        var key = CryptoJS.enc.Hex.parse(encKey);
        var encryptedData = CryptoJS.AES.encrypt(JSON.stringify(payload), key, { format: Util.JsonFormatter, mode: CryptoJS.mode.CBC, iv: key });
        return { Data: JSON.parse(encryptedData.toString()).ct };
    },
    JsonFormatter: {
        stringify: function (cipherParams) {
            var jsonObj = {
                ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)
            };
            if (cipherParams.iv) {
                jsonObj.iv = cipherParams.iv.toString();
            }
            if (cipherParams.salt) {
                jsonObj.s = cipherParams.salt.toString();
            }
            return JSON.stringify(jsonObj);
        },
        parse: function (jsonStr) {
            var jsonObj = JSON.parse(jsonStr);
            var cipherParams = CryptoJS.lib.CipherParams.create({
                ciphertext: CryptoJS.enc.Base64.parse(jsonObj.ct)
            });
            if (jsonObj.iv) {
                cipherParams.iv = CryptoJS.enc.Hex.parse(jsonObj.iv)
            }
            if (jsonObj.s) {
                cipherParams.salt = CryptoJS.enc.Hex.parse(jsonObj.s)
            }
            return cipherParams;
        }
    },
    //sent data to native layer
    fetchLogoutToNativeLayer: function () {
        var logoutObject = { url: '', token: '', user: '', params: '' };
        var logOutData = {
            "userId": x_access_user,
            "token": x_access_token
        };
        logoutObject.url = base_url + '/services/authenticationinvalidate';
        logoutObject.token = x_access_token;
        logoutObject.user = x_access_user;
        logoutObject.params = Util.payloadFormatter(logOutData, encryptKey).Data;
        return logoutObject.url+","+logoutObject.token+","+logoutObject.user+","+logoutObject.params;
        //return JSON.stringify(logoutObject);
    },
    //lead process payload generator
    leadProcessPayload: function (headerData, assigneeData, contactData, noteData, attachmentData, productData, quoteData, actionData) {
        var commonPayload = {
            'payload': {
                "listOfStcOpportunity": {
                    "opportunity": [{
                        "operation": headerData.operation ? headerData.operation : 'query',
                        "account": headerData.account ? headerData.account : "",
                        "accountId": headerData.accountId ? headerData.accountId : "",
                        "accountNumber": headerData.accountNumber ? headerData.accountNumber : "",
                        "sTCCreatedBy": headerData.sTCCreatedBy ? headerData.sTCCreatedBy : "",
                        "decimalLatitude": headerData.decimalLatitude ? headerData.decimalLatitude : "",
                        "decimalLongitude": headerData.decimalLongitude ? headerData.decimalLongitude : "",
                        "degreeLatitude1": headerData.degreeLatitude1 ? headerData.degreeLatitude1 : "",
                        "degreeLatitude2": headerData.degreeLatitude2 ? headerData.degreeLatitude2 : "",
                        "degreeLatitude3": headerData.degreeLatitude3 ? headerData.degreeLatitude3 : "",
                        "degreeLongitude1": headerData.degreeLongitude1 ? headerData.degreeLongitude1 : "",
                        "degreeLongitude2": headerData.degreeLongitude2 ? headerData.degreeLongitude2 : "",
                        "degreeLongitude3": headerData.degreeLongitude3 ? headerData.degreeLongitude3 : "",
                        "description": headerData.description ? headerData.description : "",
                        "keyContactId": headerData.keyContactId ? headerData.keyContactId : "",
                        "name": headerData.name ? headerData.name : "",
                        "rowId": headerData.rowId ? headerData.rowId : "",
                        "PrimaryRevenueAmount": headerData.PrimaryRevenueAmount ? headerData.PrimaryRevenueAmount : "",
                        "sTCBestTimeToCall": headerData.sTCBestTimeToCall ? headerData.sTCBestTimeToCall : "",
                        "sTCBulkLeadContactFirstName": headerData.sTCBulkLeadContactFirstName ? headerData.sTCBulkLeadContactFirstName : "",
                        "sTCBulkLeadContactLastName": headerData.sTCBulkLeadContactLastName ? headerData.sTCBulkLeadContactLastName : "",
                        "sTCBulkLeadContactNumber": headerData.sTCBulkLeadContactNumber ? headerData.sTCBulkLeadContactNumber : "",
                        "sTCBulkLeadCreatedDate": headerData.sTCBulkLeadCreatedDate ? headerData.sTCBulkLeadCreatedDate : "",
                        "sTCCommercialRegn": headerData.sTCCommercialRegn ? headerData.sTCCommercialRegn : "",
                        "sTCLeadCampaignName": headerData.sTCLeadCampaignName ? headerData.sTCLeadCampaignName : "",
                        "sTCLeadContactFirstName": headerData.sTCLeadContactFirstName ? headerData.sTCLeadContactFirstName : "",
                        "sTCLeadContactLastName": headerData.sTCLeadContactLastName ? headerData.sTCLeadContactLastName : "",
                        "sTCLeadNumber": headerData.sTCLeadNumber ? headerData.sTCLeadNumber : "",
                        "sTCOpportunityPriority": headerData.sTCOpportunityPriority ? headerData.sTCOpportunityPriority : "",
                        "sTCOpportunityType": headerData.sTCOpportunityType ? headerData.sTCOpportunityType : "",
                        "sTCOptyCity": headerData.sTCOptyCity ? headerData.sTCOptyCity : "",
                        "sTCSubStatusSME": headerData.sTCSubStatusSME ? headerData.sTCSubStatusSME : "",
                        "salesStage": headerData.salesStage ? headerData.salesStage : "",
                        "salesStageId": headerData.salesStageId ? headerData.salesStageId : "",
                        "sTCBulkLeadCreatedBy": headerData.sTCBulkLeadCreatedBy ? headerData.sTCBulkLeadCreatedBy : "",
                        "sTCContactNumber2": headerData.sTCContactNumber2 ? headerData.sTCContactNumber2 : "",
                        "sTCContactNumber3": headerData.sTCContactNumber3 ? headerData.sTCContactNumber3 : "",
                        "sTCFeedback": headerData.sTCFeedback ? headerData.sTCFeedback : "",
                        "sTCFeedback2": headerData.sTCFeedback2 ? headerData.sTCFeedback2 : "",
                        "sTCFeedback3": headerData.sTCFeedback3 ? headerData.sTCFeedback3 : "",
                        "sTCCreatedByLogin": headerData.sTCCreatedByLogin ? headerData.sTCCreatedByLogin : "",
                        "listOfopportunityPosition": {
                            "opportunityPosition": [{
                                "salesRep": assigneeData.salesRep ? assigneeData.salesRep : "",
                                "isPrimaryMVG": assigneeData.isPrimaryMVG ? assigneeData.isPrimaryMVG : "",
                                "position": assigneeData.position ? assigneeData.position : "",
                                "operation": assigneeData.operation ? assigneeData.operation : ""
                            }]
                        },
                        "listOfopportunityContact": {
                            "opportunityContact": [{
                                "personUId": contactData.personUId ? contactData.personUId : "",
                                "employeeNumber": contactData.employeeNumber ? contactData.employeeNumber : "",
                                "firstName": contactData.firstName ? contactData.firstName : "",
                                "lastName": contactData.lastName ? contactData.lastName : "",
                                "isPrimaryMVG": contactData.isPrimaryMVG ? contactData.isPrimaryMVG : "",
                                "operation": contactData.operation ? contactData.operation : ""
                            }]
                        },
                        "ListOfOpportunityAttachment": {
                            "OpportunityAttachment": [{
                                "opptyFileDate": attachmentData.opptyFileDate ? attachmentData.opptyFileDate : "",
                                "opptyFileExt": attachmentData.opptyFileExt ? attachmentData.opptyFileExt : "",
                                "opptyFileName": attachmentData.opptyFileName ? attachmentData.opptyFileName : "",
                                "opptyId": attachmentData.opptyId ? attachmentData.opptyId : "",
                                "opptyName": attachmentData.opptyName ? attachmentData.opptyName : "",
                                "id": attachmentData.id ? attachmentData.id : "",
                                "content": attachmentData.content ? attachmentData.content : "",
                                "sTCCreatedByLogin": attachmentData.sTCCreatedByLogin ? attachmentData.sTCCreatedByLogin : "",
                                "comments": attachmentData.comments ? attachmentData.comments : "",
                                "sTCCreatedBy": attachmentData.sTCCreatedBy ? attachmentData.sTCCreatedBy : "",
                                "operation": attachmentData.operation ? attachmentData.operation : ""
                            }]
                        },
                        "ListOfOpportunityNote": {
                            "opportunityNote": [{
                                "createdByName": noteData.createdByName ? noteData.createdByName : "",
                                "createdDate": noteData.createdDate ? noteData.createdDate : "",
                                "note": noteData.note ? noteData.note : "",
                                "noteType": noteData.noteType ? noteData.noteType : "",
                                "id": noteData.id ? noteData.id : "",
                                "sTCCreatedByLogin": noteData.sTCCreatedByLogin ? noteData.sTCCreatedByLogin : "",
                                "sTCCreatedBy": noteData.sTCCreatedBy ? noteData.sTCCreatedBy : "",
                                "operation": noteData.operation ? noteData.operation : ""
                            }]
                        },
                        "ListOfOpportunityProduct": {
                            "OpportunityProduct": [{
                                "sTCCreatedBy": productData.sTCCreatedBy ? productData.sTCCreatedBy : "",
                                "productAliasName": productData.productAliasName ? productData.productAliasName : "",
                                "productId": productData.productId ? productData.productId : "",
                                "productActualName": productData.productActualName ? productData.productActualName : "",
                                "productQuantity": productData.productQuantity ? productData.productQuantity : "",
                                "quoteNumber2": productData.quoteNumber2 ? productData.quoteNumber2 : "",
                                "sTCServiceType2": productData.sTCServiceType2 ? productData.sTCServiceType2 : "",
                                "sTCRatePlanAliasName": productData.sTCRatePlanAliasName ? productData.sTCRatePlanAliasName : "",
                                "sTCRatePlanName": productData.sTCRatePlanName ? productData.sTCRatePlanName : "",
                                "sTCRatePlanId": productData.sTCRatePlanId ? productData.sTCRatePlanId : "",
                                "sTCQuoteComments": productData.sTCQuoteComments ? productData.sTCQuoteComments : "",
                                "id": productData.id ? productData.id : "",
                                "sTCCreatedByLogin": productData.sTCCreatedByLogin ? productData.sTCCreatedByLogin : "",
                                "stcServiceAccountId": productData.stcServiceAccountId ? productData.stcServiceAccountId : "",
                                "stcGSMAssetId": productData.stcGSMAssetId ? productData.stcGSMAssetId : "",
                                "stcGSMIntegrationId": productData.stcGSMIntegrationId ? productData.stcGSMIntegrationId : "",
                                "stcGSMPhoneNumber": productData.stcGSMPhoneNumber ? productData.stcGSMPhoneNumber : "",
                                "stcPrePostCategory": productData.stcPrePostCategory ? productData.stcPrePostCategory : "",
                                "operation": productData.operation ? productData.operation : ""
                            }]
                        },
                        "ListOfQuote2": {
                            "Quote2": [{
                                "description": quoteData.description ? quoteData.description : "",
                                "name3": quoteData.name3 ? quoteData.name3 : "",
                                "quoteNumber": quoteData.quoteNumber ? quoteData.quoteNumber : "",
                                "sTCLeadNumber": quoteData.sTCLeadNumber ? quoteData.sTCLeadNumber : "",
                                "status": quoteData.status ? quoteData.status : "",
                                "sTCCommercialRegn": quoteData.sTCCommercialRegn ? quoteData.sTCCommercialRegn : "",
                                "id": quoteData.id ? quoteData.id : "",
                                "sTCCreatedByLogin": quoteData.sTCCreatedByLogin ? quoteData.sTCCreatedByLogin : "",
                                "operation": quoteData.operation ? quoteData.operation : ""
                            }]
                        },
                        "listOfAction": {
                            "action": [{
                                "activityId": actionData.activityId ? actionData.activityId : "",
                                "alarm": actionData.alarm ? actionData.alarm : "",
                                "description2": actionData.description2 ? actionData.description2 : "",
                                "done": actionData.done ? actionData.done : "",
                                "doneFlag": actionData.doneFlag ? actionData.doneFlag : "",
                                "due": actionData.due ? actionData.due : "",
                                "planned": actionData.planned ? actionData.planned : "",
                                "plannedCompletion": actionData.plannedCompletion ? actionData.plannedCompletion : "",
                                "priority": actionData.priority ? actionData.priority : "",
                                "sTCEBUOpptyActivityType": actionData.sTCEBUOpptyActivityType ? actionData.sTCEBUOpptyActivityType : "",
                                "status": actionData.status ? actionData.status : "",
                                "id": actionData.id ? actionData.id : "",
                                "sTCCreatedByLogin": actionData.sTCCreatedByLogin ? actionData.sTCCreatedByLogin : "",
                                "sTCCreatedBy": actionData.sTCCreatedBy ? actionData.sTCCreatedBy : "",
                                "operation": actionData.operation ? actionData.operation : ""
                            }]
                        }
                    }]
                }
            }
        };
        if (assigneeData === "") {
            commonPayload.payload.listOfStcOpportunity.opportunity[0].listOfopportunityPosition = "";
        }
        if (contactData === "") {
            commonPayload.payload.listOfStcOpportunity.opportunity[0].listOfopportunityContact = "";
        }
        if (attachmentData === "") {
            commonPayload.payload.listOfStcOpportunity.opportunity[0].ListOfOpportunityAttachment = "";
        }
        if (noteData === "") {
            commonPayload.payload.listOfStcOpportunity.opportunity[0].ListOfOpportunityNote = "";
        }
        if (productData === "") {
            commonPayload.payload.listOfStcOpportunity.opportunity[0].ListOfOpportunityProduct = "";
        }
        if (quoteData === "") {
            commonPayload.payload.listOfStcOpportunity.opportunity[0].ListOfQuote2 = "";
        }
        if (actionData === "") {
            commonPayload.payload.listOfStcOpportunity.opportunity[0].listOfAction = "";
        }
        return commonPayload;
    },
    ////lead process payload generator - offline
    reviewLeadProcessPayload: function (id, type, action, payload, userId, userPosition) {
        var headerData = {};
        var operation = '';
        var reviewUpdatePayload = {};
        headerData.rowId = id;
        if (action === 'create') {
            operation = 'insert';
            headerData.operation = 'skipnode';
        } else if (action === 'edit') {
            operation = 'update'
            headerData.operation = 'skipnode';
        } else if (action === 'delete') {
            operation = 'delete';
            headerData.operation = 'delete';
        }
        if (type === 'Detail') {
            headerData.accountId = payload.listOfstcOpportunity.opportunity[0].accountId;
            headerData.description = payload.listOfstcOpportunity.opportunity[0].description;
            headerData.rowId = payload.listOfstcOpportunity.opportunity[0].id;
            headerData.name = payload.listOfstcOpportunity.opportunity[0].name;
            //headerData.sTCCreatedBy = userId;
            headerData.salesStage = payload.listOfstcOpportunity.opportunity[0].salesStage;
            headerData.sTCOptyCity = payload.listOfstcOpportunity.opportunity[0].stcOptyCity;
            headerData.sTCOpportunityType = payload.listOfstcOpportunity.opportunity[0].stcOpportunityType;
            headerData.sTCBulkLeadContactFirstName = payload.listOfstcOpportunity.opportunity[0].sTCBulkLeadContactFirstName;
            headerData.sTCSubStatusSME = payload.listOfstcOpportunity.opportunity[0].stcSubStatusSME;
            headerData.sTCBulkLeadCreatedBy = payload.listOfstcOpportunity.opportunity[0].stcBulkLeadCreatedBy;
            headerData.sTCLeadNumber = payload.listOfstcOpportunity.opportunity[0].stcLeadNumber;
            headerData.sTCCommercialRegn = payload.listOfstcOpportunity.opportunity[0].stcCommercialRegn;
            headerData.sTCLeadCampaignName = payload.listOfstcOpportunity.opportunity[0].stcLeadCampaignName;
            headerData.sTCBestTimeToCall = payload.listOfstcOpportunity.opportunity[0].stcBestTimeToCall;
            headerData.sTCBulkLeadContactNumber = payload.listOfstcOpportunity.opportunity[0].stcBulkLeadContactNumber;
            headerData.sTCFeedback = payload.listOfstcOpportunity.opportunity[0].stcFeedback;
            headerData.sTCContactNumber2 = payload.listOfstcOpportunity.opportunity[0].stcContactNumber2;
            headerData.sTCFeedback2 = payload.listOfstcOpportunity.opportunity[0].stcFeedback2;
            headerData.sTCContactNumber3 = payload.listOfstcOpportunity.opportunity[0].stcContactNumber3;
            headerData.sTCFeedback3 = payload.listOfstcOpportunity.opportunity[0].stcFeedback3;
            headerData.operation = 'updatesync';
            var assigneeData = '';
            var contactData = '';
            if (payload.listOfstcOpportunity.opportunity[0].accountNo !== '' &&
                payload.listOfstcOpportunity.opportunity[0].accountNo !== null) {
                contactData = {
                    "personUId": payload.listOfstcOpportunity.opportunity[0].contactId,
                    "employeeNumber": payload.listOfstcOpportunity.opportunity[0].contactId,
                    "firstName": payload.listOfstcOpportunity.opportunity[0].firstName,
                    "lastName": payload.listOfstcOpportunity.opportunity[0].lastName,
                    "isPrimaryMVG": "N",
                    "operation": "insertsync"
                };
            }
            var processedDetailPayload = Util.leadProcessPayload(headerData, assigneeData, contactData, '', '', '', '', '');
            reviewUpdatePayload = processedDetailPayload;
        } else if (type === 'Activity') {
            var activityParams = {};
            if (action === 'delete') {
                activityParams.id = payload.activityId
                activityParams.operation = operation;
            } else {
                activityParams.id = '1234';
                if (action === 'edit') {
                    activityParams.id = payload.activityId;
                    activityParams.activityId = payload.activityId;
                }
                if(action === 'create') {
                    activityParams.sTCCreatedBy = userId;
                }
                activityParams.alarm = payload.alarm;
                activityParams.description2 = payload.description;
                activityParams.doneFlag = payload.doneFlag;
                activityParams.due = payload.due;
                activityParams.planned = payload.planned;
                activityParams.plannedCompletion = payload.plannedCompletion;
                activityParams.priority = payload.priority;
                activityParams.sTCEBUOpptyActivityType = payload.stcEBUOpptyActivityType;
                activityParams.status = payload.status;
                activityParams.operation = operation;
            }
            var processedActivityPayload = Util.leadProcessPayload(headerData, '', '', '', '', '', '', activityParams);
            reviewUpdatePayload = processedActivityPayload;
        } else if (type === 'Note') {
            var noteParams = {};
            if (action === 'delete') {
                noteParams.id = payload.id;
                noteParams.operation = operation;
            } else {
                noteParams.id = '1234';
                if (action === 'edit') {
                    noteParams.id = payload.id;
                }
                if(action === 'create') {
                    noteParams.sTCCreatedBy = payload.sTCCreatedBy;
                }
                noteParams.note = payload.note;
                noteParams.noteType = payload.noteType;
                noteParams.operation = operation;
            }
            var processedNotePayload = Util.leadProcessPayload(headerData, '', '', noteParams, '', '', '', '');
            reviewUpdatePayload = processedNotePayload;
        } else if (type === 'Product') {
            var productParams = {};
            if (action === 'delete') {
                productParams.id = '';
                productParams.operation = operation;
            } else {
                productParams.id = '1234';
                if (action === 'edit') {
                    productParams.id = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].id;
                }
                if(action === 'create') {
                    productParams.sTCCreatedBy = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcCreatedBy;
                }
                productParams.productAliasName = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].productAliasName;
                productParams.productId = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].productId;
                productParams.productQuantity = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].productQuantity;
                productParams.sTCServiceType2 = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcServiceType2;
                productParams.sTCRatePlanAliasName = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcRatePlanAliasName;
                productParams.sTCRatePlanId = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcRatePlanId;
                productParams.sTCQuoteComments = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcQuoteComments
                productParams.stcServiceAccountId = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcServiceAccountId;
                productParams.stcGSMAssetId = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcGSMAssetId;
                productParams.stcGSMIntegrationId = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcGSMIntegrationId;
                productParams.stcGSMPhoneNumber = payload.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcGSMPhoneNumber;
                productParams.operation = operation;
            }
            var processedProductPayload = Util.leadProcessPayload(headerData, '', '', '', '', productParams, '', '');
            reviewUpdatePayload = processedProductPayload;
        }
        return reviewUpdatePayload;
    },
    //reporta ction payload
    reportActionPayload: function (reportData) {
        var reportPayload =
            [{
                "token": x_access_token,
                "opportunityId": reportData.opportunityId ? reportData.opportunityId : "",
                "action": reportData.action ? reportData.action : "",
                "totalRevenue": reportData.totalRevenue ? reportData.totalRevenue : "",
                "locationsCaptured": reportData.locationsCaptured ? reportData.locationsCaptured : "",
                "quotesCreated": reportData.quotesCreated ? reportData.quotesCreated : "",
                "createdLeads": reportData.createdLeads ? reportData.createdLeads : "",
                "cancelledLeads": reportData.cancelledLeads ? reportData.cancelledLeads : "",
                "actedLeads": reportData.actedLeads ? reportData.actedLeads : ""
            }]
        return reportPayload;
    },
    getDistanceFromLatLonInMeters:function(coords1,coords2) {
        var earthRad = 6371;
        var latDiff = Util.degreesToRadius(coords2.latitude-coords1.latitude);
        var longDiff = Util.degreesToRadius(coords2.longitude-coords1.longitude); 
        var a = Math.sin(latDiff/2) * Math.sin(latDiff/2) +
          Math.cos(Util.degreesToRadius(coords1.latitude)) * Math.cos(Util.degreesToRadius(coords2.latitude)) * 
          Math.sin(longDiff/2) * Math.sin(longDiff/2); 
        var arcTangent = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var distanceDiff = earthRad * arcTangent * 1000;
        return parseInt(distanceDiff);
    },
    degreesToRadius:function(degrees){
        return degrees * (Math.PI/180)
    }
};