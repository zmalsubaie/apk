var Service = {
    doPost : function(http, url, data, successCallback, errorCallback, configValue) {
        var config = {};
        if(typeof configValue !== 'undefined') {
            config = configValue;
        }
        return http.post(url, data, config).then(successCallback, errorCallback);
    },
    doGet : function(http, url, successCallback, errorCallback, configValue) {
        var config = {};
        if(typeof configValue !== 'undefined') {
            config = configValue;
        }
        return http.get(url, config).then(successCallback, errorCallback);
    },
    doPut : function(http, url, data, successCallback, errorCallback, configValue) {
        var config = {};
        if(typeof configValue !== 'undefined') {
            config = configValue;
        }
        return http.put(url,data,config).then(successCallback, errorCallback);
    }
};
