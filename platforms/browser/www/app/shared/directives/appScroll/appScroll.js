(function() {
  'use strict';
    var appScrollDirective = function(){
        var directiveObject = {
                restrict: 'A',
                scope:{
                listScrolled: '&onListScrolled'
                },
                link: function(scope,elem){
                    angular.element(elem).bind('scroll', function(event){
                        if(event.target.offsetHeight + event.target.scrollTop >= (event.target.scrollHeight-1)){
                            scope.listScrolled();
                        }
                    });
                }
        };
        return directiveObject;
    };
    stcApp.directive('appScroll', appScrollDirective);
})();