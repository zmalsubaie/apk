var navigationStack = [];
var DATABASE_NAME = '_STCStorage.db';
var DB_SIZE = 1000000;
var sslCertificateUrl = 'https://lms.stc.com.sa';
var sslCertificateFingerPrint = '10 8e ba f9 9e 7d 19 c5 97 dd 06 85 7d 17 ba 09 8f 62 56 c1';
var x_access_user = '';
var x_access_token = '';
var encryptKey = '';
var loginSecKey = '788becf1635547f59321b3f1a6f0ef28';
//var PROD_BUILD = true;    //production
var PROD_BUILD = false;     //TEST//DEV
//var DEVICE_BUILD = true; // when running to mobile devices
var DEVICE_BUILD = false; // when running to browser
var themeProvider = '';
var mainModuleProps_sidemenu = [
    {image:'assets/img/sidebar/home.png','name':'Home','route':'root.home','enable':true},
    {image:'assets/img/sidebar/Opportunity.png','name':'Opportunities','route':'root.oppList','enable':true},
    {image:'assets/img/sidebar/Inbox.png','name':'Inbox','route':'root.inbox','enable':true},
    {image:'assets/img/sidebar/Accounts.png','name':'Accounts','route':'root.account','enable':true},
    {image:'assets/img/sidebar/Quotes.png','name':'Quotes','route':'root.quote','enable':true},
    {image:'assets/img/sidebar/Dashboard.png','name':'Dashboard','route':'root.dashboard','enable':true},
    {image:'assets/img/sidebar/Map.png','name':'Map','route':'root.map','enable':true},
    {image:'assets/img/sidebar/Calendar.png','name':'Calendar','route':'root.calender','enable':true},
    {image:'assets/img/sidebar/Offline.png','name':'Offline','route':'root.review','enable':true},
    {image:'assets/img/review/review.svg','name':'Location Tracker','route':'root.trackerview','enable':true},
    {image:'assets/img/sidebar/Settings.png','name':'Settings','route':'root.settings','enable':true}
];
var mainModuleProps_dashboard = [
    {image:'assets/img/home_assets/Opportunity_CC.png','name':'Opportunities','route':'root.oppList','enable':true},
    {image:'assets/img/home_assets/Accounts_CC.png','name':'Accounts','route':'root.account','enable':true},
    {image:'assets/img/home_assets/Quotes_CC.png','name':'Quotes','route':'root.quote','enable':true},
    {image:'assets/img/home_assets/Dashboard_CC.png','name':'Dashboard','route':'root.dashboard','enable':true},
    {image:'assets/img/home_assets/Map_CC.png','name':'Map','route':'root.map','enable':true},
    {image:'assets/img/home_assets/Calendar_CC.png','name':'Calendar','route':'root.calender','enable':true},
];
var headerData ={
    'headers' : {
         'Content-Type': 'application/json; charset=utf-8',
         'x-access-user':"",
         'x-access-token':""
    }
};
var otp_header_data = { 
    'headers' : {
         'Content-Type': 'application/json; charset=utf-8',
         'x-access-user':"",
         'x-access-token':""
    },
    'params' : {
        'token':""
    }
};
var form_multipart_header = {
   'Content-Type': 'multipart/form-data; charset=utf-8'
};
var oppTypeList = [{index:0,'name':'New Sale'},{index:1,'name':'Upgrade'}];
var statusList = [
    {index:0,'name':'Lead'},
    {index:1,'name':'Customer Visit'},
    {index:2,'name':'Contact customer'},
    {index:3,'name':'Proposal'},
    {index:4,'name':'Win'},
    {index:5,'name':'Lead Enrichment'},
    {index:6,'name':'Lost'}
];
var leadStatusToList = [
    {index:0,'name':'Lead'},
    {index:1,'name':'Contact customer'},
    {index:2,'name':'Customer Visit'},
    {index:3,'name':'Lead Enrichment'},
    {index:4,'name':'Lost'}
];
var customerContactToList = [
     {index:0,'name':'Contact customer'},
     {index:1,'name':'Customer Visit'},
     {index:2,'name':'Lost'}
];
var customerVisitToList = [
     {index:0,'name':'Customer Visit'},
     {index:1,'name':'Proposal'},
     {index:2,'name':'Lost'}
];
var proposalToList = [
     {index:0,'name':'Proposal'},
     {index:1,'name':'Customer Visit'},
     {index:2,'name':'Lost'}
];
var leadEnrichmentToList = [
     {index:0,'name':'Lead Enrichment'},
     {index:1,'name':'Lead'}
];
var winToList = [
     {index:0,'name':'Win'}
];
var lostToList = [
     {index:0,'name':'Lost'}
];
var leadSubStatusList = [
   {index:0,'name':'Lead'}
];
var custVisitSubStatusList = [
   {index:0,'name':'Set Appt'},
   {index:1,'name':'Confirmed Appt'}
];
var custContactSubStatusList = [
    {index:0,'name':'Call back'},
    {index:1,'name':'No Answer 1'},
    {index:2,'name':'No Answer 2'}
];
var proposalSubStatusList = [
   {index:0,'name':'Contract Signed'},
   {index:1,'name':'Discount approval'},
   {index:2,'name':'Sent Proposal'},
   {index:3,'name':'Sent Quotation'},
   {index:4,'name':'Waiting Feedback'}
];
var winSubStatusList = [
   {index:0,'name':'Completed'},
   {index:1,'name':'Order Placed'}
];
var leadEnrichmentSubStatusList = [
   {index:0,'name':'Contact Information'},
   {index:1,'name':'Customer Portfolio'}
];
var lostSubStatusList = [
   {index:0,'name':'Better offers from competitor'},
   {index:1,'name':'Bill experience issue'},
   {index:2,'name':'Customer experience issue'},
   {index:3,'name':'He does not know the company'},
   {index:4,'name':'He has product with competitor'},
   {index:5,'name':'Has Same Product from Consumer'},
   {index:6,'name':'Line disconnected'},
   {index:7,'name':'Low Coverage/Tech specs'},
   {index:8,'name':'No Answer After Max attempt'},
   {index:9,'name':'No Need for service offered'},
   {index:10,'name':'other'},
   {index:11,'name':'Product offer is too expensive'},
   {index:12,'name':'Wrong No'}
];
var productTypeList = [
   {index:0,'name':'Data','type':'CRMD'},
   {index:1,'name':'Landline','type':'ICMS'},
   {index:2,'name':'Mobile','type':'CRMG'},
   {index:3,'name':'Other','type':'Other'}
];
var noteTypeList = [
   {index:0,'name':'General'},
   {index:1,'name':'Important'},
   {index:2,'name':'Expired'},
   {index:3,'name':'Success'},
   {index:4,'name':'Failed'}
];
var activityTypeList = [
   {index:0,'name':'Call'},
   {index:1,'name':'Meeting'}
];
var activityPriorityList = [
   {index:0,'name':'1-ASAP'},
   {index:1,'name':'2-High'},
   {index:2,'name':'3-Medium'},
   {index:3,'name':'4-Low'}
];
var activityStatusList = [
   {index:0,'name':'Approved'},
   {index:1,'name':'New'},
   {index:2,'name':'Done'},
   {index:3,'name':'In Progress'},
   {index:4,'name':'Not Started'},
   {index:5,'name':'Unscheduled'},
   {index:6,'name':'En Route'},
   {index:7,'name':'Signed'},
   {index:8,'name':'Transferred'},
   {index:9,'name':'Signed and Synchronized'},
   {index:10,'name':'Withdrawn'},
   {index:11,'name':'Draft'},
   {index:12,'name':'Open'},
   {index:13,'name':'Initiated'},
   {index:14,'name':'Scheduled'},
   {index:15,'name':'Reject'},
   {index:16,'name':'Cancelled'},
   {index:17,'name':'Port - In NPR Request Sent'},
   {index:18,'name':'Accepted'},
   {index:19,'name':'Send RFS Broadcast Message'},
   {index:20,'name':'PortOut Request Rejected'},
   {index:21,'name':'Deactivated'},
   {index:22,'name':'Charges Info Communicated'},
   {index:23,'name':'Final Bill Communicated'},
   {index:24,'name':'Port - In NPR Request Received'},
   {index:25,'name':'PortOut Request Accepted'},
   {index:26,'name':'PortOut Request On Hold'},
   {index:27,'name':'RFS Broadcast Message Received'},
   {index:28,'name':'Activated'},
   {index:29,'name':'Deactivate Broadcast Message'},
   {index:30,'name':'Deactivate Acknowlegment Sent'},
   {index:31,'name':'PortOut NPR Request Received'},
   {index:32,'name':'Rejected'},
   {index:33,'name':'NP Hold'},
   {index:34,'name':'Port-in Activated'},
   {index:35,'name':'Error Notification Received'},
   {index:36,'name':'Final Bill Not Paid'},
   {index:37,'name':'Final Bill Paid'},
   {index:38,'name':'Initial Validation Error'},
   {index:39,'name':'Final Bill Submitted'},
   {index:40,'name':'Balance Calculation'},
   {index:41,'name':'Downgrade Credit Limit'},
   {index:42,'name':'Payment Monitoring'},
   {index:43,'name':'Error in Modify Credit Limit'},
   {index:44,'name':'Bill Email Failed'},
   {index:45,'name':'Payment Request Not Sent'},
   {index:46,'name':'F/Bill Amount Not Paid'},
   {index:47,'name':'EAI is Down'},
   {index:48,'name':'Wait for Quarantine'},
   {index:49,'name':'Create SO Disconnection'},
   {index:50,'name':'Assigned'},
   {index:51,'name':'Catalog Sent'},
   {index:52,'name':'Closed'},
   {index:53,'name':'Completed'},
   {index:54,'name':'Customer Contacted'},
   {index:55,'name':'Gift Finalized'},
   {index:56,'name':'On Hold'},
   {index:57,'name':'SMS Sent'},
   {index:58,'name':'Cancel'},
   {index:59,'name':'Error Modify Credit Limit'},
   {index:60,'name':'Failed'},
   {index:61,'name':'Initial Bill Not Paid'},
   {index:62,'name':'Initially Failed'},
   {index:63,'name':'NP RFS Sent'},
   {index:64,'name':'Upgrade Credit Limit'}
];
var subCategoryList = [
    {'index': 0, 'name': 'Details','isSelected': true},
    {'index': 1,'name': 'Notes','isSelected': false},
    {'index': 2,'name': 'Attachments','isSelected': false},
    {'index': 3,'name': 'Products','isSelected': false},
    {'index': 4,'name': 'Quotes','isSelected': false},
    {'index': 5,'name': 'Activity','isSelected': false},
    {'index': 6,'name': 'Map','isSelected': false}
];
var preferredTimeList = [
   {index:0,'name':''},
   {index:1,'name':'Early afternoon'},
   {index:2,'name':'Early morning'},
   {index:3,'name':'Evening'},
   {index:4,'name':'Late afternoon'},
   {index:5,'name':'Mid-morning'}
];
var mimeTypes = {
    "doc":"application/msword",
    "dot":"application/msword",
    "docx":"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "dotx":"application/vnd.openxmlformats-officedocument.wordprocessingml.template",
    "docm":"application/vnd.ms-word.document.macroEnabled.12",
    "dotm":"application/vnd.ms-word.template.macroEnabled.12",
    "xls":"application/vnd.ms-excel",
    "xlt":"application/vnd.ms-excel",
    "xla":"application/vnd.ms-excel",
    "xlsx":"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "xltx":"application/vnd.openxmlformats-officedocument.spreadsheetml.template",
    "xlsm":"application/vnd.ms-excel.sheet.macroEnabled.12",
    "xltm":"application/vnd.ms-excel.template.macroEnabled.12",
    "xlam":"application/vnd.ms-excel.addin.macroEnabled.12",
    "xlsb":"application/vnd.ms-excel.sheet.binary.macroEnabled.12",
    "ppt":"application/vnd.ms-powerpoint",
    "pot":"application/vnd.ms-powerpoint",
    "pps":"application/vnd.ms-powerpoint",
    "ppa":"application/vnd.ms-powerpoint",
    "pptx":"application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "potx":"application/vnd.openxmlformats-officedocument.presentationml.template",
    "ppsx":"application/vnd.openxmlformats-officedocument.presentationml.slideshow",
    "ppam":"application/vnd.ms-powerpoint.addin.macroEnabled.12",
    "pptm":"application/vnd.ms-powerpoint.presentation.macroEnabled.12",
    "potm":"application/vnd.ms-powerpoint.template.macroEnabled.12",
    "ppsm":"application/vnd.ms-powerpoint.slideshow.macroEnabled.12",
    "mdb":"application/vnd.ms-access",
    "jpg":"image/jpg",
    "png":"image/png",
    "jpeg" :"image/jpeg",
    "xml":"application/xhtml+xml",
    "pdf":"application/pdf",
    "txt":"text/plain",
    "apk":"application/vnd.android.package-archive"
};
var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
var days_short = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
var months_AR = ['يناير','فبراير','مارس','إبريل','مايو','يونيو','يوليو','أغسطس','سبتمبر','أكتوبر','نوفمبر','ديسمبر'];
var days_AR = ['الأحد','الإثنين','الثلاثاء','الأربعاء','الخميس','الجمعة','السبت'];
var gsmTypeList = [{index:0,'name':'Prepaid'},{index:1,'name':'Postpaid'}];
var translationsKey = ['logOut_Msg','enter_credentials','attchmentDelete_confirm','productDelete_confirm','Cancel',
    'activityDelete_confirm','activityCreate_Failed','id_pswd_combination_error','endofList','businessCardUploaded',
    'updateOppconfirm','updatedOpp','enterOppName','enterContact','allProgressLostConfirm','OK','Yes','No',
    'enterNoteType','addedNote','viewonlyMode','attachmentUploaded','enterProdType','enterProdName',
    'enterProdRatePlan','enterProdQuantity','productCreated','productUpdated','enterPriority','attachDownload',
    'enterActivityType','enterStatus','activityAdded','activityUpdated','setLocationConfirm','deleteNoteConfirm',
    'oppoUpdateConfirm','noaccount','myviewloading','oppoUpdated','ocrRecog','noRatePlan','servFail',
    'noteUpdate','attachDelete','myViewList','allView','netCheck','internErr','userCountNull','tvLoad','noOpp',
    'connType','prodDeleteFailed','quoNoDelete','fiftyRecords','firstTen','oppDetNoFound','couFetchFailed','usFetch',
    'mapFetch','oppDetailFetch','noConFound','quoNoEdit','prodCrFail','proUpFail','proDataLoa','ratePlanLoad','noProds',
    'quoMobCre','alreQuoted','quoFailed','quoCreate','quoLoaded','actUpFail','actDelFailed','entFileName','attaDeleFail',
    'wiLoNoSet','bCardUp','oppUpFail','nooAccCFou','locMapUp','notCreaFail','notUpFail','notDelFail','selProType','unSupp',
    'noOppCou','deleted','note','successfully','actiPlug','oppo','pro','enterAccName','locCaptured','creSuccess','proFou',
    'teamViewList','quoteMobile','autoQuote','opTypeSave','accountTypSave','gsmConnType','gsmNumber','validQty','pdctDataLoad',
    'noAccount','pdctType','min5charctrs','actvtyDataLoad','aDateGrtrThan','attchmntDataLoad','postingProgress',
    'postOpptyOnceCompltd','optyDetailsFtchFald','noDeleteOppty','enableLocation','refreshLocationEnable','opptySavedSuccess',
    'enterCity','noMoreAssginee','otpFailed','noEventsFound','eventsFound','authFailed','Date','pleaseEnter','wrongOtpMsg',
    'personalChatInvite','noAssigneesFound','cannotRefreshOpp','noAssigneesForSearch','unsavedChangesConfirm','deleteConfirm',
    'noQuotesFound','noAccountsFound','err','Account','Status','Lead','serviceName','authorizedName','Mobile','Close',
    'notPrivilegedUser','noGSM','vm','noDesc','updateCurrPositn','setNewLoc','newMessage','youHaveA','in','evnTime','minutes',
    'to','evntDetails','desc','noPrivilegeQuote','noauthdeleteattach','noResServer','noEventData','plsTryAfter','morAttmptsLeft',
    'Interval0to60','settingsSaved','SetIntervalRequest','saveSettingConfirm','sessionExpired','loggingOut','chartdetails',
    'amount','prdctName','ratePlan','subStatus','opptyName','opportys','offlineActivitySuccess','offlineActivityEditSuccess',
    'offlineActivityDelete','offlineNoteCreateSuccess','offlineNoteEditSuccess','offlineNoteDelete','offlineOpptyUpdate',
    'featureNotAvblInOffline','noAccountExistOppty','postAllOpptys','offlineOpptysEditInOnline','reviewNewTab','otpLimitReached',
    'appUpgradeFailed','noCityData','prdctsNotFound','noChatOff','onEnableOff','otpErr','offOppsSynsSucc','cityDumpedSucc',
    'cityDumpedFailed','proDumpedSucc','proDumpedFailed','oppDumpedFailed','actiStatusDumpedSucc','actiStatusDumpedFailed',
    'noOppToDump','noViewOff','oppDumpedSucc','updatingOpp','outOf','inbCantEmpty','validInbPeriod','settSavSucess','settSaveFaild',
    'noChangesFound','refreshSuccess','refreshPlsWait','locTrackNotInOffline','photoUpload','photoRemove','lSeen','cWith',
    'editHomeScreen','photoUpdated','photoRemoved','photoRmvFailed','photoUpdateFailed','selectPhoto','photoCaptureFailed',
    'photoSelectionFailed'
];