stcApp.config(['$stateProvider',function ($stateProvider) { 
    $stateProvider.state('login',{
            url: '/login',
            templateUrl: 'app/components/login/login.template.html',
            controller: 'loginController'
        })
    }
]);