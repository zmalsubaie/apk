var settingsController = function ($scope, $rootScope, $timeout, $translate, $state, MaterialReusables, sharedDataService, pluginService, apiService, dbService) {
    $scope.settings = {
        notifytime: '',
        cityStartRow: 0,
        dumpLoader: false,
        isCity: false,
        dumpView: false,
        isStatus: false,
        isProducts: false,
        isLeadOpptys: false,
        isCustContctOpptys: false,
        isCustVisitOpptys: false,
        isProposalOpptys: false,
        isWinOpptys: false,
        isLostOpptys: false,
        preferenceView: false,
        preferenceParams: [],
        inboxPeriod: '',
        prevPeriod: '',
        savedParams: '',
        isManager: false
    };
    var savedNotificationTime = 0;
    //check agent/manager logged in
    $scope.settings.isManager = sharedDataService.getIsManager();
    //fetch saved inbox period, if not set as 3 days
    if (angular.isDefined(sharedDataService.getInboxPeriod())) {
        $scope.settings.inboxPeriod = parseInt(sharedDataService.getInboxPeriod(), 10);
        $scope.settings.prevPeriod = parseInt(sharedDataService.getInboxPeriod(), 10);
    } else {
        $scope.settings.inboxPeriod = 3;// by default 3 days
    }
    //fetch notification time from internal DB - success
    var gotNotificationTime = function (data) {
        $timeout(function () {
            savedNotificationTime = data[0].notificationTime;
            $scope.settings.notifytime = savedNotificationTime;
        });
    };
    //fetch notification time from internal DB
    if (device.platform === 'windows') {
        $scope.settings.notifytime = parseInt(localStorage.getItem('stc_notificationTime'), 10);
    } else {
        dbService.getDetailsFromTable('stc_notificationTime', [], angular.noop, gotNotificationTime);
    }
    //notification time input validation on type
    $scope.minutesChange = function () {
        if ($scope.settings.notifytime !== null) {
            if ($scope.settings.notifytime.toString().length > 2) {
                MaterialReusables.showToast('' + pluginService.getTranslations().Interval0to60, 'warning');
                $scope.settings.notifytime = savedNotificationTime;
            } else {
                if (parseInt($scope.settings.notifytime, 10) > 60) {
                    $scope.settings.notifytime = savedNotificationTime;
                    MaterialReusables.showToast('' + pluginService.getTranslations().Interval0to60, 'warning');
                }
            }
        }
    };
    //validate input values in settings
    var validateSettings = function () {
        var valid = true;
        if ($scope.settings.notifytime === null) {
            valid = false;
            MaterialReusables.showToast('' + pluginService.getTranslations().SetIntervalRequest, 'warning');
        } else if ($scope.settings.inboxPeriod === '' || $scope.settings.inboxPeriod === null) {
            valid = false;
            MaterialReusables.showToast('' + pluginService.getTranslations().inbCantEmpty, 'warning');
        } else if (parseInt($scope.settings.inboxPeriod, 10) <= 0 || parseInt($scope.settings.inboxPeriod, 10) > 365) {
            valid = false;
            MaterialReusables.showToast('' + pluginService.getTranslations().validInbPeriod, 'warning');
        } else if ($scope.settings.inboxPeriod.toString().indexOf('.') != -1) {
            valid = false;
            MaterialReusables.showToast('' + pluginService.getTranslations().validInbPeriod, 'warning');
        } else {
            ////TODO
        }
        return valid;
    };
    var changeInPreferences = function () {
        var changes = false;
        if ($scope.settings.prevPeriod !== $scope.settings.inboxPeriod) {
            changes = true;
        }
        if (savedNotificationTime !== $scope.settings.notifytime) {
            changes = true;
        }
        var oldFeatures = $scope.settings.savedParams;
        var selectedFeatures = {
            'opportunities': $scope.settings.preferenceParams.opportunities ? true : false,
            'inbox': $scope.settings.preferenceParams.inbox ? true : false,
            'accounts': $scope.settings.preferenceParams.accounts ? true : false,
            'quotes': $scope.settings.preferenceParams.quotes ? true : false,
            'dashboard': $scope.settings.preferenceParams.dashboard ? true : false,
            'maps': $scope.settings.preferenceParams.maps ? true : false,
            'calendar': $scope.settings.preferenceParams.calendar ? true : false,
            'offline': $scope.settings.preferenceParams.offline ? true : false,
            'locationTracker': $scope.settings.isManager ? ($scope.settings.preferenceParams.tracker ? true : false) : false
        };
        for (var propertyName in selectedFeatures) {
            if (oldFeatures[propertyName] !== selectedFeatures[propertyName]) {
                changes = true;
                break;
            }
        }
        return changes;
    };
    //check array item exists or not
    var indexOf = function (array, item) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] === item) return i;
        }
        return -1;
    }
    //save preference - API call
    var setPreferences = function () {
        var prefArray = [];
        var sideBarFeatures = [];
        var homeFeatures = [];
        if ($scope.settings.preferenceParams.opportunities) {
            prefArray.push('Opportunities');
        }
        if ($scope.settings.preferenceParams.inbox) {
            prefArray.push('Inbox');
        }
        if ($scope.settings.preferenceParams.accounts) {
            prefArray.push('Accounts');
        }
        if ($scope.settings.preferenceParams.quotes) {
            prefArray.push('Quotes')
        }
        if ($scope.settings.preferenceParams.dashboard) {
            prefArray.push('Dashboard');
        }
        if ($scope.settings.preferenceParams.maps) {
            prefArray.push('Map');
        }
        if ($scope.settings.preferenceParams.calendar) {
            prefArray.push('Calendar');
        }
        if ($scope.settings.preferenceParams.offline) {
            prefArray.push('Offline');
        }
        if ($scope.settings.preferenceParams.tracker) {
            prefArray.push('Location Tracker');
        }
        angular.forEach(mainModuleProps_sidemenu, function (value, key) {
            if (indexOf(prefArray, value.name) !== -1) {
                sideBarFeatures.push(value);
            }
            if (value.name === 'Settings' || value.name === 'Home') { //add Home & settings for every preference configurations
                sideBarFeatures.push(value);
            }
        });
        angular.forEach(mainModuleProps_dashboard, function (value, key) {
            if (indexOf(prefArray, value.name) !== -1) {
                homeFeatures.push(value);
            }
        });
        $rootScope.sideBarFeatures = sideBarFeatures;
        sharedDataService.saveHomePreferenceArray(homeFeatures);
        var params = {
            'opportunities': $scope.settings.preferenceParams.opportunities ? true : false,
            'inbox': $scope.settings.preferenceParams.inbox ? true : false,
            'accounts': $scope.settings.preferenceParams.accounts ? true : false,
            'quotes': $scope.settings.preferenceParams.quotes ? true : false,
            'dashboard': $scope.settings.preferenceParams.dashboard ? true : false,
            'maps': $scope.settings.preferenceParams.maps ? true : false,
            'calendar': $scope.settings.preferenceParams.calendar ? true : false,
            'offline': $scope.settings.preferenceParams.offline ? true : false,
            'locationTracker': $scope.settings.preferenceParams.tracker ? true : false
        };
        $scope.settings.savedParams = angular.copy(params);
    };
    var savePreferences = function () {
        var userPrefPayLoad = {
            "opportunities": $scope.settings.preferenceParams.opportunities ? 'Y' : 'N',
            "accounts": $scope.settings.preferenceParams.accounts ? 'Y' : 'N',
            "quotes": $scope.settings.preferenceParams.quotes ? 'Y' : 'N',
            "dashboard": $scope.settings.preferenceParams.dashboard ? 'Y' : 'N',
            "maps": $scope.settings.preferenceParams.maps ? 'Y' : 'N',
            "calendar": $scope.settings.preferenceParams.calendar ? 'Y' : 'N',
            "offlineMode": $scope.settings.preferenceParams.offline ? 'Y' : 'N',
            "inbox": $scope.settings.preferenceParams.inbox ? 'Y' : 'N',
            "notificationTime": $scope.settings.notifytime ? $scope.settings.notifytime : '10',
            'locationTracker': $scope.settings.preferenceParams.tracker ? 'Y' : 'N',
            'userId': sharedDataService.getUserDetails().loginName,
            'deviceType': "MOB",
            'inboxDays': $scope.settings.inboxPeriod ? $scope.settings.inboxPeriod : 3
        };
        var userPrefSuccess = function (data) {
            $scope.settings.dumpLoader = false;
            if (data.data.returnCode === "0") {
                setPreferences();
                $scope.settings.prevPeriod = $scope.settings.inboxPeriod;
                MaterialReusables.showToast('' + pluginService.getTranslations().settSavSucess, 'success');
                sharedDataService.saveInboxPeriod($scope.settings.inboxPeriod);
            }
        };
        var userPrefError = function (data) {
            $scope.settings.inboxPeriod = $scope.settings.prevPeriod;
            $scope.settings.dumpLoader = false;
            MaterialReusables.showToast('' + pluginService.getTranslations().settSaveFaild, 'error');
        };
        $scope.settings.dumpLoader = true;
        apiService.fetchDataFromApi('opp.saveUserPreferences', userPrefPayLoad, userPrefSuccess, userPrefError);
    };
    //save settings API call
    $scope.$on('save', function (event, args) {
        if (validateSettings() && changeInPreferences()) {
            var settingConfirmCallBack = function (confirm) {
                if (confirm) {
                    savePreferences();
                    if (savedNotificationTime !== $scope.settings.notifytime) {
                        if (device.platform === 'windows') {
                            localStorage.removeItem('stc_notificationTime');
                            localStorage.removeItem('stc_notificationStatus');
                            localStorage.setItem('stc_notificationTime', $scope.settings.notifytime);
                            localStorage.setItem('stc_notificationStatus', true);
                        } else {
                            dbService.deleteFromTable('stc_notificationTime', [], angular.noop, angular.noop);
                            dbService.insertIntoDBTable('stc_notificationTime', [parseInt($scope.settings.notifytime, 10), true], angular.noop, angular.noop);
                        }
                        if ($scope.settings.prevPeriod === $scope.settings.inboxPeriod) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().settingsSaved, 'success');
                        }
                        savedNotificationTime = $scope.settings.notifytime;
                    }
                } else {
                    $scope.settings.notifytime = savedNotificationTime;
                }
            };
            MaterialReusables.showConfirmDialog('', '' + pluginService.getTranslations().saveSettingConfirm, settingConfirmCallBack);
        } else {
            MaterialReusables.showToast(''+ pluginService.getTranslations().noChangesFound, 'warning');
        }
    });
    //city list API call
    var cityListFetch = function () {
        if (sharedDataService.getLanguage() === 'en_US') {
            $scope.langtype = 'ENU';
        } else {
            $scope.langtype = 'ARA';
        }
        var newQuery = '';
        if ($scope.settings.cityStartRow === 0) {
            newQuery = true;
        } else {
            newQuery = false;
        }
        var searchCityPayLoad = {
            "busObjCacheSize": "",
            "newQuery": newQuery,
            "outputIntObjectName": "CRMDesktopListOfValuesIO",
            "pageSize": "100",
            "searchSpec": "[List Of Values.Type]='ACCOUNT_CITY' AND [List Of Values.Language]='" + $scope.langtype + "' AND [List Of Values.Active]='Y'",
            "sortSpec": "",
            "startRowNum": $scope.settings.cityStartRow,
            "viewMode": ""
        };
        var searchCitySuccess = function (data) {
            if (data.data.listOfCRMDesktopListOfValuesIO !== null) {
                angular.forEach(data.data.listOfCRMDesktopListOfValuesIO.listOfValues, function (val, key) {
                    dbService.insertIntoDBTable('stc_cities', [JSON.stringify(data.data.listOfCRMDesktopListOfValuesIO.listOfValues[key])], angular.noop, angular.noop);
                });
                MaterialReusables.showToast('' + pluginService.getTranslations().cityDumpedSucc, 'success');
                $scope.settings.dumpLoader = false;
            }
        };
        var searchCityerror = function (data) {
            $scope.settings.dumpLoader = false;
            $scope.settings.isCity = false;
            MaterialReusables.showToast('' + pluginService.getTranslations().cityDumpedFailed, 'error');
        };
        $scope.settings.dumpLoader = true;
        apiService.fetchDataFromApi('opp.searchcity', searchCityPayLoad, searchCitySuccess, searchCityerror);
    };
    //city data dump invoke API call method
    $scope.cityDump = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
            $scope.settings.isCity = !$scope.settings.isCity;
        } else {
            if ($scope.settings.isCity) {
                $scope.settings.cityStartRow = 0;
                cityListFetch();
            } else {
                dbService.deleteFromTable('stc_cities', [], angular.noop, angular.noop);
            }
        }
    };
    //activity dump API call
    $scope.activtyStatusDump = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
            $scope.settings.isStatus = !$scope.settings.isStatus;
        } else {
            if ($scope.settings.isStatus) {
                var activityStatusSuccess = function (data) {
                    if (data.data.listOfCRMDesktopListOfValuesIO != null) {
                        angular.forEach(data.data.listOfCRMDesktopListOfValuesIO.listOfValues, function (val, key) {
                            dbService.insertIntoDBTable('stc_activityStatus', [JSON.stringify({ "index": key, "name": val.name, "value": val.value })], angular.noop, angular.noop);
                        });
                        MaterialReusables.showToast('' + pluginService.getTranslations().actiStatusDumpedSucc, 'success');
                    }
                    $scope.settings.dumpLoader = false;
                };
                var activityStatusError = function (data) {
                    $scope.settings.isStatus = false;
                    $scope.settings.dumpLoader = false;
                    MaterialReusables.showToast('' + pluginService.getTranslations().actiStatusDumpedFailed, 'error');
                };
                var langtype = '';
                if (sharedDataService.getLanguage() === 'en_US') {
                    langtype = 'ENU';
                } else {
                    langtype = 'ARA';
                }
                var activityStatusPayLoad = {
                    "busObjCacheSize": "",
                    "newQuery": "",
                    "outputIntObjectName": "CRMDesktopListOfValuesIO",
                    "pageSize": "100",
                    "searchSpec": "[List Of Values.Type]='EVENT_STATUS' AND [List Of Values.Language]='" + langtype + "' AND [List Of Values.Active]='Y' AND [List Of Values.Name] NOT LIKE 'Signed*'",
                    "sortSpec": "",
                    "startRowNum": "0",
                    "viewMode": ""
                }
                $scope.settings.dumpLoader = true;
                apiService.fetchDataFromApi('opp.searchcity', activityStatusPayLoad, activityStatusSuccess, activityStatusError);
            } else {
                sharedDataService.saveActivityStatusList([]);
                dbService.deleteFromTable('stc_activityStatus', [], angular.noop, angular.noop);
            }
        }
    };
    //products dump API call
    $scope.productsDump = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
            $scope.settings.isProducts = !$scope.settings.isProducts;
        } else {
            if ($scope.settings.isProducts) {
                var oppProductsPayload = {
                    "searchSpec": "[Internal Product.Name] ='Atheer Plus' OR [Internal Product.Name] ='DIA' OR [Internal Product.Name] ='DIA Light' OR [Internal Product.Name] ='DID-DOD' OR [Internal Product.Name] ='Data SIM Card Corporate' OR [Internal Product.Name] ='IP' OR [Internal Product.Name] ='Jood A3aml' OR [Internal Product.Name] ='Mobile-Business-Flex' OR [Internal Product.Name] ='Mobile-Postpaid-Business' OR [Internal Product.Name] ='EBU Lead Supplementary' OR [Internal Product.Name] ='PoS Over GPRS' OR [Internal Product.Name] ='Universal access'",
                    "queryByUserKey": "",
                    "messageId": "",
                    "busObjCacheSize": "",
                    "outputIntObjectName": "STC LMS Mobile App Product Search",
                    "primaryRowId": "",
                    "siebelMessage": ""
                };
                var oppProductsSuccess = function (data) {
                    if (data.data.ListOfStcLmsMobileAppProductSearch != null) {
                        angular.forEach(data.data.ListOfStcLmsMobileAppProductSearch.InternalProduct, function (value, key) {
                            dbService.insertIntoDBTable('stc_products', [value.id, JSON.stringify(value)], angular.noop, angular.noop);
                        });
                        MaterialReusables.showToast('' + pluginService.getTranslations().proDumpedSucc, 'success');
                    }
                    $scope.settings.dumpLoader = false;
                };
                var oppProductsError = function (data) {
                    $scope.settings.isProducts = false;
                    $scope.settings.dumpLoader = false;
                    MaterialReusables.showToast('' + pluginService.getTranslations().proDumpedFailed, 'error');
                };
                $scope.settings.dumpLoader = true;
                apiService.fetchDataFromApi('opp.productWithRateplans', oppProductsPayload, oppProductsSuccess, oppProductsError);
            } else {
                dbService.deleteFromTable('stc_products', [], angular.noop, angular.noop);
            }
        }
    };
    //check opptys exists in DB
    var checkOpptyExistsInDB = function (rowId, status) {
        var opptyExists = false;
        var opptyCountFetchSuccess = function (count) {
            if (count !== '' || count !== 0) {
                if (parseInt(count[0].RECORDS, 10) > 0) {
                    opptyExists = true;
                }
            } else {
                opptyExists = false;
            }
        };
        var opptyCountFetchError = function (count) {
            opptyExists = false;
        };
        var fetchParam = {
            status: status,
            pushType: 'online',
            optyID: rowId
        };
        var sqlParameters = Util.convertObjectToSQL(fetchParam);
        dbService.countFromTable('stc_opptys', [], opptyCountFetchError, opptyCountFetchSuccess);
    };
    //reset dump toggles
    var resetToggle = function (status) {
        switch (status) {
            case 'Lead': $scope.settings.isLeadOpptys = !$scope.settings.isLeadOpptys; break;
            case 'Cust:Contact': $scope.settings.isCustContctOpptys = !$scope.settings.isCustContctOpptys; break;
            case 'Cust:Visits': $scope.settings.isCustVisitOpptys = !$scope.settings.isCustVisitOpptys; break;
            case 'Proposal': $scope.settings.isProposalOpptys = !$scope.settings.isProposalOpptys; break;
            case 'Win': $scope.settings.isWinOpptys = !$scope.settings.isWinOpptys; break;
            case 'Lost': $scope.settings.isLostOpptys = !$scope.settings.isLostOpptys; break;
            default:////TODO
        }
    };
    //get arabic values for status values
    var getArabic = function (status) {
        var currentValue = '';
        switch (status) {
            case 'Lead': currentValue = "فرصة"; break;
            case 'Cust:Contact': currentValue = "الإتصال بالعميل"; break;
            case 'Cust:Visits': currentValue = "زيارات العميل"; break;
            case 'Proposal': currentValue = "اقتراح"; break;
            case 'Win': currentValue = "الربح"; break;
            case 'Lost': currentValue = "فقدان"; break;
            default:////TODO
        }
        return currentValue;
    };
    //dump opptys API call
    var fetchOpportunities = function (oppListPayload, table, status) {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
        } else {
            var oppListSuccess = function (opp) {
                if (opp.data.listOfStcOpportunity.opportunity !== null) {
                    var defaultPayloadToPush = {
                        Activity: { create: [], edit: [], delete: [] },
                        Notes: { create: [], edit: [], delete: [] },
                        Products: { create: [], edit: [], delete: [] },
                        Details: { create: [], update: [] }
                    };
                    angular.forEach(opp.data.listOfStcOpportunity.opportunity, function (value, key) {
                        if (!checkOpptyExistsInDB(value.rowId, status)) {
                            dbService.insertIntoDBTable(table, [value.rowId, JSON.stringify(value), JSON.stringify(defaultPayloadToPush), status, 'offline', 'false', ''], angular.noop, angular.noop);
                        }
                    });
                }
                if (angular.isDefined(opp.data.listOfStcOpportunity.opportunity)) {
                    if (sharedDataService.getLanguage() === 'ar_AR') {
                        currentStatus = getArabic(status);
                    } else {
                        currentStatus = status;
                    }
                    MaterialReusables.showToast(currentStatus + ' ' + pluginService.getTranslations().oppDumpedSucc, 'success');
                } else {
                    MaterialReusables.showToast(currentStatus + ' ' + pluginService.getTranslations().noOppToDump, 'warning');
                    resetToggle(status);
                }
                $scope.settings.dumpLoader = false;
            };
            var oppListError = function () {
                $scope.settings.dumpLoader = false;
                MaterialReusables.showToast('' + pluginService.getTranslations().oppDumpedFailed, 'error');
            };
            $scope.settings.dumpLoader = true;
            apiService.fetchDataFromApi('opp.list', oppListPayload, oppListSuccess, oppListError);
        }
    };
    //fetch lead opptys API call/ delete dumped data
    $scope.leadOpptyDump = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
            $scope.settings.isLeadOpptys = !$scope.settings.isLeadOpptys;
        } else {
            if ($scope.settings.isLeadOpptys) {
                var oppListPayload = Util.oppPayload(30, "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "') AND ([Opportunity.STC Sub Status SME] = 'Lead'))", "Created (DESCENDING)", 0);
                fetchOpportunities(oppListPayload, 'stc_opptys', 'Lead');
            } else {
                var deleteParam = {
                    status: 'Lead',
                    pushType: 'offline'
                };
                var sqlParameters = Util.convertObjectToSQL(deleteParam);
                dbService.deleteFromTable('stc_opptys', sqlParameters, angular.noop, angular.noop);
            }
        }
    };
    //fetch customer contact opptys API call/ delete dumped data
    $scope.custContactDump = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
            $scope.settings.isCustContctOpptys = !$scope.settings.isCustContctOpptys;
        } else {
            if ($scope.settings.isCustContctOpptys) {
                var oppListPayload = Util.oppPayload(30, "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId +
                    "') AND ([Opportunity.Sales Stage] LIKE '*Contact customer*'))", "Created (DESCENDING)", 0);
                fetchOpportunities(oppListPayload, 'stc_opptys', 'Cust:Contact');
            } else {
                var deleteParam = {
                    status: 'Cust:Contact',
                    pushType: 'offline'
                };
                var sqlParameters = Util.convertObjectToSQL(deleteParam);
                dbService.deleteFromTable('stc_opptys', sqlParameters, angular.noop, angular.noop);
            }
        }
    };
    //fetch customer visit opptys API call/ delete dumped data
    $scope.custVisitDump = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
            $scope.settings.isCustVisitOpptys = !$scope.settings.isCustVisitOpptys;
        } else {
            if ($scope.settings.isCustVisitOpptys) {
                var oppListPayload = Util.oppPayload(30, "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId +
                    "') AND ([Opportunity.Sales Stage] LIKE '*Customer Visit*'))", "Created (DESCENDING)", 0);
                fetchOpportunities(oppListPayload, 'stc_opptys', 'Cust:Visits');
            } else {
                var deleteParam = {
                    status: 'Cust:Visits',
                    pushType: 'offline'
                };
                var sqlParameters = Util.convertObjectToSQL(deleteParam);
                dbService.deleteFromTable('stc_opptys', sqlParameters, angular.noop, angular.noop);
            }
        }
    };
    //fetch proposal opptys API call/ delete dumped data
    $scope.proposalDump = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
            $scope.settings.isProposalOpptys = !$scope.settings.isProposalOpptys;
        } else {
            if ($scope.settings.isProposalOpptys) {
                var oppListPayload = Util.oppPayload(30, "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId +
                    "') AND ([Opportunity.Sales Stage] LIKE '*Proposal*'))", "Created (DESCENDING)", 0);
                fetchOpportunities(oppListPayload, 'stc_opptys', 'Proposal');
            } else {
                var deleteParam = {
                    status: 'Proposal',
                    pushType: 'offline'
                };
                var sqlParameters = Util.convertObjectToSQL(deleteParam);
                dbService.deleteFromTable('stc_opptys', sqlParameters, angular.noop, angular.noop);
            }
        }
    };
    //fetch win opptys API call/ delete dumped data
    $scope.winDump = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
            $scope.settings.isWinOpptys = !$scope.settings.isWinOpptys;
        } else {
            if ($scope.settings.isWinOpptys) {
                var oppListPayload = Util.oppPayload(30, "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId +
                    "') AND ([Opportunity.Sales Stage] LIKE '*Win*'))", "Created (DESCENDING)", 0);
                fetchOpportunities(oppListPayload, 'stc_opptys', 'Win');
            } else {
                var deleteParam = {
                    status: 'Win',
                    pushType: 'offline'
                };
                var sqlParameters = Util.convertObjectToSQL(deleteParam);
                dbService.deleteFromTable('stc_opptys', sqlParameters, angular.noop, angular.noop);
            }
        }
    };
    //fetch lost opptys API call/ delete dumped data
    $scope.lostDump = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
            $scope.settings.isLostOpptys = !$scope.settings.isLostOpptys;
        } else {
            if ($scope.settings.isLostOpptys) {
                var oppListPayload = Util.oppPayload(30, "(([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId +
                    "') AND ([Opportunity.Sales Stage] LIKE '*Lost*'))", "Created (DESCENDING)", 0);
                fetchOpportunities(oppListPayload, 'stc_opptys', 'Lost');
            } else {
                var deleteParam = {
                    status: 'Lost',
                    pushType: 'offline'
                };
                var sqlParameters = Util.convertObjectToSQL(deleteParam);
                dbService.deleteFromTable('stc_opptys', sqlParameters, angular.noop, angular.noop);
            }
        }
    };
    //open dump data UI
    $scope.openDump = function () {
        $scope.settings.dumpView = !$scope.settings.dumpView;
    };
    //check city data exists or not
    var cityData = function () {
        var offlineCitiesSuccess = function (cities) {
            if (angular.isDefined(cities) && cities.length > 0) {
                $scope.settings.isCity = true;
            }
        };
        var offlineCitiesFetchError = function (cities) {
            $scope.settings.isCity = false;
        };
        dbService.getDetailsFromTable('stc_cities', [], offlineCitiesFetchError, offlineCitiesSuccess);
    };
    //check activity data exists or not
    var activityData = function () {
        var offlineActivitiesSuccess = function (activityStatusValues) {
            if (angular.isDefined(activityStatusValues) && activityStatusValues.length > 0) {
                $scope.settings.isStatus = true;
            }
        };
        var offlineActivitiesFetchError = function (activityStatusValues) {
            $scope.settings.isStatus = false;
        };
        dbService.getDetailsFromTable('stc_activityStatus', [], offlineActivitiesFetchError, offlineActivitiesSuccess);
    };
    //check product data exists or not
    var productData = function () {
        var offlineProductsSuccess = function (products) {
            if (angular.isDefined(products) && products.length > 0) {
                $scope.settings.isProducts = true;
            }
        };
        var offlineProductsFetchError = function (activityStatusValues) {
            $scope.settings.isProducts = false;
        };
        dbService.getDetailsFromTable('stc_products', [], offlineProductsFetchError, offlineProductsSuccess);
    };
    //check optys of various status exists or not
    var opptyData = function (statusValue) {
        var offlineOpptysSuccess = function (opptys) {
            if (angular.isDefined(opptys) && opptys.length > 0) {
                if (statusValue === 'Lead') {
                    $scope.settings.isLeadOpptys = true;
                } else if (statusValue === 'Cust:Contact') {
                    $scope.settings.isCustContctOpptys = true;
                } else if (statusValue === 'Cust:Visits') {
                    $scope.settings.isCustVisitOpptys = true;
                } else if (statusValue === 'Proposal') {
                    $scope.settings.isProposalOpptys = true;
                } else if (statusValue === 'Win') {
                    $scope.settings.isWinOpptys = true;
                } else if (statusValue === 'Lost') {
                    $scope.settings.isLostOpptys = true;
                } else {
                    ////TODO
                }
            }
        };
        var offlineOpptysFetchError = function (opptys) {
            if (angular.isDefined(opptys) && opptys.length > 0) {
                if (statusValue === 'Lead') {
                    $scope.settings.isLeadOpptys = false;
                } else if (statusValue === 'Cust:Contact') {
                    $scope.settings.isCustContctOpptys = false;
                } else if (statusValue === 'Cust:Visits') {
                    $scope.settings.isCustVisitOpptys = false;
                } else if (statusValue === 'Proposal') {
                    $scope.settings.isProposalOpptys = false;
                } else if (statusValue === 'Win') {
                    $scope.settings.isWinOpptys = false;
                } else if (statusValue === 'Lost') {
                    $scope.settings.isLostOpptys = false;
                } else {
                    ////TODO
                }
            }
        };
        var fetchParam = {
            status: statusValue,
            pushType: 'offline'
        };
        var sqlParameters = Util.convertObjectToSQL(fetchParam);
        dbService.getDetailsFromTable('stc_opptys', sqlParameters, offlineOpptysFetchError, offlineOpptysSuccess);
    };
    //invoke various dump data check methods
    var dumpExists = function () {
        cityData();
        activityData();
        productData();
        opptyData('Lead');
        opptyData('Cust:Contact');
        opptyData('Cust:Visits');
        opptyData('Proposal');
        opptyData('Win');
        opptyData('Lost');
    };
    dumpExists();
    //open preference UI
    $scope.openPreferences = function () {
        $scope.settings.preferenceView = !$scope.settings.preferenceView;
    };
    //select/deselect all items in preference UI
    $scope.selectAllPref = function () {
        if ($scope.settings.preferenceParams.selectAll) {
            $scope.settings.preferenceParams.opportunities = true;
            $scope.settings.preferenceParams.inbox = true;
            $scope.settings.preferenceParams.accounts = true;
            $scope.settings.preferenceParams.quotes = true;
            $scope.settings.preferenceParams.dashboard = true;
            $scope.settings.preferenceParams.maps = true;
            $scope.settings.preferenceParams.calendar = true;
            $scope.settings.preferenceParams.offline = true;
            if ($scope.settings.isManager) {
                $scope.settings.preferenceParams.tracker = true;
            }
        } else {
            $scope.settings.preferenceParams.opportunities = false;
            $scope.settings.preferenceParams.inbox = false;
            $scope.settings.preferenceParams.accounts = false;
            $scope.settings.preferenceParams.quotes = false;
            $scope.settings.preferenceParams.dashboard = false;
            $scope.settings.preferenceParams.maps = false;
            $scope.settings.preferenceParams.calendar = false;
            $scope.settings.preferenceParams.offline = false;
            if ($scope.settings.isManager) {
                $scope.settings.preferenceParams.tracker = false;
            }
        }
    };
    //check item exists in array or not
    var checkItem = function (array, item) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].name === item) return i;
        }
        return -1;
    }
    //select preference items which are already selected on load
    if (angular.isDefined(sharedDataService.getHomePreferenceArray())) {
        var savedPref = $rootScope.sideBarFeatures;
        if (checkItem(savedPref, 'Opportunities') !== -1) {
            $scope.settings.preferenceParams.opportunities = true;
        }
        if (checkItem(savedPref, 'Inbox') !== -1) {
            $scope.settings.preferenceParams.inbox = true;
        }
        if (checkItem(savedPref, 'Accounts') !== -1) {
            $scope.settings.preferenceParams.accounts = true;
        }
        if (checkItem(savedPref, 'Quotes') !== -1) {
            $scope.settings.preferenceParams.quotes = true;
        }
        if (checkItem(savedPref, 'Dashboard') !== -1) {
            $scope.settings.preferenceParams.dashboard = true;
        }
        if (checkItem(savedPref, 'Map') !== -1) {
            $scope.settings.preferenceParams.maps = true;
        }
        if (checkItem(savedPref, 'Calendar') !== -1) {
            $scope.settings.preferenceParams.calendar = true;
        }
        if (checkItem(savedPref, 'Offline') !== -1) {
            $scope.settings.preferenceParams.offline = true;
        }
        if ($scope.settings.isManager && checkItem(savedPref, 'Location Tracker') !== -1) {
            $scope.settings.preferenceParams.tracker = true;
        }
        var params = {
            'opportunities': $scope.settings.preferenceParams.opportunities ? true : false,
            'inbox': $scope.settings.preferenceParams.inbox ? true : false,
            'accounts': $scope.settings.preferenceParams.accounts ? true : false,
            'quotes': $scope.settings.preferenceParams.quotes ? true : false,
            'dashboard': $scope.settings.preferenceParams.dashboard ? true : false,
            'maps': $scope.settings.preferenceParams.maps ? true : false,
            'calendar': $scope.settings.preferenceParams.calendar ? true : false,
            'offline': $scope.settings.preferenceParams.offline ? true : false,
            'locationTracker': $scope.settings.isManager ? ($scope.settings.preferenceParams.tracker ? true : false) : false
        };
        $scope.settings.savedParams = angular.copy(params);
    } else {
        $scope.settings.preferenceParams.opportunities = true;
        $scope.settings.preferenceParams.inbox = true;
        $scope.settings.preferenceParams.accounts = false;
        $scope.settings.preferenceParams.quotes = false;
        $scope.settings.preferenceParams.dashboard = true;
        $scope.settings.preferenceParams.maps = true;
        $scope.settings.preferenceParams.calendar = true;
        $scope.settings.preferenceParams.offline = true;
        if ($scope.settings.isManager) {
            $scope.settings.preferenceParams.tracker = true;
        }
        var params = {
            'opportunities': $scope.settings.preferenceParams.opportunities ? true : false,
            'inbox': $scope.settings.preferenceParams.inbox ? true : false,
            'accounts': $scope.settings.preferenceParams.accounts ? true : false,
            'quotes': $scope.settings.preferenceParams.quotes ? true : false,
            'dashboard': $scope.settings.preferenceParams.dashboard ? true : false,
            'maps': $scope.settings.preferenceParams.maps ? true : false,
            'calendar': $scope.settings.preferenceParams.calendar ? true : false,
            'offline': $scope.settings.preferenceParams.offline ? true : false,
            'locationTracker': $scope.settings.isManager ? ($scope.settings.preferenceParams.tracker ? true : false) : false
        };
        $scope.settings.savedParams = angular.copy(params);
    }
};
settingsModule.controller('settingsController', settingsController);
settingsController.$inject = ['$scope', '$rootScope', '$timeout', '$translate', '$state', 'MaterialReusables', 'sharedDataService', 'pluginService', 'apiService', 'dbService'];