
var detailAttachments = function ($state, $translate, $timeout, $filter, sharedDataService, pluginService, apiService, MaterialReusables) {
    return {
        restrict: 'E',
        scope: {
            attachment: '=',
            updateoppFn: '&'
        },
        templateUrl: "app/components/opportunity/opportunity.detail/detail.components/Attachments/opp.detail.attachments.html",
        transclude: true,
        compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                    scope.attachments = {
                        rowId: '',
                        filename: '',
                        uploadAttachmentData: '',
                        name: '',
                        fileExtension: '',
                        showuploadAttachments: false,
                        isUploadingAttachment: false,
                        attchmentUploadBtnText: 'Upload',
                        showAttachmentLoader: false,
                        attachmentsList: [],
                        salesStage: '',
                        isImageAttachment: false,
                        attchmentComments: '',
                        enableEdit: true,
                        isNetWorkAvailable: true
                    };
                    scope.attachments.enableEdit = scope.attachment.isEditable;
                    scope.attachments.rowId = scope.attachment.rowId;
                    scope.attachments.name = scope.attachment.name;
                    scope.attachments.salesStage = scope.attachment.salesStage;
                    if (scope.attachment.allAttachments !== null) {
                        scope.attachments.attachmentsList = scope.attachment.allAttachments.OpportunityAttachment;
                    }
                    if (!sharedDataService.getNetworkState()) {
                        scope.attachments.isNetWorkAvailable = false;
                    } else {
                        scope.attachments.isNetWorkAvailable = true;
                    }
                },
                post: function postLink(scope, iElement, iAttrs, controller) {
                    //invoke resetAttachmentScope
                    scope.$on('Attachments', function (event, data) {
                        scope.resetAttachmentScope();
                    });
                    //load attachments mapped to oppty
                    scope.$on('pushAttachment', function (event, args) {
                        scope.attachments.attachmentsList = args.attachmentList;
                        scope.attachments.attachmentsList = convertDates(scope.attachments.attachmentsList);
                    });
                    //load attachments on tab load
                    if (scope.attachment.allAttachments !== null) {
                        scope.attachments.attachmentsList = convertDates(scope.attachments.attachmentsList);
                    }
                    //set UI on tab load
                    scope.resetAttachmentScope = function () {
                        $("#img").attr('src', '');
                        scope.attachments.showuploadAttachments = false;
                        scope.attachments.fileExtension = '';
                        scope.attachments.filename = '';
                        scope.attachments.attchmentUploadBtnText = 'Upload';
                        scope.attachments.isUploadingAttachment = false;
                        scope.attachments.isImageAttachment = false;
                        scope.attachments.attchmentComments = '';
                    };
                    //report update api call
                    var reportActionCall = function (rowId) {
                        var reportData = {
                            "opportunityId": rowId,
                            "action": 'UPD_ACTED_LEADS',
                            "actedLeads": '1'
                        };
                        var reportPayload = Util.reportActionPayload(reportData);
                        apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
                    };
                    //upload attachment APi call
                    scope.uploadAttachments = function () {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                        } else {
                            if (scope.attachments.filename === '') {
                                MaterialReusables.showToast('' + pluginService.getTranslations().entFileName, 'error');
                            } else {
                                var attachmentUploadPayload = {
                                    'payload': {
                                        "busObjCacheSize": "",
                                        "messageId": "", "objectLevelTransactions": "",
                                        "setMinimalReturns": "",
                                        "statusObject": "",
                                        "listOfStcOpportunityAttachment": {
                                            "Opportunity": [{
                                                "id": scope.attachments.rowId,
                                                "name": scope.attachments.name,
                                                "operation" : "skipnode",
                                                "listOfOpportunityAttachment": {
                                                    "OpportunityAttachment": [{
                                                        "operation" : "insert",
                                                        "comment": scope.attachments.attchmentComments,
                                                        "opportunitySecureFlag": "",
                                                        "opptyId2": scope.attachments.rowId,
                                                        "opptyFileExt": scope.attachments.fileExtension,
                                                        "opptyFileName": scope.attachments.filename,
                                                        "opptyName": scope.attachments.name,
                                                        "opptyFileBuffer": scope.attachments.uploadAttachmentData.replace(/\r?\n|\r/g, ""),
                                                        "stcCreatedBy": sharedDataService.getUserDetails().Id ? sharedDataService.getUserDetails().Id : "",
                                                        "id": scope.attachments.rowId
                                                    }]
                                                }
                                            }]
                                        }
                                    }
                                };
                                var attchmentUploaded = function (data) {
                                    if (data.data.errorMessage === 'Success') {
                                        var filesaved = function (inf) { };
                                        var fileNotSaved = function (err) { };
                                        pluginService.saveAttachment(scope.attachments.filename + '.' + scope.attachments.fileExtension, scope.attachments.fileExtension, scope.attachments.uploadAttachmentData, 'image/jpg', filesaved, fileNotSaved,angular.noop);
                                        scope.resetAttachmentScope();
                                        scope.attachments.attachmentsList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityAttachment.OpportunityAttachment;
                                        scope.attachments.attachmentsList = convertDates(scope.attachments.attachmentsList);
                                        MaterialReusables.showToast('' + pluginService.getTranslations().attachmentUploaded, 'success');
                                        reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                                    } else {
                                        scope.attachments.attchmentUploadBtnText = 'Upload';
                                        Util.throwError(data.data.errorMessage, MaterialReusables);
                                    }
                                    scope.attachments.isUploadingAttachment = false;
                                };
                                var attachmentFailed = function (data) {
                                    scope.attachments.attchmentUploadBtnText = 'Upload';
                                    scope.attachments.isUploadingAttachment = false;
                                    Util.throwError(data.data.Message, MaterialReusables);
                                };
                                scope.attachments.isUploadingAttachment = true;
                                scope.attachments.attchmentUploadBtnText = 'Uploading Attachment';
                                apiService.fetchDataFromApi('opp.upsertAttachment', attachmentUploadPayload, attchmentUploaded, attachmentFailed);
                            }
                        }
                    };
                    //invoke camera
                    scope.openAttachmentCam = function () {
                        var opened = function (data) {
                            $timeout(function () {
                                scope.attachments.showAttachmentLoader = false;
                                scope.attachments.showuploadAttachments = true;
                            });
                            scope.attachments.isImageAttachment = true;
                            scope.attachments.uploadAttachmentData = data;
                            $("#img").attr('src', 'data:image/jpeg;base64,' + data);
                            scope.attachments.fileExtension = 'jpeg';
                        };
                        var failed = function (msg) {
                            $timeout(function () {
                                scope.attachments.showAttachmentLoader = false;
                                scope.attachments.showuploadAttachments = false;
                            });
                        };
                        scope.attachments.showuploadAttachments = true;
                        scope.attachments.showAttachmentLoader = true;
                        pluginService.openCamera(opened, failed, false,'default');
                    };
                    // hide bottom sheet of attachment details
                    scope.closeAttachmentOptions = function () {
                        MaterialReusables.hideBottomSheetOptions();
                    };
                    //get base64 of attachment
                    scope.getDeviceAttachmentData = function (base64Data, fileName, message) {
                        if (base64Data.length) {
                            scope.attachments.uploadAttachmentData = base64Data.substr(base64Data.indexOf(",") + 1);
                            scope.attachments.fileExtension = fileName.split('.').pop().trim();
                            scope.attachments.fileExtension = scope.attachments.fileExtension.toLowerCase();
                            if (scope.attachments.fileExtension === 'png' || scope.attachments.fileExtension === 'jpg' ||
                                scope.attachments.fileExtension === 'jpeg') {
                                scope.attachments.isImageAttachment = true;
                                $("#img").attr('src', base64Data);
                            } else {
                                scope.attachments.isImageAttachment = false;
                            }
                            $timeout(function () {
                                scope.attachments.showAttachmentLoader = false;
                            });
                        } else {
                            if (message !== '') {
                                MaterialReusables.showToast(message, 'error');
                            }
                            $timeout(function () {
                                scope.attachments.showAttachmentLoader = false;
                                scope.attachments.showuploadAttachments = false;
                            });
                        }
                    };
                    //invoke device features for attachemnt view functionalities
                    scope.getDeviceAttachments = function () {
                        MaterialReusables.showAttachmentOptions(scope);
                    };
                    //invoke attachment gallery
                    scope.openAttachmentGallery = function () {
                        var attchmentSelected = function (attchmentbase64, filename, extension) {
                            if (attchmentbase64.length) {
                                scope.attachments.uploadAttachmentData = attchmentbase64.substr(attchmentbase64.indexOf(",") + 1);
                                scope.attachments.isImageAttachment = true;
                                $("#img").attr('src', attchmentbase64);
                                scope.attachments.fileExtension = extension.toLowerCase();
                                $timeout(function () {
                                    scope.attachments.showAttachmentLoader = false;
                                });
                            } else {
                                $timeout(function () {
                                    scope.attachments.showAttachmentLoader = false;
                                    scope.attachments.showuploadAttachments = false;
                                });
                            }
                        };
                        var galleryNotOpened = function (error) { scope.attachments.showAttachmentLoader = false; };
                        scope.attachments.showuploadAttachments = true;
                        scope.attachments.showAttachmentLoader = true;
                        pluginService.getImagesFromGallery(attchmentSelected, galleryNotOpened);
                    };
                    //check and invoke attachment view
                    scope.viewAttachment = function (filename, ext) {
                        var cannotView = function (filePath, error) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().unSupp + filePath, 'warning');
                        };
                        pluginService.viewAttachment(filename, ext.toLowerCase(), angular.noop, cannotView);
                    };
                    //close attachment UI - reset to basic tile view
                    scope.closeAttachmentScreen = function () {
                        scope.resetAttachmentScope();
                    };
                    //download attachment API call
                    scope.saveAttachment = function (index, attchId, filename, ext, fileExt) {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                        } else {
                            var saved = function (info) { };
                            var saveFailed = function (error) { };
                            var attachmentFetchPayload = {
                                'payload': {
                                    "busObjCacheSize": "",
                                    "messageId": "",
                                    "outputIntObjectName": "STC Opportunity Attachment",
                                    "primaryRowId": "",
                                    "queryByUserKey": "",
                                    "searchSpec": "[Opportunity.Id] = '" + scope.attachments.rowId + "' and [Opportunity Attachment.Id] = '" + attchId + "'",
                                    "siebelMessage": "",
                                    "returnAttachment": "true"
                                }
                            };
                            var downloaded = function (downloadData) {
                                scope.attachments.attachmentsList[index].showAttachmentDownload = false;
                                MaterialReusables.showToast('' + pluginService.getTranslations().attachDownload, 'success');
                                pluginService.saveAttachment(filename + '.' + ext, ext, downloadData.data.listOfStcOpportunity.opportunity[0].listOfOpportunityAttachment.opportunityAttachment[0].opptyFileBuffer, 'image/jpg', saved, saveFailed,angular.noop);
                                MaterialReusables.showToast('' + pluginService.getTranslations().attachDownload, 'success');
                            };
                            var downloadFailed = function (data) {
                                scope.attachments.attachmentsList[index].showAttachmentDownload = false;
                                Util.throwError(data.data.Message, MaterialReusables);
                            };
                            if (attchId === 'view') {
                                MaterialReusables.showToast('' + pluginService.getTranslations().viewonlyMode, 'success');
                            } else {
                                scope.attachments.attachmentsList[index].showAttachmentDownload = true;
                                apiService.fetchDataFromApi('opp.getAttachment', attachmentFetchPayload, downloaded, downloadFailed);
                            }
                        }
                    };
                    //delete attachemnt API call
                    scope.doDeleteAtachment = function (ev, attachmentId) {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                        } else {
                            if (sharedDataService.getUserAttachDeleteAllowed()) {
                                var oppAttachmentDeleteSuccess = function (data) {
                                    scope.attachments.attachmentsList[ev].showAttachmentDownload = false;
                                    if (data.data.errorMessage === 'Success') {
                                        if (data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityAttachment !== null) {
                                            scope.attachments.attachmentsList = data.data.listOfStcOpportunity.opportunity[0].ListOfOpportunityAttachment.OpportunityAttachment;
                                        } else {
                                            scope.attachments.attachmentsList = [];
                                        }
                                        MaterialReusables.showToast('' + pluginService.getTranslations().attachDelete, 'success');
                                        reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                                        // $timeout(function () {
                                        //     scope.attachments.attachmentsList.splice(ev, 1);
                                        // });
                                    } else {
                                        Util.throwError(data.data.errorMessage, MaterialReusables);
                                        //MaterialReusables.showToast('' + pluginService.getTranslations().attaDeleFail, 'error');
                                    }
                                };
                                var oppAttachmentDeleteError = function (data) {
                                    scope.attachments.attachmentsList[ev].showAttachmentDownload = false;
                                    Util.throwError(data.data.Message, MaterialReusables);
                                };
                                var result = function (confirm) {
                                    if (confirm) {
                                        scope.attachments.attachmentsList[ev].showAttachmentDownload = true;
                                        // var attachmentDeletePayLoad = {
                                        //     'rowId': noteId
                                        // };
                                        var headerData = {
                                            "operation": "skipnode",
                                            "rowId": scope.attachments.rowId
                                        };
                                        var attachmentData = {
                                            "id": attachmentId,
                                            "operation": "delete"
                                        };
                                        var attachmentDeletePayLoad = Util.leadProcessPayload(headerData, '', '', '', attachmentData, '', '', '');
                                        apiService.fetchDataFromApi('opp.leadProcess', attachmentDeletePayLoad, oppAttachmentDeleteSuccess, oppAttachmentDeleteError);
                                    }
                                };
                                MaterialReusables.showConfirmDialog(ev, '' + pluginService.getTranslations().attchmentDelete_confirm, result);
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().noauthdeleteattach, 'error');
                            }
                        }
                    };
                    //convert dates
                    function convertDates(attchmentsList) {
                        var attchments = [];
                        angular.forEach(attchmentsList, function (value, key) {
                            if (value.opptyFileDate !== null) {
                                value.opptyFileDate = moment(value.opptyFileDate).format("DD/MM/YYYY HH:mm:ss");
                            }
                            attchments.push(value);
                        });
                        return attchments;
                    };
                    //name additional validation
                    scope.nameValidate = function (newValue) {
                        scope.attachments.filename = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                    //comments additional validation
                    scope.commentsValidate = function (newValue) {
                        scope.attachments.attchmentComments = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                }
            }
        }
    }
};
opportunityModule.directive('detailAttachments', detailAttachments);
detailAttachments.$inject = ['$state', '$translate', '$timeout', '$filter', 'sharedDataService', 'pluginService', 'apiService', 'MaterialReusables'];