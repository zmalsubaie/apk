trackerviewModule.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('root.trackerview', {
        url: '/trackerView',
        templateUrl: 'app/components/trackerview/trackerview.template.html',
        controller: 'trackerViewController',
        pageTitle: 'Location Tracker',
        backbutton: false,
        buttonProp:['refresh'],
        previousState:'root.home'
    })
}]);