var trackerViewController = function ($rootScope,$scope,$timeout,pluginService,apiService,MaterialReusables,sharedDataService) {
    $scope.trackerViewModel = {
       isLoading:true,
       userMessage:'',
       infoWindowContext:'',
       filters:[],
       markerDetails:[],
       filterSelectedIndex:''
    };
    var isFilterInitialised = false;
    var intializeFilterMenu = function(){
        if(!isFilterInitialised){
            isFilterInitialised = true;
            $('.slide-out-div').tabSlideOut({
                tabHandle: '.handle', 
                pathToTabImage: 'assets/img/map_icon/mapFilter.svg',
                imageHeight: '40px',
                imageWidth: '26px',  
                tabLocation: 'right',
                speed: 300,
                action: 'click',
                topPos: '200px',
                fixedPosition: false
            });
            $('.handle').on('click', function(event) {
                event.stopImmediatePropagation();
            });  
        }   
    };
    var getPartners = function(partners){
        if(sharedDataService.getAssignedList().data.record[0].stcPartnerId === null){
            intializeFilterMenu(); 
        }    
        $timeout(function(){
            $scope.trackerViewModel.filters = partners;
        });
    };
    var getInfoWindowContext = function(InfoWindowContext){
        $scope.trackerViewModel.infoWindowContext = InfoWindowContext;
    };
    var initializeTracker = function(mapAction){
        if(!sharedDataService.getNetworkState()){
            $scope.trackerViewModel.userMessage = ''+ pluginService.getTranslations().locTrackNotInOffline;
        }else{
            var partnerId = '';
            if(sharedDataService.getAssignedList().data.record[0].stcPartnerId === null){
                partnerId = 'ALL';
            }else{
                partnerId = sharedDataService.getAssignedList().data.record[0].stcPartnerId;
            }
            var fetchUserLocationPayload = {
                'partnerId':partnerId
            };
            var gotClusterLocations = function(locationDetails){
                $scope.trackerViewModel.markerDetails = locationDetails.data.fetchLatlong;
                $scope.trackerViewModel.userMessage = '';
                $scope.trackerViewModel.isLoading = false;
                if(mapAction === 'refresh'){ MaterialReusables.showToast(''+ pluginService.getTranslations().refreshSuccess,'success');}
                pluginService.initializeClusterofMarkers(locationDetails.data.fetchLatlong,mapAction,getInfoWindowContext,getPartners,MaterialReusables);
            };
            var failedGettingLocations = function(locationFetcherror){
                $scope.trackerViewModel.isLoading = false;
                MaterialReusables.showToast(''+pluginService.getTranslations().mapFetch,'error');
            };
            if(mapAction === 'create'){ $scope.trackerViewModel.isLoading = true; }
            apiService.fetchDataFromApi('location.fetch',fetchUserLocationPayload,gotClusterLocations,failedGettingLocations);
        }     
    };
    $scope.closeInMapChatWindow = function(){
        MaterialReusables.hideBottomSheetOptions();
    };
    $scope.filterMarkers = function(filter,index){
       $scope.trackerViewModel.filterSelectedIndex = index;
       $('.handle').click();
       var markersToShow = [];
       if(filter !== 'All'){
            angular.forEach($scope.trackerViewModel.markerDetails,function(val,key){
                if(val.partnerId === filter){
                    markersToShow.push(val);
                }
            });
       }else{
        markersToShow = $scope.trackerViewModel.markerDetails;
       }
       pluginService.initializeClusterofMarkers(markersToShow,'filter',getInfoWindowContext,angular.noop,MaterialReusables);
    };
    $scope.$on('refresh', function (event, args) {
        MaterialReusables.showToast(''+ pluginService.getTranslations().refreshPlsWait,'success');
        initializeTracker('refresh'); 
    });
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if($scope.trackerViewModel.infoWindowContext!==''){
            $scope.trackerViewModel.infoWindowContext.close();
        }
    });  
    initializeTracker('create');
};
trackerviewModule.controller('trackerViewController', trackerViewController);
trackerViewController.$inject = ['$rootScope','$scope','$timeout','pluginService','apiService','MaterialReusables','sharedDataService'];