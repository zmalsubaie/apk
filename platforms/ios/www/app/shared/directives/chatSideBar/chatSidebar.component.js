(function() {
  'use strict';
    var chatSideBar = function($state,$translate,$timeout,PubnubService,apiService,sharedDataService){
        return {
            restrict: 'E',
            scope: {
                getConfig : '&'
            },
            templateUrl: "app/shared/directives/chatSideBar/chatSidebar.template.html",
            transclude : true,
            compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                    scope.chatNames = [];
                    scope.startLoading = true;
                    scope.users = {name:''};
                },
                post: function postLink(scope, iElement, iAttrs, controller) {
                    scope.refreshChatUsers = function(){
                    var userList = function(users){
                        var usersList = users;
                        updateUserList(usersList);
                    };
                    PubnubService.listOnlineUsers(userList);
                    };
                    scope.gotoPrivateChat = function(toName){
                        $state.go('root.chat',{id:toName,subscribeto:''});
                    };
                    //display filtered users in online
                    var showUsers = function(list){
                        $timeout(function(){
                            scope.startLoading = false;
                            scope.chatNames = list;
                        });
                    };
                    //API call to filter online users based on partner Ids
                    var updateUserList = function(args){
                        var assignedToPayLoad = {
                            "stcDiv": "",
                            "stcPartnerId": "",
                            "rowId": sharedDataService.getUserDetails().PrimaryPositionId
                        };
                        var oppAssignedToListSuccess = function(data){
                            var query = '';
                            if (data.data.record.length !== 0) {
                                if (data.data.record[0].stcPartnerId === null) {
                                    query = "[Position.STC Div] = 'STC EBU SME'";
                                }else if(Util.checkifPresent(data.data.record[0].name, 'Account') && data.data.record[0].stcPartnerId !== null){
                                    query = " (([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] = '" + data.data.record[0].stcPartnerId + "') OR ([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] IS NULL) OR ([Position.Name] LIKE '*Account*' AND [Position.STC Partner Id] IS NOT NULL))";
                                }else{
                                    query = "(([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] = '" + data.data.record[0].stcPartnerId + "') OR ([Position.STC Div] = 'STC EBU SME' AND [Position.STC Partner Id] IS NULL))";
                                }
                            }
                            var activeUsersPayload = {
                                'payload': {
                                    "searchSpec":query,
                                    "busObjCacheSize":"",
                                    "outputIntObjectName":"STC Position - LMS Mobile App"
                                }
                            };
                            var gotActiveUsers = function(data){
                                var tempUserList = [];
                                var activePartnersList = [];
                                function indexOf(array, item) {
                                    for (var i = 0; i < array.length; i++) {
                                        if (array[i]=== item.toLowerCase() || array[i] === item.toUpperCase()) return i;
                                    }
                                    return -1;
                                }
                                angular.forEach(data.data.listOfStcPositionLmsMobileApp.position,function(val,key){
                                    activePartnersList.push(val.activeLoginName);
                                });
                                angular.forEach(args.uuids,function(userVal,userKey){
                                    if(indexOf(activePartnersList,userVal)!== -1){
                                        if(userVal!==sharedDataService.getUserDetails().loginName){
                                            tempUserList.push(userVal);
                                        }      
                                    }
                                });
                                showUsers(tempUserList);
                            };
                            var failedGettingUsers = function(data){
                                showUsers([]);
                            };
                            apiService.fetchDataFromApi('opp.activePartnerUsers', activeUsersPayload, gotActiveUsers, failedGettingUsers);
                        };
                        var oppAssignedToListError = function(data){
                            showUsers([]);
                        };
                        scope.startLoading = true;
                        if (angular.isDefined(sharedDataService.getAssignedList()) && sharedDataService.getAssignedList().length !== 0) {
                            oppAssignedToListSuccess(sharedDataService.getAssignedList());
                        } else {
                            apiService.fetchDataFromApi('opp.assignedToList', assignedToPayLoad, oppAssignedToListSuccess, oppAssignedToListError);
                        }
                        
                    };
                    scope.$on('chatUsers', function(event, args) {
                    updateUserList(args.users);
                    });
                }
            }
            }
        }
    };
    stcApp.directive('chatSideBar',chatSideBar);
    chatSideBar.$inject = ['$state','$translate','$timeout','PubnubService','apiService','sharedDataService'];
})();