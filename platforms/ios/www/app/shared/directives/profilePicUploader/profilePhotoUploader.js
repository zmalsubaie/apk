(function() {
    'use strict';
      var profilePhotoUploader = function($rootScope,$timeout,MaterialReusables,pluginService,apiService,sharedDataService){
          return {
              restrict: 'E',
              templateUrl: "app/shared/directives/profilePicUploader/profilePhotoUploadTemplate.html",
              transclude : true,
              scope:{
                photoOnSelect:'&'
             },
              compile: function compile(tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) {},
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        scope.profilePicModel = {
                          userAction:'userSelection',
                          croppieObject:'',
                          confirmedBase64Image:'',
                          target:''
                        };
                        var uploadProfileImage = function (dataURL, type) {
                            var uploadSuccess = function (data) {
                              $rootScope.profileImage = 'data:image/jpeg;base64,' + data.data.photo;
                              if (type !== 'remove') {
                                MaterialReusables.showToast(''+ pluginService.getTranslations().photoUpdated, 'success');
                              } else {
                                if (data.data.photo === null) {
                                  $rootScope.profileImage = null;
                                  MaterialReusables.showToast(''+ pluginService.getTranslations().photoRemoved, 'success');
                                } else {
                                  MaterialReusables.showToast(''+ pluginService.getTranslations().photoRmvFailed, 'error');
                                }
                              }
                            };
                            var uploadError = function () {
                              if (type !== 'remove') {
                                MaterialReusables.showToast(''+ pluginService.getTranslations().photoUpdateFailed, 'error');
                              } else {
                                MaterialReusables.showToast(''+ pluginService.getTranslations().photoRmvFailed, 'error');
                              }
                            };
                            var imagePayload = {
                                "userId": sharedDataService.getUserDetails().loginName,
                                "photo": dataURL ? dataURL : null
                            };
                            apiService.fetchDataFromApi('user.image', imagePayload, uploadSuccess, uploadError);
                        };
                        var bindCroppietoHTML = function(imagetoCrop){
                            if (angular.isDefined(imagetoCrop)) {
                                $timeout(function(){
                                    scope.profilePicModel.userAction = 'userCrop';
                                    $('.selectedProfileImage').attr('src',imagetoCrop);
                                    scope.profilePicModel.croppieObject = $('.selectedProfileImage').croppie({
                                        viewport: {width: $('.homecard-img').width(),height: $('.homecard-img').height(),type:'circle'},
                                        boundary: {width: 250,height: 250},
                                        enableZoom:true
                                    });
                                }); 
                            }else{
                                MaterialReusables.showToast(''+ pluginService.getTranslations().selectPhoto,'warning'); 
                            }
                        };
                        scope.selectEditedPicture = function(){
                            scope.profilePicModel.croppieObject.croppie('result', {type: 'canvas',size: 'original'})
                            .then(function (resp) {
                                scope.profilePicModel.userAction = 'userConfirmation';
                                scope.profilePicModel.confirmedBase64Image = resp;
                                $('.userImageConfirmation').attr('src',resp);
                             });
                        };
                        scope.setConfirmedPicture = function(){
                            scope.profilePicModel.confirmedBase64Image = scope.profilePicModel.confirmedBase64Image.replace('data:image/png;base64,','');
                            uploadProfileImage(scope.profilePicModel.confirmedBase64Image,scope.profilePicModel.target);
                            scope.photoOnSelect();
                            MaterialReusables.hideBottomSheetOptions();
                        };
                        scope.openCamera = function(){
                            var opened = function (imageData) {
                              scope.profilePicModel.target = 'camera';
                              bindCroppietoHTML('data:image/jpeg;base64,'+imageData);
                            };
                            var failed = function (data) {
                              MaterialReusables.showToast(''+ pluginService.getTranslations().photoCaptureFailed,'error');
                            };
                            pluginService.openCamera(opened, failed, false,'profile');
                        };
                        scope.openGallery = function(){
                            var photoSelected = function (imageData) {
                              scope.profilePicModel.target = 'gallery';
                              bindCroppietoHTML(imageData);
                            };
                            var galleryNotOpened = function (imageData) {
                              MaterialReusables.showToast(''+ pluginService.getTranslations().photoSelectionFailed,'error');
                            };
                            pluginService.getImagesFromGallery(photoSelected, galleryNotOpened);
                        };
                        scope.removePhoto = function(){
                            scope.profilePicModel.target = 'Remove';
                            MaterialReusables.hideBottomSheetOptions();
                            uploadProfileImage('delete','remove');
                            MaterialReusables.showToast('' + pluginService.getTranslations().photoRemove, 'success');
                        };
                    }
                }
              }
          }
      };
      stcApp.directive('profilePhotoUploader',profilePhotoUploader);
      profilePhotoUploader.$inject = ['$rootScope','$timeout','MaterialReusables','pluginService','apiService','sharedDataService'];
  })();