(function() {
    'use strict';
      var accountList = function($timeout, $translate,apiService,dbService,sharedDataService,MaterialReusables){
          return {
              restrict: 'E',
              scope:{
                 accountOnSelect:'&',
                 accountOnClose:'&' 
              },
              templateUrl: "app/shared/directives/popups/accountList/accountlist.template.html",
              transclude : true,
              compile: function compile(tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) {},
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        scope.isLoadingAccounts = false;
                        scope.accountSearchModel = '';
                        scope.accountNames = [];
                        scope.accountSearchRow = 0;
                        scope.isLastAccount = true;
                        scope.accountSearchValidate = function(accText){
                            scope.accountSearchModel = accText.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                        };
                        scope.accountSelected = function(account){
                            scope.accountOnSelect({selectedAccount:account});
                        };
                        scope.accountWindowClosed = function(){
                            scope.accountOnClose();
                        };
                        scope.getAccountNames = function(accText){
                            if ((accText.length >= 5) && angular.isDefined(accText)) {
                                scope.accountSearchModel = accText;
                                scope.isLastAccount = true;
                                scope.accountSearchRow = 0;
                                scope.accountNames = [];
                                scope.fetchAccountPart(accText,scope.accountSearchRow,'firstCall');
                            } else {
                                MaterialReusables.showToast('' + Util.pluginServiceScope.getTranslations().min5charctrs, 'error');
                            }
                        };
                        scope.fetchAccountPart = function(searchText,startRow,type){
                            var oppAccountSuccess = function (data) {
                                if (data.data !== null) {
                                    if (data.data.account !== null && data.data.account[0].name) {
                                        if (data.data.account.length < 10) {
                                            scope.isLastAccount = true;
                                        } else {
                                            scope.isLastAccount = false;
                                        }
                                        scope.accountSearchRow = parseInt(scope.accountSearchRow, 10) + 10;
                                        scope.isLoadingAccounts = false;
                                        angular.forEach(data.data.account, function (value, key) {
                                            scope.accountNames.push({ 'id': value.rowId, 'name': value.name, 'contacts': value.contacts });
                                        });
                                    } else {
                                        if (type === 'loadMore') {
                                            MaterialReusables.showToast('' + '' + Util.pluginServiceScope.getTranslations().noMoreAssginee, 'warning');
                                        } else {
                                            MaterialReusables.showToast('' + Util.pluginServiceScope.getTranslations().noaccount, 'error');
                                        }
                                        scope.isLoadingAccounts = false;
                                        scope.isLastAccount = true;
                                        //$scope.oppAddModel.account = '';
                                    }
                                } else {
                                    if (type === 'loadMore') {
                                        MaterialReusables.showToast('' + '' + Util.pluginServiceScope.getTranslations().noMoreAssginee, 'warning');
                                    } else {
                                        MaterialReusables.showToast('' + Util.pluginServiceScope.getTranslations().noaccount, 'error');
                                    }
                                    scope.isLoadingAccounts = false;
                                    scope.isLastAccount = true;
                                    //$scope.oppAddModel.account = '';
                                }
                            };
                            var oppAccountError = function (data) {
                                scope.isLastAccount = false;
                                //$scope.oppAddModel.enableAccountSearch = true;
                                Util.throwError(data.data.Message, MaterialReusables);
                            };
                            //check text Arabic/Engl
                            var arabic = /[\u0600-\u06FF]/;
                            if (arabic.test(searchText)) {
                                searchText = "*" + searchText;
                            } else {
                                searchText = searchText + "*";
                            }
                            var newQuery = '';
                            if (startRow === 0) { newQuery = true; } 
                            else { newQuery = false; }
                            var oppAccountPayload = {
                                "sortSpec": "",
                                "searchSpec": "(([Account.STC Managed]='No' AND [Account.Account Type Code]='Customer') AND ([Account.Name] LIKE '" + searchText + "' OR [Account.STC Commercial Registration Number] LIKE  '" + searchText + "' OR [Account.CSN] LIKE  '" + searchText + "' ))",
                                "startRowNum": startRow,
                                "pageSize": 10,
                                "newQuery": newQuery,
                                "viewMode": "All",
                                "busObjCacheSize": "10",
                                "outputIntObjectName": "STC Account Contact"
                            };
                            scope.isLastAccount = true;
                            apiService.fetchDataFromApi('opp.account', oppAccountPayload, oppAccountSuccess, oppAccountError);
                        };
                    }
                }
              }
          }
      };
      stcApp.directive('accountList',accountList);
      accountList.$inject = ['$timeout', '$translate','apiService','dbService','sharedDataService','MaterialReusables'];
  })();