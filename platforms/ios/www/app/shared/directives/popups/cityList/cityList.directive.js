(function() {
    'use strict';
      var cityList = function($timeout, $translate,apiService,dbService,sharedDataService,MaterialReusables){
          return {
              restrict: 'E',
              scope:{
                 cityOnSelect:'&',
                 cityOnClose:'&' 
              },
              replace: true,
              templateUrl: "app/shared/directives/popups/cityList/cityList.template.html",
              transclude : true,
              compile: function compile(tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) {},
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        scope.isCityLoading = false;
                        scope.listOfCities = [];
                        scope.citySearchModel = '';
                        var cityStartRowNo = 0;
                        var langtype = '';
                        var fetchCityListFromAPI = function(){
                            if (sharedDataService.getLanguage() === 'en_US') { langtype = 'ENU'; } 
                            else { langtype = 'ARA'; }
                            var newQuery = '';
                            if (cityStartRowNo === 0) {  newQuery = true; } 
                            else { newQuery = false; }
                            var searchCityPayLoad = {
                                'busObjCacheSize': '',
                                'newQuery': newQuery,
                                'outputIntObjectName': 'CRMDesktopListOfValuesIO',
                                'pageSize': '100',
                                'searchSpec': "[List Of Values.Type]='ACCOUNT_CITY' AND [List Of Values.Language]='" + langtype + "' AND [List Of Values.Active]='Y'",
                                'sortSpec': '',
                                'startRowNum':cityStartRowNo,
                                'viewMode': ''
                            };
                            var searchCitySuccess = function (data) {
                                if (data.data.listOfCRMDesktopListOfValuesIO !== null) {
                                    angular.forEach(data.data.listOfCRMDesktopListOfValuesIO.listOfValues, function (val, key) {
                                        scope.listOfCities.push(val);
                                    });
                                    scope.isCityLoading = false;
                                    sharedDataService.saveCityList(scope.listOfCities);
                                }
                            };
                            var searchCityerror = function (data) {
                                scope.isCityLoading = false;
                                Util.throwError(data.data.Message, MaterialReusables);
                            };
                            scope.isCityLoading = true;
                            apiService.fetchDataFromApi('opp.searchcity', searchCityPayLoad, searchCitySuccess, searchCityerror);
                        };
                        var fetchCityListFromDB = function(){
                            var tempCityList = [];
                            var offlineCityFetchSuccess = function (offlineCityData) {
                                angular.forEach(offlineCityData, function (val, key) {
                                    tempCityList.push(JSON.parse(val.cities));
                                });
                                $timeout(function () {
                                    scope.isCityLoading = false;
                                    scope.listOfCities = tempCityList;
                                });
                            };
                            var offlineCityFetchError = function (err) {
                                MaterialReusables.hideDialog();
                                scope.isCityLoading = false;
                                MaterialReusables.showToast('' + Util.pluginServiceScope.getTranslations().noCityData, 'error');
                            };
                            scope.isCityLoading = true;
                            dbService.getDetailsFromTable('stc_cities', [], offlineCityFetchError, offlineCityFetchSuccess);
                        };
                        scope.citySelect = function(city){
                            scope.cityOnSelect({selectedCity:city});
                        };
                        scope.cityClose = function(){
                            scope.cityOnClose();
                        };
                        if (!sharedDataService.getNetworkState()) {
                            fetchCityListFromDB();
                        } else {
                            scope.listOfCities = [];
                            cityStartRowNo = 0;
                            if (angular.isDefined(sharedDataService.getCityList()) && sharedDataService.getCityList().length) {
                                scope.isCityLoading = true;
                                $timeout(function () {
                                    scope.listOfCities = sharedDataService.getCityList();
                                    scope.isCityLoading = false;
                                });
                            } else {
                                fetchCityListFromAPI();
                            }
                        }
                    }
                }
              }
          }
      };
      stcApp.directive('cityList',cityList);
      cityList.$inject = ['$timeout', '$translate','apiService','dbService','sharedDataService','MaterialReusables'];
  })();