(function() {
  'use strict';
    var PubnubService = function(Pubnub,sharedDataService,$pubnubChannel,$rootScope,$state,$translate,MaterialReusables){
        var PubnubService = {};
        PubnubService.initializeChat = function() {
            Pubnub.init({
                publish_key: Util.getChatCredentials().pubKey,
                subscribe_key: Util.getChatCredentials().subKey,
                cipher_key:Util.getChatCredentials().cipherKey,
                ssl: true,
                uuid: sharedDataService.getUserDetails().loginName,
                error: function (error) {}
            });
        };
        PubnubService.startChatProcedures = function(){
            try {
                Pubnub.where_now({
                    uuid     : sharedDataService.getUserDetails().loginName,
                    callback : function(channels){
                    if(channels.channels.length){
                        PubnubService.subscibetoOwnChannels(channels); 
                    }
                    else{ PubnubService.subscribeCoreChannelChat(angular.noop); } 
                    },
                    error : function(m){}
                });
            } catch (err) {
                PubnubService.initializeChat();
                PubnubService.startChatProcedures();
            }
        };
        PubnubService.subscibetoOwnChannels = function(subscribedChannels){
            if(subscribedChannels.channels.indexOf(Util.getChatCredentials().chatChannel) === -1){
                subscribedChannels.channels.push(Util.getChatCredentials().chatChannel);
            }
            Pubnub.subscribe({
                channel : subscribedChannels.channels,
                withPresence: true,
                message : function( message, env, channel ){
                    if(message.recipient === sharedDataService.getUserDetails().loginName){
                        PubnubService.handleIncomingChatRequest(message.message,message.sender,angular.noop);
                    }
                    var popupResponse = function(response,to){
                    if(response){$state.go('root.chat',{id:to,subscribeto:''});}
                    };
                    if (typeof message.sender_uuid != 'undefined' && $state.current.name!=='root.chat'){
                        Util.pluginServiceScope.createMessageNotification(message.content,message.sender_uuid);
                        MaterialReusables.showChatMsgToast(message.sender_uuid,popupResponse);
                    }
                },
                connect : function(){},
                disconnect : function(){},
                reconnect : function(){},
                error : function(){}
            });
            //PubnubService.updateAllOnlineUsers();
        };
        PubnubService.subscribeCoreChannelChat = function(successCallback){
            Pubnub.subscribe({
                channel : Util.getChatCredentials().chatChannel,
                message : function( message, env, channel ){
                    if(message.recipient === sharedDataService.getUserDetails().loginName){
                        PubnubService.handleIncomingChatRequest(message.message,message.sender,angular.noop);
                    }
                },
                connect : function(){
                    //PubnubService.updateAllOnlineUsers();
                },
                disconnect : function(){},
                reconnect : function(){},
                error : function(){}
            });
        };
        PubnubService.subscribePrivateChannelChat = function(privateChannelName,successCallback){
            Pubnub.subscribe({
                channel : privateChannelName,
                message : function( message, env, channel ){
                    var popupResponse = function(response,to){
                    if(response){ $state.go('root.chat',{id:to,subscribeto:''}); }
                    };
                    if(privateChannelName!==channel){
                        MaterialReusables.showChatMsgToast(message.sender_uuid,popupResponse);
                    }
                    successCallback(message);
                },
                connect : function(){},
                disconnect : function(){},
                reconnect : function(){},
                error : function(){}
            });
        };
        PubnubService.unSubscribeChat = function(){
            try {
               Pubnub.unsubscribe({
                channel : Util.getChatCredentials().chatChannel
               });
            }
            catch(e){
                $state.go('login');
            };
        };
        PubnubService.listOnlineUsers = function(successCallback){
            try {
               Pubnub.here_now({
                    channel :Util.getChatCredentials().chatChannel,
                    callback :function(messages) { 
                        successCallback(messages);
                    }
                });  
            } catch (error) {}
        };
        PubnubService.publishMessage = function(messageContent,secretchannel){
            Pubnub.publish({
                channel : secretchannel,
                message :{
                    content:messageContent,
                    sender_uuid:sharedDataService.getUserDetails().loginName,
                    date: new Date()
                },
                callback : function(m){}
            });
        };
        PubnubService.getHistory = function(privatechannel,historyContent){
            Pubnub.history({
                channel : privatechannel,
                callback : function(m){
                    historyContent(m);
                },
                count : 100,
                reverse : false
            });
        };
        PubnubService.publishSecretRequest = function(toname,secretChannel,successCallBack){
            Pubnub.publish({
                channel: Util.getChatCredentials().chatChannel,
                message: {
                    recipient: toname,
                    sender: sharedDataService.getUserDetails().loginName,
                    message: secretChannel,
                    ttl: 100000
                },
                callback:function(m){
                successCallBack(m);
                }
            });
        };
        PubnubService.handleIncomingChatRequest = function(message,sender,successCallBack){
            var checked = function(whichChannel){
                if(whichChannel === ''){
                    var chatConfirmCallBack = function(confirm){
                        if(confirm){ $state.go('root.chat',{id:sender,subscribeto:message});}
                    };
                    MaterialReusables.showConfirmDialog('',sender+' '+Util.pluginServiceScope.getTranslations().personalChatInvite,chatConfirmCallBack);
                }else{ $state.go('root.chat',{id:sender,subscribeto:''}); }   
            };
            PubnubService.checkIfSubscribed(message,checked); 
        };
        PubnubService.checkIfSubscribed = function(message,successCallBack){
            var checkStatus = '';
            Pubnub.where_now({
                uuid     : sharedDataService.getUserDetails().loginName,
                callback : function(channels){
                var checkchannel = Util.isChannelSubscribed(message,channels,angular);
                if(checkchannel!==''){
                    checkStatus = checkchannel;
                    successCallBack(checkStatus);
                }else{
                    checkStatus = '';
                    successCallBack(checkStatus);
                }
                },
                error : function(m){}
            });
        };
        PubnubService.updateAllOnlineUsers = function(){
        var userListSuccess = function(users){
            $rootScope.$broadcast('chatUsers',{users:users});
        };
        PubnubService.listOnlineUsers(userListSuccess);
        };
        return PubnubService;
    };
    stcApp.service('PubnubService',PubnubService);
    PubnubService.$inject = ['Pubnub','sharedDataService','$pubnubChannel','$rootScope','$state','$translate','MaterialReusables'];
})();