chatModule.config(['$stateProvider',function ($stateProvider) { 
    $stateProvider.state('root.chat',{
            url: '/chat',
            template: '<chat-handler></chat-handler>',
            controller: '',
            pageTitle:'',
            backbutton:true,
            previousState:'root.home',
            buttonProp:['none'],
            params:{'id':'','subscribeto':''}
    })
}]);