var reviewController = function ($scope, $timeout, $translate, $state, dbService, apiService, MaterialReusables, sharedDataService, pluginService) {
    $scope.reviewModel = {
        offlineList: [],
        isLoading: true,
        isPosting: false,
        tabs: [],
        selectedIndex: '',
        updateOppList: { oppData: [], payload: [], message: [] },
        tab: '',
        bulkPostSelected: false,
        bulkPostRecords: [],
        updateOppsMessage: '',
        syncCount: { new: '', update: '' },
        newOpptyIndex: 0,
        isOnline: false
    };
    $scope.reviewModel.isOnline = sharedDataService.getNetworkState();
    $scope.reviewselectedIndex = 0;
    $scope.postingIndex = '';
    //delete new opty from local DB
    $scope.deleteOpportunity = function (context, id, detail, index) {
        var oppDeleteConfirm = function (input) {
            if (input) {
                var deleteParam = {
                    oppID: id
                };
                var sqlParameters = Util.convertObjectToSQL(deleteParam);
                dbService.deleteFromTable('stc_offlineOppCreate', sqlParameters, angular.noop, angular.noop);
                $scope.reviewModel.syncCount.new = parseInt($scope.reviewModel.syncCount.new, 10) - 1;
                $scope.reviewModel.offlineList.splice(index, 1);
            }
        };
        if ($scope.reviewModel.offlineList[index].posting) {
            MaterialReusables.showToast('' + pluginService.getTranslations().noDeleteOppty, 'warning');
        } else {
            MaterialReusables.showConfirmDialog('', '' + pluginService.getTranslations().deleteConfirm, oppDeleteConfirm);
        }
    };
    //report update api call
    // var reportCreateActionCall = function (rowId) {
    //     var reportData = {
    //         "opportunityId": rowId,
    //         "action": 'UPD_CREATED_LEADS',
    //         "createdLeads": '1'
    //     };
    //     var reportPayload = Util.reportActionPayload(reportData);
    //     apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
    // };
    //create lead - API call
    var postOppty = function (context, id, detail, index, type, contactDetails) {
        if (!$scope.reviewModel.isPosting) {
            var oppAddSuccess = function (data) {
                if (data.data.errorMessage === 'Success') {
                    var deleteParam = {
                        oppID: id
                    };
                    $scope.reviewModel.syncCount.new = parseInt($scope.reviewModel.syncCount.new, 10) - 1;
                    var sqlParameters = Util.convertObjectToSQL(deleteParam);
                    $scope.reviewModel.offlineList.splice(index, 1);
                    dbService.deleteFromTable('stc_offlineOppCreate', sqlParameters, angular.noop, angular.noop);
                    $scope.reviewModel.isPosting = false;
                    if ($scope.reviewModel.bulkPostSelected) {
                        $scope.reviewModel.newOpptyIndex = 0;
                        bulkUploadOpptys();
                    }
                    MaterialReusables.showToast('' + pluginService.getTranslations().oppo + ': ' + data.data.listOfStcOpportunity.opportunity[0].sTCLeadNumber + ' ' + pluginService.getTranslations().creSuccess, 'success');
                    //reportCreateActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                } else {
                    $scope.reviewModel.isPosting = false;
                    var error = Util.throwError(data.data.errorMessage, MaterialReusables);
                    errorOpptyCreatePost('stc_offlineOppCreate', id, error);
                    if ($scope.reviewModel.bulkPostSelected) {
                        $scope.reviewModel.newOpptyIndex = index + 1;
                        bulkUploadOpptys();
                    }
                    $scope.reviewModel.offlineList[index].postStatus = error;
                    $scope.reviewModel.offlineList[index].posting = false;
                }
            };
            var oppCreateError = function (data) {
                $scope.reviewModel.isPosting = false;
                var error = Util.throwError(data.data.Message, MaterialReusables);
                errorOpptyCreatePost('stc_offlineOppCreate', id, error);
                if ($scope.reviewModel.bulkPostSelected) {
                    $scope.reviewModel.newOpptyIndex = index + 1;
                    bulkUploadOpptys();
                }
                $scope.reviewModel.offlineList[index].postStatus = error;
                $scope.reviewModel.offlineList[index].posting = false;
            };
            var headerData = {
                "sTCBulkLeadCreatedBy": detail.payload.listOfstcOpportunity.opportunity[0].stcBulkLeadCreatedBy,
                "sTCBulkLeadContactNumber": detail.payload.listOfstcOpportunity.opportunity[0].stcBulkLeadContactNumber,
                "sTCFeedback": detail.payload.listOfstcOpportunity.opportunity[0].stcFeedback,
                "sTCSubStatusSME": 'Lead',
                "sTCCreatedBy": sharedDataService.getUserDetails().Id,
                "rowId": '1234',
                "name": detail.payload.listOfstcOpportunity.opportunity[0].name,
                "sTCOpportunityType": detail.payload.listOfstcOpportunity.opportunity[0].sTCOpportunityType,
                "sTCOptyCity": detail.payload.listOfstcOpportunity.opportunity[0].sTCOptyCity,
                "accountId": detail.payload.listOfstcOpportunity.opportunity[0].accountId,
                "operation": 'insert'
            };
            var assigneeData = {
                "position": sharedDataService.getUserDetails().primaryPosition,
                "isPrimaryMVG": 'Y',
                "operation": 'insert'
            };
            var contactData = '';
            if (contactDetails !== "") {
                contactData = {
                    "firstName": contactDetails.firstName,
                    "lastName": contactDetails.lastName,
                    "personUId": contactDetails.contactId,
                    "employeeNumber": contactDetails.contactId,
                    "isPrimaryMVG": 'N',
                    "operation": 'insert'
                };
            }
            var oppAddPayload = Util.leadProcessPayload(headerData, assigneeData, contactData, '', '', '', '', '');
            apiService.fetchDataFromApi('opp.leadProcess', oppAddPayload, oppAddSuccess, oppCreateError);
        } else {
            if ($scope.reviewModel.offlineList[index].posting) {
                MaterialReusables.showToast('' + pluginService.getTranslations().postingProgress, 'warning');
            } else {
                MaterialReusables.showToast('' + pluginService.getTranslations().postOpptyOnceCompltd, 'warning');
            }
        }
    };
    //fetch acount details
    var getAccountDetails = function (context, id, detail, index, type) {
        var contactData = '';
        var accountEntered = detail.payload.listOfstcOpportunity.opportunity[0].accountId;
        if (accountEntered !== null) {
            var oppAccountSuccess = function (data) {
                if (data.data !== null) {
                    var accountId = data.data.account[0].rowId;
                    detail.payload.listOfstcOpportunity.opportunity[0].accountId = data.data.account[0].rowId;
                    var primaryContact = data.data.account[0].primaryContactId;
                    angular.forEach(data.data.account[0].contacts, function (val, key) {
                        if (val.contactId === primaryContact) {
                            contactData = { 'accountId': accountId, 'contactId': primaryContact, 'firstName': val.FirstName, 'lastName': val.lastName };
                        }
                    });
                    if (contactData !== '') {
                        postOppty(context, id, detail, index, type, contactData);
                    } else {
                        $scope.reviewModel.isPosting = false;
                        var error = '' + pluginService.getTranslations().noAccountExistOppty + ' : ' + accountEntered;
                        MaterialReusables.showToast('' + error, 'error');
                        errorOpptyCreatePost('stc_offlineOppCreate', id, error);
                        if ($scope.reviewModel.bulkPostSelected) {
                            $scope.reviewModel.newOpptyIndex = index + 1;
                            bulkUploadOpptys();
                        }
                        $scope.reviewModel.offlineList[index].postStatus = error;
                        $scope.reviewModel.offlineList[index].posting = false;
                    }
                } else {
                    $scope.reviewModel.isPosting = false;
                    var error = '' + pluginService.getTranslations().noAccountExistOppty + ' : ' + accountEntered;
                    MaterialReusables.showToast('' + error, 'error');
                    errorOpptyCreatePost('stc_offlineOppCreate', id, error);
                    if ($scope.reviewModel.bulkPostSelected) {
                        $scope.reviewModel.newOpptyIndex = index + 1;
                        bulkUploadOpptys();
                    }
                    $scope.reviewModel.offlineList[index].postStatus = error;
                    $scope.reviewModel.offlineList[index].posting = false;
                }
            };
            var oppAccountError = function (data) {
                var error = Util.throwError(data.data.Message, MaterialReusables);
                errorOpptyCreatePost('stc_offlineOppCreate', id, error);
                if ($scope.reviewModel.bulkPostSelected) {
                    $scope.reviewModel.newOpptyIndex = index + 1;
                    bulkUploadOpptys();
                }
                $scope.reviewModel.offlineList[index].postStatus = error;
                $scope.reviewModel.offlineList[index].posting = false;
                $scope.reviewModel.isPosting = false;
            };
            var oppAccountPayload = {
                "sortSpec": "",
                "searchSpec": "(([Account.STC Managed]='No' AND [Account.Account Type Code]='Customer') AND ([Account.CSN] LIKE  '" + '*' + accountEntered + '*' + "' ))",
                "startRowNum": "0",
                "pageSize": "10",
                "newQuery": true,
                "viewMode": "All",
                "busObjCacheSize": "10",
                "outputIntObjectName": "STC Account Contact"
            };
            apiService.fetchDataFromApi('opp.account', oppAccountPayload, oppAccountSuccess, oppAccountError);
        }
    };
    //lead creation - error
    var errorOpptyCreatePost = function (table, id, error) {
        var updateParam = { postStatus: error };
        var sqlUpdateParameters = Util.convertObjectToSQL(updateParam);
        var checkParam = { oppID: id };
        var sqlCheckParameters = Util.convertObjectToSQL(checkParam);
        var offlineUpdateSuccess = function () { };
        var offlineUpdateError = function () { };
        dbService.updateTable(table, sqlUpdateParameters, sqlCheckParameters, offlineUpdateError, offlineUpdateSuccess)
    };
    //invoke lead create method
    var individualCreatePost = function (context, id, detail, index, type) {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
        } else {
            $scope.reviewModel.offlineList[index].posting = true;
            if (type === 'New Sale') {
                postOppty(context, id, detail, index, type, '');
            } else {
                getAccountDetails(context, id, detail, index, type);
            }
        }
    };
    //invoke lead create method
    $scope.postOpportunity = function (context, id, detail, index, type) {
        $scope.reviewModel.bulkPostSelected = false;
        individualCreatePost(context, id, detail, index, type);
    };
    //popup info of opptys post status
    $scope.infoOpportunity = function (postStatus) {
        if (postStatus !== "" && postStatus !== null) {
            MaterialReusables.showErrorAlert(postStatus);
        }
    };
    //navigate to details of opptys
    $scope.gotoDetails = function (context, id, detail, index, type, recordStatus) {
        if (!sharedDataService.getNetworkState()) {
            if (recordStatus === 'new') {
                sharedDataService.saveReviewTab('new');
                $state.go('root.oppAddition', { id: id, detail: detail, fromView: $state.current.name });
            } else {
                sharedDataService.saveReviewTab('update');
                $state.go('root.oppDetail', { id: id, oppDetails: detail, fromView: $state.current.name, enableEdit: true });
            }
        } else {
            MaterialReusables.showToast('' + pluginService.getTranslations().offlineOpptysEditInOnline, 'error');
        }
    };
    //local DB fetch for oportunities to be updated
    var fetchOppsToBeUpdated = function () {
        var gotRecords = function (records) {
            var tempPushList = { oppData: [], payload: [], message: [] };
            angular.forEach(records, function (val, key) {
                tempPushList.oppData.push(JSON.parse(val.opportunities));
                tempPushList.payload.push(JSON.parse(val.payload));
                tempPushList.message.push({ isPosted: '', errorMsg: val.postStatus });
            });
            $timeout(function () {
                $scope.reviewModel.updateOppList = tempPushList;
                $scope.reviewModel.syncCount.update = $scope.reviewModel.updateOppList.oppData.length;
            });
            $scope.reviewModel.isLoading = false;
        };
        var failedToGetRecords = function (err) {
            $scope.reviewModel.isLoading = false;
        };
        dbService.getAllUpdatableRecords(gotRecords, angular.noop);
    };
    //local DB fetch for oportunities to be created/updated
    var fetchOfflineNewOpptys = function () {
        var offlineFetchSuccess = function (offlineData) {
            angular.forEach(offlineData, function (val, key) {
                var details = JSON.parse(val.oppDetail);
                details.payload.listOfstcOpportunity.opportunity[0].accountNumber = details.payload.listOfstcOpportunity.opportunity[0].accountId;
                $scope.reviewModel.offlineList.push({ 'id': val.oppID, 'type': val.oppType, 'detail': details, 'postStatus': val.postStatus, 'recordStatus': val.recordStatus })
            });
            $scope.reviewModel.syncCount.new = $scope.reviewModel.offlineList.length;
            fetchOppsToBeUpdated();
        };
        var offlineFetchError = function (err) {
            fetchOppsToBeUpdated();
        };
        var fetchParam = {
            recordStatus: 'new'
        };
        var sqlParameters = Util.convertObjectToSQL(fetchParam);
        $scope.reviewModel.offlineList = [];
        dbService.getDetailsFromTable('stc_offlineOppCreate', sqlParameters, offlineFetchError, offlineFetchSuccess);
    };
    //delete update opty from local DB
    $scope.deleteUpdatedOpportunity = function (id, index) {
        var updateOppDeleteConfirm = function (confirm) {
            if (confirm) {
                var deletedRecord = function () {
                    $timeout(function () {
                        $scope.reviewModel.syncCount.update = parseInt($scope.reviewModel.syncCount.update, 10) - 1;
                        $scope.reviewModel.updateOppList.oppData.splice(index, 1);
                    });
                };
                var notDeleted = function () { };
                dbService.deleteFromTable('stc_opptys', Util.convertObjectToSQL({ optyID: id }), notDeleted, deletedRecord);
            }
        };
        MaterialReusables.showConfirmDialog('', '' + pluginService.getTranslations().deleteConfirm, updateOppDeleteConfirm);
    };
    //oppty update payload creation
    var categorisedPayloadCreator = function (oppID, payloadsToUpdate) {
        var categorisedPayload = {
            typesToPost: ['Detail', 'Activity', 'Note', 'Product'],
            Detail: {
                id: oppID,
                update: { url: 'opp.leadProcess', payload: payloadsToUpdate.Details.update },
                length: payloadsToUpdate.Details.update.length
            },
            Activity: {
                id: oppID,
                create: { url: 'opp.leadProcess', payload: payloadsToUpdate.Activity.create },
                edit: { url: 'opp.leadProcess', payload: payloadsToUpdate.Activity.edit },
                delete: { url: 'opp.leadProcess', payload: payloadsToUpdate.Activity.delete },
                length: payloadsToUpdate.Activity.create.length + payloadsToUpdate.Activity.edit.length + payloadsToUpdate.Activity.delete.length
            },
            Note: {
                id: oppID,
                create: { url: 'opp.leadProcess', payload: payloadsToUpdate.Notes.create },
                edit: { url: 'opp.leadProcess', payload: payloadsToUpdate.Notes.edit },
                delete: { url: 'opp.leadProcess', payload: payloadsToUpdate.Notes.delete },
                length: payloadsToUpdate.Notes.create.length + payloadsToUpdate.Notes.edit.length + payloadsToUpdate.Notes.delete.length
            },
            Product: {
                id: oppID,
                create: { url: 'opp.leadProcess', payload: payloadsToUpdate.Products.create,revenueAmount:payloadsToUpdate.Products.create.opptyRevenueAmount },
                edit: { url: 'opp.leadProcess', payload: payloadsToUpdate.Products.edit,revenueAmount:payloadsToUpdate.Products.edit.opptyRevenueAmount },
                delete: { url: 'opp.leadProcess', payload: payloadsToUpdate.Products.delete,revenueAmount:payloadsToUpdate.Products.delete.opptyRevenueAmount },
                length: payloadsToUpdate.Products.create.length + payloadsToUpdate.Products.edit.length + payloadsToUpdate.Products.delete.length
            }
        };
        delete categorisedPayload.Product.create.payload.opptyRevenueAmount;
        delete categorisedPayload.Product.edit.payload.opptyRevenueAmount;
        delete categorisedPayload.Product.delete.payload.opptyRevenueAmount;
        if (!categorisedPayload.Detail.length) {
            categorisedPayload.typesToPost.splice(categorisedPayload.typesToPost.indexOf('Detail'), 1);
            delete categorisedPayload.Detail;
        }
        if (!categorisedPayload.Activity.length) {
            categorisedPayload.typesToPost.splice(categorisedPayload.typesToPost.indexOf('Activity'), 1);
            delete categorisedPayload.Activity;
        }
        if (!categorisedPayload.Note.length) {
            categorisedPayload.typesToPost.splice(categorisedPayload.typesToPost.indexOf('Note'), 1);
            delete categorisedPayload.Note;
        }
        if (!categorisedPayload.Product.length) {
            categorisedPayload.typesToPost.splice(categorisedPayload.typesToPost.indexOf('Product'), 1);
            delete categorisedPayload.Product;
        }
        return categorisedPayload;
    };
    //fetch accoutn details for update oppty API call
    var fetchAccountDetails = function (accountNo, callback) {
        var oppAccountPayload = {
            "sortSpec": "",
            "searchSpec": "(([Account.STC Managed]='No' AND [Account.Account Type Code]='Customer') AND ([Account.CSN] LIKE  '" + '*' + accountNo + '*' + "' ))",
            "startRowNum": "0",
            "pageSize": "10",
            "newQuery": true,
            "viewMode": "All",
            "busObjCacheSize": "10",
            "outputIntObjectName": "STC Account Contact"
        };
        var gotAccountDetail = function (data) {
            var contactData = '';
            if (data.data !== null) {
                var accountId = data.data.account[0].rowId;
                var primaryContact = data.data.account[0].primaryContactId;
                angular.forEach(data.data.account[0].contacts, function (val, key) {
                    if (val.contactId === primaryContact) {
                        contactData = { 'accountId': accountId, 'contactId': primaryContact, 'firstName': val.FirstName, 'lastName': val.lastName };
                    }
                });
                if (contactData !== '') {
                    callback(true, contactData);
                } else {
                    callback(false, 'Opportunity Update Failed Since No Account Found For : ' + accountNo);
                }
            } else {
                callback(false, 'Opportunity Update Failed Since No Account Found For : ' + accountNo);
            }
        };
        var failedToGetAccounts = function (data) {
            Util.throwError(data.data.Message, MaterialReusables);
        };
        apiService.fetchDataFromApi('opp.account', oppAccountPayload, gotAccountDetail, failedToGetAccounts);
    };
    //close post info popup
    $scope.dismissOppSyncPopup = function () {
        MaterialReusables.hideDialog();
    };
    //local DB post update error
    var updatePostError = function (table, id, error) {
        var updateParam = { postStatus: error };
        var sqlUpdateParameters = Util.convertObjectToSQL(updateParam);
        var checkParam = { optyID: id };
        var sqlCheckParameters = Util.convertObjectToSQL(checkParam);
        var offlineUpdateSuccess = function () { };
        var offlineUpdateError = function () { };
        dbService.updateTable(table, sqlUpdateParameters, sqlCheckParameters, offlineUpdateError, offlineUpdateSuccess)
    };
    //check for all opptys get posted or not
    var allPosted = function () {
        var posted = true;
        angular.forEach($scope.reviewModel.updateOppList.message, function (val, key) {
            if (val.errorMsg !== '') {
                posted = false;
            }
        });
        return posted;
    }
    //report update api call
    var reportActionCall = function (rowId, status) {
        var reportData = '';
        if (status.includes('Lost')) {
            reportData = {
                "opportunityId": rowId,
                "action": 'UPD_CANCEL_LEADS',
                "cancelledLeads": '1'
            };
        } else {
            reportData = {
                "opportunityId": rowId,
                "action": 'UPD_ACTED_LEADS',
                "actedLeads": '1'
            };
        }
        var reportPayload = Util.reportActionPayload(reportData);
        apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
    };
    //update bulk opptys - API call
    $scope.postUpdatedOpp = function (id, index, postType) {
        $scope.postingIndex = index;
        var payloadToPost, isPostingPayload = false, postingType = '', postingIndex = 0, postingAction = '', reportAction = 'false';
        var kickStartApiCall = function () {
            if (payloadToPost.typesToPost.length) {
                if (payloadToPost.typesToPost[0] === 'Detail') {
                    postingType = 'Detail';
                    postingAction = 'update';
                    $scope.reviewModel.updateOppsMessage = '' + pluginService.getTranslations().updatingOpp + ' : ' + payloadToPost[payloadToPost.typesToPost[0]].id;
                    if (payloadToPost[payloadToPost.typesToPost[0]].update.payload[0].payload.listOfstcOpportunity.opportunity[0].accountNo !== '' &&
                        payloadToPost[payloadToPost.typesToPost[0]].update.payload[0].payload.listOfstcOpportunity.opportunity[0].accountNo !== null) {
                        var callback = function (status, details) {
                            if (status) {
                                payloadToPost[payloadToPost.typesToPost[0]].update.payload[0].payload.listOfstcOpportunity.opportunity[0].contactId = details.contactId;
                                payloadToPost[payloadToPost.typesToPost[0]].update.payload[0].payload.listOfstcOpportunity.opportunity[0].accountId = details.accountId;
                                payloadToPost[payloadToPost.typesToPost[0]].update.payload[0].payload.listOfstcOpportunity.opportunity[0].firstName = details.firstName;
                                payloadToPost[payloadToPost.typesToPost[0]].update.payload[0].payload.listOfstcOpportunity.opportunity[0].lastName = details.lastName;
                                var leadProcessDetailPayload = Util.reviewLeadProcessPayload(id, payloadToPost.typesToPost[0], 'edit', payloadToPost[payloadToPost.typesToPost[0]].update.payload[0].payload, sharedDataService.getUserDetails().Id, sharedDataService.getUserDetails().primaryPosition);
                                apiService.fetchDataFromApi(payloadToPost[payloadToPost.typesToPost[0]].update.url, leadProcessDetailPayload, postedPayload, failedtoPostPayload);
                            } else {
                                failedtoPostPayload({ data: { Message: details } });
                            }
                        };
                        fetchAccountDetails(payloadToPost[payloadToPost.typesToPost[0]].update.payload[0].payload.listOfstcOpportunity.opportunity[0].accountNo, callback);
                    } else {
                        var leadProcessDetailPayload = Util.reviewLeadProcessPayload(id, payloadToPost.typesToPost[0], 'edit', payloadToPost[payloadToPost.typesToPost[0]].update.payload[0].payload, sharedDataService.getUserDetails().Id, sharedDataService.getUserDetails().primaryPosition);
                        apiService.fetchDataFromApi(payloadToPost[payloadToPost.typesToPost[0]].update.url, leadProcessDetailPayload, postedPayload, failedtoPostPayload);
                    }
                } else {
                    if (payloadToPost[payloadToPost.typesToPost[0]].create.payload.length) {
                        $scope.reviewModel.updateOppsMessage = 'Creating ' + payloadToPost.typesToPost[0] + ' for Opp: ' + payloadToPost[payloadToPost.typesToPost[0]].id;
                        postingType = payloadToPost.typesToPost[0];
                        postingAction = 'create';
                        angular.forEach(payloadToPost[payloadToPost.typesToPost[0]].create.payload, function (val, key) {
                            if (!isPostingPayload) {
                                postingIndex = key;
                                isPostingPayload = true;
                                var leadProcessPayload = Util.reviewLeadProcessPayload(id, payloadToPost.typesToPost[0], 'create', val, sharedDataService.getUserDetails().Id, sharedDataService.getUserDetails().primaryPosition);
                                apiService.fetchDataFromApi(payloadToPost[payloadToPost.typesToPost[0]].create.url, leadProcessPayload, postedPayload, failedtoPostPayload);
                            }
                        });
                    } else if (payloadToPost[payloadToPost.typesToPost[0]].edit.payload.length) {
                        $scope.reviewModel.updateOppsMessage = 'Updating ' + payloadToPost.typesToPost[0] + ' for Oppo: ' + payloadToPost[payloadToPost.typesToPost[0]].id;
                        postingType = payloadToPost.typesToPost[0];
                        postingAction = 'edit';
                        angular.forEach(payloadToPost[payloadToPost.typesToPost[0]].edit.payload, function (val, key) {
                            if (!isPostingPayload) {
                                postingIndex = key;
                                isPostingPayload = true;
                                var leadProcessPayload = Util.reviewLeadProcessPayload(id, payloadToPost.typesToPost[0], 'edit', val, sharedDataService.getUserDetails().Id, sharedDataService.getUserDetails().primaryPosition);
                                apiService.fetchDataFromApi(payloadToPost[payloadToPost.typesToPost[0]].edit.url, leadProcessPayload, postedPayload, failedtoPostPayload);
                            }
                        });
                    } else if (payloadToPost[payloadToPost.typesToPost[0]].delete.payload.length) {
                        $scope.reviewModel.updateOppsMessage = 'Deleting ' + payloadToPost.typesToPost[0] + ' for Opp: ' + payloadToPost[payloadToPost.typesToPost[0]].id;
                        postingType = payloadToPost.typesToPost[0];
                        postingAction = 'delete';
                        angular.forEach(payloadToPost[payloadToPost.typesToPost[0]].delete.payload, function (val, key) {
                            if (!isPostingPayload) {
                                postingIndex = key;
                                isPostingPayload = true;
                                var leadProcessPayload = Util.reviewLeadProcessPayload(id, payloadToPost.typesToPost[0], 'delete', val, sharedDataService.getUserDetails().Id, sharedDataService.getUserDetails().primaryPosition);
                                if (payloadToPost.typesToPost[0] === 'Product') {
                                    apiService.fetchDataFromApi(payloadToPost[payloadToPost.typesToPost[0]].delete.url, { 'rowId': val.id }, postedPayload, failedtoPostPayload);
                                } else if (payloadToPost.typesToPost[0] === 'Note') {
                                    apiService.fetchDataFromApi(payloadToPost[payloadToPost.typesToPost[0]].delete.url, { 'id': val.id }, postedPayload, failedtoPostPayload);
                                } else if (payloadToPost.typesToPost[0] === 'Activity') {
                                    apiService.fetchDataFromApi(payloadToPost[payloadToPost.typesToPost[0]].delete.url, { 'activityId': val.activityId }, postedPayload, failedtoPostPayload);
                                }
                            }
                        });
                    }
                }
            } else {
                $scope.postingIndex = '';
                var checkParameters = Util.convertObjectToSQL({ optyID: id });
                $scope.reviewModel.updateOppList.message[index].isPosted = true;
                $scope.reviewModel.updateOppList.message[index].errorMsg = '';
                $scope.reviewModel.syncCount.update = parseInt($scope.reviewModel.syncCount.update, 10) - 1;
                //$scope.reviewModel.updateOppList.message.splice(index,0,{isPosted:true,errorMsg:''});
                dbService.deleteFromTable('stc_opptys', checkParameters, angular.noop, angular.noop);
                if (postType === 'multiple') {
                    if ($scope.reviewModel.bulkPostRecords.length - 1 === index) {
                        MaterialReusables.hideDialog();
                        var deleteCount = 0;
                        angular.forEach($scope.reviewModel.updateOppList.message, function (val, key) {
                            if (val.isPosted) {
                                deleteCount++;
                            }
                        });
                        if (deleteCount !== $scope.reviewModel.bulkPostRecords.length) {
                            if (parseInt(deleteCount, 10) > 0) {
                                MaterialReusables.showToast(deleteCount + ' ' + pluginService.getTranslations().outOf + ' ' + $scope.reviewModel.bulkPostRecords.length + ' Offline Opportunities were Synced Successfully', 'success');
                            }
                        } else {
                            if (allPosted()) {
                                MaterialReusables.showToast('' + pluginService.getTranslations().offOppsSynsSucc, 'success');
                            }
                        }
                        $scope.reviewModel.updateOppList = { oppData: [], payload: [], message: [] };
                        fetchOppsToBeUpdated();
                    } else {
                        $scope.postUpdatedOpp($scope.reviewModel.bulkPostRecords[index + 1].optyID, index + 1, 'multiple');
                    }
                } else {
                    MaterialReusables.hideDialog();
                }
            }
        };
        var updatePayloadToDB = function(dbID,replacePayload){
            var updateParam = { payload: JSON.stringify(replacePayload) };
            var sqlUpdateParameters = Util.convertObjectToSQL(updateParam);
            var checkParam = { oppID: dbID };
            var sqlCheckParameters = Util.convertObjectToSQL(checkParam);
            dbService.updateTable('stc_opptys', sqlUpdateParameters, sqlCheckParameters, angular.noop, angular.noop);
        }; 
        var reportProductsActionCall = function(id,totRevenueAmount,oppPrimaryAmount){
            console.log(oppPrimaryAmount);
            console.log(totRevenueAmount);
           if(totRevenueAmount!==oppPrimaryAmount){
               var diffRevenueAmount = oppPrimaryAmount - totRevenueAmount;
               var reportData = {
                    "opportunityId": id,
                    "action": 'UPD_REVENUE',
                    "totalRevenue": "" + diffRevenueAmount + ""
                };
                var reportPayload = Util.reportActionPayload(reportData);
                apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
           } 
        }
        var postedPayload = function (data) {
            isPostingPayload = false;
            $scope.postingIndex = '';
            if (data.data.errorMessage !== 'Success') {
                $scope.postingIndex = '';
                isPostingPayload = false;
                $scope.reviewModel.updateOppList.message[index].isPosted = false;
                $scope.reviewModel.updateOppList.message[index].errorMsg = data.data.errorMessage;
                updatePostError('stc_opptys', $scope.reviewModel.updateOppList.oppData[index].rowId, data.data.errorMessage);
                // $scope.reviewModel.updateOppList.message.splice(index,0,{isPosted:false,errorMsg:data.data.errorMessage});
                Util.throwError(data.data.errorMessage, MaterialReusables);
                $scope.reviewModel.bulkPostSelected = false;
                if (postType === 'multiple') {
                    if ($scope.reviewModel.bulkPostRecords.length - 1 === index) {
                        MaterialReusables.hideDialog();
                        var deleteCount = 0;
                        angular.forEach($scope.reviewModel.updateOppList.message, function (val, key) {
                            if (val.isPosted) {
                                deleteCount++;
                            }
                        });
                        if (deleteCount !== $scope.reviewModel.bulkPostRecords.length) {
                            if (parseInt(deleteCount, 10) > 0) {
                                MaterialReusables.showToast(deleteCount + ' ' + pluginService.getTranslations().outOf + ' ' + $scope.reviewModel.bulkPostRecords.length + ' Offline Opportunities were Synced Successfully', 'success');
                            }
                        } else {
                            if (allPosted()) {
                                MaterialReusables.showToast('' + pluginService.getTranslations().offOppsSynsSucc, 'success');
                            }
                        }
                        $scope.reviewModel.updateOppList = { oppData: [], payload: [], message: [] };
                        fetchOppsToBeUpdated();
                    } else {
                        $scope.postUpdatedOpp($scope.reviewModel.bulkPostRecords[index + 1].optyID, index + 1, 'multiple');
                    }
                } else {
                    MaterialReusables.hideDialog();
                }
            } else {
                if (postingType === 'Detail') {
                    payloadToPost.typesToPost.splice(payloadToPost.typesToPost.indexOf('Detail'), 1);
                    payloadToPost[postingType][postingAction].payload.splice(postingIndex, 1);
                } else {
                    if(postingType === 'Product'){
                        reportProductsActionCall(payloadToPost[postingType].id,payloadToPost[postingType][postingAction].revenueAmount,data.data.listOfStcOpportunity.opportunity[0].PrimaryRevenueAmount);
                    }
                    if (payloadToPost[postingType].create.payload.length + payloadToPost[postingType].edit.payload.length + payloadToPost[postingType].delete.payload.length > 1) {
                        payloadToPost[postingType][postingAction].payload.splice(postingIndex, 1);
                    } else {
                        payloadToPost.typesToPost.splice(payloadToPost.typesToPost.indexOf(postingType), 1);
                    }
                }
                updatePayloadToDB(id,payloadToPost);
                reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId, data.data.listOfStcOpportunity.opportunity[0].salesStage);
                kickStartApiCall();
            }
        };
        var failedtoPostPayload = function (data) {
            $scope.postingIndex = '';
            isPostingPayload = false;
            $scope.reviewModel.updateOppList.message[index].isPosted = false;
            $scope.reviewModel.updateOppList.message[index].errorMsg = data.data.Message;
            updatePostError('stc_opptys', $scope.reviewModel.updateOppList.oppData[index].rowId, data.data.Message);
            // $scope.reviewModel.updateOppList.message.splice(index,0,{isPosted:false,errorMsg:data.data.errorMessage});
            Util.throwError(data.data.Message, MaterialReusables);
            $scope.reviewModel.bulkPostSelected = false;
            if (postType === 'multiple') {
                if ($scope.reviewModel.bulkPostRecords.length - 1 === index) {
                    MaterialReusables.hideDialog();
                    var deleteCount = 0;
                    angular.forEach($scope.reviewModel.updateOppList.message, function (val, key) {
                        if (val.isPosted) {
                            deleteCount++;
                        }
                    });
                    if (deleteCount !== $scope.reviewModel.bulkPostRecords.length) {
                        if (parseInt(deleteCount, 10) > 0) {
                            MaterialReusables.showToast(deleteCount + ' ' + pluginService.getTranslations().outOf + ' ' + $scope.reviewModel.bulkPostRecords.length + ' Offline Opportunities were Synced Successfully', 'success');
                        }
                    } else {
                        if (allPosted()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().offOppsSynsSucc, 'success');
                        }
                    }
                    $scope.reviewModel.updateOppList = { oppData: [], payload: [], message: [] };
                    fetchOppsToBeUpdated();
                } else {
                    $scope.postUpdatedOpp($scope.reviewModel.bulkPostRecords[index + 1].optyID, index + 1, 'multiple');
                }
            } else {
                MaterialReusables.hideDialog();
            }
        };
        var gotOppty = function (oppData) {
            payloadToPost = categorisedPayloadCreator(JSON.parse(oppData[0].opportunities).sTCLeadNumber, JSON.parse(oppData[0].payload));
            kickStartApiCall();
        };
        var failedToGetOpp = function (err) { };
        dbService.getDetailsFromTable('stc_opptys', Util.convertObjectToSQL({ optyID: id }), failedToGetOpp, gotOppty);
    };
    //update individual oppty - API call
    $scope.postIndividualPayloads = function (id, index, postType) {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
        } else {
            MaterialReusables.showOppSyncPopup($scope);
            $scope.postUpdatedOpp(id, index, postType);
        }
    };
    //invoke individual post method
    var bulkUploadOpptys = function () {
        if ($scope.reviewModel.offlineList.length > $scope.reviewModel.newOpptyIndex) {
            $scope.reviewModel.offlineList[$scope.reviewModel.newOpptyIndex].posting = true;
            individualCreatePost('', $scope.reviewModel.offlineList[$scope.reviewModel.newOpptyIndex].id, $scope.reviewModel.offlineList[$scope.reviewModel.newOpptyIndex].detail, $scope.reviewModel.newOpptyIndex, $scope.reviewModel.offlineList[$scope.reviewModel.newOpptyIndex].type);
        }
    };
    //invoke bulk update opptys method
    $scope.bulkuploadUpdatedOpps = function (index) {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
        } else {
            var gotAllRecords = function (records) {
                if (records.length) {
                    $scope.reviewModel.bulkPostSelected = true;
                    $scope.reviewModel.bulkPostRecords = records;
                    MaterialReusables.showOppSyncPopup($scope);
                    $scope.postUpdatedOpp($scope.reviewModel.bulkPostRecords[index].optyID, index, 'multiple');
                } else {
                    $scope.reviewModel.bulkPostSelected = false;
                }
            };
            var failedToGetRecords = function () {
                $scope.reviewModel.bulkPostSelected = false;
            };
            dbService.getAllUpdatableRecords(gotAllRecords, failedToGetRecords);
        }
    };
    //show error in post operation
    $scope.showUpdateOppInfo = function (index) {
        // if ($scope.reviewModel.updateOppList.message[index].isPosted !== '' && !$scope.reviewModel.updateOppList.message[index].errorMsg !== '') {
        if ($scope.reviewModel.updateOppList.message[index].errorMsg !== '') {
            MaterialReusables.showErrorAlert($scope.reviewModel.updateOppList.message[index].errorMsg);
        }
    };
    //bulk upload invoke method
    $scope.bulkupload = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
        } else {
            if (!$scope.reviewModel.isPosting) {
                var oppBulkPostConfirm = function (confirm) {
                    if (confirm) {
                        $scope.reviewModel.bulkPostSelected = true;
                        $scope.reviewModel.newOpptyIndex = 0;
                        bulkUploadOpptys();
                    }
                };
                MaterialReusables.showConfirmDialog('', '' + pluginService.getTranslations().postAllOpptys, oppBulkPostConfirm);
            } else {
                MaterialReusables.showToast('' + pluginService.getTranslations().postingProgress, 'warning');
            }
        }
    };
    fetchOfflineNewOpptys();
    if (angular.isUndefined(sharedDataService.getReviewTab()) || sharedDataService.getReviewTab() === 'new') {
        $scope.reviewselectedIndex = 0;
    } else {
        $scope.reviewselectedIndex = 1;
    }
};
reviewModule.controller('reviewController', reviewController);
reviewController.$inject = ['$scope', '$timeout', '$translate', '$state', 'dbService', 'apiService', 'MaterialReusables', 'sharedDataService', 'pluginService'];