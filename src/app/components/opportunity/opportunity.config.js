opportunityModule.config(['$stateProvider',function ($stateProvider) { 
    $stateProvider.state('root.oppList',{
            url: '/oppList',
            templateUrl: 'app/components/opportunity/opportunity.list/opp.list.template.html',
            controller: 'opportunityListController',
            pageTitle:'Opportunity',
            backbutton:false,
            previousState:'root.home',
            buttonProp:['add','search','refresh']
        })
        .state('root.oppDetail',{
            url: '/oppDetail',
            templateUrl: 'app/components/opportunity/opportunity.detail/opp.detail.template.html',
            controller: 'opportunityDetailController',
            pageTitle:'OPTY:',
            backbutton:true,
            buttonProp:['refresh'],
            previousState:'',
            validatingPage:true,
            params:{id:'',oppDetails:'',fromView:'',enableEdit:''}
        })
        .state('root.oppAddition',{
            url: '/oppAddition',
            templateUrl: 'app/components/opportunity/opportunity.addition/opp.add.template.html',
            controller: 'opportunityAddController',
            pageTitle:'Create Opportunity',
            backbutton:true,
            previousState:'',
            validatingPage:true,
            params:{fromView:'',id:'',detail:''}
        })
    }
]);