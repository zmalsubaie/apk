var detailMap = function ($state, $translate, $timeout, sharedDataService, pluginService, MaterialReusables, apiService) {
    return {
        restrict: 'E',
        scope: {
            map: '=',
            updateoppFn: '&'
        },
        templateUrl: "app/components/opportunity/opportunity.detail/detail.components/Map/opp.detail.map.html",
        transclude: true,
        compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                    scope.mapDetails = {
                        mapInfo: {},
                        oppDetails: {},
                        positionCoordinates: {},
                        isSendingUSerLocation: false,
                        enableEdit: true,
                        noLocation: false,
                        noNetwork: false
                    };
                    scope.mapDetails.enableEdit = scope.map.isEditable;
                    scope.mapDetails.mapInfo = scope.map;
                },
                post: function postLink(scope, iElement, iAttrs, controller) {
                    //initilize map view
                    scope.initializeMapView = function (lat, long) {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                        } else {
                            var latitude = lat;
                            var longitude = long;
                            var markerLocation = function (coordinates, mapInst) {
                                scope.mapDetails.positionCoordinates = coordinates;
                            };
                            if (!latitude || latitude === '' || !angular.isDefined(latitude) &&
                                !longitude || longitude === '' || !angular.isDefined(longitude)) {
                                var gotLocation = function (pos) {
                                    latitude = pos.coords.latitude;
                                    longitude = pos.coords.longitude;
                                    scope.mapDetails.positionCoordinates = { lat: latitude, lng: longitude };
                                    pluginService.initializeMap([{ 'decimalLatitude': latitude, 'decimalLongitude': longitude, 'name': pluginService.getTranslations().setNewLoc, 'sTCLeadNumber': '' + pluginService.getTranslations().updateCurrPositn }], 'single', "opp_map", false, markerLocation, angular.noop);
                                };
                                var failedtoLocate = function (err) {
                                    var setLocationConfirm = function (confirm) {
                                        if (confirm) {
                                            MaterialReusables.showToast('' + pluginService.getTranslations().refreshLocationEnable, 'warning');
                                        }
                                    };
                                    MaterialReusables.showSuccessAlert('' + pluginService.getTranslations().enableLocation, setLocationConfirm);
                                };
                                var isEnabled = function (isEnabled) {
                                    if (isEnabled) { pluginService.getCurrentPosition(gotLocation, failedtoLocate); }
                                    else {
                                        failedtoLocate('');
                                    }
                                };
                                var errorInCheck = function (error) { pluginService.getCurrentPosition(gotLocation, failedtoLocate); };
                                if (scope.mapDetails.enableEdit) {
                                    pluginService.isLocationEnabled(isEnabled, errorInCheck);
                                } else {
                                    scope.mapDetails.noLocation = true;
                                }
                            } else {
                                var mapInst = plugin.google.maps.Map.getMap(document.getElementById("opp_map"));
                                var locationUpdateConfirm = function (confirm) {
                                    mapInst.setClickable(true);
                                    if (confirm) {
                                        scope.initializeMapView('', '');
                                    } else {
                                        pluginService.initializeMap([{ 'decimalLatitude': latitude, 'decimalLongitude': longitude, 'name': '', 'sTCLeadNumber': '' }], 'single', "opp_map", false, markerLocation, angular.noop);
                                    }
                                };
                                $timeout(function () {
                                    mapInst.setClickable(false);
                                    MaterialReusables.showConfirmDialog('', '' + pluginService.getTranslations().locCaptured, locationUpdateConfirm);
                                }, 4000);
                                scope.mapDetails.positionCoordinates = { lat: latitude, lng: longitude };
                                if (scope.mapDetails.enableEdit) {
                                    pluginService.initializeMap([{ 'decimalLatitude': latitude, 'decimalLongitude': longitude, 'name': pluginService.getTranslations().setNewLoc, 'sTCLeadNumber': '' + pluginService.getTranslations().updateCurrPositn }], 'single', "opp_map", false, markerLocation, angular.noop);
                                } else {
                                    pluginService.initializeMap([{ 'decimalLatitude': latitude, 'decimalLongitude': longitude, 'name': '', 'sTCLeadNumber': '' }], 'single', "opp_map", false, markerLocation, angular.noop);
                                }
                            }
                        }
                    };
                    //report update api call
                    var reportActionCall = function (rowId) {
                        var reportData = {
                            "opportunityId": rowId,
                            "action": 'UPD_LOCATION',
                            "locationsCaptured": '1'
                        };
                        var reportPayload = Util.reportActionPayload(reportData);
                        apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
                    };
                    //set location API call
                    scope.$on('setSelectedLocation', function (event, args) {
                        if (!sharedDataService.getNetworkState()) {
                            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
                        } else {
                            if (scope.mapDetails.mapInfo.details.salesStage.includes('Win') || scope.mapDetails.mapInfo.details.salesStage.includes('Lost')) {
                                MaterialReusables.showToast('' + pluginService.getTranslations().wiLoNoSet, 'warning');
                            } else {
                                var isEnabled = function (enabled) {
                                    if (enabled) {
                                        var mapInst = plugin.google.maps.Map.getMap(document.getElementById("opp_map"));
                                        var locationConfirm = function (confirm) {
                                            if (confirm) {
                                                var headerData = {
                                                    "rowId": scope.mapDetails.mapInfo.rowId,
                                                    "opptyName": scope.mapDetails.mapInfo.name,
                                                    "decimalLatitude": scope.mapDetails.positionCoordinates.lat,
                                                    "decimalLongitude": scope.mapDetails.positionCoordinates.lng,
                                                    "operation": "updatesync"
                                                }
                                                var locationPayload = Util.leadProcessPayload(headerData, '', '', '', '', '', '', '');
                                                var locationSend = function (data) {
                                                    if (data.data.errorMessage === 'Success') {
                                                        scope.mapDetails.isSendingUSerLocation = false;
                                                        MaterialReusables.showToast('' + pluginService.getTranslations().locMapUp, 'success');
                                                        $timeout(function () {
                                                            mapInst.setClickable(true);
                                                        }, 4000)
                                                        reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
                                                    } else {
                                                        scope.mapDetails.isSendingUSerLocation = false;
                                                        Util.throwError(data.data.errorMessage, MaterialReusables);
                                                    }
                                                };
                                                var locationSetFailed = function (data) {
                                                    scope.mapDetails.isSendingUSerLocation = false;
                                                    Util.throwError(data.data.Message, MaterialReusables);
                                                };
                                                scope.mapDetails.isSendingUSerLocation = true;
                                                apiService.fetchDataFromApi('opp.leadProcess', locationPayload, locationSend, locationSetFailed);
                                            } else {
                                                mapInst.setClickable(true);
                                            }
                                        };
                                        mapInst.setClickable(false);
                                        MaterialReusables.showConfirmDialog('', '' + pluginService.getTranslations().setLocationConfirm, locationConfirm);
                                    } else {
                                        MaterialReusables.showSuccessAlert('' + pluginService.getTranslations().enableLocation, angular.noop);
                                    }
                                };
                                var errorInCheck = function () { MaterialReusables.showSuccessAlert('' + pluginService.getTranslations().enableLocation, angular.noop); };
                                pluginService.isLocationEnabled(isEnabled, errorInCheck);
                            }
                        }
                    });
                    //check and load map if online
                    if (!sharedDataService.getNetworkState()) {
                        scope.mapDetails.noNetwork = true;
                    } else {
                        scope.mapDetails.noNetwork = false;
                        scope.initializeMapView(scope.mapDetails.mapInfo.decimalLatitude, scope.mapDetails.mapInfo.decimalLongitude);
                    }
                }
            }
        }
    }
};
opportunityModule.directive('detailMap', detailMap);
detailMap.$inject = ['$state', '$translate', '$timeout', 'sharedDataService', 'pluginService', 'MaterialReusables', 'apiService'];