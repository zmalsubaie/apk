var accountController = function ($rootScope, $scope, $timeout, $translate, $state, $http, apiService, sharedDataService, MaterialReusables, pluginService, dbService) {
  $scope.accListModel = {
    isLoading: true,
    subCategoryList: [],
    selectedCategory: 'LEAD',
    selectedIndex: '',
    accounts: [],
    searchText: '',
    startRow: 0,
    isRefresh: true,
    search: false
  };
  $scope.endOfPage = false;
  //check for item exists in array or not
  function indexOf(array, item) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].masterAccountNumber === item.masterAccountNumber) return i;
    }
    return -1;
  }
  //fetch accounts from local DB in offline
  var fetchAccountsFromDB = function () {
    var gotAccountsFromDB = function (accounts) {
      $scope.accListModel.isRefresh = false;
      $scope.accListModel.isLoading = false;
      $timeout(function () {
        $scope.accListModel.accounts = JSON.parse(accounts[0].accounts);
      });
    };
    dbService.getDetailsFromTable('stc_accounts', [], angular.noop, gotAccountsFromDB);
  };
  //accounts list fetch API call success
  var oppAccountSuccess = function (data) {
    $scope.accListModel.isRefresh = false;
    $scope.accListModel.isLoading = false;
    if (data.data !== null) {
      angular.forEach(data.data.account, function (value, key) {
        if (indexOf($scope.accListModel.accounts, value) === -1) {
          $scope.accListModel.accounts.push(value);
        }
      });
      if ($scope.action === 'load') {
        dbService.insertIntoDBTable('stc_accounts', [JSON.stringify($scope.accListModel.accounts), 'online'], angular.noop, angular.noop);
      }
    } else {
      if ($scope.action === 'search') {
        MaterialReusables.showToast('' + pluginService.getTranslations().noAccountsFound, 'warning');
      } else {
        $scope.endOfPage = true;
        MaterialReusables.showToast('' + pluginService.getTranslations().endofList, 'warning');
      }
    }
    if (angular.isDefined(data.data.account) && data.data.account !== null) {
      if (data.data.account.length < 10) {
        $scope.endOfPage = true;
        MaterialReusables.showToast('' + pluginService.getTranslations().endofList, 'warning');
      } else {
        $scope.endOfPage = false;
      }
    }
  };
  //accounts list fetch API call error
  var oppAccountError = function (data) {
    $scope.accListModel.isRefresh = false;
    $scope.accListModel.isLoading = false;
    Util.throwError(data.data.Message, MaterialReusables);
  };
  //account list API call
  $scope.fetchAccountList = function (searchSpec) {
    var newQuery = '';
    if ($scope.accListModel.startRow === 0) {
      newQuery = true;
    } else {
      newQuery = false;
    }
    var oppAccountPayload = {
      "sortSpec": "",
      "searchSpec": searchSpec,
      "startRowNum": $scope.accListModel.startRow,
      "pageSize": "30",
      "newQuery": newQuery,
      "viewMode": "All",
      "busObjCacheSize": "",
      "outputIntObjectName": "STC Account Contact"
    };
    apiService.fetchDataFromApi('opp.account', oppAccountPayload, oppAccountSuccess, oppAccountError);
  };
  //invoke account list call method
  if (!sharedDataService.getNetworkState()) {
    fetchAccountsFromDB();
  } else {
    $scope.action = 'load';
    $scope.fetchAccountList("[Account.STC Managed]='No' AND [Account.Account Type Code] = 'Customer'");
  }
  //load more accounts
  $scope.loadMoreAccounts = function () {
    if (sharedDataService.getNetworkState()) {
      if (!$scope.endOfPage) {
        $scope.action = 'load';
        $scope.accListModel.isRefresh = true;
        $scope.accListModel.startRow = parseInt($scope.accListModel.startRow, 10) + 30;
        $scope.fetchAccountList("[Account.STC Managed]='No' AND [Account.Account Type Code] = 'Customer'");
      }
    }
  };
  //accoutn search
  $scope.$on('openSearch', function (event, args) {
    $scope.accListModel.search = !$scope.accListModel.search;
    if (!$scope.accListModel.search) {
      $scope.accListModel.searchText = '';
    }
  });
  //account search api call
  $scope.searchAccountsByText = function () {
    if(sharedDataService.getNetworkState()){
        if ($scope.accListModel.searchText !== '') {
          $scope.action = 'search';
          $scope.accListModel.isRefresh = true;
          $scope.accListModel.startRow = 0;
          var searchSpecifictn = "(([Account.STC Managed]='No' AND [Account.Account Type Code]='Customer') AND ([Account.Name] LIKE '" + '*' + $scope.accListModel.searchText + '*' + "' OR [Account.STC Commercial Registration Number] LIKE  '" + '*' + $scope.accListModel.searchText + '*' + "' OR [Account.CSN] LIKE  '" + '*' + $scope.accListModel.searchText + '*' + "' ))";
          $scope.fetchAccountList(searchSpecifictn);
        }
    }
  };
  //accoutn search filter o typing
  $scope.searchFilter = function (obj) {
    if (typeof obj !== 'undefined') {
      $scope.accListModel.searchText = $scope.accListModel.searchText.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g,'');
      var re = new RegExp($scope.accListModel.searchText, 'i');
      return !$scope.accListModel.searchText || re.test(obj.masterAccountNumber) || re.test(obj.name) || re.test(obj.stcCommercialRegistrationNumber);
    }
  };
};
accountModule.controller('accountController', accountController);
accountController.$inject = ['$rootScope', '$scope', '$timeout', '$translate', '$state', '$http', 'apiService', 'sharedDataService', 'MaterialReusables', 'pluginService', 'dbService'];
