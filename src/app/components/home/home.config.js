stcApp.config(['$stateProvider',function ($stateProvider) { 
    $stateProvider.state('root.home',{
            url: '/home',
            templateUrl: 'app/components/home/home.template.html',
            controller: 'homeController',
            pageTitle:'Home',
            backbutton:false
        })
    }
]);