var calenderController = function ($scope, $state, $filter, $translate, sharedDataService, dbService, apiService, pluginService, MaterialReusables) {
    $scope.eventsList = [];
    $scope.events = [];
    $scope.isLoading = true;
    $scope.calenderViewMode = 'calender';
    var selectedDate = '';
    $scope.calender = {
        selectedType: '',
        selectedPriority: '',
        selectedStatus: '',
        selectedOpportunityName: '',
        selectedOppRowId: '',
        type: activityTypeList,
        priority: activityPriorityList,
        status: [],
        description: '',
        selectedStartTime: '',
        selectedEndTime: '',
        selectedDueTime: '',
        doneFlag: '',
        alarmFlag: '',
        showActivityAddLoader: false,
        showOpportunities: false,
        oppList: [],
        isFetchingMoreOpps: false,
        startRow: '0',
        isFetchingNexMonth: false,
        monthText: [],
        monthShortText: [],
        dayText: [],
        dayShortText: [],
        isFetchingSpecificActivity: false,
        specificActivity: {},
        isNetworkAvailable: false
    };
    //select Arabic/English view based on app language selection
    if (sharedDataService.getLanguage() === 'ar_AR') {
        $scope.calender.monthText = months_AR;
        $scope.calender.dayText = days_AR;
        $scope.calender.dayShortText = days_AR;
    } else {
        $scope.calender.monthText = months;
        $scope.calender.dayText = days;
        $scope.calender.dayShortText = days_short;
    }
    //push newly added events to device local notifications
    var pushEvents = function () {
        var eventToPush = [{
            'planned': $scope.calender.selectedStartTime,
            'plannedCompletion': $scope.calender.selectedEndTime,
            'description2': $scope.calender.description,
            'sTCEBUOpptyActivityType': $scope.calender.selectedType,
            'alarm': $scope.calender.alarmFlag ? $scope.calender.alarmFlag : "N",
            'done': $scope.calender.doneFlag ? $scope.calender.doneFlag : "N"
        }];
        pluginService.createLocalNotification(eventToPush);
    };
    //fetch all events and plot in calendar
    var getAllActivities = function (month) {
        $scope.events = [];
        var gotActivities = function (data) {
            $scope.isLoading = false;
            $scope.calender.isFetchingNexMonth = false;
            if (data.data.listOfAction !== null) {
                angular.forEach(data.data.listOfAction.action, function (actionVal, actionKey) {
                    var eventStatus = '';
                    if (moment() > moment(actionVal.planned) && moment() < moment(actionVal.plannedCompletion)) {
                        eventStatus = 'active';
                    } else if (moment() > moment(actionVal.plannedCompletion)) {
                        eventStatus = 'past';
                    } else if (moment() < moment(actionVal.planned)) {
                        eventStatus = 'future';
                    }
                    $scope.events.push({ id: actionVal.id, title: actionVal.description2, start: moment(actionVal.planned, "MM-DD-YYYY HH:mm").format(), end: moment(actionVal.plannedCompletion, "MM-DD-YYYY HH:mm").format(), status: eventStatus })
                });
                renderAllEvents('', month);
                MaterialReusables.showToast($scope.events.length + ' ' + pluginService.getTranslations().eventsFound + ' ' + months[moment(month).month()] + ' ' + moment(month).year(), 'success');
            } else {
                MaterialReusables.showToast('' + pluginService.getTranslations().noEventsFound + ' ' + months[moment(month).month()] + ' ' + moment(month).year(), 'warning');
            }
        };
        var failedGettingActivities = function (error) {
            $scope.isLoading = false;
            $scope.calender.isFetchingNexMonth = false;
            Util.throwError(error.data.Message, MaterialReusables);
        };
        var queryStartDate = moment(month).startOf('month').subtract(1, 'days').format('MM/DD/YYYY 23:59:59');
        var queryEndDate = moment(month).endOf('month').add(1, 'days').format('MM/DD/YYYY 00:00:00');
        var activityPayload = {
            'payload': {
                "searchSpec": "[STC Action Thin.Opportunity Primary Position Id]='" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND [STC Action Thin.Planned] > '" + queryStartDate + "' AND [STC Action Thin.Planned] < '" + queryEndDate + "'",
                "outputIntObjectName": "STC Action Thin"
            }
        };
        if (sharedDataService.getNetworkState()) {
            $scope.calender.isFetchingNexMonth = true;
            $scope.calender.isNetworkAvailable = true;
            apiService.fetchDataFromApi('opp.activitiesList', activityPayload, gotActivities, failedGettingActivities);
        } else {
            $scope.calender.isNetworkAvailable = false;
            $scope.isLoading = false;
        }
    };
    //rendering events in calendar
    var renderAllEvents = function (currEvt, date) {
        selectedDate = new Date(date);
        selectedDate.setHours(00);
        selectedDate.setMinutes(00);
        $(".fc-other-month .fc-day-number").remove();
        $('.fc-day').removeClass('calenderActive');
        if (!$(currEvt).hasClass("fc-other-month")) {
            $(currEvt).addClass('calenderActive');
        }
        $('#eventsCalendar').fullCalendar('removeEvents');
        $('#eventsCalendar').fullCalendar('gotoDate', date);
        angular.forEach($scope.events, function (v, k) {
            var activityDates = $(".fc-bg").find("[data-date='" + moment(v.start).format('YYYY-MM-DD') + "']");
            var eventNotifydiv = $("<div>", { "class": "notice" });
            if (activityDates.has('div').length === 0) {
                activityDates.append(eventNotifydiv);
            }
            $('#eventsCalendar').fullCalendar('renderEvent', v, true);
        });
    };
    //invoke and load month wise activities on calendar navigation
    $('#calendar').fullCalendar({
        monthNames: $scope.calender.monthText,
        dayNames: $scope.calender.dayText,
        dayNamesShort: $scope.calender.dayShortText,
        height: $(window).height() / 2,
        dayClick: function (date, jsEvent, view) {
            renderAllEvents(this, date);
        },
        viewRender: function (view, element) {
            getAllActivities($('#calendar').fullCalendar('getDate'));
            renderAllEvents(this, $('#calendar').fullCalendar('getDate'));
        }
    });
    //event details display
    $('#eventsCalendar').fullCalendar({
        height: $(window).height() / 2,
        monthNames: $scope.calender.monthText,
        dayNames: $scope.calender.dayText,
        dayNamesShort: $scope.calender.dayShortText,
        allDaySlot: false,
        header: false,
        timeFormat: 'H:mm',
        slotLabelFormat: 'H:mm',
        defaultDate: Date.now(),
        slotEventOverlap: false,
        eventAfterRender: function (event, $el, view) {
            $el.removeClass('fc-short');
        },
        eventRender: function (event, element) {
            if (event.status === 'active') {
                element.addClass('calenderEventActive');
            } else if (event.status === 'past') {
                element.addClass('calenderEventPast');
            } else if (event.status === 'future') {
                element.addClass('calenderEventFuture');
            }
        },
        eventClick: function (calEvent, jsEvent, view) {
            var title = '';
            var textToShow = '';
            if (calEvent.title || calEvent.title !== null) {
                title = calEvent.title;
            } else {
                title = '' + pluginService.getTranslations().noDesc;
            }
            var onEventPopup = function (feedback) {
                MaterialReusables.hideDialog();
                if (feedback) {
                    var viewActivityPayload = {
                        "searchSpec": "[STC Action Thin.Id]='" + calEvent.id + "'",
                        "outputIntObjectName": "STC Action Thin"
                    }
                    var gotSpecificActivity = function (data) {
                        $scope.calender.isFetchingSpecificActivity = false;
                        if (data.data.listOfAction !== null) {
                            $scope.calender.specificActivity = data.data.listOfAction.action[0];
                        } else {
                            MaterialReusables.hideBottomSheetOptions();
                            MaterialReusables.showToast('' + pluginService.getTranslations().noEventData, 'error');
                        }
                    };
                    var failedGettingSpecActivity = function (data) {
                        MaterialReusables.hideBottomSheetOptions();
                        $scope.calender.isFetchingSpecificActivity = false;
                        Util.throwError(data.data.Message, MaterialReusables);
                    };
                    $scope.calender.isFetchingSpecificActivity = true;
                    $scope.calender.specificActivity = {};
                    MaterialReusables.showActivityListSheet($scope);
                    apiService.fetchDataFromApi('opp.activitiesList', viewActivityPayload, gotSpecificActivity, failedGettingSpecActivity);
                }
            };
            if (angular.isUndefined(calEvent.end) || calEvent.end === null || calEvent.end === '') {
                textToShow = '<span>' + '' + pluginService.getTranslations().evnTime + ' : ' + '</span><span>' + moment(calEvent.start).format('HH:mm') + '</span><span></span><br><span>' + '' + pluginService.getTranslations(). desc+' : </span><span>' + title + '</span>'
            } else {
                textToShow = '<span>' + '' + pluginService.getTranslations().evnTime + ' : ' + '</span><span>' + moment(calEvent.start).format('HH:mm') + '</span><span>' + ' ' + pluginService.getTranslations().to + ' ' + moment(calEvent.end).format('HH:mm') + '</span><br><span>' + '' + pluginService.getTranslations().desc + ' :' + ' </span><span>' + title + '</span>';
            }
            MaterialReusables.showInfoPopup('' + pluginService.getTranslations().evntDetails, textToShow, '' + pluginService.getTranslations().vm, '' + pluginService.getTranslations().Close, onEventPopup);
        }
    });
    $('#eventsCalendar').fullCalendar('changeView', 'agendaDay');
    $scope.switchMonth = function (dir) {
        $('#calendar').fullCalendar(dir);
    };
    //adding an event
    $scope.doCalenderAddActivity = function () {
        $scope.calenderViewMode = 'createActivity';
        $scope.cancel = true;
        resetAllCalenderParams();
        selectedDate.setHours(new Date().getHours());
        selectedDate.setMinutes(new Date().getMinutes());
        $scope.calender.selectedStartTime = selectedDate;
        $scope.calender.selectedEndTime = selectedDate;
        $scope.opptyName = '';
    };
    //cancel add event operation
    $scope.cancelActivityEntry = function () {
        $scope.calenderViewMode = 'calender';
    };
    //close activity details bottom tab
    $scope.closeSpecActivityWindow = function () {
        MaterialReusables.hideBottomSheetOptions();
    };
    //reset calendar activities
    var resetAllCalenderParams = function () {
        $scope.calender.selectedOppRowId = '';
        $scope.calender.selectedOpportunityName = '';
        $scope.calender.selectedType = '';
        $scope.calender.description = '';
        $scope.calender.selectedPriority = '';
        $scope.calender.selectedStatus = '';
        $scope.calender.selectedStartTime = '';
        $scope.calender.selectedEndTime = '';
        $scope.calender.selectedDueTime = '';
        $scope.calender.doneFlag = '';
        $scope.calender.alarmFlag = '';
        if (!$scope.cancel) {
            $scope.calender.startRow = '0';
            $scope.eventsList = [];
        }
    };
    //load new activity to calendar
    var pushNewActvityToCalendar = function (actionVal) {
        var eventStatus = '';
        if (moment() > moment(actionVal.planned) && moment() < moment(actionVal.plannedCompletion)) {
            eventStatus = 'active';
        } else if (moment() > moment(actionVal.plannedCompletion)) {
            eventStatus = 'past';
        } else if (moment() < moment(actionVal.planned)) {
            eventStatus = 'future';
        }
        $scope.events.push({ id: actionVal.id, title: actionVal.description2, start: moment(actionVal.planned, "MM-DD-YYYY HH:mm").format(), end: moment(actionVal.plannedCompletion, "MM-DD-YYYY HH:mm").format(), status: eventStatus });
        renderAllEvents($('#calendar').fullCalendar('getDate'));
    };
    //create activity API call
    var createCalenderActivity = function (context) {
        if (!$scope.calender.doneFlag) {
            $scope.calender.doneFlag = 'N'
        } else {
            $scope.calender.doneFlag = 'Y';
        }
        if (!$scope.calender.alarmFlag) {
            $scope.calender.alarmFlag = 'N'
        } else {
            $scope.calender.alarmFlag = 'Y';
        }
        var headerData = {
            "operation": "skipnode",
            "rowId": $scope.calender.selectedOppRowId
        };
        var actionData = {
            "alarm": $scope.calender.alarmFlag,
            "description2": $scope.calender.description,
            "doneFlag": $scope.calender.doneFlag,
            "due": $filter('date')($scope.calender.selectedDueTime, 'MM/dd/yyyy HH:mm:ss'),
            "planned": $filter('date')($scope.calender.selectedStartTime, 'MM/dd/yyyy HH:mm:ss'),
            "plannedCompletion": $filter('date')($scope.calender.selectedEndTime, 'MM/dd/yyyy HH:mm:ss'),
            "priority": $scope.calender.selectedPriority,
            "sTCEBUOpptyActivityType": $scope.calender.selectedType,
            "status": $scope.calender.selectedStatus,
            "id": "1234",
            "sTCCreatedBy": sharedDataService.getUserDetails().Id,
            "operation": "insert"
        };
        var calenderActivityAddPayload = Util.leadProcessPayload(headerData, '', '', '', '', '', '', actionData);
        var oppAddActivitySuccess = function (data) {
            if (data.data.errorMessage == 'Success') {
                pushEvents();
                MaterialReusables.showToast('' + pluginService.getTranslations().activityAdded, 'success');
                $scope.calenderViewMode = 'calender';
                $scope.cancel = false;
                angular.forEach(data.data.listOfStcOpportunity.opportunity[0].listOfAction.action, function (val, key) {
                    if (actionData.planned === val.planned) {
                        pushNewActvityToCalendar(val);
                    }
                });
                resetAllCalenderParams();
                //getAllActivities(moment());
                $scope.calender.showActivityAddLoader = false;
            } else {
                $scope.calender.showActivityAddLoader = false;
                MaterialReusables.showToast('' + pluginService.getTranslations().activityCreate_Failed, 'error');
            }
        };
        var oppAddActivityError = function (data) {
            $scope.calender.showActivityAddLoader = false;
            Util.throwError(data.data.Message, MaterialReusables);
        };
        var pushCreatedActivityToDB = function (params) {
            var gotOpptyDetails = function (dbDetails) {
                var payload = JSON.parse(dbDetails[0].payload);
                payload.Activity.create.push(params);
                var checkParameters = Util.convertObjectToSQL({ optyID: params.opportunityId });
                var valueParameters = Util.convertObjectToSQL({ payload: JSON.stringify(payload), toBeUpdated: 'true' });
                var failedToAddPayload = function () { };
                var payloadAdded = function () {
                    pushEvents();
                    $scope.calenderViewMode = 'calender';
                    $scope.cancel = false;
                    resetAllCalenderParams();
                    getAllActivities(moment());
                    $scope.calender.showActivityAddLoader = false;
                    MaterialReusables.showToast('' + pluginService.getTranslations().offlineActivitySuccess, 'success');
                };
                dbService.updateTable('stc_opptys', valueParameters, checkParameters, failedToAddPayload, payloadAdded);
            };
            var getOpptyByID = Util.convertObjectToSQL({ optyID: params.opportunityId });
            dbService.getDetailsFromTable('stc_opptys', getOpptyByID, angular.noop, gotOpptyDetails);
        };
        var postCreatedActivityOnline = function (payload) {
            apiService.fetchDataFromApi('opp.leadProcess', payload, oppAddActivitySuccess, oppAddActivityError);
        };
        if (!sharedDataService.getNetworkState()) {
            pushCreatedActivityToDB(calenderActivityAddPayload);
        } else {
            postCreatedActivityOnline(calenderActivityAddPayload);
        }
    };
    //activity create - input validation
    var validateCalenderActivityCreation = function () {
        var validation = '';
        if ($scope.calender.selectedType === '') {
            validation = '' + pluginService.getTranslations().enterActivityType;
        } else if ($scope.calender.selectedPriority === '') {
            validation = '' + pluginService.getTranslations().enterPriority;
        } else if ($scope.calender.selectedStatus === '') {
            validation = '' + pluginService.getTranslations().enterStatus;
        } else if ($scope.calender.selectedOpportunityName === '') {
            validation = '' + pluginService.getTranslations().enterOppName;
        }
        return validation;
    };
    //invoke create activity API call fun()
    $scope.createActivity = function () {
        var validationResult = validateCalenderActivityCreation();
        if (validationResult === '') {
            $scope.calender.showActivityAddLoader = true;
            createCalenderActivity();
        } else {
            MaterialReusables.showToast('' + validationResult, 'error');
        }
    };
    //date comparison
    $scope.compareDates = function (date, datetoCompareWith, prevEvent) {
        var dateCompareResult = false;
        if (datetoCompareWith || datetoCompareWith !== '') {
            dateCompareResult = Util.compareDate(moment(date), moment(datetoCompareWith));
            if (!dateCompareResult) {
                $scope.calender.selectedEndTime = '';
                MaterialReusables.showToast('' + pluginService.getTranslations().aDateGrtrThan + ' ' + prevEvent + ' ' + pluginService.getTranslations().Date, 'warning');
            }
        } else {
            $scope.calender.selectedEndTime = '';
            MaterialReusables.showToast('' + pluginService.getTranslations().pleaseEnter + ' ' + prevEvent + ' ' + pluginService.getTranslations().Date, 'warning');
        }
    };
    //fetch activity status LOV values
    var fetchActivityStatus = function () {
        var fetchActivityStatusOnline = function () {
            if (angular.isUndefined(sharedDataService.getActivityList())) {
                var langtype = '';
                if (sharedDataService.getLanguage() === 'en_US') {
                    langtype = 'ENU';
                } else {
                    langtype = 'ARA';
                }
                var activityStatusPayLoad = {
                    "busObjCacheSize": "",
                    "newQuery": "",
                    "outputIntObjectName": "CRMDesktopListOfValuesIO",
                    "pageSize": "100",
                    "searchSpec": "[List Of Values.Type]='EVENT_STATUS' AND [List Of Values.Language]='" + langtype + "' AND [List Of Values.Active]='Y' AND [List Of Values.Name] NOT LIKE 'Signed*'",
                    "sortSpec": "",
                    "startRowNum": "0",
                    "viewMode": ""
                }
                apiService.fetchDataFromApi('opp.searchcity', activityStatusPayLoad, activityStatusSuccess, activityStatusError);
            } else {
                $scope.calender.status = sharedDataService.getActivityList();
            }
        };
        var activityStatusSuccess = function (data) {
            if (data.data.listOfCRMDesktopListOfValuesIO != null) {
                angular.forEach(data.data.listOfCRMDesktopListOfValuesIO.listOfValues, function (val, key) {
                    $scope.calender.status.push({ "index": key, "name": val.name, "value": val.value });
                });
            }
            sharedDataService.saveActivityStatusList($scope.calender.status);
        };
        var activityStatusError = function (data) {
            Util.throwError(data.data.Message, MaterialReusables);
        };
        var gotActivityStatus = function (activityStatus) {
            if (activityStatus.length) {
                tempActivityList = [];
                angular.forEach(activityStatus, function (val, key) {
                    tempActivityList.push(JSON.parse(val.activities));
                });
                $scope.calender.status = tempActivityList;
                sharedDataService.saveActivityStatusList(tempActivityList);
            } else {
                fetchActivityStatusOnline();
            }
        };
        var failedToGetStatus = function () {
            fetchActivityStatusOnline();
        };
        dbService.getDetailsFromTable('stc_activityStatus', [], failedToGetStatus, gotActivityStatus);
    };
    fetchActivityStatus();
    //show opportunities popup
    $scope.searchOpportunities = function () {
        //$scope.calender.showOpportunities = true;
        MaterialReusables.showCustomPopup($scope,'app/shared/layout/opportunityListPopupTemplate.html');
    };
    //fetch opportunities
    $scope.fetchOpportunites = function () {
        var oppListSuccess = function (data) {
            $scope.calender.oppList = $scope.calender.oppList.concat(data.data.listOfStcOpportunity.opportunity);
            $scope.calender.isFetchingMoreOpps = false;
            $scope.calender.startRow = parseInt($scope.calender.startRow, 10) + 30;
        };
        var opplistFailed = function (data) {
            Util.throwError(data.data.Message, MaterialReusables);
        };
        var oppListPayload = Util.oppThinIoPayload("30", "[Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "'", "Created (DESCENDING)",
            $scope.calender.startRow);
        var fetchOpportunitesFromDB = function () {
            var gotOpportunity = function (opportunities) {
                angular.forEach(opportunities, function (val, key) {
                    $scope.calender.oppList.push(JSON.parse(val.opportunities));
                });
            };
            dbService.getDetailsFromTable('stc_opptys', [], angular.noop, gotOpportunity);
        };
        var fetchOpportunitesOnline = function () {
            apiService.fetchDataFromApi('opp.opportunitylistmyview', oppListPayload, oppListSuccess, opplistFailed);
        };
        if (!sharedDataService.getNetworkState()) {
            fetchOpportunitesFromDB();
        } else {
            fetchOpportunitesOnline();
        }
    };
    //loading more opportunities
    $scope.loadMoreOpportunities = function () {
        $scope.calender.isFetchingMoreOpps = true;
        $scope.fetchOpportunites($scope.calender.startRow);
    };
    //set selected opportunity to add activity form
    $scope.onOpportunityClick = function (name, rowId) {
        //$scope.calender.showOpportunities = false;
        MaterialReusables.hideDialog();
        $scope.calender.selectedOpportunityName = name;
        $scope.calender.selectedOppRowId = rowId;
    };
    $scope.closeActivityListView = function () {
    };
    //security ino\put validation
    $scope.descValidate = function (newValue) {
        $scope.calender.description = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
    };
    $scope.fetchOpportunites();
};
calenderModule.controller('calenderController', calenderController);
calenderController.$inject = ['$scope', '$state', '$filter', '$translate', 'sharedDataService', 'dbService', 'apiService', 'pluginService', 'MaterialReusables'];
