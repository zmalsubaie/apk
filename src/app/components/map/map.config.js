mapModule.config(['$stateProvider',function ($stateProvider) { 
    $stateProvider.state('root.map',{
            url: '/map',
            templateUrl: 'app/components/map/map.template.html',
            controller: 'mapController',
            pageTitle:'Map',
            backbutton:false,
            previousState:'root.home'
        });
    }
]);