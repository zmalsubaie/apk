//var base_url = 'http://94.97.1.209:9123'; //DEV VPN
//var base_url = 'http://10.23.50.8:9123'; // DEV Local
//var base_url = 'http://94.97.1.207:9113'; // TEST VPN
var base_url = 'http://10.23.50.3:9113'; // TEST Local
//var base_url = 'https://lms.stc.com.sa'; // Prod Public
var ApiUrl = {
    'authenticate': {
        'url': base_url + '/services/authentication',
        'method': 'POST'
    },
    'userdetails': {
        'url': base_url + '/services/userdetails',
        'method': 'GET'
    },
    'opp.list': {
        'url': base_url + '/services/opportunitylist',
        'method': 'POST',
        'encode': true
    },
    'opp.contactname': {
        'url': base_url + '/services/accountcontactnamequery',
        'method': 'POST',
        'encode': true
    },
    'opp.notesAddition': {
        'url': base_url + '/services/createnote0pportunity',
        'method': 'POST',
        'encode': true
    },
    'opp.noteUpdate': {
        'url': base_url + '/services/noteopportunityupdate',
        'method': 'POST',
        'encode': true
    },
    'opp.noteDelete': {
        'url': base_url + '/services/noteopportuntiydelete',
        'method': 'POST'
    },
    'opp.activityCreate': {
        'url': base_url + '/services/activitycreationopportunity',
        'method': 'POST',
        'encode': true
    },
    'opp.activityUpdate': {
        'url': base_url + '/services/activityupdateopportunity',
        'method': 'POST',
        'encode': true
    },
    'opp.activityDelete': {
        'url': base_url + '/services/activitydeleteopportunity',
        'method': 'POST'
    },
    'opp.productDelete': {
        'url': base_url + '/services/productdeleteopportunity',
        'method': 'POST'
    },
    'opp.getAttachment': {
        'url': base_url + '/services/opportunityattachmentapi',
        'method': 'POST'
    },
    'opp.account': {
        'url': base_url + '/services/accountlist',
        'method': 'POST',
        'encode': true
    },
    'opp.quoteList': {
        'url': base_url + '/services/queryquote',
        'method': 'POST'
    },
    'opp.attachmentDelete': {
        'url': base_url + '/services/deleteattachmentopportunity',
        'method': 'POST'
    },
    'opp.setUserLocation': {
        'url': base_url + '/services/mapopportunityupdatewithview',
        'method': 'POST'
    },
    'opp.searchcity': {
        'url': base_url + '/services/citylist',
        'method': 'POST',
        'encode': true
    },
    'opp.assignedToList': {
        'url': base_url + '/services/assignedtopositionquery',
        'method': 'POST',
        'encode': true
    },
    'opp.upsertAttachment': {
        'url': base_url + '/services/eaiupsertattachment',
        'method': 'POST',
        'encode': true
    },
    'opp.createopportunity': {
        'url': base_url + '/services/opportunitycreate',
        'method': 'POST',
        'encode': true
    },
    'opp.assigntoopportunity': {
        'url': base_url + '/services/assigntoopportunity',
        'method': 'POST',
        'encode': true
    },
    'opp.autoquotemobile': {
        'url': base_url + '/services/createquotemobile',
        'method': 'POST'
    },
    'opp.odscountbystatus': {
        'url': base_url + '/services/ods/countbystatus',
        'method': 'POST'
    },
    'opp.odscountbysubstatus': {
        'url': base_url + '/services/ods/countbysubstatus',
        'method': 'POST'
    },
    'opp.odsHighCount': {
        'url': base_url + '/services/ods/highcount',
        'method': 'POST'
    },
    'opp.odsMrc': {
        'url': base_url + '/services/ods/mrc',
        'method': 'POST'
    },
    'opp.odsTopThree': {
        'url': base_url + '/services/ods/top3',
        'method': 'POST'
    },
    'opp.odsTopFive': {
        'url': base_url + '/services/ods/top5',
        'method': 'POST',
        'encode': true
    },
    'map.allOpportunities': {
        'url': base_url + '/services/maplistallopportunities',
        'method': 'POST'
    },
    'opp.productCreate': {
        'url': base_url + '/services/productcreationopportunity',
        'method': 'POST',
        'encode': true
    },
    'opp.productList': {
        'url': base_url + '/services/productnamequery',
        'method': 'POST'
    },
    'opp.rateplanlist': {
        'url': base_url + '/services/rateplanquery',
        'method': 'POST',
        'encode': true
    },
    'opp.oppupdatenew': {
        'url': base_url + '/services/opportunityupdatedetails',
        'method': 'POST',
        'encode': true
    },
    'opp.odssubordinateslist': {
        'url': base_url + '/services/manangersubordinateslist',
        'method': 'POST'
    },
    'opp.odsoppcountofstatus': {
        'url': base_url + '/services/odsoppcountofstatus',
        'method': 'POST'
    },
    'opp.opplistformanager': {
        'url': base_url + '/services/odsopplistformanager',
        'method': 'POST',
        'encode': true
    },
    'opp.contactupdate': {
        'url': base_url + '/services/opportunity/contactupdate',
        'method': 'POST',
        'encode': true
    },
    'opp.getAllActivities': {
        'url': base_url + '/services/opportunityactionlist',
        'method': 'POST'
    },
    'opp.gsmproducts': {
        'url': base_url + '/services/gsmphonenumberquery',
        'method': 'POST'
    },
    'opp.autoquotewf': {
        'url': base_url + '/services/autoquotewf',
        'method': 'POST'
    },
    'opp.posQueryAssignedTo': {
        'url': base_url + '/services/positionqueryassignedto',
        'method': 'POST'
    },
    'opp.subStatusUser': {
        'url': base_url + '/services/normaluser/ods/countbysubstatus',
        'method': 'POST'
    },
    'opp.mrcUser': {
        'url': base_url + '/services/normaluser/ods/mrc',
        'method': 'POST'
    },
    'opp.highCountUser': {
        'url': base_url + '/services/normaluser/ods/highcount',
        'method': 'POST'
    },
    'opp.topThreeUser': {
        'url': base_url + '/services/normaluser/ods/top3',
        'method': 'POST'
    },
    'opp.topFiveUser': {
        'url': base_url + '/services/normaluser/ods/top5',
        'method': 'POST',
        'encode': true
    },
    'opp.oppListNormalUser': {
        'url': base_url + '/services/odsopplistformanager/normal',
        'method': 'POST',
        'encode': true
    },
    'opp.activePartnerUsers': {
        'url': base_url + '/services/activepartnerusers',
        'method': 'POST'
    },
    'opp.odsoppquerysearchformanager': {
        'url': base_url + '/services/odsoppquerysearchformanager',
        'method': 'POST',
        'encode': true
    },
    'opp.opportunitylistmyview': {
        'url': base_url + '/services/opportunitylistmyview',
        'method': 'POST'
    },
    'opp.userpositions': {
        'url': base_url + '/services/employeeposition',
        'method': 'POST'
    },
    'app.logout': {
        'url': base_url + '/services/authenticationinvalidate',
        'method': 'POST'
    },
    'app.upgrade': {
        'url': base_url + '/services/appupgrade',
        'method': 'POST'
    },
    'opp.highCountList': {
        'url': base_url + '/services/odssearchforopphighpriority/normaluser',
        'method': 'POST',
        'encode': true
    },
    'opp.highCountListManager': {
        'url': base_url + '/services/odssearchforopphighpriority/manager',
        'method': 'POST',
        'encode': true
    },
    'opp.TopThreeList': {
        'url': base_url + '/services/odssearchforopptop3prod/normaluser',
        'method': 'POST',
        'encode': true
    },
    'opp.TopThreeListManager': {
        'url': base_url + '/services/odssearchforopptop3prod/manager',
        'method': 'POST',
        'encode': true
    },
    'opp.mrclistformanager': {
        'url': base_url + '/services/odsopplistformanagerrevenue',
        'method': 'POST'
    },
    'opp.mrcListNormalUser': {
        'url': base_url + '/services/odsopplistfornormaluserrevenue',
        'method': 'POST',
        'encode': true
    },
    'opp.productWithRateplans': {
        'url': base_url + '/services/productrateplansearch',
        'method': 'POST'
    },
    'opp.activitiesList': {
        'url': base_url + '/services/listofactivities',
        'method': 'POST',
        'encode': true
    },
    'generateOTP': {
        'url': base_url + '/services/otp/generate',
        'method': 'GET'
    },
    'submitOTP': {
        'url': base_url + '/services/otp/validate?',
        'method': 'OTP',
        'params': ''
    },
    'opp.leadProcess':{
        'url':base_url+'/services/leadprocess', 
        'method':'POST',
        'encode': true
    },
    'opp.reportaction':{
        'url':base_url+'/services/reportaction', 
        'method':'POST'
    },
    'opp.saveUserPreferences':{
        'url':base_url+'/services/savepreferences', 
        'method':'POST'
    },
    'opp.fetchUserPreferences':{
        'url':base_url+'/services/fetchpreferences', 
        'method':'POST'
    },
    'location.update':{
        'url':base_url+'/services/updateLocation',
        'method':'PUT',
        'encode':true
    },
    'location.fetch':{
        'url':base_url+'/services/fetchlatlong',
        'method':'POST'
    },
    'user.image':{
        'url':base_url+'/services/userimage',
        'method':'POST'
    }
};
