(function() {
  'use strict';
	var apiService = function($http,$q,$base64,sharedDataService){
		var apiService = {};
		var requestPayload;
		apiService.triggerAppLogout = function(MaterialReusablesInstance,PunbubInstance,stateInstance){
			var resetPreferences = function () {
				sharedDataService.saveAssignedToList([]);
				sharedDataService.saveAssigneeToRowNum(0);
				sharedDataService.saveviewSelected(null);
				sharedDataService.saveIsManager(false);
				sharedDataService.saveSubordinateList([]);
			};
			var logOutSuccess = function () {
				MaterialReusablesInstance.hideDialog();
				PunbubInstance.unSubscribeChat();
				stateInstance.go('login');
				resetPreferences();
			};
			var logOutFailed = function (data) {
				MaterialReusablesInstance.hideDialog();
				stateInstance.go('login');
			};
			var logOutObj = {
				'payload': {
					"userId": x_access_user,
					"token": x_access_token
				}
			};
			apiService.fetchDataFromApi('app.logout', logOutObj, logOutSuccess, logOutFailed);
		};
		apiService.fetchDataFromApi = function (model, condition, successCallback, errorCallback) {
			var apiSuccessCallback = function (data, status, header) {
				successCallback(data, 'api');
			};
			var apiErrorCallback = function (data, status, header) {
				if(data.status === 503) {
					data.data = '';
					data.data = {'Message': data.statusText, 'Code': data.status};
				}
				errorCallback(data, status, header);
			};
			var initiateApiCall = function(){
                if(condition.payload) { requestPayload = condition.payload; }
				else { requestPayload = condition; }
				console.log(ApiUrl[model].url+"\n\n"+ JSON.stringify(requestPayload)+"\n\n");
				if(angular.isDefined(ApiUrl[model])){
					var payloadtoSent;
					headerData.headers['x-access-user'] = x_access_user;
					headerData.headers['x-access-token'] = x_access_token;
					if(ApiUrl[model].method === 'POST'){
						if(model === 'authenticate'){
                           payloadtoSent = Util.payloadFormatter(requestPayload,loginSecKey);
						}else if(model === 'app.upgrade'){
							payloadtoSent = requestPayload;
						}else{
							if(ApiUrl[model].encode){
                               requestPayload = $base64.encode(unescape(encodeURIComponent(JSON.stringify(requestPayload))));
							}
							payloadtoSent = Util.payloadFormatter(requestPayload,encryptKey);
						}
						Service.doPost($http,ApiUrl[model].url,payloadtoSent,apiSuccessCallback,apiErrorCallback,headerData);
					}else if(ApiUrl[model].method === 'OTP'){
						otp_header_data.headers['x-access-user'] = x_access_user;
						otp_header_data.headers['x-access-token'] = x_access_token;
						otp_header_data.params['token'] = condition.token;
						Service.doGet($http,ApiUrl[model].url,apiSuccessCallback,apiErrorCallback,otp_header_data);
					}else if(ApiUrl[model].method === 'GET'){
						Service.doGet($http,ApiUrl[model].url,apiSuccessCallback,apiErrorCallback,headerData);
					}else if(ApiUrl[model].method === 'PUT'){
						if(ApiUrl[model].encode){
							requestPayload = $base64.encode(unescape(encodeURIComponent(JSON.stringify(requestPayload))));
						 }
						 payloadtoSent = Util.payloadFormatter(requestPayload,encryptKey);
						Service.doPut($http,ApiUrl[model].url,payloadtoSent,apiSuccessCallback,apiErrorCallback,headerData);
					}       
				}
			};
			var sslSuccess = function(){
              initiateApiCall();
			};
			var sslError = function(msg){
			   var sslErrorData = { data:{ Message:msg } };	
               apiErrorCallback(sslErrorData);
			};
			if(device.platform === 'browser'){
               initiateApiCall();
			}else{
			   if(sharedDataService.getNetworkState()){
				Util.pluginServiceScope.checkifValidSSLCertificate(sslSuccess,sslError);
			   }	
			}
		};
		return apiService;
	};
	stcApp.service('apiService',apiService);
	apiService.$inject = ['$http','$q','$base64','sharedDataService'];
})();