(function() {
  'use strict';
    var scrollToTopWhen = function($timeout){
      function link (scope, element, attrs) {
        scope.$on(attrs.scrollToTopWhen, function () {
          $timeout(function () {
            angular.element(element)[0].scrollTop = 0;
          });
        });
      }
    };
    stcApp.directive('scrollToTopWhen',scrollToTopWhen);
    scrollToTopWhen.$inject = ['$timeout'];
})();