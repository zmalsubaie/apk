(function () {
    'use strict';
    var pluginService = function ($compile,$rootScope,$state,$stateParams, $translate, $timeout, $interval, sharedDataService, dbService, apiService, PubnubService) {
        var pluginService = {};
        var translations = {};
        var locationTrackerMarkers = [];
        var clustermap,locationWatchID;
        pluginService.openCamera = function (success, failed, editState, type) {
            var onSuccess = function (imageURI) { success(imageURI); };
            var onFail = function (message) { failed(message); };
            navigator.camera.getPicture(onSuccess, onFail, {
                quality: type !== 'profile' ? 50 : 80,
                // quality:50,
                destinationType: Camera.DestinationType.DATA_URL,
                allowEdit: editState,
                correctOrientation: true,
                targetWidth: type !== 'profile' ? 1024 : 256,
                targetHeight: type !== 'profile' ? 768 : 192
                // targetWidth:1024,
                // targetHeight:768
            });
        };
        pluginService.initializeClusterofMarkers = function(corrdinatesToMap,action,infoWindowCallBack,partnerFilterCallBack,MaterialReusables){
            var fetchedCoordinates = corrdinatesToMap;
            var clusterData = function(){
               var locationDetails = [];
               var partnerFilters = ['All'];
               angular.forEach(fetchedCoordinates,function(val,key){
                    if(val.partnerId === null){
                        val.partnerId = 'SME';
                    }
                    if(val.userId !== sharedDataService.getUserDetails().loginName){
                        locationDetails.push({
                            'position': {'lat': val.latitude,'lng': val.longitude },
                            'name': val.userId,
                            'phone':val.cellularPhone,
                            'timestamp':val.timestamp,
                            'partnerOrg':val.partnerId
                        });
                    }
                    if(val.partnerId!=='SME'){
                        if(partnerFilters.indexOf(val.partnerId) === -1){
                            partnerFilters.push(val.partnerId);
                        }
                    }  
               });
               partnerFilterCallBack(partnerFilters);
               return locationDetails;
            };
            var addClusterMarkers = function(data,callback){
                angular.forEach(data, function (val, key) {
                    var onMarkerAdded = function (marker) {
                        if(val.phone === null){val.phone = '';}
                        marker.set('params',{id:val.name,phNo:val.phone,updatedTime:val.timestamp});
                        marker.setIcon({
                            'url': "www/assets/img/marker_set/"+val.partnerOrg+".png",
                            'size': { width: Math.round(35), height: Math.round(45) }
                        });
                        marker.on(plugin.google.maps.event.MARKER_CLICK, function (position, marker) {
                            var onlineUsers = function(users){
                                var chatStatus = 'mapOffline';
                                if(users.uuids.indexOf(marker.get('params').id) >- 1){
                                    chatStatus = 'mapOnline';
                                }
                                var infoWindow = new plugin.google.maps.HtmlInfoWindow();
                                var infoDetails =
                                     '<div class="'+chatStatus+'"></div>'+
                                     '<span class="mapTrackerName">'+marker.get('params').id+'</span><br/>'+
                                     '<span class="mapTrackerTime"> '+pluginService.getTranslations().lSeen+' : '+moment(marker.get('params').updatedTime).format("DD/MM/YYYY HH:mm:ss")+'</span>';
                                if(marker.get('params').phNo!==''){
                                    infoDetails = infoDetails + '<br/><div id="mapcall" class="mapTrackerCallDiv"><img src="assets/img/map_incall.svg" height="35" width="35"/><span class="mapTrackerOptions">'+marker.get('params').phNo+'</span></div>';
                                }
                                if(chatStatus === 'mapOnline'){
                                    infoDetails = infoDetails + '<div id="mapchat" class="mapTrackerChatDiv"><img src="assets/img/map_inchat.svg" height="25" width="25" style="margin-left: 10px;"/><span class="mapTrackerOptions"> '+pluginService.getTranslations().cWith+' '+marker.get('params').id+'</span></div>';
                                }
                                infoWindow.setContent(infoDetails);
                                infoWindow.open(marker);
                                infoWindowCallBack(infoWindow);
                                $timeout(function(){
                                    if(marker.get('params').phNo!==''){
                                        document.getElementById("mapcall").addEventListener ("click", function(){
                                            pluginService.dialNumber(marker.get('params').phNo,angular.noop,angular.noop);
                                        }, true);
                                    }
                                    if(chatStatus === 'mapOnline'){
                                        document.getElementById("mapchat").addEventListener ("click", function(){
                                            var onliners = function(userList){
                                               if(userList.uuids.indexOf(marker.get('params').id) > -1){
                                                $stateParams.id = marker.get('params').id;
                                                $stateParams.subscribeto = '';
                                                MaterialReusables.showInMapChatWindow();
                                               }
                                            }; 
                                            PubnubService.listOnlineUsers(onliners);
                                        }, true);
                                    }
                                },1000);
                            };
                            PubnubService.listOnlineUsers(onlineUsers);
                        });
                        locationTrackerMarkers.push(marker);
                        if (locationTrackerMarkers.length === data.length) {
                            callback(locationTrackerMarkers); 
                        }
                    };
                    clustermap.addMarker({
                        'position': { lat: val.position.lat, lng: val.position.lng },
                        'styles': {
                            'font-weight': 'bold',
                            'color': 'rgb(85, 16, 144)'
                        },
                        'optimized': false
                    }, onMarkerAdded);
                });
            };
            var onClusterMapReady = function(){
                clustermap.animateCamera({
                    'target': { 'lat': clusterData()[0].position.lat, 'lng': clusterData()[0].position.lng },
                    'zoom': 8,
                    'tilt': 60,
                    'bearing': 0
                },function (){
                    addClusterMarkers(clusterData(), function (markers) {});
                });
            };
            if(action === 'create'){
                plugin.google.maps.Map.getMap(document.getElementById("map_canvas")).remove();
                plugin.google.maps.Map.getMap(document.getElementById("opp_map")).remove();
                var clusterMapDiv = document.getElementById('trackerview_canvas');
                clustermap = plugin.google.maps.Map.getMap(clusterMapDiv);
                clustermap.on(plugin.google.maps.event.MAP_READY, onClusterMapReady);
            }else if(action === 'refresh'){
                var markerUpdates = clusterData();
                angular.forEach(locationTrackerMarkers,function(markerVal,markerKey){
                   angular.forEach(markerUpdates,function(coordsVal,coordsKey){
                      if(coordsVal.name === markerVal.get('params').id){
                        locationTrackerMarkers[markerKey].setPosition(new plugin.google.maps.LatLng(coordsVal.position.lat,coordsVal.position.lng));
                        locationTrackerMarkers[markerKey].set('params',{id:coordsVal.name,phNo:coordsVal.phone,updatedTime:coordsVal.timestamp});
                        markerUpdates.splice(coordsKey,1);
                      }
                   });
                });
                if(markerUpdates.length){
                    addClusterMarkers(markerUpdates, function (markers) {});
                }
            }else if(action === 'filter'){
                clustermap.clear()
                addClusterMarkers(clusterData(),function(markers){});
            }
        };
        pluginService.initializeMap = function (coordinates, mapVal, mapInstance, dragProp, mapMarkerLocation, clickedMarkerId) {
            var markers = [];
            var map, instance;
            var checkIfValue = function (value) {
                var val = '';
                if (value) { val = value; }
                else { val = ' - '; }
                return val;
            };
            var checkAuthorizedNumber = function (obj) {
                var mobNo = '';
                if (obj.sTCFeedback === 'Authorized person') {
                    mobNo = checkIfValue(obj.sTCContactNumber);
                } else if (obj.sTCFeedback2 === 'Authorized person') {
                    mobNo = checkIfValue(obj.sTCContactNumber2);
                } else if (obj.sTCFeedback3 === 'Authorized person') {
                    mobNo = checkIfValue(obj.sTCContactNumber3);
                } else {
                    mobNo = ' - ';
                }
                return mobNo;
            };
            var getServicetypes = function (obj) {
                var services = [];
                if (obj.ListOfOpportunityProduct) {
                    angular.forEach(obj.ListOfOpportunityProduct.OpportunityProduct, function (val, key) {
                        if (services.indexOf(val.sTCServiceType2) === -1) {
                            services.push('' + val.sTCServiceType2);
                        }
                    });
                } else {
                    services = ' - ';
                }
                return services;
            };
            if (mapInstance === 'opp_map') {
                instance = plugin.google.maps.Map.getMap(document.getElementById("map_canvas"));
            } else {
                instance = plugin.google.maps.Map.getMap(document.getElementById("opp_map"));
            }
            instance.remove();
            var coordinatesToShow = [];
            var onMapReady = function () {
                if (mapVal === 'multiple') {
                    angular.forEach(coordinates, function (val, key) {
                        coordinatesToShow.push({
                            'title': val.name, 'snippet': '' + pluginService.getTranslations().Lead + ' # : ' + val.sTCLeadNumber + ' '
                            + '\n' + pluginService.getTranslations().Account + ' : ' + checkIfValue(val.account) + ' ' + '\n' + pluginService.getTranslations().Status + ' : ' + checkIfValue(val.salesStage) + ' '
                            + '\n' + pluginService.getTranslations().authorizedName + ' : ' + checkIfValue(val.sTCBulkLeadContactFirstName) + ' '
                            + '\n' + pluginService.getTranslations().Mobile + ' : ' + checkAuthorizedNumber(val) + ' ' + '\n' + pluginService.getTranslations().serviceName + ' : ' + getServicetypes(val),
                            'position': { lat: val.decimalLatitude, lng: val.decimalLongitude }
                        });
                    });
                } else {
                    coordinatesToShow = [{ 'title': coordinates[0].name, 'snippet': '' + coordinates[0].sTCLeadNumber, 'position': { lat: coordinates[0].decimalLatitude, lng: coordinates[0].decimalLongitude } }];
                }
                var addMarkers = function (data, callback) {
                    var markers = [];
                    var onMarkerAdded = function (marker) {
                        marker.setIcon({
                            'url': 'www/assets/img/map_icon/ellipse7@2x.png',
                            'size': { width: Math.round(25), height: Math.round(40) }
                        });
                        marker.setDraggable(dragProp);
                        marker.showInfoWindow();
                        markers.push(marker);
                        marker.addEventListener(plugin.google.maps.event.INFO_CLICK, function () {
                            var clickedCoorinate = {}
                            var clickedTitle = this.getTitle();
                            var rowId = '';
                            var rowdetails = '';
                            this.getPosition(function (coordinate) {
                                clickedCoorinate = coordinate;
                            });
                            angular.forEach(coordinates, function (val, key) {
                                if (val.name === clickedTitle) {
                                    rowId = val.rowId;
                                    rowdetails = val;
                                }
                            });
                            clickedMarkerId(rowId, rowdetails);
                        });
                        if (markers.length === data.length) { callback(markers); }
                    }
                    angular.forEach(data, function (val, key) {
                        map.addMarker({
                            'position': { lat: val.position.lat, lng: val.position.lng },
                            'title': [val.title].join('\n'),
                            'snippet': val.snippet,
                            'styles': {
                                'font-weight': 'bold',
                                'color': 'rgb(85, 16, 144)'
                            },
                            'optimized': false
                        }, onMarkerAdded);
                    });
                };
                var placeMultipleMarkers = function () {
                    addMarkers(coordinatesToShow, function (markers) {
                        markers[markers.length - 1].showInfoWindow();
                    });
                };
                var placeSingleMarker = function () {
                    map.addMarker({
                        'position': { lat: coordinatesToShow[0].position.lat, lng: coordinatesToShow[0].position.lng },
                        'title': [coordinatesToShow[0].title].join('\n'),
                        'snippet': coordinatesToShow[0].snippet,
                        'optimized': false
                    }, function (marker) {
                        marker.setIcon({
                            'url': 'www/assets/img/map_icon/ellipse7@2x.png',
                            'size': { width: Math.round(25), height: Math.round(40) }
                        });
                        marker.setDraggable(dragProp);
                        marker.showInfoWindow();
                        markers.push(marker);
                        marker.addEventListener(plugin.google.maps.event.MARKER_DRAG_END, function (marker) {
                            marker.getPosition(function (latLng) {
                                mapMarkerLocation(latLng);
                            });
                        });
                    });
                };
                map.animateCamera({
                    'target': { 'lat': coordinatesToShow[0].position.lat, 'lng': coordinatesToShow[0].position.lng },
                    'zoom': 8,
                    'tilt': 60,
                    'bearing': 0
                },function () {
                    if (mapVal === 'multiple') { placeMultipleMarkers(); }
                    else { placeSingleMarker(); }
                });
            };
            document.addEventListener("deviceready", function () {
                var mapDiv = document.getElementById(mapInstance);
                map = plugin.google.maps.Map.getMap(mapDiv);
                map.on(plugin.google.maps.event.MAP_READY, onMapReady);
            });
        };
        pluginService.getOCRDetails = function (http, imageURI, success, failed) {
            var req = {
                method: 'POST',
                url: 'https://api.ocr.space/parse/image',
                headers: { 'Content-type': 'application/x-www-form-urlencoded' },
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    'base64image': 'data:image/png;base64,' + imageURI,
                    'language': 'eng',
                    'apikey': '2763e1539f88957',
                    'isOverlayRequired': 'false'
                }
            };
            http(req).then(function (data) {
                success(data);
            }, function (error) {
                failed(error)
            });
        };
        pluginService.saveAttachment = function (fileName, extension, fileContent, fileType, successCallBack, errorCallBack, progressCallBack) {
            var rootdirectory = pluginService.getRootDirectory();
            var mimeType = '';
            angular.forEach(mimeTypes, function (val, key) {
                if (extension === key || extension.toLowerCase() === key) {
                    mimeType = val;
                }
            });
            if (device.platform !== 'iOS' && device.platform !== 'windows') {
                cordova.plugins.diagnostic.requestRuntimePermission(function (success) { }, function (error) { }, 'READ_EXTERNAL_STORAGE');
            }
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
                var fileTransfer = new FileTransfer();
                var uri, fileURL;
                if (mimeType === '') {
                    uri = fileContent;
                    fileURL = rootdirectory + 'LMSApp.apk';
                } else {
                    uri = encodeURI('data:' + mimeType + ';base64,' + fileContent)
                    fileURL = rootdirectory + fileName.replace(/ /g, '%20');
                }
                fileTransfer.download(uri, fileURL, function (entry) {
                    successCallBack(entry);
                }, function (error) {
                    errorCallBack(error);
                }, false, {});
                var progressCalculator = function (progressEvent) {
                    $timeout(function () {
                        var percentageDownloaded = (progressEvent.loaded / progressEvent.total * 100).toString();
                        progressCallBack(percentageDownloaded.slice(0, percentageDownloaded.indexOf(".")));
                    });
                };
                fileTransfer.onprogress = progressCalculator;
            }, function () { });
        };
        pluginService.viewAttachment = function (fileName, extension, successCallBack, errorCallBack) {
            var mimeType = '';
            angular.forEach(mimeTypes, function (val, key) {
                if (extension === key || extension.toLowerCase() === key) {
                    mimeType = val;
                }
            });
            var rootDirectory = pluginService.getRootDirectory();
            if (device.platform === 'iOS') {
                window.plugins.Base64.encodeFile(rootDirectory + fileName + '.' + extension, function (base64) {
                    var fileView = window.open(base64, '_blank', 'location=no,clearsessioncache=yes,clearcache=yes,EnableViewPortScale=no');
                });
            } else if (device.platform === 'windows') {
                var gotFileEntry = function (fileEntry) {
                    fileEntry.file(function (file) {
                        var reader = new FileReader();
                        reader.onloadend = function (evt) {
                            var fileView = window.open(evt.target.result, '_blank', 'location=no,clearsessioncache=yes,clearcache=yes,EnableViewPortScale=no');
                        };
                        reader.readAsDataURL(file);
                    }, onFail);
                };
                var onFail = function (msg) {
                    errorCallBack(rootDirectory, '');
                };
                window.resolveLocalFileSystemURL(rootDirectory + fileName + '.' + extension, gotFileEntry, onFail);
            } else {
                cordova.plugins.fileOpener2.open(rootDirectory + fileName + '.' + extension, mimeType, {
                    error: function (error) { errorCallBack(rootDirectory, error); }, success: function () { successCallBack(); }
                });
            }
        };
        pluginService.getRootDirectory = function () {
            var directory = '';
            if (device.platform === 'iOS') { directory = cordova.file.documentsDirectory + '.STC/'; }
            else if (device.platform === 'windows') { directory = cordova.file.tempDirectory + './STC'; }
            else { directory = cordova.file.externalDataDirectory + '.STC/'; }
            return directory;
        };
        pluginService.getImagesFromGallery = function (successCallBack, errorCallBack) {
            var filename = '';
            var extension = '';
            var onPhotoURISuccess = function (imageUri) {
                var gotFileEntry = function (fileEntry) {
                    fileEntry.file(function (file) {
                        var reader = new FileReader();
                        reader.onloadend = function (evt) {
                            var fileProperty = evt.target._localURL.split('/').pop();
                            successCallBack(evt.target.result, fileProperty, fileProperty.split('.').pop().trim());
                        };
                        reader.readAsDataURL(file);
                    }, onFail);
                };
                window.resolveLocalFileSystemURL(imageUri, gotFileEntry, onFail);
            };
            var onFail = function (msg) {
                successCallBack('', '', '');
            };
            navigator.camera.getPicture(onPhotoURISuccess, onFail, {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                correctOrientation: true,
                targetWidth: 1024,
                targetHeight: 768
            });
        };
        pluginService.getCurrentPosition = function (successCallBack, errorCallBack) {
            var positionID = '';
            function onSuccess(position) {
                navigator.geolocation.clearWatch(positionID);
                successCallBack(position);
            }
            function onError(error) {
                navigator.geolocation.clearWatch(positionID);
                errorCallBack(error);
            }
            positionID = navigator.geolocation.getCurrentPosition(onSuccess, onError, { maximumAge: Infinity, timeout: 30000, enableHighAccuracy: false });
        };
        pluginService.watchUserPosition = function(){
            var referencePosition = {coords:{latitude:0.0,longitude:0.0}};
            var watchOnSuccess = function(position){
              if(referencePosition.coords.latitude === 0.0 &&
                  referencePosition.coords.longitude === 0.0){
                    referencePosition.coords = position.coords;
                    referencePosition.coords = position.coords;
              }else{
                  //if(Util.getDistanceFromLatLonInMeters(referencePosition.coords,position.coords)>=0){
                      referencePosition.coords = position.coords;
                      var presentDate = new Date();
                      var timeStamp = presentDate.getFullYear()+'-'+Util.getFormattedDateTimeMonth(presentDate.getMonth())+'-'+Util.getFormattedDateTimeMonth(presentDate.getDate())+'T'+Util.getFormattedDateTimeMonth(presentDate.getHours())+':'+Util.getFormattedDateTimeMonth(presentDate.getMinutes())+':'+Util.getFormattedDateTimeMonth(presentDate.getSeconds());
                      var updateCoordsPayload = {
                        'userId'   : sharedDataService.getUserDetails().loginName,
                        'latitude' : referencePosition.coords.latitude,
                        'longitude': referencePosition.coords.longitude,
                        'timeStamp': timeStamp,
                        'cellularPhone':sharedDataService.getUserDetails().cellularPhone
                       };
                      apiService.fetchDataFromApi('location.update',updateCoordsPayload, angular.noop,angular.noop);
                  //}  
              }
            };
            var watchOnError = function(watchError){};
            locationWatchID = navigator.geolocation.watchPosition(watchOnSuccess, watchOnError, { maximumAge: Infinity, timeout: 30000, enableHighAccuracy: false })
        };
        pluginService.unregisterUserPositionWatch = function(){
            navigator.geolocation.clearWatch(locationWatchID);
        };
        pluginService.isLocationEnabled = function (successCallBack, errorCallBack) {
            cordova.plugins.diagnostic.isLocationEnabled(function (result) {
                successCallBack(result);
            }, function (error) {
                errorCallBack(error);
            });
        };
        pluginService.clearAllNotifications = function () {
            cordova.plugins.notification.local.clearAll(function () { }, this);
            cordova.plugins.notification.local.cancelAll(function () { }, this);
        };
        pluginService.createMessageNotification = function(message,senderName){
            var messagetoShow = [{id:senderName,badge:1,at:new Date(),title:'New Message',text:'You have a new message from '+senderName}];
            cordova.plugins.notification.local.schedule(messagetoShow);
        };
        pluginService.createLocalNotification = function (eventsList) {
            var events = [];
            var getDesc = function (desc) {
                var description = '';
                if (angular.isUndefined(desc) || desc === null || desc === '') {
                    description = '' + pluginService.getTranslations().noDesc;
                } else {
                    description = desc;
                }
                return description;
            };
            var gotNotificationTime = function (data) {
                angular.forEach(eventsList, function (eventVal, eventKey) {
                    if (eventVal !== null) {
                        if (!(eventVal.done === 'Y' || eventVal.done === true)) {
                            if (eventVal.alarm === 'Y' || eventVal.alarm === true) {
                                var presentDate = new Date();
                                var eventTime = new Date(eventVal.planned);
                                var eventEndTime = new Date(eventVal.plannedCompletion);
                                var eventDetails = { 'text': '', 'time': '', 'date': '', 'eventAlertTime': '' };
                                if (presentDate <= eventTime) {
                                    eventDetails.text = getDesc(eventVal.description2);
                                    if (eventTime.getDay() !== eventEndTime.getDay() && !isNaN(eventEndTime.getDay())) {
                                        eventDetails.time = days[eventTime.getDay()] + ' ' + eventTime.getHours() + ':' + eventTime.getMinutes() + ' ' + pluginService.getTranslations().to + ' ' + days[eventEndTime.getDay()] + ' ' + eventEndTime.getHours() + ':' + eventEndTime.getMinutes();
                                    } else if (isNaN(eventEndTime.getDay())) {
                                        eventDetails.time = days[eventTime.getDay()] + ' ' + eventTime.getHours() + ':' + eventTime.getMinutes() + ' ' + pluginService.getTranslations().to + ' ' + '-';
                                    } else {
                                        eventDetails.time = days[eventTime.getDay()] + ' ' + eventTime.getHours() + ':' + eventTime.getMinutes() + ' ' + pluginService.getTranslations().to + ' ' + eventEndTime.getHours() + ':' + eventEndTime.getMinutes();
                                    }
                                    eventDetails.date = eventTime.getDate() + Util.getDateOrdinals(eventTime.getDate()) + ' ' + months[eventTime.getMonth()] + ' ' + eventTime.getFullYear();
                                    var eventDescription = eventDetails.date + "\n" + eventDetails.time + "\n" + eventDetails.text;
                                    if (Util.diff_minutes(eventTime, presentDate) > data[0].notificationTime) {
                                        eventTime.setMinutes(eventTime.getMinutes() - data[0].notificationTime);
                                        events.push({ id: eventTime, badge: 1, at: eventTime, title: pluginService.getTranslations().youHaveA + ' ' + eventVal.sTCEBUOpptyActivityType + ' ' + pluginService.getTranslations().in + ' ' + data[0].notificationTime + ' ' + pluginService.getTranslations().minutes, text: eventDescription });
                                    } else {
                                        var notificateTime = Util.diff_minutes(eventTime, presentDate);
                                        eventTime.setMinutes(eventTime.getMinutes() - presentDate.getMinutes());
                                        events.push({ id: eventTime, badge: 1, at: eventTime, title: pluginService.getTranslations().youHaveA + ' ' + eventVal.sTCEBUOpptyActivityType + ' ' + pluginService.getTranslations().in + ' ' + notificateTime + ' ' + pluginService.getTranslations().minutes, text: eventDescription });
                                    }

                                }
                            }
                        }
                    }
                });
                $timeout(function () {
                    cordova.plugins.notification.local.schedule(events);
                });
            };
            if (device.platform === 'windows') {
                var notifyTime = localStorage.getItem('stc_notificationTime');
                gotNotificationTime([{ notificationTime: notifyTime }]);
            } else {
                dbService.getDetailsFromTable('stc_notificationTime', [], angular.noop, gotNotificationTime);
            }
        };
        pluginService.listenToAllNotifications = function (MaterialReusables) {
            var handleOnNotificationEvents = function (id, enableRoute, toRoute) {
                cordova.plugins.notification.local.clear(id, function () { });
                cordova.plugins.notification.local.cancel(id, function () { });
                MaterialReusables.setDialogState(false);
                if (enableRoute) { $state.go(toRoute); }
            };
            cordova.plugins.notification.local.on('trigger', function (notification, state) {
                if (sharedDataService.getAppState() !== 'Logged out') {
                    if(notification.title === 'New Message'){
                        var sender = notification.id;
                        cordova.plugins.notification.local.clear(notification.id, function () { });
                        cordova.plugins.notification.local.cancel(notification.id, function () { });
                        $state.go('root.chat',{id:sender,subscribeto:''});
                    }else{
                        var popupCallBack = function (result) {
                            if (result) {
                                handleOnNotificationEvents(notification.id, true, 'root.calender');
                            } else {
                                handleOnNotificationEvents(notification.id, false, $state.current.name);
                            }
                        };
                        notification.text = '<span>' + notification.text.substr(0, notification.text.indexOf('\n')) + '</span><br><span>' + notification.text.substring(notification.text.indexOf('\n') + 1, notification.text.indexOf('\n', notification.text.indexOf('\n') + 1)) + '</span><br><span>' + notification.text.split("\n").pop() + '</span>'
                        MaterialReusables.showAlarmPopup(notification, popupCallBack);
                    } 
                }
            }, this);
            cordova.plugins.notification.local.on('click', function (notification, state) {
                if (sharedDataService.getAppState() === 'Logged out') {
                    handleOnNotificationEvents(notification.id, true, 'login');
                } else {
                    handleOnNotificationEvents(notification.id, true, 'root.calender');
                }
            }, this);
        };
        pluginService.dialNumber = function (tel, successCallBack, errorCallBack) {
            if (device.platform === 'windows') {
                window.open('tel:' + tel, '_system');
            } else {
                var dialerSuccess = function (success) {
                    successCallBack(success);
                };
                var dialerError = function (error) {
                    errorCallBack(error);
                };
                window.plugins.CallNumber.callNumber(dialerSuccess, dialerError, tel, true);
            }
        };
        pluginService.checkifValidSSLCertificate = function (successCallBack, errorCallBack) {
            var sslSuccess = function (message) {
                successCallBack(message);
            };
            var sslFaiure = function (message) {
                errorCallBack(message);
            };
            window.plugins.sslCertificateChecker.check(sslSuccess, sslFaiure, sslCertificateUrl, sslCertificateFingerPrint);
        };
        pluginService.handleAppUpgrade = function (upgradeUrl, upgradeSuccessCallBack, upgradeErrorCallBack, upgradeProgressCallBack) {
            if (device.platform === 'iOS') {
                var progressMeter = 0;
                var upgradeView = window.open(upgradeUrl, '_blank', 'location=no,clearsessioncache=yes,clearcache=yes,hidden=yes');
                upgradeView.addEventListener('loadstop', function (event) {
                    var clickDownload = "var click=document.getElementsByClassName('download');click[0].click();"
                    upgradeView.executeScript({ code: clickDownload }, function (values) { });
                    var progressInterval = $interval(function () {
                        if (progressMeter > 95 && progressMeter <= 100) {
                            progressMeter = progressMeter + 1;
                        } else if (progressMeter >= 70 && progressMeter <= 95) {
                            progressMeter = progressMeter + 5;
                        } else {
                            progressMeter = progressMeter + 10;
                        }
                        if (progressMeter <= 100) {
                            upgradeProgressCallBack(progressMeter.toString());
                        } else {
                            $interval.cancel(progressInterval);
                        }
                    }, 2000);
                });
            } else {
                var downloadLink = upgradeUrl.replace('install.html', '');
                var downloadFailed = function (data) {
                    upgradeErrorCallBack(data)
                };
                var downloadSuccess = function (data) {
                    cordova.plugins.fileOpener2.open(
                        pluginService.getRootDirectory() + 'LMSApp.apk',
                        'application/vnd.android.package-archive', {
                            error: function (error) { upgradeErrorCallBack(error); },
                            success: function () {
                                upgradeSuccessCallBack(data);
                            }
                        });
                };
                pluginService.saveAttachment('', '', downloadLink + 'LMSApp.apk', '', downloadSuccess, downloadFailed, upgradeProgressCallBack);
            }
        };
        pluginService.setTranslations = function (data) {
            translations = data;
        };
        pluginService.getTranslations = function () {
            return translations;
        };
        pluginService.translationsArray = function () {
            return translationsKey;
        };
        pluginService.getAppVersion = function () {
            var appVersion;
            if (device.platform === 'windows') {
                cordova.getAppVersion.getVersionNumber(function (version) {
                    appVersion = version;
                });
            } else {
                appVersion = AppVersion.version;
            }
            return appVersion;
        };
        pluginService.isSuperUserDevice = function (success, error) {
            if (device.platform === 'iOS') {
                jailbreakdetection.isJailbroken(success, error);
            } else {
                rootdetection.isDeviceRooted(success, error);
            }
        };
        pluginService.initBackGroundService = function () {
            if (device.platform === 'iOS') {
                window.BackgroundFetch.start(angular.noop, angular.noop);
            } else {
                window.BackgroundService.start(angular.noop, angular.noop);
            }
        };
        pluginService.listenToIncomingMessages = function () {
            var options = {
                delimiter: "B2B OTP:",
                length: 5,
                origin: "STC-IT-Sec"
            };
            var success = function (otp) {
                $rootScope.$broadcast('otpDetect', { code: otp });
                OTPAutoVerification.stopOTPListener();
            };
            var failure = function () {
                OTPAutoVerification.stopOTPListener();
            };
            OTPAutoVerification.startOTPListener(options, success, failure);
        };
        pluginService.stopListener = function () {
            OTPAutoVerification.stopOTPListener();
        };
        pluginService.isfingerPrintAvailable = function (fingerPrintSuccess,fingerPrintFailed) {
            var isAvailableSuccess = function (result) {
                var successCallback = function (data) {
                    fingerPrintSuccess(data); 
                };
                var errorCallback = function (err) {
                    fingerPrintFailed(err);
                };
                Fingerprint.show({
                    clientId: "STC WINNER",
                    clientSecret: "STC WINNER"
                }, successCallback, errorCallback);
            };
            var isAvailableError = function (message) {};
            Fingerprint.isAvailable(isAvailableSuccess, isAvailableError);  
        };
        return pluginService;
    };
    stcApp.service('pluginService', pluginService);
    pluginService.$inject = ['$compile','$rootScope', '$state','$stateParams','$translate', '$timeout', '$interval', 'sharedDataService', 'dbService','apiService','PubnubService'];
})();