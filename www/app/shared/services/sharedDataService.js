(function() {
  'use strict';
    var sharedDataService = function($mdToast,$mdDialog,$mdSidenav,$state){
        var userInfo;
        var activity = [];
        var userCredentials = '';
        var assignedToList = [];
        var subordinateList =[]; 
        var isManager = false;  
        var viewSelected;
        var language;
        var assigneeToRowNum;
        var appState = '';
        var quoteMobileAllowed;
        var userAttachDeleteAllowed;
        var selectedTab;
        var loggedUser;
        var settings;
        var lastDashBoardListPayload;
        var lastDashBoardState;
        var firstTab ='';
        var secondTab ='';
        var reviewTab;
        var cityList = [];
        var opptyList = [];
        var opptyFilterText;
        var homePreferenceArray = [];
        var inboxPeriod = '';
        return {
            saveUserCredentials:function(credentials){
                this.userCredentials = credentials;
            },
            getUserCredentials:function(){
                return this.userCredentials;
            },
            saveUserDetails:function(user){
                this.userInfo = user;
            },
            getUserDetails:function(){
                return this.userInfo;
            },
            getNetworkState:function(){
                return navigator.onLine;
            },
            saveActivityStatusList:function(activity){
                this.activity = activity;
            },
            getActivityList:function(){
                return this.activity;
            },
            saveAssignedToList:function(assignedList){
                this.assignedToList = assignedList;
            },
            getAssignedList:function(){
                return this.assignedToList;
            },
            saveSubordinateList:function(subordinateList){
                this.subordinateList = subordinateList;
            },
            getSubordinateList:function(){
                return this.subordinateList;
            },
            saveIsManager:function(flag){
                this.isManager = flag;
            },
            getIsManager:function(){
                return this.isManager;
            },
            saveviewSelected:function(view){
                this.viewSelected = view;
            },
            getViewSelected:function(){
                return this.viewSelected;
            },
            saveLanguage:function(lang){
                this.language = lang;
            },
            getLanguage:function(){
                return this.language;
            },
            saveAssigneeToRowNum:function(num){
                this.rowNum = num;
            },
            getAssigneeToRowNum:function(){
                return this.rowNum;
            },
            setAppState:function(state){
                this.appState = state;
            },
            getAppState:function(){
                return this.appState;
            },
            setQuoteMobileAllowed:function(value){
                this.quoteMobileAllowed = value;
            },
            getQuoteMobileAllowed:function(){
                return this.quoteMobileAllowed;
            },
            setUserAttachDeleteAllowed:function(value){
                this.userAttachDeleteAllowed = value;
            },
            getUserAttachDeleteAllowed:function(){
                return this.userAttachDeleteAllowed;
            },
            setSelectedTab:function(value){
                this.selectedTab = value;
            },
            getSelectedTab:function(){
                return this.selectedTab;
            },
            setLoggedUser:function(value){
                this.loggedUser = value;
            },
            getLoggedUser:function(){
                return this.loggedUser;
            },
            setSettings:function(settingParams){
               this.settings = settingParams;
            },
            getSettings:function(){
                return this.settings;
            },
            setLastDashBoardListPayload:function(params){
                this.lastDashBoardListPayload = params;
            },
            getLastDashBoardListPayload:function(){
                return this.lastDashBoardListPayload;
            },
            setSelectedFirstTab:function(tab){
                this.firstTab = tab;
            },
            getSelectedFirstTab:function(){
                return this.firstTab;
            },
            setSelectedSecondTab:function(tab){
                this.secondTab = tab;
            },
            getSelectedSecondTab:function(){
                return this.secondTab;
            },
            saveReviewTab:function(view){
                this.reviewTab = view;
            },
            getReviewTab:function(){
                return this.reviewTab;
            },
            saveCityList:function(cityList){
                this.cityList = cityList;
            },
            getCityList:function(){
                return this.cityList;
            },
            saveOpptyList:function(opptyList){
                this.opptyList = opptyList;
            },
            getOpptyList:function(){
                return this.opptyList;
            },
            saveOpptyFilterText:function(opptyFilterText){
                this.opptyFilterText = opptyFilterText;
            },
            getOpptyFilterText:function(){
                return this.opptyFilterText;
            },
            saveHomePreferenceArray:function(preferences){
                this.homePreferenceArray = preferences;
            },
            getHomePreferenceArray:function(){
                return this.homePreferenceArray;
            },
            saveInboxPeriod:function(period){
                this.inboxPeriod = period;
            },
            getInboxPeriod:function(){
                return this.inboxPeriod;
            }
        }
    };
    stcApp.service('sharedDataService',sharedDataService);
    sharedDataService.$inject = ['$mdToast','$mdDialog','$mdSidenav','$state'];
})();