(function() {
    'use strict';
      var chatHandler = function($timeout, $translate,$stateParams,PubnubService, sharedDataService, MaterialReusables){
          return {
              restrict: 'E',
              templateUrl: "app/shared/directives/chatWindow/chatTemplate.html",
              transclude : true,
              compile: function compile(tElement, tAttrs, transclude) {
              return {
                  pre: function preLink(scope, iElement, iAttrs, controller) {},
                  post: function postLink(scope, iElement, iAttrs, controller) {
                    scope.messages = [];
                    scope.currentUSer = sharedDataService.getUserDetails().loginName;
                    var subscribedChannel = '';
                    scope.subscribeto = $stateParams.subscribeto;
                    scope.id = $stateParams.id;
                    //load new messages
                    var getMessageUpdates = function (message) {
                        scope.$apply(function () {
                            scope.messages.push(message);
                        });
                    };
                    //load history messages
                    var getHistorymsgs = function (historymsgs) {
                        $timeout(function () {
                            scope.messages = historymsgs[0];
                        });
                    };
                    //chat channel subscription
                    var subscribetoPrivateChannel = function (pChannel) {
                        subscribedChannel = pChannel;
                        PubnubService.subscribePrivateChannelChat(pChannel, getMessageUpdates);
                        PubnubService.getHistory(pChannel, getHistorymsgs)
                    };
                    //invoke subscribetoPrivateChannel()
                    var secretReqSend = function (m) {
                        subscribetoPrivateChannel(sharedDataService.getUserDetails().loginName + '-' + scope.id + '-channel');
                    };
                    //check and subscribe to private channel
                    var initializeChat = function(){
                        if (scope.subscribeto !== '') {
                            subscribetoPrivateChannel(scope.subscribeto);
                        } else {
                            var checkedChannel = function (whichChannel) {
                                if (whichChannel === '') {
                                    PubnubService.publishSecretRequest(scope.id, sharedDataService.getUserDetails().loginName + '-' + scope.id + '-channel', secretReqSend);
                                } else {
                                    subscribetoPrivateChannel(whichChannel);
                                }
                            }
                            PubnubService.checkIfSubscribed(sharedDataService.getUserDetails().loginName + '-' + scope.id + '-channel', checkedChannel);
                        }
                    };
                    scope.closeInMapChatWindow = function(){
                        MaterialReusables.hideBottomSheetOptions();
                    };
                    //sending message
                    scope.sendMessage = function () {
                        if (!scope.messageContent || scope.messageContent === '') { return; }
                        PubnubService.publishMessage(scope.messageContent, subscribedChannel);
                        scope.messageContent = '';
                    };
                    //security input validation
                    scope.messageValidate = function (newValue) {
                        scope.messageContent = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
                    };
                    initializeChat();
                  }
              }
              }
          }
      };
      stcApp.directive('chatHandler',chatHandler);
      chatHandler.$inject = ['$timeout', '$translate','$stateParams','PubnubService', 'sharedDataService', 'MaterialReusables'];
  })();