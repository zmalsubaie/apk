(function() {
  'use strict';
    var loading = function(){
        return {
            restrict: 'E',
            replace:true,
            scope: {loaderText: '@'},
            template: '<div style="position: fixed;top: 46%;left: 0;z-index: 999;width: 100%;height: 100vh;text-align:center;"><img src="assets/img/newloader.svg" width="70" height="70" /></div>',
            link: function (scope, element, attr) {}
        }
    };
    stcApp.directive('loading',loading);
})();