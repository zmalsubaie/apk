var quoteController = function ($scope, $http, $timeout, $translate, $state, apiService, dbService,MaterialReusables, sharedDataService, pluginService) {
  $scope.quoteListModel = {
    isLoading: true,
    quotes: [],
    startRow: 0,
    isRefresh: true,
    search: false,
    searchText: ''
  };
  $scope.endOfPage = false;
  //check item exists in array
  function indexOf(array, item) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].quoteNumber === item.quoteNumber) return i;
    }
    return -1;
  }
  //quote list - API call success
  var oppQuoteListSuccess = function (data) {
    $scope.quoteListModel.isLoading = false;
    $scope.quoteListModel.isRefresh = false;
    if (data.data !== null) {
      angular.forEach(data.data.listOfQuote, function (value, key) {
        if (indexOf($scope.quoteListModel.quotes, value) === -1) {
          value.colorCode = getColorCode(value.created);
          $scope.quoteListModel.quotes.push(value);
        }
      });
      dbService.insertIntoDBTable('stc_quotes', [JSON.stringify($scope.quoteListModel.quotes), 'online'], angular.noop, angular.noop);
    } else {
      if ($scope.action === 'search') {
        MaterialReusables.showToast('' + pluginService.getTranslations().noQuotesFound, 'warning');
      } else {
        $scope.endOfPage = true;
        MaterialReusables.showToast('' + pluginService.getTranslations().endofList, 'warning');
      }
    }
    if (angular.isDefined(data.data.listOfQuote) && data.data.listOfQuote !== null) {
      if (data.data.listOfQuote.length < 10) {
        $scope.endOfPage = true;
        MaterialReusables.showToast('' + pluginService.getTranslations().endofList, 'warning');
      } else {
        $scope.endOfPage = false;
      }
    }
  };
  //quote list - API call error 
  var oppQuoteListError = function (data) {
    $scope.quoteListModel.isLoading = false;
    $scope.quoteListModel.isRefresh = false;
    Util.throwError(data.data.Message, MaterialReusables);
  };
  //loading more quotes
  $scope.loadMoreQuotes = function () {
    if (sharedDataService.getNetworkState()) {
      if (!$scope.endOfPage) {
        $scope.quoteListModel.isRefresh = true;
        $scope.quoteListModel.startRow = parseInt($scope.quoteListModel.startRow, 10) + 30;
        $scope.action = 'load';
        $scope.fetchQuotes("[Quote.STC Opportunity Primary Position Id] ='" + sharedDataService.getUserDetails().PrimaryPositionId + "'");
      }
    }
  };
  //fetch quotes - API call
  $scope.fetchQuotes = function (searchSpec) {
    if(!sharedDataService.getNetworkState()){
       var gotQuotesFromDB = function(data){
          $scope.quoteListModel.isLoading = false;
          $scope.quoteListModel.isRefresh = false;
          $timeout(function () {
            $scope.quoteListModel.quotes = JSON.parse(data[0].quotes);
          });
       };
       dbService.getDetailsFromTable('stc_quotes', [], angular.noop, gotQuotesFromDB);
    }else{
        var newQuery = '';
        if ($scope.quoteListModel.startRow === 0) {
          newQuery = true;
        } else {
          newQuery = false;
        }
        var payLoad = {
          "busObjCacheSize": "",
          "newQuery": newQuery,
          "outputIntObjectName": "Quote",
          "pageSize": "30",
          "searchSpec": searchSpec,
          "sortSpec": "Created(DESCENDING)",
          "startRowNum": $scope.quoteListModel.startRow,
          "viewMode": ""
        }
        apiService.fetchDataFromApi('opp.quoteList', payLoad, oppQuoteListSuccess, oppQuoteListError);
    }
  }
  //set color code based on quote creation date
  var getColorCode = function (leadCreatedDate) {
    var colorCode;
    var leadDate = new Date(leadCreatedDate);
    var sysDate = new Date();
    var dateDiff = parseInt((sysDate - leadDate) / (24 * 3600 * 1000), 10);
    if (dateDiff < 4) {
      colorCode = 'green';
    } else if (dateDiff > 3 && dateDiff < 10) {
      colorCode = 'blue';
    } else if (dateDiff > 9 && dateDiff < 16) {
      colorCode = 'yellow';
    } else if (dateDiff > 15 && dateDiff < 21) {
      colorCode = 'orange';
    } else {
      colorCode = 'red';
    }
    return colorCode;
  };
  $scope.action = 'load';
  $scope.fetchQuotes("[Quote.STC Opportunity Primary Position Id] ='" + sharedDataService.getUserDetails().PrimaryPositionId + "'");
  //quote list search enable/disable
  $scope.$on('openSearch', function (event, args) {
    $scope.quoteListModel.search = !$scope.quoteListModel.search;
    if (!$scope.quoteListModel.search) {
      $scope.quoteListModel.searchText = '';
    }
  });
  //quote list search by text
  $scope.searchQuotesByText = function () {
    if(sharedDataService.getNetworkState()){
      if ($scope.quoteListModel.searchText !== '') {
        $scope.action = 'search';
        $scope.quoteListModel.startRow = 0;
        $scope.quoteListModel.isRefresh = true;
        var searchSpec = "(([Quote.STC Opportunity Primary Position Id]= '" + sharedDataService.getUserDetails().PrimaryPositionId + "') AND ([Quote.Quote Number] LIKE '*" + $scope.quoteListModel.searchText + "*' OR [Quote.STC Lead Number] LIKE '*" + $scope.quoteListModel.searchText + "*'))"
        $scope.fetchQuotes(searchSpec);
      }
    }
  };
  //search field - input field validation
  $scope.searchFilter = function (obj) {
    if (typeof obj !== 'undefined') {
      $scope.quoteListModel.searchText = $scope.quoteListModel.searchText.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g,'');
      var re = new RegExp($scope.quoteListModel.searchText, 'i');
      return !$scope.quoteListModel.searchText || re.test(obj.name) || re.test(obj.quoteNumber) || re.test(obj.opportunityNumber);
    }
  };
};
quoteModule.controller('quoteController', quoteController);
quoteController.$inject = ['$scope', '$http', '$timeout', '$translate', '$state', 'apiService','dbService','MaterialReusables', 'sharedDataService', 'pluginService'];