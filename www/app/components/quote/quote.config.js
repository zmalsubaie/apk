quoteModule.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('root.quote', {
        url: '/quote',
        templateUrl: 'app/components/quote/quote.template.html',
        controller: 'quoteController',
        pageTitle: 'Quotes',
        backbutton: false,
        buttonProp:['search'],
        previousState:'root.home'
    })
}
]);