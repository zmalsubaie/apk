(function () {
    'use strict';
    var loginController = function ($scope, $rootScope, $state, $filter, $http, $mdDialog, $translate, $timeout, MaterialReusables, apiService, PubnubService, sharedDataService, pluginService, dbService) {
        $scope.loginModel = {
            isEnglish: true,
            loginBtnText: 'Login',
            userId: '',
            password: '',
            disableLogin: false,
            selectedLanguage: 'English',
            showOTPlink: false,
            isApiLoginSuccess: false,
            isOTPValidationSuccess: false,
            loginAttemps: 0,
            isFetchingSSLStatus: false,
            isCheckingforUpdates: false,
            updateLoaderText: '',
            isDeviceRooted: false,
            upgradePercentage: ''
        };
        //load app language English by default as app language
        $translate.use('en_US');
        //fetch login attempts
        if (localStorage.getItem('attemptCount') !== null) {
            $scope.loginModel.loginAttemps = parseInt(localStorage.getItem('attemptCount'), 10);
        } else {
            localStorage.setItem('attemptCount', $scope.loginModel.loginAttemps);
        }
        sharedDataService.setAppState('Logged out');
        sharedDataService.saveLanguage('en_US');
        //login field validation
        var checkLoginFields = function () {
            var loginValidation = false;
            if ($scope.loginModel.password !== '' && $scope.loginModel.userId !== '') {
                loginValidation = true;
            } else {
                loginValidation = false;
            }
            return loginValidation;
        };
        //update login failure count
        var updateLoginFailures = function () {
            $scope.loginModel.loginAttemps++;
            localStorage.setItem('attemptCount', $scope.loginModel.loginAttemps);
            localStorage.setItem('lastFailedLoginTime', new Date());
        };
        //login method call
        $scope.checkIfLoginEnabled = function () {
            if (!sharedDataService.getNetworkState()) {
                if (checkLoginFields()) {
                    if (device.platform === 'windows') {
                        if (localStorage.getItem('username') === $scope.loginModel.userId && localStorage.getItem('password') === $scope.loginModel.password) {
                            sharedDataService.saveUserDetails(JSON.parse(localStorage.getItem('userDetails')));
                            $rootScope.firstName = sharedDataService.getUserDetails().firstName;
                            checkIfUserCanLogin(true, true);
                        } else {
                            MaterialReusables.showToast('' + pluginService.getTranslations().id_pswd_combination_error, 'error');
                        }
                    } else {
                        var gotUserCredentials = function (userCredentials) {
                            if (userCredentials.length) {
                                if (userCredentials[0].username === $scope.loginModel.userId && userCredentials[0].password === $scope.loginModel.password) {
                                    sharedDataService.saveUserDetails(JSON.parse(userCredentials[0].userDetails));
                                    $rootScope.firstName = sharedDataService.getUserDetails().firstName;
                                    checkIfUserCanLogin(true, true);
                                } else {
                                    MaterialReusables.showToast('' + pluginService.getTranslations().id_pswd_combination_error, 'error');
                                }
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().onEnableOff, 'warning');
                            }
                        };
                        var failedGettingCredentials = function () {
                            MaterialReusables.showToast('' + pluginService.getTranslations().onEnableOff, 'warning');
                        };
                        dbService.getDetailsFromTable('stc_userCredentials', [], failedGettingCredentials, gotUserCredentials);
                    }
                } else {
                    MaterialReusables.showToast('' + pluginService.getTranslations().enter_credentials, 'warning');
                }
            } else {
                if (parseInt(localStorage.getItem('attemptCount'), 10) < 3) {
                    if (checkLoginFields()) { $scope.initiateLogin(); }
                    else { MaterialReusables.showToast('' + pluginService.getTranslations().enter_credentials, 'warning'); }
                } else {
                    if (localStorage.getItem('lastFailedLoginTime') !== null) {
                        if (Util.diff_minutes(new Date(), new Date(localStorage.getItem('lastFailedLoginTime'))) >= 3) {
                            if (checkLoginFields()) {
                                $scope.loginModel.loginAttemps = 0;
                                localStorage.setItem('attemptCount', $scope.loginModel.loginAttemps);
                                $scope.initiateLogin();
                            }
                            else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().enter_credentials, 'warning');
                            }
                        } else {
                            var tryAfterMinutes = 3 - parseInt(Util.diff_minutes(new Date(), new Date(localStorage.getItem('lastFailedLoginTime'))), 10);
                            MaterialReusables.showToast('' + pluginService.getTranslations().plsTryAfter + ' ' + tryAfterMinutes + ' ' + pluginService.getTranslations().minutes, 'error');
                        }
                    }
                }
            }
        };
        //enable login
        var enableLoginButon = function () {
            $scope.loginModel.loginBtnText = 'Login';
            $scope.loginModel.disableLogin = false;
        };
        //disable login
        var disableLoginButton = function () {
            $scope.loginModel.disableLogin = true;
            $scope.loginModel.loginBtnText = 'Logging In';
        };
        //check and navigate user into the app
        var checkIfUserCanLogin = function (isOtpSuccess, isApiSuccess) {
            if (isOtpSuccess && isApiSuccess) {
                if (DEVICE_BUILD) {
                    pluginService.initBackGroundService();
                }
                sharedDataService.setAppState('Logged In');
                localStorage.removeItem('otpAttempts');
                $state.go('root.home');
            }
        };
        //submit OTP - API call
        var sendOTPRequest = function (payload, scope) {
            var verifiedTrue = false;
            var otpInitSuccess = function (data) {
                if (data.data.statusCode === '0') {
                    verifiedTrue = true;
                    $mdDialog.hide();
                    $scope.loginModel.isOTPValidationSuccess = true;
                    checkIfUserCanLogin($scope.loginModel.isOTPValidationSuccess, $scope.loginModel.isApiLoginSuccess);
                    fetchUserDetails();
                } else {
                    if (parseInt(localStorage.getItem('otpAttempts'), 10) < 3) {
                        if (payload.token !== '') {
                            var updateOtpAttempt = parseInt(localStorage.getItem('otpAttempts'), 10) + 1;
                            localStorage.setItem('otpAttempts', updateOtpAttempt);
                            var remainingOTPAttempts = 3 - parseInt(localStorage.getItem('otpAttempts'), 10);
                            if (remainingOTPAttempts === 0) {
                                MaterialReusables.showToast('' + pluginService.getTranslations().otpLimitReached, 'error');
                                scope.submitFlag = true;
                                $mdDialog.hide();
                                enableLoginButon();
                                localStorage.setItem('attemptCount', '0');
                            } else {
                                MaterialReusables.showToast('' + pluginService.getTranslations().wrongOtpMsg + '.' + remainingOTPAttempts + ' more Attempts Remaining', 'error');
                            }
                        } else {
                            $scope.loginModel.showOTPlink = true;
                        }
                    }
                    scope.submitFlag = true;
                    scope.submitBtnText = 'Submit';
                }
            };
            var otpInitFailed = function (data) {
                if (data.data.Code === '-1') {
                    MaterialReusables.showToast('' + pluginService.getTranslations().otpLimitReached, 'error');
                } else {
                    MaterialReusables.showToast('' + pluginService.getTranslations().otpErr, 'error');
                }
                scope.submitBtnText = 'Submit';
                scope.submitFlag = true;
                verifiedTrue = true;
                $mdDialog.hide();
                localStorage.setItem('lastFailedLoginTime', new Date());
                enableLoginButon();
            };
            apiService.fetchDataFromApi('submitOTP', payload, otpInitSuccess, otpInitFailed);
        };
        //invoke OTP popup
        $scope.showOTPPrompt = function (ev) {
            if (device.platform === 'Android') {
                pluginService.listenToIncomingMessages();
            }
            $mdDialog.show({
                controller: ['$scope', '$mdDialog', '$translate', OTPDialogController],
                templateUrl: 'app/shared/layout/OTPDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false
            })
                .then(function (answer) { }, function () { });
        };
        //process entered OTP
        function OTPDialogController($scope, $mdDialog, $translate) {
            $scope.otp = {
                otpCode: ''
            };
            $scope.submitBtnText = 'Submit';
            $scope.submitFlag = true;
            $scope.OTPDialogHide = function () {
                localStorage.setItem('attemptCount', 0);
                enableLoginButon();
                if (device.platform === 'Android') {
                    pluginService.stopListener();
                }
                $mdDialog.hide();
            };
            $scope.$on('otpDetect', function (event, args) {
                $scope.otp.otpCode = args.code;
                if ($scope.otp.otpCode !== null) {
                    $scope.otpSubmit();
                }
            });
            $scope.otpSubmit = function () {
                if ($scope.otp.otpCode !== null) {
                    $scope.submitBtnText = 'Submitting';
                    $scope.submitFlag = false;
                    sendOTPRequest({ 'token': $scope.otp.otpCode }, $scope);
                }
            };
        }
        //generate OTP
        var generateOTPRequest = function () {
            var otpPayload = '';
            var otpHitSuccess = function (data) {
                if (data.data.statusCode === '0') {
                    if ($state.current.name === 'login') {
                        $scope.showOTPPrompt();
                    }
                } else {
                    MaterialReusables.showToast('' + pluginService.getTranslations().otpErr, 'error');
                    enableLoginButon();
                    localStorage.setItem('lastFailedLoginTime', new Date());
                }
            };
            var otpHitError = function (data) {
                MaterialReusables.showToast('' + pluginService.getTranslations().otpErr, 'error');
                enableLoginButon();
                localStorage.setItem('lastFailedLoginTime', new Date());
            };
            apiService.fetchDataFromApi('generateOTP', otpPayload, otpHitSuccess, otpHitError);
        }
        //login API call
        $scope.initiateLogin = function () {
            x_access_user = '';
            x_access_token = '';
            localStorage.setItem('otpAttempts', '0');
            disableLoginButton();
            var authPayload = {
                'payload': {
                    "getPrivateCredentials": 'Y',
                    "password": $scope.loginModel.password,
                    "userName": $scope.loginModel.userId.toUpperCase(),
                    "deviceType": device.platform.toLowerCase()
                }
            };
            var loginSuccess = function (data) {
                if (data.data.returnCode === '0' && data.data.returnMessage === 'Success') {
                    localStorage.removeItem('attemptCount');
                    localStorage.removeItem('lastFailedLoginTime');
                    x_access_user = data.data.siebelUserName;
                    x_access_token = data.data.token;
                    encryptKey = x_access_token.replace(/-/g, '');
                    if (PROD_BUILD) {
                        generateOTPRequest();
                    } else {
                        fetchUserDetails();
                    }
                } else {
                    enableLoginButon();
                    MaterialReusables.showToast('' + pluginService.getTranslations().id_pswd_combination_error, 'error');
                }
            };
            var loginFailed = function (data) {
                enableLoginButon();
                if (data.data !== null) {
                    if (data.data.Code === 'OSB-380001') {
                        updateLoginFailures();
                        var remainingAttemptsID = 3 - parseInt(localStorage.getItem('attemptCount'), 10);
                        MaterialReusables.showToast('' + pluginService.getTranslations().id_pswd_combination_error + '.' + remainingAttemptsID + ' ' + pluginService.getTranslations().morAttmptsLeft, 'error');
                    } else {
                        Util.throwError(data.data.Message, MaterialReusables);
                    }
                } else if (data.data === null) {
                    MaterialReusables.showToast('' + pluginService.getTranslations().noResServer, 'error');
                }
            };
            apiService.fetchDataFromApi('authenticate', authPayload, loginSuccess, loginFailed);
        };
        var checkCredentialsInDB = function () {
            var failedGettingCredentials = function (err) {
            };
            var gotUserCredentials = function (userCredentials) {
                if (userCredentials.length > 0) {
                    var isFingerPrintValid = function (data) {
                        $scope.loginModel.userId = userCredentials[0].username;
                        $scope.loginModel.password = userCredentials[0].password;
                        $scope.checkIfLoginEnabled();
                    };
                    var isFingerPrintInvalid = function () {
                    };
                    pluginService.isfingerPrintAvailable(isFingerPrintValid, isFingerPrintInvalid);
                } else {
                    checkLoginFields();
                }
            };
            dbService.getDetailsFromTable('stc_userCredentials', [], failedGettingCredentials, gotUserCredentials);
        };
        //set app language on select
        $scope.languageChange = function () {
            if ($scope.loginModel.isEnglish) {
                $scope.loginModel.selectedLanguage = 'English';
                $translate.use('en_US');
                sharedDataService.saveLanguage('en_US');
            } else {
                $scope.loginModel.selectedLanguage = 'Arabic';
                $translate.use('ar_AR');
                sharedDataService.saveLanguage('ar_AR');
            }
        };
        //close app
        $scope.closeApplication = function () {
            navigator.app.exitApp();
        };
        //set translations
        $rootScope.$on('$translateChangeSuccess', function () {
            $translate(pluginService.translationsArray()).then(function (translations) {
                pluginService.setTranslations(translations);
            });
        });
        //on enter key login function call
        $(document).keypress(function (e) {
            if (e.which == 13) {
                var usernameContent = angular.element('#username').val();
                if (usernameContent.length > 0) {
                    angular.element('#password').focus();
                }
                var passwordContent = angular.element('#password').val();
                if (passwordContent.length > 0) {
                    angular.element('#password').blur();
                    disableLoginButton();
                    $scope.initiateLogin();

                }
            }
        });
        //app upgrade function if upgrade exists
        var checkForAppUpgrade = function () {
            var upgradeInfoSuccess = function (data) {
                if (data.data !== '') {
                    var upgradeConfirmCallBack = function (confim) {
                        if (confim) {
                            var upgradeSuccess = function (upgrSuccinfo) {
                                //TODO
                            };
                            var upgradeFailed = function (upgrErrinfo) {
                                MaterialReusables.showToast('' + pluginService.getTranslations().appUpgradeFailed, 'error');
                            };
                            var progressCallBack = function (progressPercent) {
                                $timeout(function () {
                                    $scope.loginModel.upgradePercentage = progressPercent;
                                    $scope.loginModel.updateLoaderText = 'Downloading Latest Package...' + progressPercent + '%';
                                });
                            };
                            $scope.loginModel.updateLoaderText = 'Downloading Latest Package...';
                            pluginService.handleAppUpgrade(data.data.upgradeurl, upgradeSuccess, upgradeFailed, progressCallBack);
                        } else {
                            $scope.loginModel.isCheckingforUpdates = false;
                            checkCredentialsInDB();
                        }
                    };
                    var currentAppVersion = AppVersion.version;
                    var currentVersion = parseInt(currentAppVersion.split('.').join(""), 10);;
                    var availableVersion = parseInt(data.data.versionNo.split('.').join(""), 10);
                    if (currentVersion < availableVersion) {
                        var upgradeText = '<span>New Version of App is Available for Download.Would you like to Upgrade?</span><br><br><span><strong>Current Version : ' + currentAppVersion + '</strong></span><br><span><strong>Available Version :' + data.data.versionNo + '</strong></span>';
                        MaterialReusables.showInfoPopup('Upgrade Available', upgradeText, 'Upgrade', 'Cancel', upgradeConfirmCallBack);
                    } else {
                        $scope.loginModel.isCheckingforUpdates = false;
                        checkCredentialsInDB();
                    }
                } else {
                    checkCredentialsInDB();
                    $scope.loginModel.isCheckingforUpdates = false;
                }
            };
            var upgradeInfoFailed = function (data) {
                $scope.loginModel.isCheckingforUpdates = false;
            };
            $scope.loginModel.isCheckingforUpdates = true;
            $scope.loginModel.updateLoaderText = 'Checking For Updates..';
            var upgradeCheckPayload = {
                'payload': {
                    'platform': device.platform.toLowerCase()
                }
            };
            apiService.fetchDataFromApi('app.upgrade', upgradeCheckPayload, upgradeInfoSuccess, upgradeInfoFailed);
        };
        //identify logged in user as manager or sales agent and get suboridnates
        var getManagerSubordinates = function () {
            var managerSubordinatesSuccess = function (data) {
                $scope.userList = [];
                if (data.data !== null) {
                    if (data.data.record !== null) {
                        angular.forEach(data.data.record, function (value, key) {
                            if (value.parentPosition !== value.subordinatePosition) {
                                $scope.userList.push(value);
                            }
                        });
                        if ($scope.userList.length > 0) {
                            sharedDataService.saveIsManager(true);
                            sharedDataService.saveSubordinateList($scope.userList);
                        } else {
                            sharedDataService.saveIsManager(false);
                            sharedDataService.saveSubordinateList($scope.userList);
                        }
                    } else {
                        sharedDataService.saveIsManager(false);
                    }
                } else {
                    sharedDataService.saveIsManager(false);
                }
                $scope.loginModel.isApiLoginSuccess = true;
                if (PROD_BUILD) {
                    checkIfUserCanLogin($scope.loginModel.isOTPValidationSuccess, $scope.loginModel.isApiLoginSuccess);
                } else {
                    checkIfUserCanLogin(true, true);
                }
            };
            var managerSubordinatesError = function (data) {
                sharedDataService.saveIsManager(false);
                $scope.loginModel.isApiLoginSuccess = true;
                if (PROD_BUILD) {
                    checkIfUserCanLogin($scope.loginModel.isOTPValidationSuccess, $scope.loginModel.isApiLoginSuccess);
                } else {
                    checkIfUserCanLogin(true, true);
                }
                enableLoginButon();
                localStorage.setItem('lastFailedLoginTime', new Date());
                localStorage.setItem('attemptCount', $scope.loginModel.loginAttemps);
                apiService.triggerAppLogout(MaterialReusables,PubnubService,$state);
            };
            var odsManagerPayLoad = {
                "select": {
                    "parentPosition": sharedDataService.getUserDetails().primaryPosition
                }
            };
            apiService.fetchDataFromApi('opp.odssubordinateslist', odsManagerPayLoad, managerSubordinatesSuccess, managerSubordinatesError);
        };
        //fetch user image
        var fetchUserImage = function() {
            var downloadSuccess = function(data) {
                if(data.data.photo !== null) {
                    $rootScope.profileImage = 'data:image/jpeg;base64,' + data.data.photo;
                } else {
                    $rootScope.profileImage = null;
                }
            };
            var downloadError = function(data) {
                $rootScope.profileImage = null;
            };
            var imagePayload = {
                "userId": sharedDataService.getUserDetails().loginName
              };
            apiService.fetchDataFromApi('user.image', imagePayload, downloadSuccess, downloadError);
        }
        //user details fetch api call
        var fetchUserDetails = function () {
            var userDetailsPayload = {
                'payload': {
                    "busObjCacheSize": "",
                    "messageId": "",
                    "outputIntObjectName": "Users",
                    "primaryRowId": "",
                    "queryByUserKey": "",
                    "searchSpec": "[User.Login Name]='" + $scope.loginModel.userId.toUpperCase() + "'",
                    "siebelMessage": ""
                }
            };
            var userSuccess = function (data) {
                if (data.data.listOfSSUser !== null) {
                    sharedDataService.saveUserDetails(data.data.listOfSSUser.user[0]);
                    if (device.platform === 'windows') {
                        localStorage.removeItem('username');
                        localStorage.removeItem('password');
                        localStorage.removeItem('userDetails');
                        localStorage.setItem('username', $scope.loginModel.userId);
                        localStorage.setItem('password', $scope.loginModel.password);
                        localStorage.setItem('userDetails', JSON.stringify(data.data.listOfSSUser.user[0]));
                    } else {
                        dbService.userDetailsHandler({ name: $scope.loginModel.userId, password: $scope.loginModel.password, apiDetails: data.data.listOfSSUser.user[0] });
                    }
                    fetchPreferences();
                    PubnubService.initializeChat();
                    $rootScope.firstName = sharedDataService.getUserDetails().firstName;
                    fetchUserImage();
                    //fetch subordinates
                    getManagerSubordinates();
                    //validate responsibilities
                    var responsibilities = sharedDataService.getUserDetails().listOfSSUserResponsibility;
                    var postPaidAllowed = false;
                    var prePaidAllowed = false;
                    if (angular.isDefined(responsibilities)) {
                        if (responsibilities.ssUserResponsibility !== null) {
                            var respArray = responsibilities.ssUserResponsibility;
                            for (var i = 0; i < respArray.length; i++) {
                                if (respArray[i].responsibility === 'STC EBU OM Responsibility') {
                                    postPaidAllowed = true;
                                }
                                if (respArray[i].responsibility === 'STC OM Prepaid Modify VAS Responsibility') {
                                    prePaidAllowed = true;
                                }
                            }
                            if (postPaidAllowed && prePaidAllowed) {
                                sharedDataService.setQuoteMobileAllowed(true);
                            } else {
                                sharedDataService.setQuoteMobileAllowed(false);
                            }
                        } else {
                            sharedDataService.setQuoteMobileAllowed(false);
                        }
                    } else {
                        sharedDataService.setQuoteMobileAllowed(false);
                    }
                    //validate user positions
                    var userDeleteAlowed = false;
                    if (angular.isDefined(sharedDataService.getUserDetails().listOfEmployeePosition)) {
                        angular.forEach(sharedDataService.getUserDetails().listOfEmployeePosition.employeePosition, function (value, key) {
                            if (value.Position === 'STC EBU Lead Admin Position') {
                                userDeleteAlowed = true;
                            }
                        });
                        if (userDeleteAlowed) {
                            sharedDataService.setUserAttachDeleteAllowed(true);
                        } else {
                            sharedDataService.setUserAttachDeleteAllowed(false);
                        }
                    } else {
                        sharedDataService.setUserAttachDeleteAllowed(false);
                    }
                } else {
                    enableLoginButon();
                    MaterialReusables.showToast('' + pluginService.getTranslations().notPrivilegedUser, 'error');
                }
            };
            var userFailed = function (data) {
                enableLoginButon();
                localStorage.setItem('lastFailedLoginTime', new Date());
                localStorage.setItem('attemptCount', $scope.loginModel.loginAttemps);
                if (angular.isDefined(data.data.errorMessage)) {
                    Util.throwError(data.data.errorMessage, MaterialReusables);
                } else if (angular.isDefined(data.data.Message)) {
                    Util.throwError(data.data.Message, MaterialReusables);
                }
            };
            apiService.fetchDataFromApi('userdetails', userDetailsPayload, userSuccess, userFailed);
        };
        //check array item exists or not
        var indexOf = function (array, item) {
            for (var i = 0; i < array.length; i++) {
                if (array[i] === item) return i;
            }
            return -1;
        }
        //set user preferences based on API response
        var setPreferences = function (data) {
            var prefArray = [];
            var sideBarFeatures = [];
            var homeFeatures = [];
            if (data.opportunities === 'Y' || data.opportunities === null) {
                prefArray.push('Opportunities');
            }
            if (data.inbox === 'Y' || data.inbox === null) {
                prefArray.push('Inbox');
            }
            if (data.accounts === 'Y' || data.accounts === null) {
                prefArray.push('Accounts');
            }
            if (data.quotes === 'Y' || data.quotes === null) {
                prefArray.push('Quotes');
            }
            if (data.dashboard === 'Y' || data.dashboard === null) {
                prefArray.push('Dashboard');
            }
            if (data.maps === 'Y' || data.maps === null) {
                prefArray.push('Map');
            }
            if (data.calendar === 'Y' || data.calendar === null) {
                prefArray.push('Calendar');
            }
            if (data.offlineMode === 'Y' || data.offlineMode === null) {
                prefArray.push('Offline');
            }
            if (data.locationTracker === 'Y' || data.locationTracker === null) {
                prefArray.push('Location Tracker');
            }
            angular.forEach(mainModuleProps_sidemenu, function (value, key) {
                if (indexOf(prefArray, value.name) !== -1) {
                    sideBarFeatures.push(value);
                }
                if (value.name === 'Settings' || value.name === 'Home') { //add Home & settings for every preference configurations
                    sideBarFeatures.push(value);
                }
            });
            angular.forEach(mainModuleProps_dashboard, function (value, key) {
                if (indexOf(prefArray, value.name) !== -1) {
                    homeFeatures.push(value);
                }
            });
            $rootScope.sideBarFeatures = sideBarFeatures;
            sharedDataService.saveHomePreferenceArray(homeFeatures);
        };
        //set default preferences if no preferences found
        var setDefaultPreferences = function () {
            var sideBarFeatures = [];
            var homeFeatures = [];
            angular.forEach(mainModuleProps_sidemenu, function (value, key) {
                if (!(value.name === 'Accounts' || value.name === 'Quotes')) {
                    sideBarFeatures.push(value);
                }
            });
            angular.forEach(mainModuleProps_dashboard, function (value, key) {
                if (!(value.name === 'Accounts' || value.name === 'Quotes')) {
                    homeFeatures.push(value);
                }
            });
            $rootScope.sideBarFeatures = sideBarFeatures;
            sharedDataService.saveHomePreferenceArray(homeFeatures);
        };
        //fetch User Preferences
        var fetchPreferences = function () {
            var userPrefPayLoad = {
                'userId': sharedDataService.getUserDetails().loginName,
                'deviceType': "MOB"
            };
            var userPrefSuccess = function (data) {
                if (data.data.returnCode === "0") {
                    if (angular.isDefined(data.data.userPref)) {
                        if (data.data.userPref[0].inboxDays !== null) {
                            sharedDataService.saveInboxPeriod(data.data.userPref[0].inboxDays);
                        } else {
                            sharedDataService.saveInboxPeriod('3');
                        }
                        setPreferences(data.data.userPref[0]);
                    } else {
                        sharedDataService.saveInboxPeriod('3');
                        setDefaultPreferences();
                    }
                }
            };
            var userPrefError = function (data) {
                sharedDataService.saveInboxPeriod('3'); //default inbox period is 3 days
                setDefaultPreferences();
            };
            apiService.fetchDataFromApi('opp.fetchUserPreferences', userPrefPayLoad, userPrefSuccess, userPrefError);
        };
        //check for app upgrade on app launch if device is not rooted
        $scope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            if (sharedDataService.getNetworkState()) {
                if (from.name.indexOf('root') === -1) {
                    if (PROD_BUILD) {
                        $timeout(function () {
                            var rootSuccess = function (result) {
                                if (result === 0 || !result) {
                                    checkForAppUpgrade();
                                } else {
                                    $scope.loginModel.isDeviceRooted = true;
                                }
                            };
                            var rootError = function (err) {
                                checkForAppUpgrade();
                            };
                            if (device.platform === 'browser') {
                                $scope.loginModel.isDeviceRooted = false;
                            } else {
                                pluginService.isSuperUserDevice(rootSuccess, rootError);
                            }
                        }, 3000);      
                    }
                    $timeout(function(){
                        checkCredentialsInDB();
                    },3000);
                }else{
                    pluginService.unregisterUserPositionWatch();
                    checkCredentialsInDB();
                }
            } else {
                $scope.loginModel.isCheckingforUpdates = false;
            }
            $timeout(function () {
                if (device.platform === 'windows') {
                    if (localStorage.getItem('stc_notificationTime') === null) {
                        localStorage.setItem('stc_notificationTime', '10');
                        localStorage.setItem('stc_notificationStatus', 'true');
                    }
                } else {
                    dbService.injectNotificationTimeDetails();
                }
            }, 3000);
            $rootScope.profileImage = null;
        });
        setDefaultPreferences();
    };
    loginController.$inject = ['$scope', '$rootScope', '$state', '$filter', '$http', '$mdDialog', '$translate', '$timeout', 'MaterialReusables', 'apiService', 'PubnubService', 'sharedDataService', 'pluginService', 'dbService'];
    stcApp.controller('loginController', loginController);
})();
