var chatController = function ($scope, $stateParams) {
    $scope.chatOptions = {
        subscribeto:$stateParams.subscribeto,
        id:$stateParams.id
    };
};
stcApp.controller('chatController', chatController);
chatController.$inject = ['$scope', '$stateParams'];
