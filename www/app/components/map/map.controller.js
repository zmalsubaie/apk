var mapController = function($scope,$state,pluginService,sharedDataService,apiService,MaterialReusables){
  $scope.isMapLoading = true;
  $scope.netWorkState = false;
  var allMapOppPayload = {
      'payload':{
        "busObjCacheSize" : "",
        "newQuery" : "",
        "outputIntObjectName" : "STC Opportunity LMS Mobile App",
        "pageSize" : "100",
        "searchSpec" : "([Opportunity.Primary Position Id] = '"+ sharedDataService.getUserDetails().PrimaryPositionId + "') AND [Opportunity.Decimal Latitude] IS NOT NULL AND [Opportunity.Decimal Longitude] IS NOT NULL",
        "sortSpec" : "",
        "startRowNum" : "",
        "viewMode" : ""
      }
  };
  //fetch all map locations API call - success
  var allMapOppFetchSuccess = function(mapData){
       $scope.isMapLoading = false;
       if(mapData.data.listOfStcOpportunity === null){
            MaterialReusables.showToast(''+pluginService.getTranslations().noOpp,'error');
       }
       var getSelectedMarkerId = function(rowId,details){
         if(rowId!=='' && details!==''){
             $state.go('root.oppDetail',{id:rowId,oppDetails:details,fromView:'root.map',enableEdit:true});
         }
       };
       pluginService.initializeMap(mapData.data.listOfStcOpportunity.opportunity,'multiple',"map_canvas",false,angular.noop,getSelectedMarkerId);

  };
  //fetch all map locations API call - error
  var allMapOppFetchError = function(err){
       $scope.isMapLoading = false;
       MaterialReusables.showToast(''+pluginService.getTranslations().mapFetch,'error');
  };
  ////fetch all map locations API call by checking network
  if(!sharedDataService.getNetworkState()){
     $scope.netWorkState = false;
     $scope.isMapLoading = false;
  }else{
     $scope.netWorkState = true;
     apiService.fetchDataFromApi('map.allOpportunities', allMapOppPayload, allMapOppFetchSuccess,allMapOppFetchError);
  }
};
mapModule.controller('mapController',mapController);
mapController.$inject = ['$scope','$state','pluginService','sharedDataService','apiService','MaterialReusables'];

