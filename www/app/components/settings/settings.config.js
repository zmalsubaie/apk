settingsModule.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('root.settings', {
        url: '/settings',
        templateUrl: 'app/components/settings/settings.template.html',
        controller: 'settingsController',
        pageTitle: 'Settings',
        backbutton: false,
        buttonProp:['save'],
        previousState:'root.home'
    })
}]);