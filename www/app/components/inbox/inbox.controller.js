var inboxController = function ($rootScope, $scope, $timeout, $state, apiService, pluginService, sharedDataService, MaterialReusables, dbService) {
    $scope.inboxListModel = {
        isLoading: true,
        startRow: 0,
        inboxList: [],
        inboxPeriod: '',
        endOfPage: false,
        loadMore: false
    }
    //fetch saved inbox period from shared data if data setting it as 3
    if (angular.isDefined(sharedDataService.getInboxPeriod())) {
        $scope.inboxListModel.inboxPeriod = sharedDataService.getInboxPeriod();
    } else {
        $scope.inboxListModel.inboxPeriod = 3;// by default 3 days
    }
    //load more opportunities in inbox
    $scope.loadMoreOpportunities = function () {
        if (sharedDataService.getNetworkState()) {
            if (!$scope.inboxListModel.endOfPage) {
                $scope.inboxListModel.loadMore = true;
                myViewListFetch();
            }
        }
    };
    //refresh inbox data to first 30 records
    $scope.$on('refresh', function (event, args) {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
        } else {
            $scope.inboxListModel.isLoading = true;
            $scope.inboxListModel.endOfPage = false;
            $scope.inboxListModel.startRow = 0;
            $scope.inboxListModel.inboxList = [];
            myViewListFetch();
        }
    });
    //redirect to oppty details
    $scope.gotoOpportunityDetails = function (selectedId, detailObj) {
        $state.go('root.oppDetail', { id: selectedId, oppDetails: detailObj, fromView: $state.current.name, enableEdit: true });
    };
    //inbox API call - success
    var inboxListSuccess = function (data) {
        $scope.inboxListModel.isLoading = false;
        $scope.inboxListModel.loadMore = false;
        if (data.data.listOfStcOpportunity.opportunity !== null) {
            angular.forEach(data.data.listOfStcOpportunity.opportunity, function (value, key) {
                if ((value.salesStage !== null) && (angular.isDefined(value.salesStage))) {
                    if (value.sTCCreatedByLogin !== value.listOfopportunityPosition.opportunityPosition[0].salesRep) {
                        var leadCreatedDate = new Date(value.sTCBulkLeadCreatedDate);
                        var sysDate = new Date();
                        var dateDiff = parseInt((sysDate - leadCreatedDate) / (24 * 3600 * 1000));
                        if (dateDiff <= $scope.inboxListModel.inboxPeriod) {
                            $scope.inboxListModel.inboxList.push(value);
                        }
                    }
                }
            });
            if (data.data.listOfStcOpportunity.lastPage === "true") {
                $scope.inboxListModel.endOfPage = true;
                MaterialReusables.showToast('' + pluginService.getTranslations().endofList, 'warning');
            }
        }
        $scope.inboxListModel.startRow = parseInt($scope.inboxListModel.startRow) + 30;
    };
    //inbox API call - error
    var inboxListError = function (data) {
        $scope.inboxListModel.isLoading = false;
        Util.throwError(data.data.Message, MaterialReusables);
    };
    //inbox data - fetch API call
    var myViewListFetch = function () {
        var inboxListPayload = Util.oppPayload("30", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "')", "Created (DESCENDING)", $scope.inboxListModel.startRow);
        apiService.fetchDataFromApi('opp.list', inboxListPayload, inboxListSuccess, inboxListError);
    };
    //fetch inbox mapi call by checking network
    if (sharedDataService.getNetworkState()) {
        $scope.inboxListModel.isLoading = true;
        myViewListFetch();
    } else {
        $scope.inboxListModel.isLoading = false;
    }
}
inboxModule.controller('inboxController', inboxController);
inboxController.$inject = ['$rootScope', '$scope', '$timeout', '$state', 'apiService', 'pluginService', 'sharedDataService', 'MaterialReusables', 'dbService'];