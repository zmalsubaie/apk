var dashboardListController = function ($scope, $rootScope, $http, $timeout, apiService, sharedDataService, $state, $stateParams, MaterialReusables, $translate, pluginService) {
    $scope.dashboardListModel = {
        isLoading: true,
        opportunities: [],
        noDataToShow: false,
        isRefreshTop: false,
        loadMoreOpp: false,
        searchText: '',
        search: false
    };
    var mainTab, salesStage, selectedAmount, selectedText, chartSelected, loggedInUser, mrcCountIndex;
    var oppGraphListPerSubstatusPayload, oppGraphListMrcPayload, oppGraphListHighPriorityPayload, oppGraphListTopThreePayload, dashboardUserListPayload;
    var pagination = true;
    var endRowValue = 0;
    var topThreeValue = 0;
    var mrcValue = 0;
    $scope.dashboardListModel.searchText = '';
    //set values from chart to list controller values
    if (sharedDataService.getLastDashBoardListPayload() !== '') {
        var mainTab = sharedDataService.getLastDashBoardListPayload().mainTab;
        var salesStage = sharedDataService.getLastDashBoardListPayload().secTab;
        $scope.startRowNum = 0;
        $scope.endRowNum = sharedDataService.getLastDashBoardListPayload().subTabCount;
        var selectedAmount = sharedDataService.getLastDashBoardListPayload().amount;
        var selectedText = sharedDataService.getLastDashBoardListPayload().text;
        var chartSelected = sharedDataService.getLastDashBoardListPayload().chartSelected;
        var loggedInUser = sharedDataService.getLastDashBoardListPayload().loggedInUser;
        var topThreeEndIndex = sharedDataService.getLastDashBoardListPayload().topThreeEndIndex;
        var mrcCountIndex = sharedDataService.getLastDashBoardListPayload().mrcCountIndex;
    } else {
        var mainTab = $stateParams.mainTab;
        var salesStage = $stateParams.secTab;
        $scope.statusDisplayed = $stateParams.secTab;
        $scope.startRowNum = 0;
        $scope.endRowNum = $stateParams.subTabCount;
        var selectedAmount = $stateParams.amount;
        var selectedText = $stateParams.text;
        var chartSelected = $stateParams.chartSelected;
        var loggedInUser = $stateParams.loggedInUser;
        var topThreeEndIndex = $stateParams.topThreeEndIndex;
        var mrcCountIndex = $stateParams.mrcCountIndex;
        sharedDataService.setLastDashBoardListPayload($stateParams);
    }
    //set siebel Eng-Arabic values for status values
    if (salesStage == 'Lead') {
        opptyStatus = 'Lead ~ فرصه';
    } else if (salesStage == 'Cust:Contact') {
        opptyStatus = 'Contact customer ~ الاتصال بالعميل';
    } else if (salesStage == 'Cust:Visits') {
        opptyStatus = 'Customer Visit ~ زياره العميل';
    } else if (salesStage == 'Proposal') {
        opptyStatus = 'Proposal ~ اقتراح';
    } else if (salesStage == 'Win') {
        opptyStatus = 'Win ~ الربح';
    } else if (salesStage == 'Lost') {
        opptyStatus = 'Lost ~ فقدان';
    }
    //set color code
    var getColorCode = function (leadCreatedDate) {
        var colorCode;
        var leadDate = new Date(leadCreatedDate);
        var sysDate = new Date();
        var dateDiff = parseInt((sysDate - leadDate) / (24 * 3600 * 1000), 10);
        if (dateDiff < 4) {
            colorCode = 'green';
        } else if (dateDiff > 3 && dateDiff < 10) {
            colorCode = 'blue';
        } else if (dateDiff > 9 && dateDiff < 16) {
            colorCode = 'yellow';
        } else if (dateDiff > 15 && dateDiff < 21) {
            colorCode = 'orange';
        } else {
            colorCode = 'red';
        }
        return colorCode;
    };
    //loading more opportunities
    $scope.loadMoreOpportunities = function () {
        if (pagination == true) {
            if (chartSelected == "chOne" || chartSelected == "chThree") {
                $scope.startRowNum = parseInt($scope.endRowNum, 10) + 1;
                $scope.endRowNum = parseInt($scope.endRowNum, 10) + 100;
                $scope.dashboardListModel.loadMoreOpp = true;
                firstFourApiPayload();
                selectedChartApiCall();
            } else if (chartSelected == "chTwo") {
                $scope.startRowNum = parseInt(mrcCountIndex, 10) + 1;
                mrcCountIndex = parseInt(mrcCountIndex, 10) + 100;
                $scope.dashboardListModel.loadMoreOpp = true;
                firstFourApiPayload();
                selectedChartApiCall();
            } else if (chartSelected == "chFour") {
                $scope.startRowNum = parseInt(topThreeEndIndex, 10) + 1;
                topThreeEndIndex = parseInt(topThreeEndIndex, 10) + 100;
                $scope.dashboardListModel.loadMoreOpp = true;
                firstFourApiPayload();
                selectedChartApiCall();
            }
        } else {
            MaterialReusables.showToast('' + pluginService.getTranslations().endofList, 'warning');
        }
    };
    //invoke api call based on navigation from different graphs
    if ($scope.endRowNum > 100 || mrcCountIndex > 100 || topThreeEndIndex > 100) {
        endRowValue = $scope.endRowNum;
        topThreeValue = topThreeEndIndex;
        mrcValue = mrcCountIndex;
        if ($scope.startRowNum == '0' && (chartSelected == "chOne" || chartSelected == "chThree")) {
            $scope.endRowNum = '100';
            firstFourApiPayload();
            selectedChartApiCall();
        } else if (mrcCountIndex > 100 && chartSelected == "chTwo") {
            mrcCountIndex = '100';
            firstFourApiPayload();
            selectedChartApiCall();
        } else if (topThreeEndIndex > 100 && chartSelected == "chFour") {
            topThreeEndIndex = '100';
            firstFourApiPayload();
            selectedChartApiCall();
        }
    } else {
        pagination = false;
        firstFourApiPayload();
        selectedChartApiCall();
    }
    //payload generation based on selected graph
    function firstFourApiPayload() {
        if (chartSelected == 'chOne') {
            oppGraphListPerSubstatusPayload = Util.oppMgrPayload(sharedDataService.getUserDetails().loginName, opptyStatus, $scope.startRowNum, $scope.endRowNum);
        } else if (chartSelected == 'chTwo') {
            oppGraphListMrcPayload = {
                "login": sharedDataService.getUserDetails().loginName,
                "opptyStatus": opptyStatus,
                "SumRevenueAmount": "0",
                "startIndex": $scope.startRowNum,
                "endIndex": mrcCountIndex
            };
        } else if (chartSelected == 'chThree') {
            oppGraphListHighPriorityPayload = {
                "login": sharedDataService.getUserDetails().loginName,
                "opptyStatus": opptyStatus,
                "PriorityFlag": "High",
                "opptySubStatus": selectedText,
                "startindex": $scope.startRowNum,
                "endindex": $scope.endRowNum
            };
        } else if (chartSelected == 'chFour') {
            oppGraphListTopThreePayload = {
                "login": sharedDataService.getUserDetails().loginName,
                "prodcuctName": selectedText,
                "opptyStatus": opptyStatus,
                "ratePlan": selectedAmount,
                "startindex": $scope.startRowNum,
                "endindex": parseInt(topThreeEndIndex, 10)
            };
        }
    };
    //API call based on selected graph
    function selectedChartApiCall() {
        if (chartSelected == 'chOne') {
            chartOneTwoUserApiCalls();
        }
        if (chartSelected == 'chTwo') {
            if (mainTab === "My View") {
                apiService.fetchDataFromApi('opp.mrcListNormalUser', oppGraphListMrcPayload, oppGraphListPerSubstatusSuccess, oppGraphListPerSubstatusError);
            } else if (mainTab === "Team View") {
                apiService.fetchDataFromApi('opp.mrclistformanager', oppGraphListMrcPayload, oppGraphListPerSubstatusSuccess, oppGraphListPerSubstatusError);
            }
        };
        if (chartSelected == 'chThree') {
            if (mainTab === "My View") {
                apiService.fetchDataFromApi('opp.highCountList', oppGraphListHighPriorityPayload, oppGraphListPerSubstatusSuccess, oppGraphListPerSubstatusError);
            } else if (mainTab === "Team View") {
                apiService.fetchDataFromApi('opp.highCountListManager', oppGraphListHighPriorityPayload, oppGraphListPerSubstatusSuccess, oppGraphListPerSubstatusError);
            }
        };
        if (chartSelected == 'chFour') {
            if (mainTab === "My View") {
                apiService.fetchDataFromApi('opp.TopThreeList', oppGraphListTopThreePayload, oppGraphListPerSubstatusSuccess, oppGraphListPerSubstatusError);
            } else if (mainTab === "Team View") {
                apiService.fetchDataFromApi('opp.TopThreeListManager', oppGraphListTopThreePayload, oppGraphListPerSubstatusSuccess, oppGraphListPerSubstatusError);
            }
        };
        if (mainTab === 'User') {
            dashboardUserListPayload = Util.oppMgrPayload(loggedInUser, opptyStatus, $scope.startRowNum, $scope.endRowNum);
            chartOneTwoUserApiCalls();
        }
    };
    //opportunities API call - based on selected tab view in DB UI
    function chartOneTwoUserApiCalls() {
        if (mainTab === "My View") {
            apiService.fetchDataFromApi('opp.oppListNormalUser', oppGraphListPerSubstatusPayload, oppGraphListPerSubstatusSuccess, oppGraphListPerSubstatusError);
        } else if (mainTab === "Team View") {
            apiService.fetchDataFromApi('opp.opplistformanager', oppGraphListPerSubstatusPayload, oppGraphListPerSubstatusSuccess, oppGraphListPerSubstatusError);
        } else if (mainTab === "User") {
            apiService.fetchDataFromApi('opp.oppListNormalUser', dashboardUserListPayload, oppGraphListPerSubstatusSuccess, oppGraphListPerSubstatusError);
        }
    };
    //opportunities API call - based on selected tab view in DB UI - success
    function oppGraphListPerSubstatusSuccess(opp) {
        $scope.dashboardListModel.loadMoreOpp = false;
        $scope.dashboardListModel.isLoading = false;
        if (opp.data != null) {
            if (chartSelected == 'chOne' || chartSelected == 'chThree') {
                angular.forEach(opp.data.opportunityList, function (value, key) {
                    if (value.sTCSubStatusSME === selectedText) {
                        value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
                        $scope.dashboardListModel.opportunities.push(value);
                    }
                });
                if ($scope.dashboardListModel.opportunities.length == 0) {
                    $scope.dashboardListModel.noDataToShow = true;
                } else {
                    $scope.dashboardListModel.noDataToShow = false;
                }
            } else if (chartSelected == 'chTwo') {
                angular.forEach(opp.data.opportunityList, function (value, key) {
                    if (value.sTCSubStatusSME === selectedText && value.PrimaryRevenueAmount !== null && value.PrimaryRevenueAmount > 0) {
                        value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
                        $scope.dashboardListModel.opportunities.push(value);
                    }
                });
                if ($scope.dashboardListModel.opportunities.length == 0) {
                    $scope.dashboardListModel.noDataToShow = true;
                } else {
                    $scope.dashboardListModel.noDataToShow = false;
                }
            } else if (chartSelected == 'chFour') {
                angular.forEach(opp.data.opportunityList, function (value, key) {
                    value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
                    $scope.dashboardListModel.opportunities.push(value);
                });
                if ($scope.dashboardListModel.opportunities.length == 0) {
                    $scope.dashboardListModel.noDataToShow = true;
                } else {
                    $scope.dashboardListModel.noDataToShow = false;
                }
            } else if (mainTab === 'User' && chartSelected == '') {
                angular.forEach(opp.data.opportunityList, function (value, key) {
                    value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
                    $scope.dashboardListModel.opportunities.push(value);
                });
                if ($scope.dashboardListModel.opportunities.length == 0) {
                    $scope.dashboardListModel.noDataToShow = true;
                } else {
                    $scope.dashboardListModel.noDataToShow = false;
                }
            }
        } else {
            if ($scope.startRowNum == 0) {
                $scope.dashboardListModel.noDataToShow = true;
            } else {
                MaterialReusables.showToast('' + pluginService.getTranslations().endofList, 'warning');
            }
        }
    };
    //opportunities API call - based on selected tab view in DB UI - error
    function oppGraphListPerSubstatusError(opp) {
        $scope.dashboardListModel.loadMoreOpp = false;
        $scope.dashboardListModel.isLoading = false;
        $scope.dashboardListModel.noDataToShow = true;
        Util.throwError(opp.data.Message, MaterialReusables);
    };
    //invoke opportunity api call for the selected opportunity in top 5 opportunities
    if (chartSelected == 'chFive') {
        if (mainTab === "My View") {
            if (salesStage === 'Lead') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND [Opportunity.Name] = '" + selectedText + "' AND [Opportunity.Sales Stage] LIKE '*Lead*')", "", '0');
            } else if (salesStage == 'Cust:Contact') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND [Opportunity.Name] = '" + selectedText + "' AND [Opportunity.Sales Stage] LIKE '*Contact customer*')", "", '0');
            } else if (salesStage == 'Cust:Visits') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND [Opportunity.Name] = '" + selectedText + "' AND [Opportunity.Sales Stage] LIKE '*Customer Visit*')", "", '0');
            } else if (salesStage == 'Proposal') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND [Opportunity.Name] = '" + selectedText + "' AND [Opportunity.Sales Stage] LIKE '*Proposal*')", "", '0');
            } else if (salesStage == 'Win') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND [Opportunity.Name] = '" + selectedText + "' AND [Opportunity.Sales Stage] LIKE '*Win*')", "", '0');
            } else if (salesStage == 'Lost') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND [Opportunity.Name] = '" + selectedText + "' AND [Opportunity.Sales Stage] LIKE '*Lost*')", "", '0');
            }
            chartFiveApiCallNormal();
        } else if (mainTab === "Team View") {
            var opptySearchMgrPayload = {
                "opportunityName": "%" + selectedText + "%",
                "endindex": "51",
                "startindex": "1",
                "opportunityNumber": "%" + selectedText + "%",
                "login": sharedDataService.getUserDetails().loginName,
                "opptyStatus": opptyStatus
            };
            chartFiveApiCallManager();
        }
    }
    //opportunity api call for the selected opportunity in top 5 opportunities
    function chartFiveApiCallNormal() {
        apiService.fetchDataFromApi('opp.list', opptyPayload, opptySuccess, oppGraphListPerSubstatusError);
    }
    //normal user opportunity api call for the selected opportunity in top 5 opportunities - success
    function opptySuccess(data) {
        $scope.dashboardListModel.isLoading = false;
        if (angular.isDefined(data.data.listOfStcOpportunity.opportunity)) {
            angular.forEach(data.data.listOfStcOpportunity.opportunity, function (value, key) {
                var opptyData = {};
                value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
                opptyData = {
                    'rowId': value.rowId,
                    'sTCLeadNumber': value.sTCLeadNumber,
                    'name': value.name,
                    'sTCOpportunityType': value.sTCOpportunityType,
                    'salesStage': value.salesStage,
                    'sTCSubStatusSME': value.sTCSubStatusSME,
                    'sTCBulkLeadContactNumber': value.sTCBulkLeadContactNumber,
                    'sTCBulkLeadContactFirstName': value.sTCBulkLeadContactFirstName,
                    'sTCLeadContactFirstName': value.sTCLeadContactFirstName,
                    'PrimaryRevenueAmount': value.PrimaryRevenueAmount,
                    'sTCOpportunityPriority': value.sTCOpportunityPriority,
                    'sTCOptyCity': value.sTCOptyCity,
                    'sTCBulkLeadCreatedDate': value.sTCBulkLeadCreatedDate,
                    'sTCContactNumber2': value.sTCContactNumber2,
                    'sTCContactNumber3': value.sTCContactNumber3,
                    'sTCFeedback': value.sTCFeedback,
                    'sTCFeedback2': value.sTCFeedback2,
                    'sTCFeedback3': value.sTCFeedback3,
                    'colorCode': value.colorCode
                };
                $scope.dashboardListModel.opportunities.push(opptyData);
            });
        } else {
            MaterialReusables.showToast('' + pluginService.getTranslations().noOpp, 'warning');
        }
    }
    //manager user opportunity api call for the selected opportunity in top 5 opportunities
    function chartFiveApiCallManager() {
        apiService.fetchDataFromApi('opp.odsoppquerysearchformanager', opptySearchMgrPayload, opptySearchMgrSuccess, oppGraphListPerSubstatusError);
    }
    //manager user opportunity api call for the selected opportunity in top 5 opportunities - success
    function opptySearchMgrSuccess(data) {
        $scope.dashboardListModel.isLoading = false;
        if (angular.isDefined(data.data.opportunityList)) {
            angular.forEach(data.data.opportunityList, function (value, key) {
                value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
                $scope.dashboardListModel.opportunities.push(value);
            });
            if ($scope.dashboardListModel.opportunities.length == 0) {
                $scope.dashboardListModel.noDataToShow = true;
            } else {
                $scope.dashboardListModel.noDataToShow = false;
            }
        }
    }
    //opportunity search API call
    $scope.$on('openSearch', function () {
        $scope.dashboardListModel.search = !$scope.dashboardListModel.search;
        if (!$scope.dashboardListModel.search) {
            $scope.dashboardListModel.searchText = '';
        }
    });
    //navigation to opportunity details UI
    $scope.gotoDashboardListDetails = function (selectedId, detailObj) {
        var oppListOpptyPayload = Util.oppPayload("1", "[Opportunity.Id] = '" + detailObj.rowId + "' ", "", '0');
        apiService.fetchDataFromApi('opp.list', oppListOpptyPayload, oppListOpptySuccess, oppGraphListPerSubstatusError);
        $scope.dashboardListModel.isRefreshTop = true;
        function oppListOpptySuccess(data) {
            $scope.dashboardListModel.isRefreshTop = false;
            if (angular.isDefined(data.data.listOfStcOpportunity.opportunity[0])) {
                $state.go('root.oppDetail', {
                    id: selectedId,
                    oppDetails: data.data.listOfStcOpportunity.opportunity[0],
                    fromView: $state.current.name,
                    enableEdit: true
                });
            } else {
                MaterialReusables.showToast('' + pluginService.getTranslations().oppDetNoFound, 'error');
            }
        }
    };
    //switch between normal user/ manager user search for opportunity
    $scope.searchOppty = function (searchText) {
        if (searchText !== "") {
            if (mainTab === "My View") {
                searchNormalUserOppty(searchText);
            } else {
                searchTeamOpptyList(searchText);
            }
        }
    }
    //search filter upon typing
    $scope.searchFilter = function (obj) {
        if (typeof obj !== 'undefined') {
            $scope.dashboardListModel.searchText = $scope.dashboardListModel.searchText.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g,'');
            var re = new RegExp($scope.dashboardListModel.searchText, 'i');
            return !$scope.dashboardListModel.searchText || re.test(obj.name) || re.test(obj.sTCLeadNumber);
        }
    };
    //normal user - opportunity search
    function searchNormalUserOppty(text) {
        if (text !== '') {
            var opptySuccessSearch = function (data) {
                $scope.dashboardListModel.isLoading = false;
                if (angular.isDefined(data.data.listOfStcOpportunity.opportunity)) {
                    var opptyData = {};
                    angular.forEach(data.listOfStcOpportunity.opportunity, function (value, key) {
                        opptyData = { 'rowId': value.rowId, 'sTCLeadNumber': value.sTCLeadNumber, 'name': value.name, 'sTCOpportunityType': value.sTCOpportunityType, 'salesStage': value.salesStage, 'sTCSubStatusSME': value.sTCSubStatusSME, 'sTCBulkLeadContactNumber': value.sTCBulkLeadContactNumber, 'sTCBulkLeadContactFirstName': value.sTCBulkLeadContactFirstName, 'serviceName': value.serviceName, 'sTCLeadContactFirstName': value.sTCLeadContactFirstName, 'PrimaryRevenueAmount': value.PrimaryRevenueAmount, 'sTCOpportunityPriority': value.sTCOpportunityPriority, 'sTCOptyCity': value.sTCOptyCity, 'sTCBulkLeadCreatedDate': value.sTCBulkLeadCreatedDate, 'sTCContactNumber2': value.sTCContactNumber2, 'sTCContactNumber3': value.sTCContactNumber3, 'sTCFeedback': value.sTCFeedback, 'sTCFeedback2': value.sTCFeedback2, 'sTCFeedback3': value.sTCFeedback3 };
                    });
                    if (indexOf($scope.dashboardListModel.opportunities, opptyData) == -1) {
                        $scope.dashboardListModel.opportunities.push(opptyData);
                    }
                } else {
                    MaterialReusables.showToast('' + pluginService.getTranslations().noOpp, 'warning');
                }
            }
            var opptyErrorSearch = function (data) {
                dashboardListVM.isLoading = false;
                Util.throwError(data.data.Message, MaterialReusables);
            }
            $scope.dashboardListModel.isLoading = true;
            var tab = salesStage;
            if (tab == 'Lead') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*') AND [Opportunity.Sales Stage] LIKE '*Lead*' AND [Opportunity.Sales Stage] NOT LIKE '*Lead Enrichment*')", "", '0');
            } else if (tab == 'Cust:Contact') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*' ) AND [Opportunity.Sales Stage] LIKE '*Contact customer*')", "", '0');
            } else if (tab == 'Cust:Visits') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*' ) AND [Opportunity.Sales Stage] LIKE '*Customer Visit*')", "", '0');
            } else if (tab == 'Proposal') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*') AND [Opportunity.Sales Stage] LIKE '*Proposal*')", "", '0');
            } else if (tab == 'Win') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*') AND [Opportunity.Sales Stage] LIKE '*Win*')", "", '0');
            } else if (tab == 'Lost') {
                var opptyPayload = Util.oppPayload("10", "([Opportunity.Primary Position Id] = '" + sharedDataService.getUserDetails().PrimaryPositionId + "' AND ([Opportunity.STC Lead Number] LIKE '*" + text + "*' OR [Opportunity.Name] LIKE '*" + text + "*') AND [Opportunity.Sales Stage] LIKE '*Lost*')", "", '0');
            }
            apiService.fetchDataFromApi('opp.list', opptyPayload, opptySuccessSearch, opptyErrorSearch);
        }
    }
    //manager user - opportunity search
    function searchTeamOpptyList(text) {
        var opptySearchMgrPayload = {
            "opportunityName": "%" + text + "%",
            "endindex": "51",
            "startindex": "1",
            "opportunityNumber": "%" + text + "%",
            "login": sharedDataService.getUserDetails().loginName,
            "opptyStatus": opptyStatus
        };
        $scope.dashboardListModel.isLoading = true;
        apiService.fetchDataFromApi('opp.odsoppquerysearchformanager', opptySearchMgrPayload, opptySearchMgrSuccessSearch, opptySearchMgrError);
        function opptySearchMgrSuccessSearch(data) {
            $scope.dashboardListModel.isLoading = false;
            if (angular.isDefined(data.data.opportunityList)) {
                angular.forEach(data.data.opportunityList, function (value, key) {
                    value.colorCode = getColorCode(value.sTCBulkLeadCreatedDate);
                    if (indexOf($scope.dashboardListModel.opportunities, value) == -1) {
                        $scope.dashboardListModel.opportunities.push(value);
                    }
                });
            } else {
                MaterialReusables.showToast('' + pluginService.getTranslations().noOpp, 'warning');
            }
        };
        function opptySearchMgrError() {
        };
    };
};
dashboardModule.controller('dashboardListController', dashboardListController);
dashboardListController.$inject = ['$scope', '$rootScope', '$http', '$timeout', 'apiService', 'sharedDataService', '$state', '$stateParams', 'MaterialReusables', '$translate', 'pluginService'];