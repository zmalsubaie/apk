reviewModule.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('root.review', {
        url: '/review',
        templateUrl: 'app/components/review/review.template.html',
        controller: 'reviewController',
        pageTitle: 'Offline',
        backbutton: false,
        buttonProp:['add'],
        previousState:'root.home'
    })
}
]);