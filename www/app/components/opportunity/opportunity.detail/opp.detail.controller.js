var opportunityDetailController = function($scope, $timeout, $q, $filter, $translate, $http, $state, $stateParams, MaterialReusables, $rootScope, apiService, sharedDataService, pluginService,dbService){
      $scope.oppDetailModel = {
        isLoading: true,
        selectedCategory: '',
        reloading: false,
        isDetailEditable:true
    };
    $scope.selectedIndex = 0;
    var allDetails = $stateParams.oppDetails;
    //process oppty details from internal DB - offline
    var updateDetailsAndProceed = function(details){
       var offlinepayloads =  JSON.parse(details[0].payload);
       var activityPayloads = offlinepayloads.Activity.create.concat(offlinepayloads.Activity.edit);
       var productPayloads = [];
       var notePayloads = offlinepayloads.Notes.create.concat(offlinepayloads.Notes.edit);
       angular.forEach(activityPayloads,function(val,key){
           val.description2 = val.description;
           val.sTCEBUOpptyActivityType = val.stcEBUOpptyActivityType;
       });
       angular.forEach(offlinepayloads.Products.create.concat(offlinepayloads.Products.edit),function(val,key){
           var productObj = val.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0];
           productObj.quoteNumber2 = null;
           productObj.sTCServiceType2 = productObj.stcServiceType2;
           productObj.sTCRatePlanAliasName = productObj.stcRatePlanAliasName;
           productObj.sTCQuoteComments = productObj.stcQuoteComments;
           productPayloads.push(productObj);
       });
       angular.forEach(notePayloads,function(val,key){
           val.sTCCreatedByLogin = sharedDataService.getUserDetails().loginName;
       });
       var catergorisedPayloads = {
           'Activity':activityPayloads,
           'Notes':notePayloads,
           'Products':productPayloads
       };
       if(allDetails.ListOfOpportunityNote === null){
         allDetails.ListOfOpportunityNote = {opportunityNote:[]};
       }
       if(allDetails.ListOfOpportunityProduct === null){
         allDetails.ListOfOpportunityProduct = {OpportunityProduct:[]};
       } 
       if(allDetails.listOfAction === null){
         allDetails.listOfAction = {action:[]};
       }
       angular.forEach(catergorisedPayloads.Notes,function(noteVal,noteKey){
         allDetails.ListOfOpportunityNote.opportunityNote.push(noteVal);
       }); 
       angular.forEach(catergorisedPayloads.Activity,function(activityVal,activityKey){
         allDetails.listOfAction.action.push(activityVal);
       }); 
       angular.forEach(catergorisedPayloads.Products,function(prodVal,prodKey){
         allDetails.ListOfOpportunityProduct.OpportunityProduct.push(prodVal);
       });
       $scope.oppDetails = allDetails;
       $scope.oppDetailModel.reloading = false;
    };
    //fetch oppty details from internal DB - offline success
    var gotDetails = function(details){
       if(details.length){
          updateDetailsAndProceed(details);
       }else{
          $scope.oppDetailModel.reloading = false;
          $scope.oppDetails = allDetails;  
       } 
    };
    //fetch oppty details from internal DB - offline error
    var failedToGet = function(){
       $scope.oppDetailModel.reloading = false;
       $scope.oppDetails = allDetails; 
    };
    $scope.oppDetailModel.reloading = true;
    var getOpptyByID = Util.convertObjectToSQL({optyID:allDetails.rowId});
    dbService.getDetailsFromTable('stc_opptys', getOpptyByID,failedToGet, gotDetails);
    //check and enable location update button ont ab change
    $scope.onOppDetailTabChanges = function (selectedTab, tab) {
        $scope.selectedIndex = tab;
        $scope.oppDetailModel.selectedCategory = selectedTab;
        $scope.$broadcast('' + selectedTab);
        if($scope.oppDetailModel.isDetailEditable){
            if (selectedTab === 'Map') {
                if(sharedDataService.getNetworkState()){
                   $rootScope.$broadcast('enableSetLocation', []);
                }
            } else {
                $rootScope.$broadcast('disableSetLocation', []);
            }
        }
    };
    //API call for the selected oppty details
    var refreshOpportunity = function (rowId) {
        var currentTab = $scope.selectedIndex;
        var oppListSuccess = function (data) {
            if (data.data.listOfStcOpportunity.opportunity[0] !== null) {
                $scope.oppDetails = data.data.listOfStcOpportunity.opportunity[0];
                $scope.$broadcast(''+currentTab);
            } else {
                MaterialReusables.showToast(''+pluginService.getTranslations().cannotRefreshOpp, 'error');
            }
            $scope.oppDetailModel.isLoading = false;
            $scope.oppDetailModel.reloading = false;
        };
        var oppListError = function (data) {
            $scope.oppDetailModel.isLoading = false;
            Util.throwError(data.data.Message, MaterialReusables);
        };
        $rootScope.$broadcast('disableSetLocation', []);
        var oppListPayload = Util.oppPayload("1", "[Opportunity.Id] = '" + rowId + "' ", "", '0');
        apiService.fetchDataFromApi('opp.list', oppListPayload, oppListSuccess, oppListError);
    };
    //invoke api call method on refresh action
    $scope.$on('refresh', function (event, args) {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast(''+pluginService.getTranslations().netCheck, 'warning');
        } else {
            refreshOpportunity($scope.oppDetails.rowId);
            $scope.oppDetailModel.reloading = true;
            $scope.oppDetailModel.isLoading = true;
        }
    });
    //set previous state based on coming navigation
    $scope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
        if(from.name!=='root.oppAddition'){
           $state.current.previousState = from.name;
        }else{
           $state.current.previousState = 'root.oppList'; 
        }
        $scope.oppDetailModel.isDetailEditable = $stateParams.enableEdit;
        if(from.name === 'root.map'){
           $scope.oppDetailModel.reloading = true;
           $scope.oppDetailModel.isLoading = true; 
           refreshOpportunity($stateParams.id);
        }
    });
    //update oppty details in offline - activity
    var activityOfflinePayloadChecker = function(activityDetail,actionDBPayload,payloadToCheck){
        var activityCheckResult = {source:'',indexinSource:''};
           angular.forEach(activityDetail,function(val,key){
                if(val.activityId === payloadToCheck.activityId){
                    activityCheckResult.source = 'Detail';
                    activityCheckResult.indexinSource = key;
                }
            });
        if(activityCheckResult.source === ''){
           angular.forEach(actionDBPayload.create,function(val,key){
                if(val.description === payloadToCheck.description){
                    activityCheckResult.source = 'Create';
                    activityCheckResult.indexinSource = key;
                }
            });
        }
        if(activityCheckResult.source === ''){
            angular.forEach(actionDBPayload.edit,function(val,key){
                if(val.activityId === payloadToCheck.activityId){
                    activityCheckResult.source = 'Edit';
                    activityCheckResult.indexinSource = key;
                }
            });
        }
        return activityCheckResult;
    };
    //update oppty details in offline - note
    var noteOfflinePayloadChecker = function(notesDetail,noteDBPayload,payloadToCheck){
         var noteCheckResult = {source:'',indexinSource:''};
            angular.forEach(notesDetail,function(val,key){
                if(val.id === payloadToCheck.id){
                    noteCheckResult.source = 'Detail';
                    noteCheckResult.indexinSource = key;
                }
            });
         if(noteCheckResult.source === ''){
             angular.forEach(noteDBPayload.create,function(val,key){
                  if(val.note === payloadToCheck.note){
                      noteCheckResult.source = 'Create';
                      noteCheckResult.indexinSource = key;
                  }
             });
         }
         if(noteCheckResult.source === ''){
             angular.forEach(noteDBPayload.edit,function(val,key){
                if(val.id === payloadToCheck.id){
                    noteCheckResult.source = 'Edit';
                    noteCheckResult.indexinSource = key;
                }
             });
         }
         return noteCheckResult;
    };
    //update oppty details in offline - product
    var productOfflineChecker = function(productsDetail,productsDBPayload,payloadToCheck){
         var productCheckResult = {source:'',indexinSource:''};
         angular.forEach(productsDetail,function(val,key){
             if(val.id === payloadToCheck.id){
                 productCheckResult.source = 'Detail';
                 productCheckResult.indexinSource = key;
             }
         });
         if(productCheckResult.source === ''){
             angular.forEach(productsDBPayload.create,function(val,key){
                 if(payloadToCheck.productAliasName === val.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].productAliasName
                    && payloadToCheck.stcRatePlanAliasName === val.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcRatePlanAliasName
                    && payloadToCheck.stcServiceType2 === val.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].stcServiceType2){
                       productCheckResult.source = 'Create';
                       productCheckResult.indexinSource = key;
                    }
             });
         }
         if(productCheckResult.source === ''){
             angular.forEach(productsDBPayload.edit,function(val,key){
                if(payloadToCheck.id === val.listOfStcOpportunity.opportunity[0].listOfOpportunityProduct.opportunityProduct[0].id){
                    productCheckResult.source = 'Edit';
                    productCheckResult.indexinSource = key;
                }
             });
         }
         return productCheckResult;
    };
    //update oppty details in offline -update header details
    var updateOfflineDetailPayload = function(detailinDetails,detailDBPayload,payloadToCheck){
        var payloadExtractedDetail = payloadToCheck.payload.listOfstcOpportunity.opportunity[0];
        var detailCheckPayload = {payloadInfo:[],detailPayloadInfo:{}};
        if(detailDBPayload.length){
            detailDBPayload.splice(0,1);
        }
        detailDBPayload.push(payloadToCheck);
        detailinDetails['accountId'] = payloadExtractedDetail.accountId;
        detailinDetails['accountNumber'] = payloadExtractedDetail.accountNo;
        detailinDetails['keyContactId'] = payloadExtractedDetail.contactId;
        detailinDetails['description'] = payloadExtractedDetail.description;
        detailinDetails['rowId'] = payloadExtractedDetail.id;
        detailinDetails['name'] = payloadExtractedDetail.name;
        detailinDetails['sTCBulkLeadContactFirstName'] = payloadExtractedDetail.sTCBulkLeadContactFirstName;
        detailinDetails['salesStage'] = payloadExtractedDetail.salesStage;
        detailinDetails['sTCBestTimeToCall'] = payloadExtractedDetail.stcBestTimeToCall;
        detailinDetails['sTCBulkLeadContactNumber'] = payloadExtractedDetail.stcBulkLeadContactNumber;
        detailinDetails['sTCBulkLeadCreatedBy'] = payloadExtractedDetail.stcBulkLeadCreatedBy;
        detailinDetails['sTCCommercialRegn'] = payloadExtractedDetail.stcCommercialRegn;
        detailinDetails['sTCContactNumber2'] = payloadExtractedDetail.stcContactNumber2;
        detailinDetails['sTCContactNumber3'] = payloadExtractedDetail.stcContactNumber3;
        detailinDetails['sTCFeedback'] = payloadExtractedDetail.stcFeedback;
        detailinDetails['sTCFeedback2'] = payloadExtractedDetail.sTCFeedback2;
        detailinDetails['sTCFeedback3'] = payloadExtractedDetail.sTCFeedback3;
        detailinDetails['sTCLeadCampaignName'] = payloadExtractedDetail.stcLeadCampaignName;
        detailinDetails['sTCLeadNumber'] = payloadExtractedDetail.stcLeadNumber;
        detailinDetails['sTCOpportunityType'] = payloadExtractedDetail.stcOpportunityType;
        detailinDetails['sTCOptyCity'] = payloadExtractedDetail.stcOptyCity;
        detailinDetails['sTCSubStatusSME'] = payloadExtractedDetail.stcSubStatusSME;
        detailCheckPayload.payloadInfo = detailDBPayload;
        detailCheckPayload.detailPayloadInfo = detailinDetails;
        return detailCheckPayload;
    };
    //update oppty details in offline
    $scope.updateOfflineOpportunity = function(params,lastParams,viewType,action,callback){
      var pushCreatedPayloadtoDB = function(oppDetailObj){
          var oppDetailPayload = JSON.parse(oppDetailObj[0].opportunities);
          var payload = JSON.parse(oppDetailObj[0].payload);
          var userMessage = '';
          if(viewType === 'Activity'){
              if(oppDetailPayload.listOfAction === null){
                  oppDetailPayload.listOfAction = {action:[]};
              }
              if(action === 'create'){
                 userMessage = ''+pluginService.getTranslations().offlineActivitySuccess;
                 payload.Activity.create.push(params);
              }else if(action === 'edit'){
                 userMessage = ''+pluginService.getTranslations().offlineActivityEditSuccess; 
                 var activityCheckResult = activityOfflinePayloadChecker(oppDetailPayload.listOfAction.action,payload.Activity,lastParams);
                 if(activityCheckResult.source === 'Detail'){
                    oppDetailPayload.listOfAction.action.splice(activityCheckResult.indexinSource,1);
                    payload.Activity.edit.push(params);
                 }else if(activityCheckResult.source === 'Create'){
                    payload.Activity.create.splice(activityCheckResult.indexinSource,1);
                    payload.Activity.create.push(params);
                 }else if(activityCheckResult.source === 'Edit'){
                    payload.Activity.edit.splice(activityCheckResult.indexinSource,1);
                    payload.Activity.edit.push(params);
                 }
              }else if(action === 'delete'){
                 userMessage = ''+pluginService.getTranslations().offlineActivityDelete;  
                 var activityCheckResult = activityOfflinePayloadChecker(oppDetailPayload.listOfAction.action,payload.Activity,lastParams);
                 if(activityCheckResult.source === 'Detail'){
                    oppDetailPayload.listOfAction.action.splice(activityCheckResult.indexinSource,1);
                    payload.Activity.delete.push(params);
                 }else if(activityCheckResult.source === 'Create'){
                    payload.Activity.create.splice(activityCheckResult.indexinSource,1);
                 }else if(activityCheckResult.source === 'Edit'){
                    payload.Activity.edit.splice(activityCheckResult.indexinSource,1);
                    payload.Activity.delete.push(params);
                 }
              }
          }else if(viewType === 'Note'){
              if(oppDetailPayload.ListOfOpportunityNote === null){
                  oppDetailPayload.ListOfOpportunityNote = {opportunityNote:[]};
              }
              if(action === 'create'){
                 userMessage = ''+pluginService.getTranslations().offlineNoteCreateSuccess; 
                 payload.Notes.create.push(params);
              }else if(action === 'edit'){ 
                 userMessage = ''+pluginService.getTranslations().offlineNoteEditSuccess; 
                 var noteCheckResult = noteOfflinePayloadChecker(oppDetailPayload.ListOfOpportunityNote.opportunityNote,payload.Notes,lastParams); 
                 if(noteCheckResult.source === 'Detail'){
                    oppDetailPayload.ListOfOpportunityNote.opportunityNote.splice(noteCheckResult.indexinSource,1);
                    payload.Notes.edit.push(params);
                 }else if(noteCheckResult.source === 'Create'){
                    payload.Notes.create.splice(noteCheckResult.indexinSource,1);
                    payload.Notes.create.push(params);
                 }else if(noteCheckResult.source === 'Edit'){
                    payload.Notes.edit.splice(noteCheckResult.indexinSource,1);
                    payload.Notes.edit.push(params);
                 } 
              }else if(action === 'delete'){
                 userMessage = ''+pluginService.getTranslations().offlineNoteDelete;  
                 var noteCheckResult = noteOfflinePayloadChecker(oppDetailPayload.ListOfOpportunityNote.opportunityNote,payload.Notes,lastParams);
                 if(noteCheckResult.source === 'Detail'){
                    oppDetailPayload.ListOfOpportunityNote.opportunityNote.splice(noteCheckResult.indexinSource,1);
                    payload.Notes.delete.push(params);
                 }else if(noteCheckResult.source === 'Create'){
                    payload.Notes.create.splice(noteCheckResult.indexinSource,1);
                 }else if(noteCheckResult.source === 'Edit'){
                    payload.Notes.edit.splice(noteCheckResult.indexinSource,1);
                    payload.Notes.delete.push(params);
                 } 
              }
          }else if(viewType === 'Product'){
              if(action === 'create'){
                userMessage = 'Successfully Created Product in Offline Mode'; 
                 payload.Products.create.push(params);
              }else if(action === 'edit'){
                 userMessage = 'Successfully Edited Product in Offline Mode';  
                 var productCheckResult = productOfflineChecker(oppDetailPayload.ListOfOpportunityProduct.OpportunityProduct,payload.Products,lastParams);
                 if(productCheckResult.source === 'Detail'){
                    oppDetailPayload.ListOfOpportunityProduct.OpportunityProduct.splice(productCheckResult.indexinSource,1);
                    payload.Products.edit.push(params);
                 }else if(productCheckResult.source === 'Create'){
                    payload.Products.create.splice(productCheckResult.indexinSource,1);
                    payload.Products.create.push(params);
                 }else if(productCheckResult.source === 'Edit'){
                    payload.Products.edit.splice(productCheckResult.indexinSource,1);
                    payload.Products.edit.push(params);
                 }
              }else if(action === 'delete'){
                  userMessage = 'Successfully Deleted Product in Offline Mode';
                  var productCheckResult = productOfflineChecker(oppDetailPayload.ListOfOpportunityProduct.OpportunityProduct,payload.Products,lastParams);  
                  if(productCheckResult.source === 'Detail'){
                    oppDetailPayload.ListOfOpportunityProduct.OpportunityProduct.splice(productCheckResult.indexinSource,1);
                    payload.Products.delete.push(params);
                 }else if(productCheckResult.source === 'Create'){
                    payload.Products.create.splice(productCheckResult.indexinSource,1);
                 }else if(productCheckResult.source === 'Edit'){
                    payload.Products.edit.splice(productCheckResult.indexinSource,1);
                    payload.Products.delete.push(params);
                 }  
              }
          }else if(viewType === 'Detail'){
              if(action === 'update'){
                 userMessage = ''+pluginService.getTranslations().offlineOpptyUpdate; 
                 var detailCheckResult = updateOfflineDetailPayload(oppDetailPayload,payload.Details.update,params);
                 oppDetailPayload = detailCheckResult.detailPayloadInfo;
                 payload.Details.update = detailCheckResult.payloadInfo;
              }
          }
          var checkParameters = Util.convertObjectToSQL({optyID:$scope.oppDetails.rowId});
          var valueParameters = Util.convertObjectToSQL({opportunities:JSON.stringify(oppDetailPayload),payload:JSON.stringify(payload),toBeUpdated:'true'});
          var payloadAdded = function(){
             callback(true,params); 
             MaterialReusables.showToast(userMessage,'success');
          };
          var failedToAddPayload = function(){
              callback(false,params); 
          };
          dbService.updateTable('stc_opptys', valueParameters, checkParameters,failedToAddPayload, payloadAdded);
      };
      var gotOpptyDetails = function(payloadObj){
           pushCreatedPayloadtoDB(payloadObj);
      };
      var getOpptyByID = Util.convertObjectToSQL({optyID:$scope.oppDetails.rowId});
      dbService.getDetailsFromTable('stc_opptys', getOpptyByID,angular.noop, gotOpptyDetails);
    };
    //check and set content loading state
    $scope.$on('$viewContentLoaded', function () {
        $scope.completed = true;
    });
    //set map index value on map icon click
    $scope.loadMap = function () {
        $scope.selectedIndex = 6;
    };
    $timeout(function () {
        $scope.oppDetailModel.isLoading = false;
    }, 3000);
};
opportunityModule.controller('opportunityDetailController',opportunityDetailController);
opportunityDetailController.$inject = ['$scope', '$timeout', '$q', '$filter', '$translate', '$http', '$state', '$stateParams', 'MaterialReusables', '$rootScope', 'apiService', 'sharedDataService', 'pluginService','dbService'];