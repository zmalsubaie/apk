var opportunityAddController = function ($rootScope, $scope, $stateParams, $translate, $timeout, $state, $http, apiService, dbService, sharedDataService, pluginService, MaterialReusables) {
    $scope.oppAddModel = {
        isLoading: false,
        oppTypes: [],
        showUpgradeItems: false,
        requestParams: [{
            'name': '',
            'sTCOpportunityType': '',
            'sTCCommercialRegn': '',
            'accountId': '',
            'sTCOptyCity': '',
            'stcCreatedBy': '',
            'rowId': '',
            'sTCSubStatusSME': '',
            'sTCBulkLeadCreatedBy': '',
            'sTCBulkLeadContactNumber': '',
            'sTCFeedback': '',
            'contactNumber': '',
            'contactId': '',
            'operation': ''
        }],
        assigneeParams: [{
            'isPrimaryMVG': '',
            'personUId': '',
            'firstName': '',
            'operation': ''
        }],
        contactParams: [{
            'personUId': '',
            'isPrimaryMVG': '',
            'employeeNumber': '',
            'firstName': '',
            'lastName': '',
            'operation': ''
        }],
        accountNames: [],
        enableAccountSearch: false,
        accountId: '',
        contactName: '',
        contactId: '',
        isFetchingContacts: false,
        contactNameList: [],
        loadMore: false,
        account: '',
        showAccountList: false,
        lastAccountReached: false,
        accountStartRow: 0,
        loadAccounts: false,
        accountQuery: '',
        isOffline: false
    };
    var oppTypeSelected = '';
    $scope.oppAddModel.oppTypes = oppTypeList;
    $scope.oppAddModel.isOffline = !sharedDataService.getNetworkState();
    //oppoty dropdown type change - set/reset values
    $scope.onOppTypeChange = function (selectedType) {
        oppTypeSelected = selectedType;
        if (oppTypeSelected == 'New Sale') {
            $scope.oppAddModel.accountNames = [];
            $scope.oppAddModel.contactNameList = [];
            $scope.oppAddModel.requestParams.accountId = '';
            $scope.oppAddModel.requestParams.contactId = '';
            $scope.oppAddModel.contactName = '';
            $scope.oppAddModel.account = '';
            $scope.oppAddModel.showUpgradeItems = false;
            $scope.oppAddModel.requestParams.sTCOptyCity = '';
            $scope.oppAddModel.requestParams.sTCOptyCityName = '';
        }
        else if (oppTypeSelected == 'Upgrade') { $scope.oppAddModel.showUpgradeItems = true; }
    };
    //report action api call on oppty creation
    // var reportActionCall = function (rowId) {
    //     var reportData = {
    //         "opportunityId": rowId,
    //         "action": 'UPD_CREATED_LEADS',
    //         "createdLeads": '1'
    //     };
    //     var reportPayload = Util.reportActionPayload(reportData);
    //     apiService.fetchDataFromApi('opp.reportaction', reportPayload, angular.noop, angular.noop);
    // };
    //oppty create API call - success
    var lpCreateSuccess = function (data) {
        if (data.data.errorMessage === 'Success') {
            var result = function (confirm) {
                if (confirm) {
                    $state.go('root.oppDetail', {
                        id: data.data.listOfStcOpportunity.opportunity[0].sTCLeadNumber,
                        oppDetails: data.data.listOfStcOpportunity.opportunity[0],
                        fromView: $state.current.name,
                        enableEdit: true
                    });
                }
            };
            $scope.oppAddModel.isLoading = false;
            MaterialReusables.showSuccessAlert('' + pluginService.getTranslations().oppo + ' : ' + data.data.listOfStcOpportunity.opportunity[0].sTCLeadNumber + ' ' + pluginService.getTranslations().creSuccess, result);
            //reportActionCall(data.data.listOfStcOpportunity.opportunity[0].rowId);
        } else {
            $scope.oppAddModel.isLoading = false;
            Util.throwError(data.data.errorMessage, MaterialReusables);
        }
    };
    //oppty create API call - success
    var oppCreateError = function (data) {
        $scope.oppAddModel.isLoading = false;
        Util.throwError(data.data.Message, MaterialReusables);
    };
    //Oppty create - payload creation and invoke API call
    var addOppor = function () {
        $scope.oppAddModel.requestParams.sTCBulkLeadCreatedBy = sharedDataService.getUserDetails().loginName;
        $scope.oppAddModel.requestParams.sTCBulkLeadContactNumber = $scope.oppAddModel.requestParams.contactNumber;
        $scope.oppAddModel.requestParams.sTCFeedback = $scope.oppAddModel.requestParams.contactNumber ? "Authorized person" : '';
        $scope.oppAddModel.requestParams.sTCSubStatusSME = "Lead";
        $scope.oppAddModel.requestParams.sTCCreatedBy = sharedDataService.getUserDetails().Id;
        $scope.oppAddModel.requestParams.operation = 'insert';
        $scope.oppAddModel.requestParams.rowId = '1234' //for insert it sholud be 1234
        var headerData = $scope.oppAddModel.requestParams;

        $scope.oppAddModel.assigneeParams.position = sharedDataService.getUserDetails().primaryPosition;
        $scope.oppAddModel.assigneeParams.isPrimaryMVG = 'Y';
        $scope.oppAddModel.assigneeParams.operation = 'insert';
        var assigneeData = $scope.oppAddModel.assigneeParams;

        $scope.oppAddModel.contactParams.firstName = $scope.contactFirstName ? $scope.contactFirstName : "";
        $scope.oppAddModel.contactParams.lastName = $scope.contactLaststName ? $scope.contactLaststName : "";
        $scope.oppAddModel.contactParams.personUId = $scope.oppAddModel.requestParams.contactId;
        $scope.oppAddModel.contactParams.employeeNumber = $scope.oppAddModel.requestParams.contactId;
        $scope.oppAddModel.contactParams.isPrimaryMVG = 'N';
        $scope.oppAddModel.contactParams.operation = 'insert';
        var contactData = '';
        if ($scope.oppAddModel.requestParams.sTCOpportunityType === 'Upgrade') {
            contactData = $scope.oppAddModel.contactParams;
        }
        var oppAddPayload = Util.leadProcessPayload(headerData, assigneeData, contactData, '', '', '', '', '');
        var payLoad = $scope.oppAddModel.requestParams;
        var oppAddOfflinePayload = {
            'payload': {
                "statusObject": "",
                "objectLevelTransactions": "",
                "setMinimalReturns": "",
                "listOfstcOpportunity":
                {
                    "opportunity":
                    [
                        {
                            "accountId": payLoad.accountId ? payLoad.accountId : "",
                            "stcOpportunityType": payLoad.sTCOpportunityType ? payLoad.sTCOpportunityType : payLoad.sTCOpportunityType,
                            "stcCommercialRegn": payLoad.sTCCommercialRegn ? payLoad.sTCCommercialRegn : "",
                            "stcBulkLeadCreatedBy": sharedDataService.getUserDetails().loginName ? sharedDataService.getUserDetails().loginName : "",
                            "stcLeadCampaignName": "",
                            "name": payLoad.name ? payLoad.name : "",
                            "description": "",
                            "stcBulkLeadContactNumber": payLoad.contactNumber ? payLoad.contactNumber : "",
                            "stcFeedback": payLoad.contactNumber ? "Authorized person" : '',
                            "stcBestTimeToCall": "",
                            "stcSubStatusSME": "Lead",
                            "contactId": payLoad.contactId ? payLoad.contactId : "",
                            "salesStage": "",
                            "stcOptyCity": payLoad.sTCOptyCityName ? payLoad.sTCOptyCityName : "",
                            "stcCreatedBy": sharedDataService.getUserDetails().Id ? sharedDataService.getUserDetails().Id : ""
                        }
                    ]
                },
                "messageId": "",
                "busObjCacheSize": ""
            }
        };
        var newOfflineOpptyInsert = function () {
            var success = function () {
                MaterialReusables.showToast('' + pluginService.getTranslations().opptySavedSuccess, 'success');
                $state.go($state.current.previousState);
            };
            var error = function () { };
            var params = [moment().unix(), JSON.stringify(oppAddOfflinePayload), oppTypeSelected, '', 'new'];
            dbService.insertIntoDBTable('stc_offlineOppCreate', params, error, success);
        };
        var updateNewOfflineOppty = function () {
            var updateParam = {
                oppDetail: JSON.stringify(oppAddOfflinePayload),
                oppType: oppTypeSelected,
                postStatus: ''
            };
            var sqlUpdateParameters = Util.convertObjectToSQL(updateParam);
            var checkParam = {
                oppID: $scope.offlineOpptyId
            };
            var sqlCheckParameters = Util.convertObjectToSQL(checkParam);
            var offlineUpdateSuccess = function () {
                MaterialReusables.showToast('' + pluginService.getTranslations().opptySavedSuccess, 'success');
                $state.go('root.review');
            };
            var offlineUpdateError = function () { };
            dbService.updateTable('stc_offlineOppCreate', sqlUpdateParameters, sqlCheckParameters, offlineUpdateError, offlineUpdateSuccess)
        };
        if (!sharedDataService.getNetworkState()) {
            if ($scope.offlineOpptyId === '') {
                newOfflineOpptyInsert();
            } else {
                updateNewOfflineOppty();
            }
        } else {
            $scope.oppAddModel.isLoading = true;
            apiService.fetchDataFromApi('opp.leadProcess', oppAddPayload, lpCreateSuccess, oppCreateError);
        }
    };
    //oppty create - data validation
    var validateOppCreation = function () {
        var validation = '';
        if (oppTypeSelected === 'New Sale') {
            var newSaleValidators = [
                { 'field': $scope.oppAddModel.requestParams.name, 'message': pluginService.getTranslations().enterOppName }
            ];
            validation = Util.validateElements(angular, newSaleValidators);
        } else if (oppTypeSelected === 'Upgrade') {
            if (sharedDataService.getNetworkState()) {
                var upgradeValidators = [
                    { 'field': $scope.oppAddModel.requestParams.sTCOptyCity, 'message': pluginService.getTranslations().enterCity },
                    { 'field': $scope.oppAddModel.requestParams.contactId, 'message': pluginService.getTranslations().enterContact },
                    { 'field': $scope.oppAddModel.requestParams.accountId, 'message': pluginService.getTranslations().enterAccName },
                    { 'field': $scope.oppAddModel.requestParams.name, 'message': pluginService.getTranslations().enterOppName }
                ];
                validation = Util.validateElements(angular, upgradeValidators);
            } else {
                var upgradeValidators = [
                    { 'field': $scope.oppAddModel.requestParams.sTCOptyCity, 'message': pluginService.getTranslations().enterCity },
                    { 'field': $scope.oppAddModel.requestParams.accountId, 'message': pluginService.getTranslations().enterAccName },
                    { 'field': $scope.oppAddModel.requestParams.name, 'message': pluginService.getTranslations().enterOppName }
                ];
                validation = Util.validateElements(angular, upgradeValidators);
            }
        }
        if ($scope.oppAddModel.isOffline && validation === pluginService.getTranslations().enterContact) {
            validation = '';
        }
        return validation;
    };
    //cancel oppty creation
    $scope.cancelOppCreate = function () {
        var createPageValidators = [
            { 'field': $scope.oppAddModel.requestParams.name, 'message': 'Opportunity Name' }
        ];
        var uncommittedData = false;
        if (angular.isDefined($scope.oppAddModel.requestParams.contactNumber) && $scope.oppAddModel.requestParams.contactNumber !== '') {
            uncommittedData = true;
        }
        if (angular.isDefined($scope.oppAddModel.requestParams.sTCCommercialRegn) && $scope.oppAddModel.requestParams.sTCCommercialRegn !== '') {
            uncommittedData = true;
        }
        if (angular.isDefined($scope.oppAddModel.contactNameList.contactName) && $scope.oppAddModel.contactNameList.contactName !== '') {
            uncommittedData = true;
        }
        if (angular.isDefined($scope.oppAddModel.accountNames.length) && $scope.oppAddModel.accountNames.length !== 0) {
            uncommittedData = true;
        }
        if (angular.isDefined($scope.oppAddModel.requestParams.sTCOptyCity) && $scope.oppAddModel.requestParams.sTCOptyCity !== '') {
            uncommittedData = true;
        }
        if ((Util.validateElements(angular, createPageValidators).length) && !uncommittedData) {
            $state.go($state.current.previousState);
        }
        else {
            var cancelConfirm = function (confirm) {
                if (confirm) { $state.go($state.current.previousState); }
            };
            MaterialReusables.showConfirmDialog('', '' + pluginService.getTranslations().allProgressLostConfirm, cancelConfirm);
        }
    };
    //invoke creat eoppty API call based on validation result
    $scope.createOpportunity = function () {
        var validationResult = validateOppCreation();
        if (validationResult === '') { addOppor(); }
        else { MaterialReusables.showToast('' + validationResult, 'error'); }
    };
    //enable disable search option
    $scope.$watch('searchText', function () {
        try {
            if ($scope.searchText.length >= 5) {
                $scope.oppAddModel.enableAccountSearch = true;
            } else {
                $scope.oppAddModel.enableAccountSearch = false;
            }
        } catch (err) {
            //console.log(err);
        }
    });
    //check item exists in array or not
    var itemExists = function (array, item) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].contactName === item) return i;
        }
        return -1;
    }
    //fetch contacts mapped to account API call
    $scope.accountOnSelect = function (selectedAccount) {
        $scope.oppAddModel.account = selectedAccount.name;
        $scope.oppAddModel.requestParams.accountId = selectedAccount.id;
        $scope.oppAddModel.contactNameList = [];
        $scope.oppAddModel.requestParams.contactId = '';
        MaterialReusables.hideDialog();
        if (angular.isDefined(selectedAccount.contacts)) {
            if (selectedAccount.contacts.length > 0) {
                angular.forEach(selectedAccount.contacts, function (value, key) {
                    if (itemExists($scope.oppAddModel.contactNameList, value.FirstName + ' ' + value.lastName) == -1) {
                        $scope.oppAddModel.contactNameList.push({ "contactName": value.FirstName + ' ' + value.lastName, "contactId": value.contactId, "firstName": value.FirstName, "lastName": value.lastName });
                    }
                });
            } else {
                MaterialReusables.showToast('' + pluginService.getTranslations().nooAccCFou, 'error');
            }
        } else {
            MaterialReusables.showToast('' + pluginService.getTranslations().nooAccCFou, 'error');
        }
    };
    //set selected contact from drop down
    $scope.oppContactSelect = function (contact) {
        $scope.oppAddModel.requestParams.contactId = contact.contactId;
        $scope.contactFirstName = contact.firstName;
        $scope.contactLaststName = contact.lastName;
    };
    //security input field validation
    $scope.oppNumberChange = function (newValue, index) {
        $scope.oppAddModel.requestParams.contactNumber = newValue.replace(/[^0-9-. ]/g, '').substring(0, 18);
    };
    //security input field validation
    $scope.nameValidate = function (newValue) {
        $scope.oppAddModel.requestParams.name = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
    };
    //security input field validation
    $scope.accountSearchValidate = function (newValue) {
        $scope.accountText = newValue.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/g, '');
    };
    //security input field validation
    $scope.idValidate = function (newValue) {
        $scope.oppAddModel.requestParams.sTCCommercialRegn = newValue.replace(/[^a-zA-Z0-9-._ ]/g, '');
    };
    //cancel oppty create on discard
    $scope.$on('validate', function (event, args) {
        if (!$scope.oppAddModel.isLoading) {
            $scope.cancelOppCreate();
        }
    });
    //set incoming view to previous state value
    $state.current.previousState = $stateParams.fromView;
    //city list data process from memory/internal table
    $scope.cityOnSelect = function(city){
        MaterialReusables.hideDialog();
        $scope.oppAddModel.requestParams.sTCOptyCity = city.value;
        $scope.oppAddModel.requestParams.sTCOptyCityName = city.name;
    };
    $scope.cityOnClose = function(){
        MaterialReusables.hideDialog();
        $scope.oppAddModel.loadMore = false;
    };
    $scope.searchCity = function (){
        MaterialReusables.invokeCustomPopUp($scope,'City');
    };
    //set city selected
    $scope.onCityClick = function (data) {
        MaterialReusables.hideDialog();
        $scope.oppAddModel.requestParams.sTCOptyCity = data.value;
        $scope.oppAddModel.requestParams.sTCOptyCityName = data.name;
    };
    //invoke account popup
    $scope.accountList = function () {
        if (!sharedDataService.getNetworkState()) {
            MaterialReusables.showToast('' + pluginService.getTranslations().netCheck, 'warning');
        } else {
            MaterialReusables.invokeCustomPopUp($scope,'Account');
        }
    };
    //close account list popup
    $scope.accountOnClose = function () {
        MaterialReusables.hideDialog();
    };
    //set firld values while coming to edit offline created oppty
    if (!sharedDataService.getNetworkState()) {
        $scope.offlineOpptyId = $stateParams.id;
        $scope.oppDetails = $stateParams.detail;
        if ($scope.offlineOpptyId !== '' && $scope.oppDetails !== '') {
            $scope.oppAddModel.requestParams.name = $scope.oppDetails.payload.listOfstcOpportunity.opportunity[0].name;
            $scope.oppAddModel.requestParams.accountId = $scope.oppDetails.payload.listOfstcOpportunity.opportunity[0].accountId;
            $scope.oppAddModel.requestParams.sTCOptyCityName = $scope.oppDetails.payload.listOfstcOpportunity.opportunity[0].stcOptyCity;
            $scope.oppAddModel.requestParams.sTCOptyCity = $scope.oppAddModel.requestParams.sTCOptyCityName;
            $scope.oppAddModel.requestParams.sTCCommercialRegn = $scope.oppDetails.payload.listOfstcOpportunity.opportunity[0].stcCommercialRegn;
            $scope.oppAddModel.requestParams.sTCOpportunityType = $scope.oppDetails.payload.listOfstcOpportunity.opportunity[0].stcOpportunityType;
            $scope.onOppTypeChange($scope.oppAddModel.requestParams.sTCOpportunityType);
            $scope.oppAddModel.requestParams.contactNumber = $scope.oppDetails.payload.listOfstcOpportunity.opportunity[0].stcBulkLeadContactNumber;
        } else {
            $scope.onOppTypeChange('New Sale');
            $scope.oppAddModel.requestParams.sTCOpportunityType = 'New Sale';
        }
    } else {
        $scope.onOppTypeChange('New Sale');
        $scope.oppAddModel.requestParams.sTCOpportunityType = 'New Sale';
    }
};
opportunityModule.controller('opportunityAddController', opportunityAddController);
opportunityAddController.$inject = ['$rootScope', '$scope', '$stateParams', '$translate', '$timeout', '$state', '$http', 'apiService', 'dbService', 'sharedDataService', 'pluginService', 'MaterialReusables'];